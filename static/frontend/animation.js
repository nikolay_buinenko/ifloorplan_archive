(function ($) {
    $(document).ready(function () {
        $(".mainApp button:first-child").on("click", function () {
            $(".header").hide();
            $(".header").addClass("not_first_step");
        });
        $(".tempWrap").on("click", function () {
            $(".header").hide();
            $(".header").addClass("not_first_step");
        });
        $(document).on('click', '.firstOpt .fa.fa-caret-up', function() {
            $("body").removeClass("first_step");
            $(".header").slideDown();
            $(".header").css({
                "position": "absolute",
                "width": "100%",
                "z-index": "999"
            });
        });
        $(document).mouseup(function (e){
            var modalctr = $("body");   
            var modal = $(".header.not_first_step");
            if (!modal.is(e.target) && modal.has(e.target).length === 0){
                modal.slideUp();
            }
        });


        $(document).on('click', '.accountBtn a', function () {
            $(".otherTools").fadeToggle();
        });
    });
})(jQuery);