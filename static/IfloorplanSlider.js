import React, {useEffect, useRef, useState} from 'react';
import Slider from 'react-slick';

const IfloorplanSlider = ({src, index, setIndexMarker, count}) => {

  const [settings, setSettings] = useState({
    dots: false,
    accessibility: false,
    fade: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 1500,
    pauseOnHover: true,
    pauseOnFocus: true,
    beforeChange: function(currentSlide, nextSlide) {
      setIndexMarker(nextSlide);
      console.log("before change", currentSlide, nextSlide);
    }
  })
  const [sliderPlay, setSliderPlay] = useState(false)
  const [indexSlider, setIndexSlider] = useState(0)


  useEffect(() => {
    setIndexSlider(index)
    slider.current.slickGoTo(index)
  })

  const slider = useRef();

  const Play = () => {
    slider.current.slickPlay();
    setSliderPlay(!sliderPlay)
  }

  const Pause = () => {
    slider.current.slickPause();
    setSliderPlay(!sliderPlay)
  }

  const Next = () => {
    if (index + 1 == count) {
      setIndexMarker(0);
    } else {
      setIndexMarker(indexSlider + 1);
    }
    slider.current.slickNext();
  }

  const Prev = () => {
    if (index - 1 < 0) {
      setIndexMarker(count - 1);
    } else {
      setIndexMarker(indexSlider - 1);
    }
    slider.current.slickPrev();
  }

  return (
    <div className='ifloorPlanSlider'>
      <Slider ref={slider} {...settings}>
        {src.map(el => (
          <div key={el.id}>
            <img src={el.img}/>
          </div>
        ))}
      </Slider>
      <div className="sliderPanel">
        <svg xmlns="http://www.w3.org/2000/svg"
             width="8%" viewBox="0 0 34 66"
             fill="none"
             onClick={Prev}>>
          <path d="M30 3L6 33L30 63"
                stroke="white"
                strokeOpacity="0.8"
                strokeWidth="9"/>
        </svg>

        {sliderPlay
          ?
          <svg xmlns="http://www.w3.org/2000/svg"
               width="23"
               viewBox="0 0 21 31"
               fill="none"
               onClick={Pause}>
            <path fillRule="evenodd"
                  clipRule="evenodd"
                  d="M14 19h4V5h-4M6 19h4V5H6v14z"
                  fill="#FFFEFE"
                  stroke="#979797"
                  strokeOpacity="0.01"/>
          </svg>
          :
          <svg xmlns="http://www.w3.org/2000/svg"
               width='15%'
               viewBox="0 0 63 78"
               fill="none"
               onClick={Play}>
            <path opacity="0.8"
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1 1V77L62 39L1 1Z"
                  fill="#FFFEFE"
                  stroke="#979797"
                  strokeOpacity="0.01"/>
          </svg>
        }

        <svg xmlns="http://www.w3.org/2000/svg"
             width="8%"
             viewBox="0 0 34 66"
             fill="none"
             onClick={Next}>>
          <path d="M4 3L28 33L4 63"
                stroke="white"
                strokeOpacity="0.8"
                strokeWidth="9"/>
        </svg>
      </div>
    </div>
  )
}

export default IfloorplanSlider;