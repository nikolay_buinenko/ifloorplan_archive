from django.contrib import admin
import nested_admin
from .models import *
# class KenBurnsInline(nested_admin.NestedStackedInline):
#     model = KenBurns
#     extra = 1
#
# class CamInline(nested_admin.NestedStackedInline):
#     model = Cam
#     inlines = (KenBurnsInline,)
#     extra = 1
#
#
# class ImageInline(nested_admin.NestedStackedInline):
#     model = Image
#     inlines = (CamInline,)
#     extra = 1
#
# class PlanImgInline(nested_admin.NestedStackedInline):
#     model = PlanImg
#     extra = 1
#
#
# class LevelInline(nested_admin.NestedStackedInline):
#     inlines = (PlanImgInline, ImageInline,)
#     model = Level
#     extra = 1
#
#
# @admin.register(IFloorPlan)
# class IFloorPlanAdmin(nested_admin.NestedModelAdmin):
#     inlines = (LevelInline,)

# ====================
admin.site.register(IFloorPlan)
admin.site.register(Client)
admin.site.register(ClientLogo)
admin.site.register(Brand)
admin.site.register(PanelumLibrary)
admin.site.register(SystemMarkers)
admin.site.register(UserMarkers)
