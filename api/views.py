from django.shortcuts import render
from django.http import Http404
from django.http.response import JsonResponse
from rest_framework import status, generics, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from api.serializers import ( IFloorPlanSerializer,
    PlanImgSourseUploadSerializator,
    ImageSourseUploadSerializator,
    ClientsSerializer,
    ClientLogoSerializer,
    BrandSerializer,
    SystemMarkersSerializator,
    UserMarkersSerializator,
    PanelumLibSerializator
)
from rest_framework.views import APIView
from public.models import ApiToken
from .models import *
from rest_framework.parsers import FileUploadParser
from django.contrib.auth.models import User
from loguru import logger

logger.add("debug.log", format="{time} {level} {message}", level="DEBUG")

class SystemMarkersView(viewsets.ModelViewSet):
    queryset = SystemMarkers.objects.all()
    serializer_class = SystemMarkersSerializator
    http_method_names = ['post', 'put', 'delete', 'get']

    def get(self, request):
        instance = SystemMarkers.objects.all()
        serializer = SystemMarkersSerializator(instance, many=True)
        return Response(serializer.data)

class UserMarkersView(viewsets.ModelViewSet):
    http_method_names = ['post', 'put', 'delete', 'get']

    def get(self, request):
        user = User.objects.get(id=request.user.id)
        instance = UserMarkers.objects.filter(author=user)
        serializer = UserMarkersSerializator(instance, many=True)
        return Response(serializer.data)

    def update(self, request):
        instance = UserMarkers.objects.get(id=int(request.data["id"]))
        user = User.objects.get(id=request.user.id)
        instance.marker = request.data.get('marker', instance.marker)
        instance.marker_hover = request.data.get('marker_hover', instance.marker_hover)
        instance.type = request.data.get('type', instance.type)
        instance.save()
        serializer = UserMarkersSerializator(instance)
        return Response(serializer.data)

    def update_markers(self, marker, plans):
        system_marker = SystemMarkers.objects.filter(type="IMAGE", default=True).first()
        chenges_id = []
        for plan in plans:
            for level in plan.level.all():
                for image in level.images.all():
                    if image.cam_img == marker.url:
                        chenges_id.append(image.id)
        for id in chenges_id:
            instance = Image.objects.get(id=id)
            instance.cam_img = system_marker.marker.url
            instance.cam_img_hover = system_marker.marker_hover.url
            instance.save()


    def delete(self, request):
        instance = UserMarkers.objects.get(id=int(request.data["id"]))
        plans = IFloorPlan.objects.filter(author=request.user.id)
        self.update_markers(instance.marker, plans)
        instance.delete()
        return Response({"message": "OK"})

    def post(self, request):
        user = User.objects.get(id=request.user.id)
        # logger.debug(request.data)
        # user = User.objects.get(id=int(request.data["user_id"]))
        instance = UserMarkers.objects.create(author=user,
                                              marker=request.data["marker"],
                                              marker_hover=request.data["marker_hover"],
                                              type=request.data["type"],
                                              )
        serializer = UserMarkersSerializator(instance)
        return Response(serializer.data)




class BrandView(viewsets.ModelViewSet):
    http_method_names = ['post', 'put', 'delete', 'get']

    def post(self, request):
        user = User.objects.get(id=request.user.id)
        instance = Brand.objects.create(logo=request.FILES["logo"],
                                             author=user,
                                             width=request.data['width'],
                                             transparency = request.data['transparency'],
                                             position_x=request.data['position_x'],
                                             position_y=request.data['position_y'],
                                             url=request.data['url'])
        serializer_class = BrandSerializer(instance)
        return Response(serializer_class.data, status=status.HTTP_201_CREATED)

    def delete(self, request):
        try:
            instance = Brand.objects.get(id=int(request.data["id"]))
            instance.delete()
        except Http404:
            pass
        return Response({"message": "OK"}, status=status.HTTP_204_NO_CONTENT)

    def get(self, request, pk=None):
        queryset = Brand.objects.filter(author=int(pk))
        serializer = BrandSerializer(queryset, many=True)
        return Response(serializer.data)

    def put(self, request):
        brand = Brand.objects.get(id=request.data['brand_id'])
        if 'logo' in request.FILES:
            brand.logo = request.FILES['logo']
        brand.width = request.data.get('width', brand.width)
        brand.position_x = request.data.get('position_x', brand.position_x)
        brand.position_y = request.data.get('position_y', brand.position_y)
        brand.url = request.data.get('url', brand.url)
        brand.transparency = request.data.get('transparency', brand.transparency)
        brand.save()
        serializer_class = BrandSerializer(brand)
        return Response(serializer_class.data, status=status.HTTP_201_CREATED)





class ClientLogoView(viewsets.ModelViewSet):
    # create new client logo
    queryset = ClientLogo.objects.all()
    serializer_class = ClientLogoSerializer
    http_method_names = ['post', 'put']


    def post(self, request, *args, **kwargs):
        from django.core.files import File
        # import json
        # file = open("test.json", "w")
        # json.dump(req_dict, file, ensure_ascii=False, indent=4)
        # file.close()
        if 'double' in request.data:
            f = open("/home/django/ifloorplan"+request.data['logo'], 'rb')
            img = File(f)
        else:
            img = request.FILES["logo"]
        user = User.objects.get(id=request.user.id)
        client = Client.objects.get(id=int(request.data['client_id']))
        instance = ClientLogo.objects.create(logo=img,
                                             author=user,
                                             client=client,
                                             width=request.data['width'],
                                             position_logo_x=request.data['position_logo_x'],
                                             position_logo_y=request.data['position_logo_y'],
                                             position_name_x=request.data['position_name_x'],
                                             position_name_y=request.data['position_name_y'],
                                             position_phone_x=request.data['position_phone_x'],
                                             position_phone_y=request.data['position_phone_y'],
                                             position_brand_x=request.data['position_brand_x'],
                                             position_brand_y=request.data['position_brand_y'],
                                             position_contact_x=request.data['position_contact_x'],
                                             position_contact_y=request.data['position_contact_y'],)
        serializer_class = ClientLogoSerializer(instance)
        if 'double' in request.data:
            f.close()
        return Response(serializer_class.data, status=status.HTTP_201_CREATED)

    def update(self, request):
        clientLogo = ClientLogo.objects.get(id=request.data['client_logo_id'])
        if 'logo' in request.FILES:
            clientLogo.logo = request.FILES['logo']
        clientLogo.width = request.data.get('width', clientLogo.width)
        clientLogo.position_logo_x = request.data.get('position_logo_x', clientLogo.position_logo_x)
        clientLogo.position_logo_y = request.data.get('position_logo_y', clientLogo.position_logo_y)
        clientLogo.position_name_x = request.data.get('position_name_x', clientLogo.position_name_x)
        clientLogo.position_name_y = request.data.get('position_name_y', clientLogo.position_name_y)
        clientLogo.position_phone_x = request.data.get('position_phone_x', clientLogo.position_phone_x)
        clientLogo.position_phone_y = request.data.get('position_phone_y', clientLogo.position_phone_y)
        clientLogo.position_brand_x = request.data.get('position_brand_x', clientLogo.position_brand_x)
        clientLogo.position_brand_y = request.data.get('position_brand_y', clientLogo.position_brand_y)
        clientLogo.position_contact_x = request.data.get('position_contact_x', clientLogo.position_contact_x)
        clientLogo.position_contact_y = request.data.get('position_contact_y', clientLogo.position_contact_y)
        clientLogo.save()
        serializer_class = ClientLogoSerializer(clientLogo)
        return Response(serializer_class.data, status=status.HTTP_201_CREATED)




class ClientLogoDownload(viewsets.ViewSet):
    # get all client logo
    queryset = ClientLogo.objects.all()
    http_method_names = ['post']

    def list(self, request):
        queryset = ClientLogo.objects.filter(author=int(request.data['user_id']))
        serializer = ClientLogoSerializer(queryset, many=True)
        return Response(serializer.data)










class ClientView(viewsets.ModelViewSet):
    # create new client
    queryset = Client.objects.all()
    serializer_class = ClientsSerializer
    http_method_names = ['post', 'put', 'delete']


    def post(self, request, *args, **kwargs):
        if request.data:
            user = User.objects.get(id=request.user.id)
            client = Client.objects.create(author=user, **request.data)
            serializer = ClientsSerializer(client)
            return Response(serializer.data)
        else:
            return JsonResponse({"message": "Error"}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        instance = Client.objects.get(id=int(request.data["id"]))
        instance.client_name = request.data.get('client_name', instance.client_name)
        instance.contact_name = request.data.get('contact_name', instance.contact_name)
        instance.contact_phone = request.data.get('contact_phone', instance.contact_phone)
        instance.contact_email = request.data.get('contact_email', instance.contact_email)
        instance.url_click = request.data.get('url_click', instance.url_click)
        instance.save()
        serializer = ClientsSerializer(instance)
        return Response(serializer.data)

    def destroy(self, request):
        try:
            instance = Client.objects.get(id=int(request.data["id"]))
            instance.delete()
        except Http404:
            pass
        return JsonResponse({"message": "OK"},status=status.HTTP_204_NO_CONTENT)


class ClientDownload(viewsets.ViewSet):
    # get all clients
    queryset = Client.objects.all()

    def list(self, request):
        queryset = Client.objects.filter(author = int(request.data["user_id"]))
        serializer = ClientsSerializer(queryset, many=True)
        return Response(serializer.data)





class PanelumView(viewsets.ModelViewSet):

    def list(self, request):
        ifloorplan_obj = IFloorPlan.objects.filter(id=int(request.GET['id'])).first()
        obj = PanelumLibrary.objects.filter(ifloorplan_id=ifloorplan_obj.id)
        serializer = PanelumLibSerializator(obj, many=True)
        return Response(serializer.data)

    def post(self, request):
        ifloorplan_obj = IFloorPlan.objects.filter(id=int(request.data.pop("ifloorplan_id")[0])).first()
        if 'many' in request.data:
            for i in range(len(request.FILES)):
                PanelumLibrary.objects.create(ifloorplan_id=ifloorplan_obj, image=request.FILES['image_' + str(i)])
        else:
            PanelumLibrary.objects.create(ifloorplan_id=ifloorplan_obj, image=request.FILES['image'])

        obj = PanelumLibrary.objects.filter(ifloorplan_id=ifloorplan_obj.id)
        serializer = PanelumLibSerializator(obj, many=True)
        return Response(serializer.data)

    def update(self, request):
        instance = PanelumLibrary.objects.get(id=int(request.data["id"]))
        if 'image' in request.FILES:
            instance.image = request.FILES['image']
        instance.save()
        ifloorplan_obj = IFloorPlan.objects.filter(id=int(request.data['ifloorplan_id'])).first()
        obj = PanelumLibrary.objects.filter(ifloorplan_id=ifloorplan_obj.id)
        serializer = PanelumLibSerializator(obj, many=True)
        return Response(serializer.data)

    def delete(self, request):
        if 'many' in request.data:
            for i in request.data['many']:
                instance = PanelumLibrary.objects.get(id=int(i))
                instance.delete()
        else:
            instance = PanelumLibrary.objects.get(id=int(request.data['simple']))
            instance.delete()
        ifloorplan_obj = IFloorPlan.objects.filter(id=int(request.data['ifloorplan_id'])).first()
        obj = PanelumLibrary.objects.filter(ifloorplan_id=ifloorplan_obj.id)
        serializer = PanelumLibSerializator(obj, many=True)
        return Response(serializer.data)




# =====================================================================================================
class ImgView(viewsets.ModelViewSet):
    queryset = Image.objects.all()

    def list(self, request):
        ifloorplan_obj = IFloorPlan.objects.filter(id=int(request.GET['id'])).first()
        obj = LibraryImage.objects.filter(ifloorplan_id=ifloorplan_obj.id)
        serializer = ImageSourseUploadSerializator(obj, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        ifloorplan_obj = IFloorPlan.objects.filter(id=int(request.data.pop("ifloorplan_id")[0])).first()
        level_obj = Level.objects.filter(id=int(request.data.pop("level_id")[0])).first()
        for file in range(len(request.FILES)):
            LibraryImage.objects.create(level_id=level_obj,
                                        ifloorplan_id=ifloorplan_obj,
                                        image=request.FILES['image_' + str(file)])
        obj = LibraryImage.objects.filter(ifloorplan_id=ifloorplan_obj.id)
        serializer = ImageSourseUploadSerializator(obj, many=True)
        return Response(serializer.data)

    def update(self, request):
        instance = LibraryImage.objects.get(id=int(request.data["id"]))
        if 'image' in request.FILES:
            instance.image = request.FILES['image']
        instance.save()
        ifloorplan_obj = IFloorPlan.objects.filter(id=int(request.data['ifloorplan_id'])).first()
        obj = LibraryImage.objects.filter(ifloorplan_id=ifloorplan_obj.id)
        serializer = ImageSourseUploadSerializator(obj, many=True)
        return Response(serializer.data)

    def destroy(self, request):
        # try:
        ifloorplan_obj = IFloorPlan.objects.filter(id=int(request.data["ifloorplan_id"])).first()
        for id in request.data['multi_delete']:
            instance = LibraryImage.objects.get(id=int(id))
            instance.delete()
        obj = LibraryImage.objects.filter(ifloorplan_id=ifloorplan_obj.id)
        serializer = ImageSourseUploadSerializator(obj, many=True)
        return Response(serializer.data)


class ImgDownload(viewsets.ViewSet):
    queryset = Image.objects.all()

    def list(self, request, pk=1):
        queryset = LibraryImage.objects.filter(level_id=pk)
        serializer = ImageSourseUploadSerializator(queryset, many=True)
        return Response(serializer.data)

# =====================================================================================================


class PlanImgView(viewsets.ModelViewSet):
    queryset = PlanImg.objects.all()

    def list(self, request):
        ifloorplan_obj = IFloorPlan.objects.filter(id=int(request.GET['id'])).first()
        obj = LibraryPlan.objects.filter(ifloorplan=ifloorplan_obj.id)
        serializer = PlanImgSourseUploadSerializator(obj, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        ifloorplan_obj = IFloorPlan.objects.filter(id = int(request.data.pop("ifloorplan_id")[0])).first()
        for file in range(len(request.FILES)):
            LibraryPlan.objects.create(ifloorplan=ifloorplan_obj, plan_image=request.FILES['plan_image_'+str(file)])
        obj = LibraryPlan.objects.filter(ifloorplan=ifloorplan_obj.id)
        serializer = PlanImgSourseUploadSerializator(obj, many=True)
        return Response(serializer.data)

    def update(self, request):
        instance = LibraryPlan.objects.get(id=int(request.data["id"]))
        if 'plan' in request.FILES:
            instance.plan_image = request.FILES['plan']
        instance.save()
        ifloorplan_obj = IFloorPlan.objects.filter(id=int(request.data['ifloorplan_id'])).first()
        obj = LibraryPlan.objects.filter(ifloorplan=ifloorplan_obj.id)
        serializer = PlanImgSourseUploadSerializator(obj, many=True)
        return Response(serializer.data)

    def destroy(self, request):
        ifloorplan_obj = IFloorPlan.objects.filter(id=int(request.data["ifloorplan_id"])).first()
        level_obj = Level.objects.filter(id=int(request.data.pop("level_id"))).first()
        try:
            instance = LibraryPlan.objects.get(id=int(request.data['id']))
            instance.delete()
        except:
            return Response({'message': "Failed!"})
        obj = LibraryPlan.objects.filter(ifloorplan=ifloorplan_obj.id)
        serializer = PlanImgSourseUploadSerializator(obj, many=True)
        return Response(serializer.data)


class PlanImgDownload(viewsets.ViewSet):
    queryset = LibraryPlan.objects.all()

    def list(self, request, pk=1):
        queryset = LibraryPlan.objects.filter(ifloorplan=pk)
        serializer = PlanImgSourseUploadSerializator(queryset, many=True)
        return Response(serializer.data)

# ========================================================================================================




class IFloorPlanCreateSet(viewsets.ViewSet):
    """
        Created ifloorplan
        for application react
        create:
            input data: post json
            return: json
    """
    serializer_class = IFloorPlanSerializer # get object serializer
    queryset = IFloorPlan.objects.all()
    http_method_names = ['get', 'head', 'post']

    def create(self, request, *args, **kwargs):
        # logger.debug(request.COOKIES)
        user = User.objects.get(id=request.user.id)
        levels = request.data.pop('level', None)
        media = request.data.pop('media', None)
        order = request.data.pop('order', None)
        plan = IFloorPlan.objects.create(author=user, **request.data)
        if media:
            for item in media:
                Media.objects.create(ifloorplan=plan, **item)
        if order:
            for item in order:
                Media.objects.create(ifloorplan=plan, **item)
        for level in levels:
            images = level.pop("images", None)
            plan_image = level.pop("plan_level", None)
            new_level = Level.objects.create(plan=plan, **level)
            PlanImg.objects.create(level=new_level, **plan_image)
            for image in images:
                Image.objects.create(level=new_level, **image)
        plans = IFloorPlan.objects.filter(id=plan.id)
        serializer = IFloorPlanSerializer(plans, many=True)
        return Response(serializer.data)



# =====================================================================================================

class IFloorPlanViewSet(APIView):
    """
        Get data for application react

    """
    def get(self, request):
        if "token" in request.GET and "ifloorplan" in request.GET:
            token_obj = ApiToken.objects.filter(token=request.GET["token"])
            if token_obj:
                if "user_id" in request.GET:
                    user = User.objects.get(id=int(request.GET["user_id"]))
                    plans = IFloorPlan.objects.filter(author=user.id)
                else:
                    plans = IFloorPlan.objects.filter(id=request.GET["ifloorplan"])
                serializer = IFloorPlanSerializer(plans, many=True)
                return Response(serializer.data)
            else:
                return JsonResponse({"message": "Don't valid token"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return JsonResponse({"message": "Don't valid GET parameter"}, status=status.HTTP_400_BAD_REQUEST)





class IFloorPlanDelete(viewsets.ViewSet):
    serializer_class = IFloorPlanSerializer
    queryset = IFloorPlan.objects.all()

    def destroy(self, request):
        instance = IFloorPlan.objects.get(id=int(request.data["id"]))
        instance.delete()
        return Response({'message':"OK"})
# =====================================================================================================

class IFloorPlanUpdateSet(viewsets.ViewSet):
    serializer_class = IFloorPlanSerializer
    queryset = IFloorPlan.objects.all()
    http_method_names = ['get', 'head', 'post', 'put']

    def update(self, request, *args, **kwargs):
        # import json
        # file = open("test.json", "w")
        # json.dump(request.data, file,ensure_ascii=False, indent=4)
        # file.close()
        instance = IFloorPlan.objects.get(id=int(request.data["id"]))
        levels = request.data.pop('level', None)
        media = request.data.pop('media', None)
        orders = request.data.pop('order', None)
        instance.textColor = request.data.get('textColor', instance.textColor)
        instance.bgColor = request.data.get('bgColor', instance.bgColor)
        instance.lat_long = request.data.get('lat_long', instance.lat_long)
        instance.camColor = request.data.get('camColor', instance.camColor)
        instance.tabs_style = request.data.get('tabs_style', instance.tabs_style)
        instance.address = request.data.get('address', instance.address)
        instance.textBlock = request.data.get('textBlock', instance.textBlock)
        instance.addressFontWeight = request.data.get('addressFontWeight', instance.addressFontWeight)
        instance.addressFontStyle = request.data.get('addressFontStyle', instance.addressFontStyle)
        instance.addressFontSize = request.data.get('addressFontSize', instance.addressFontSize)
        instance.addressFontFamily = request.data.get('addressFontFamily', instance.addressFontFamily)
        instance.addressPosition_x = request.data.get('addressPosition_x', instance.addressPosition_x)
        instance.addressPosition_y = request.data.get('addressPosition_y', instance.addressPosition_y)
        instance.addressInputWidth = request.data.get('addressInputWidth', instance.addressInputWidth)
        instance.template_name = request.data.get('template_name', instance.template_name)
        instance.client_data = request.data.get('client_data', instance.client_data)
        instance.client_position = request.data.get('client_position', instance.client_position)
        instance.client_block_x = request.data.get('client_block_x', instance.client_block_x)
        instance.client_block_y = request.data.get('client_block_y', instance.client_block_y)
        instance.client_scale = request.data.get('client_scale', instance.client_scale)
        instance.brand_logo = request.data.get('brand_logo', instance.brand_logo)
        instance.brand_width = request.data.get('brand_width', instance.brand_width)
        instance.brand_transparency = request.data.get('brand_transparency', instance.brand_transparency)
        instance.brand_pos_x = request.data.get('brand_pos_x', instance.brand_pos_x)
        instance.brand_pos_y = request.data.get('brand_pos_y', instance.brand_pos_y)
        instance.brand_url = request.data.get('brand_url', instance.brand_url)
        instance.published = request.data.get('published', instance.published)
        instance.end_live = request.data.get('end_live', instance.end_live)
        instance.splash_screen = request.data.get('splash_screen', instance.splash_screen)
        instance.save()

        keep_orders = []
        for item in orders:
            if 'id' in item.keys():
                if SlideShowOrder.objects.filter(id=item['id']).exists():
                    ord = SlideShowOrder.objects.get(id=item['id'])
                    ord.index = item.get('index', ord.index)
                    ord.save()
                    keep_orders.append(ord.id)
                else:
                    continue
            else:
                new_order = Media.objects.create(ifloorplan=instance, **item)
                keep_orders.append(new_order.id)
        for item_ord in instance.order.all():
            if item_ord.id not in keep_orders:
                item_ord.delete()


        keep_levels = []
        keep_media = []
        for item in media:
            if 'id' in item.keys():
                if Media.objects.filter(id=item['id']).exists():
                    vid = Media.objects.get(id=item['id'])
                    vid.url = item.get('url', vid.url)
                    vid.label = item.get('label', vid.label)
                    vid.type = item.get('type', vid.type)
                    vid.index = item.get('index', vid.index)
                    vid.save()
                    keep_media.append(vid.id)
                else:
                    continue
            else:
                new_media = Media.objects.create(ifloorplan=instance, **item)
                keep_media.append(new_media.id)
        for med in instance.media.all():
            if med.id not in keep_media:
                med.delete()

        for level in levels:
            if 'id' in level.keys():
                if Level.objects.filter(id=level['id']).exists():
                    images = level.pop('images', None)
                    plan_images = level.pop('plan_level', None)
                    panelums = level.pop('panelum', None)
                    video_markers = level.pop('video_marker', None)
                    l = Level.objects.get(id=level['id'])
                    # =======================panelum==================================
                    keep_panelum = []
                    for panelum in panelums:
                        if 'id' in panelum.keys():
                            if Panelum.objects.filter(id=panelum['id']).exists():
                                pan = Panelum.objects.get(id=panelum['id'])
                                pan.img = panelum.get('img', pan.img)
                                pan.cam_img = panelum.get('cam_img', pan.cam_img)
                                pan.cam_img_hover = panelum.get('cam_img_hover', pan.cam_img_hover)
                                pan.cam_width = panelum.get('cam_width', pan.cam_width)
                                pan.pos_x = panelum.get('pos_x', pan.pos_x)
                                pan.pos_y = panelum.get('pos_y', pan.pos_y)
                                pan.id_lib_img = panelum.get('id_lib_img', pan.id_lib_img)
                                pan.save()
                                keep_panelum.append(pan.id)
                            else:
                                continue
                        else:
                            panelum_new = Panelum.objects.create(level=l, **panelum)
                            keep_panelum.append(panelum_new.id)
                    instance_panelum = Panelum.objects.filter(level=level['id'])
                    for panelum_check in instance_panelum:
                        if panelum_check.id not in keep_panelum:
                            panelum_check.delete()
                    # ==========================end==================================

                    # ========================video marker ==========================
                    keep_video_marker = []
                    for video_marker in video_markers:
                        if 'id' in video_marker.keys():
                            if VideoMarker.objects.filter(id=video_marker['id']).exists():
                                vd_mark = VideoMarker.objects.get(id=video_marker['id'])
                                vd_mark.video = video_marker.get('video', vd_mark.video)
                                vd_mark.cam_img = video_marker.get('cam_img', vd_mark.cam_img)
                                vd_mark.cam_img_hover = video_marker.get('cam_img_hover', vd_mark.cam_img_hover)
                                vd_mark.cam_width = video_marker.get('cam_width', vd_mark.cam_width)
                                vd_mark.pos_x = video_marker.get('pos_x', vd_mark.pos_x)
                                vd_mark.pos_y = video_marker.get('pos_y', vd_mark.pos_y)
                                vd_mark.save()
                                keep_video_marker.append(vd_mark.id)
                            else:
                                continue
                        else:
                            video_marker_new = VideoMarker.objects.create(level=l, **video_marker)
                            keep_video_marker.append(video_marker_new.id)
                    instance_video_marker = VideoMarker.objects.filter(level=level['id'])
                    for video_marker_check in instance_video_marker:
                        if video_marker_check.id not in keep_video_marker:
                            video_marker_check.delete()
                    # ============================end================================

                    # ======================plan_img=================================
                    keep_plan_images = []
                    for plan_img in plan_images:
                        if 'id' in plan_img.keys():
                            if PlanImg.objects.filter(id=plan_img['id']).exists():
                                p = PlanImg.objects.get(id=plan_img['id'])
                                p.img = plan_img.get('img', p.img)
                                # p.level = plan_img.get('level', p.level)
                                p.save()
                                keep_plan_images.append(p.id)
                            else:
                                continue
                        else:
                            plan_img_new = PlanImg.objects.create(level=l, **plan_img)
                            keep_plan_images.append(plan_img_new.id)
                    instance_planimg = PlanImg.objects.filter(level=level['id'])
                    for plan_img in instance_planimg:
                        if plan_img.id not in keep_plan_images:
                            plan_img.delete()
                    # ===================end plan_img====================================
                    # ==============================image====================
                    keep_images = []
                    for image in images:
                        if 'id' in image.keys():
                            if Image.objects.filter(id=image['id']).exists():
                                cams = image.pop('cam', None)
                                i = Image.objects.get(id=image['id'])
                                i.img = image.get('img', i.img)
                                i.img_id = image.get('img_id', i.img_id)
                                i.cam_pos_x = image.get('cam_pos_x', i.cam_pos_x)
                                i.cam_pos_y = image.get('cam_pos_y', i.cam_pos_y)
                                i.cam_rotation = image.get('cam_rotation', i.cam_rotation)
                                i.cam_img = image.get('cam_img', i.cam_img)
                                i.cam_img_hover = image.get('cam_img_hover', i.cam_img_hover)
                                i.cam_width = image.get('cam_width', i.cam_width)
                                i.index = image.get('index', i.index)
                                i.button = image.get('button', i.button)
                                i.button_bg = image.get('button_bg', i.button_bg)
                                i.button_color_text = image.get('button_color_text', i.button_color_text)
                                i.button_label = image.get('button_label', i.button_label)
                                i.button_scale = image.get('button_scale', i.button_scale)
                                i.button_font_size = image.get('button_font_size', i.button_font_size)
                                i.button_padding_vert = image.get('button_padding_vert', i.button_padding_vert)
                                i.button_padding_gor = image.get('button_padding_gor', i.button_padding_gor)
                                i.button_radius = image.get('button_radius', i.button_radius)
                                i.save()
                                keep_images.append(i.id)
                            else:
                                continue
                        else:
                            image_new = Image.objects.create(level=l, **image)
                            keep_images.append(image_new.id)
                    instance_image = Image.objects.filter(level=level['id'])
                    for image in instance_image:
                        if image.id not in keep_images:
                            image.delete()
                    # =============================end image =====================
                    l.tabLabel = level.get('tabLabel', l.tabLabel)
                    l.index = level.get('index', l.index)
                    l.img_pos_x = level.get('img_pos_x', l.img_pos_x)
                    l.img_pos_y = level.get('img_pos_y', l.img_pos_y)
                    l.img_width = level.get('img_width', l.img_width)
                    l.plan_pos_x = level.get('plan_pos_x', l.plan_pos_x)
                    l.plan_pos_y = level.get('plan_pos_y', l.plan_pos_y)
                    l.plan_width = level.get('plan_width', l.plan_width)
                    l.save()
                    keep_levels.append(l.id)
                else:
                    continue
            else:
                new_level = Level.objects.create(plan=instance, **level)
                keep_levels.append(new_level.id)
        for level in instance.level.all():
            if level.id not in keep_levels:
                level.delete()
        # =====================end level ===============
        plans = IFloorPlan.objects.filter(id=instance.id)
        serializer = IFloorPlanSerializer(plans, many=True)
        return Response(serializer.data)

# =====================================================================================================