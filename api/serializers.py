from django.contrib.auth.models import User
from .models import *
from rest_framework import serializers



class BrandSerializer(serializers.ModelSerializer):

    class Meta:
        model = Brand
        fields = '__all__'




class ClientsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = '__all__'


class ClientLogoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientLogo
        fields = '__all__'





class ImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Image
        fields = '__all__'

class PanelumSerializer(serializers.ModelSerializer):

    class Meta:
        model = Panelum
        fields = '__all__'


class PlanImgSerializer(serializers.ModelSerializer):
    # plan = IFloorPlanSerializer(required=True)

    class Meta:
        model = PlanImg
        fields = '__all__'

class VideoMarkerSerializer(serializers.ModelSerializer):

    class Meta:
        model = VideoMarker
        fields = '__all__'


class LevelSerializer(serializers.ModelSerializer):
    plan_level = PlanImgSerializer(many=True)
    # panelum = PanelumSerializer(many=True)
    panelum = serializers.SerializerMethodField()
    video_marker = VideoMarkerSerializer(many=True)
    # images = ImageSerializer(many=True)
    images = serializers.SerializerMethodField()

    class Meta:
        model = Level
        fields = '__all__'

    def get_images(self, instance):
        images = instance.images.all().order_by('index')
        return ImageSerializer(images, many=True).data

    def get_panelum(self, instance):
        panelum = instance.panelum.all().order_by('pk')
        return PanelumSerializer(panelum, many=True).data


class MediaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Media
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = SlideShowOrder
        fields = '__all__'



class IFloorPlanSerializer(serializers.ModelSerializer):
    # level = LevelSerializer(many=True)
    # media = MediaSerializer(many=True)
    media = serializers.SerializerMethodField()
    level = serializers.SerializerMethodField()
    order = serializers.SerializerMethodField()


    class Meta:
        model = IFloorPlan
        fields = '__all__'

    def get_level(self, instance):
        levels = instance.level.all().order_by('index')
        return LevelSerializer(levels, many=True).data

    def get_media(self, instance):
        medias = instance.media.all().order_by('index')
        return MediaSerializer(medias, many=True).data

    def get_order(self, instance):
        orders = instance.order.all().order_by('index')
        return MediaSerializer(orders, many=True).data



# ------------------------------------------------------------------------

class PlanImgSourseUploadSerializator(serializers.ModelSerializer):

    class Meta:
        model = LibraryPlan
        fields = '__all__'


class ImageSourseUploadSerializator(serializers.ModelSerializer):
    class Meta:
        model = LibraryImage
        fields = '__all__'

class PanelumLibSerializator(serializers.ModelSerializer):
    class Meta:
        model = PanelumLibrary
        fields = '__all__'


class SystemMarkersSerializator(serializers.ModelSerializer):
    class Meta:
        model = SystemMarkers
        fields = '__all__'

class UserMarkersSerializator(serializers.ModelSerializer):
    class Meta:
        model = UserMarkers
        fields = '__all__'