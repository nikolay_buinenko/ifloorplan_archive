# Generated by Django 3.0.6 on 2020-09-24 08:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20200924_0805'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='cam_pos_x',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='image',
            name='cam_pos_y',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
    ]
