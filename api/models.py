from django.db import models
from django.contrib.auth.models import User
from ckeditor_uploader.fields import RichTextUploadingField
from ckeditor.fields import RichTextField
import time


def startLive():
    return time.time()

def endLive():
    times = time.time()
    return times + 3600

class IFloorPlan(models.Model):
    textColor = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=300, blank=True, null=True)
    addressFontWeight = models.CharField(max_length=300, default='normal')
    addressFontStyle = models.CharField(max_length=300, default='normal')
    addressFontSize = models.FloatField(default=1, blank=True)
    addressFontFamily = models.CharField(max_length=300, default='Roboto, sans-serif')
    addressPosition_x = models.FloatField(blank=True, default=30)
    addressPosition_y = models.FloatField(blank=True, default=70)
    addressInputWidth = models.FloatField(blank=True, default=370)
    textBlock = RichTextField(blank=True, null=True)
    lat_long = models.CharField(max_length=100, blank=True, null=True)
    bgColor = models.CharField(max_length=100, blank=True, null=True)
    camColor = models.CharField(max_length=10, blank=True, null=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    date_created = models.DateField(auto_now_add=True)
    splash_screen = models.CharField(blank=True, null=True, max_length=300)
    tabs_style = models.CharField(max_length=500, blank=True, null=True)
    start_live = models.FloatField(blank=True, default=startLive)
    end_live = models.FloatField(blank=True, default=endLive)
    template_name = models.CharField(max_length=300, default='', blank=True, null=True)
    client_data = models.CharField(max_length=1000, default='', blank=True, null=True)
    client_position = models.CharField(max_length=1000, default='', blank=True, null=True)
    client_block_x = models.FloatField(blank=True, null=True, default=0)
    client_block_y = models.FloatField(blank=True, null=True, default=0)
    client_scale = models.FloatField(blank=True, null=True, default=1)
    brand_logo = models.CharField(max_length=1000, default='', blank=True, null=True)
    brand_width = models.FloatField(blank=True, null=True)
    brand_transparency = models.FloatField(blank=True, null=True)
    brand_pos_x = models.FloatField(blank=True, null=True)
    brand_pos_y = models.FloatField(blank=True, null=True)
    brand_url = models.CharField(blank=True, null=True, max_length=300)
    published = models.BooleanField(default=False)
    viewed = models.IntegerField(blank=True,default=0)

class SlideShowOrder(models.Model):
    plan = models.ForeignKey(IFloorPlan, on_delete=models.CASCADE, related_name='order', blank=True, null=True)
    img = models.CharField(max_length=1000, blank=True, null=True)
    index_marker = models.IntegerField(blank=True, default=0)
    index_level = models.IntegerField(blank=True, default=0)
    index = models.IntegerField(blank=True, default=0)



class Level(models.Model):
    plan = models.ForeignKey(IFloorPlan, on_delete=models.CASCADE, related_name='level', blank=True, null=True)
    tabLabel = models.CharField(max_length=100, blank=True, null=True)
    img_pos_x = models.FloatField(blank=True, null=True, default=20)
    img_pos_y = models.FloatField(blank=True, null=True, default=20)
    img_width = models.FloatField(blank=True, null=True, default=30)
    plan_pos_x = models.FloatField(blank=True, null=True, default=68)
    plan_pos_y = models.FloatField(blank=True, null=True, default=20)
    plan_width = models.FloatField(blank=True, null=True, default=30)
    index = models.IntegerField(blank=True, null=True, default=0)

class PlanImg(models.Model):
    level = models.ForeignKey(Level, on_delete=models.CASCADE, related_name='plan_level', blank=True, null=True)
    img = models.CharField(max_length=1000, blank=True, null=True)

class Image(models.Model):
    level = models.ForeignKey(Level, on_delete=models.CASCADE, related_name='images', blank=True, null=True)
    img = models.CharField(max_length=1000, blank=True, null=True)
    img_id = models.IntegerField(blank=True, null=True)
    index = models.IntegerField(blank=True, null=True, default=0)
    cam_pos_x = models.FloatField(blank=True, null=True, default=0)
    cam_pos_y = models.FloatField(blank=True, null=True, default=0)
    cam_rotation = models.FloatField(blank=True, null=True)
    cam_img = models.CharField(max_length=1000, blank=True, null=True)
    cam_img_hover = models.CharField(max_length=1000, blank=True, null=True)
    cam_width = models.FloatField(blank=True, null=True, default=2)
    button = models.BooleanField(default=False)
    button_bg = models.CharField(max_length=100, blank=True, null=True)
    button_color_text = models.CharField(max_length=100, blank=True, null=True)
    button_label = models.CharField(max_length=100, blank=True, null=True)
    button_scale = models.FloatField(blank=True, null=True, default=2)
    button_font_size = models.FloatField(blank=True, null=True, default=2)
    button_padding_vert = models.FloatField(blank=True, null=True, default=2)
    button_padding_gor = models.FloatField(blank=True, null=True, default=2)
    button_radius = models.FloatField(blank=True, null=True, default=2)

    def save(self, *args, **kwargs):
        if not self.pk:
            instance_marker = SystemMarkers.objects.filter(type="IMAGE", default=True).first()
            self.cam_img = instance_marker.marker.url
            self.cam_img_hover = instance_marker.marker_hover.url
        super(Image, self).save(*args, **kwargs)

class Panelum(models.Model):
    level = models.ForeignKey(Level, on_delete=models.CASCADE, related_name='panelum', blank=True, null=True)
    img = models.CharField(max_length=1000, blank=True, null=True)
    cam_img_hover = models.CharField(max_length=1000, blank=True, null=True)
    cam_img = models.CharField(max_length=1000, blank=True, null=True)
    cam_width = models.FloatField(blank=True, null=True, default=2)
    id_lib_img = models.IntegerField(blank=True, null=True)
    pos_x = models.FloatField(blank=True, null=True)
    pos_y = models.FloatField(blank=True, null=True)

class VideoMarker(models.Model):
    level = models.ForeignKey(Level, on_delete=models.CASCADE, related_name='video_marker', blank=True, null=True)
    video = models.CharField(max_length=1000, blank=True, null=True)
    cam_img = models.CharField(max_length=1000, blank=True, null=True)
    cam_img_hover = models.CharField(max_length=1000, blank=True, null=True)
    cam_width = models.FloatField(blank=True, null=True, default=2)
    pos_x = models.FloatField(blank=True, null=True)
    pos_y = models.FloatField(blank=True, null=True)


def upload_to_dir_plan(instance, filename):
    return 'ifloorplans/ifloorplan_{0}/plans/{1}'.format(instance.ifloorplan.id, filename)

class LibraryPlan(models.Model):
    ifloorplan = models.ForeignKey(IFloorPlan, on_delete=models.CASCADE, related_name='library_plan', blank=True, null=True)
    plan_image = models.ImageField(upload_to=upload_to_dir_plan, blank=True, null=True)


def upload_to_dir_image(instance, filename):
    return 'ifloorplans/ifloorplan_{0}/images/{1}'.format(instance.ifloorplan_id.id, filename)

class LibraryImage(models.Model):
    level_id = models.ForeignKey(Level, on_delete=models.CASCADE, related_name='library_image', blank=True, null=True)
    ifloorplan_id = models.ForeignKey(IFloorPlan, on_delete=models.CASCADE, related_name='library_images', blank=True, null=True)
    image = models.ImageField(upload_to=upload_to_dir_image, blank=True, null=True)
    selected = models.BooleanField(blank=True, default=False)
    deleted = models.BooleanField(blank=True, default=False)

class PanelumLibrary(models.Model):
    ifloorplan_id = models.ForeignKey(IFloorPlan,
                                   on_delete=models.CASCADE,
                                   related_name='panelum_lib',
                                   blank=True, null=True)
    image = models.ImageField(upload_to=upload_to_dir_image, blank=True, null=True)



# =======================================================================================================================
# ===================================CLIENTS=============================================================================

class Client(models.Model):
    client_name = models.CharField(max_length=1000, blank=True, null=True)
    contact_name = models.CharField(max_length=1000, blank=True, null=True)
    contact_phone = models.CharField(max_length=1000, blank=True, null=True)
    contact_email = models.CharField(max_length=1000, blank=True, null=True)
    url_click = models.CharField(max_length=1000, blank=True, null=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='author_client', blank=True, null=True)

def upload_to_dir_image_client(instance, filename):
    return 'clients_logo/client_{0}/{1}'.format(instance.client.id, filename)

class ClientLogo(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='author_logo', blank=True, null=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='client', blank=True, null=True)
    logo = models.ImageField(upload_to=upload_to_dir_image_client, blank=True, null=True)
    width = models.FloatField(blank=True, null=True)
    position_logo_x = models.FloatField(blank=True, null=True)
    position_logo_y = models.FloatField(blank=True, null=True)
    position_name_x = models.FloatField(blank=True, null=True)
    position_name_y = models.FloatField(blank=True, null=True)
    position_phone_x = models.FloatField(blank=True, null=True)
    position_phone_y = models.FloatField(blank=True, null=True)
    position_brand_x = models.FloatField(blank=True, null=True)
    position_brand_y = models.FloatField(blank=True, null=True)
    position_contact_x = models.FloatField(blank=True, null=True)
    position_contact_y = models.FloatField(blank=True, null=True)


# =======================================================================END=============================================

# =======================================================================================================================
# ==================================================BRANDING=============================================================

def upload_to_dir_image_brand(instance, filename):
    return 'brandImage/brand_{0}/{1}'.format(instance.author.id, filename)


class Brand(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='author_brand', blank=True, null=True)
    logo = models.ImageField(upload_to=upload_to_dir_image_brand, blank=True, null=True)
    width = models.FloatField(blank=True, null=True)
    transparency = models.FloatField(blank=True, null=True)
    position_x = models.FloatField(blank=True, null=True)
    position_y = models.FloatField(blank=True, null=True)
    url = models.CharField(blank=True, null=True, max_length=300)



# ==================================================================END==================================================


# =========================video=====================
class Media(models.Model):
    ifloorplan = models.ForeignKey(IFloorPlan, on_delete=models.CASCADE, related_name='media', blank=True, null=True)
    url = models.CharField(blank=True, null=True, max_length=1000)
    label = models.CharField(blank=True, null=True, max_length=50)
    type = models.CharField(blank=True, null=True, max_length=50)
    index = models.IntegerField(blank=True, null=True, default=0)
# ================================END===============

#=========================System markers ==============
class SystemMarkers(models.Model):
    IMAGE = "IMAGE"
    VIDEO = "VIDEO"
    PANORAMA = "PANORAMA"
    TYPE_CHOOSE = (
        (IMAGE, "IMAGE"),
        (VIDEO, "VIDEO"),
        (PANORAMA, "PANORAMA"),
    )
    marker = models.FileField(upload_to="markers", blank=True, null=True)
    marker_hover = models.FileField(upload_to="markers", blank=True, null=True)
    default = models.BooleanField(default=True)
    type = models.CharField(max_length=20,
                                      choices=TYPE_CHOOSE,
                                      default=IMAGE)
#=========================END==========================

#======================Users markers===================
class UserMarkers(models.Model):
    IMAGE = "IMAGE"
    VIDEO = "VIDEO"
    PANORAMA = "PANORAMA"
    TYPE_CHOOSE = (
        (IMAGE, "IMAGE"),
        (VIDEO, "VIDEO"),
        (PANORAMA, "PANORAMA"),
    )
    marker = models.FileField(upload_to="markers", blank=True, null=True)
    marker_hover = models.FileField(upload_to="markers", blank=True, null=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='author_marker', blank=True, null=True)
    type = models.CharField(max_length=20,
                                      choices=TYPE_CHOOSE,
                                      default=IMAGE)
#==========================END=========================



