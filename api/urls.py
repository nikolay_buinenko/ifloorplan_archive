from django.urls import path, include, re_path
from .views import *

urlpatterns = [
    path('get/', IFloorPlanViewSet.as_view()),
    path('update/', IFloorPlanUpdateSet.as_view({'put': 'update'})),
    path('create/', IFloorPlanCreateSet.as_view({'post': 'create'})),
    path('delete/', IFloorPlanDelete.as_view({'delete':'destroy'})),

    path('imageupload/', ImgView.as_view({'post': 'post', 'delete': 'destroy', 'put': 'update', 'get': 'list'})),
    path('imageplanuload/', PlanImgView.as_view({'get': 'list', 'put': 'update', 'delete': 'destroy'})),
    path('imagedownload/<int:pk>/', ImgDownload.as_view({'get': 'list'})),
    path('imageplandownload/<int:pk>/', PlanImgDownload.as_view({'get': 'list'})),

    path('clientimguload/', ClientLogoView.as_view({'post': 'post', 'put':'update'})),
    path('clientupload/', ClientView.as_view({'post': 'post', 'put': 'update', 'delete': 'destroy'})),
    path('clientimgdownload/', ClientLogoDownload.as_view({'post': 'list'})),
    path('clientdownload/', ClientDownload.as_view({'post': 'list'})),

    path('brand/', BrandView.as_view({'post': 'post', 'put': 'put', 'delete': 'delete'})),
    path('getbrand/<int:pk>/', BrandView.as_view({'get': 'get'})),

    path('panelum/', PanelumView.as_view({'post': 'post', 'delete': 'delete', 'put': 'update', 'get': 'list'})),

    path('system_markers/', SystemMarkersView.as_view({'get': 'get'})),
    path('user_markers/', UserMarkersView.as_view({'get': 'get', 'post': 'post', 'put': 'update', 'delete': 'delete'}))
]