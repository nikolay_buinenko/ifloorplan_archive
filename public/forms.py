from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from .models import Feedback
from loguru import logger

logger.add("debug.log", format="{time} {level} {message}", level="DEBUG")

COUNTRY_LIST = (('United States', 'United States'), ('Australia', 'Australia'), ('United Kingdom', 'United Kingdom'))


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = '__all__'

class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(username=username, password=password)
            logger.debug(username)
            if not user:
                raise forms.ValidationError('This user does not exist')
            if not user.check_password(password):
                raise forms.ValidationError('Incorrect password')
            if not user.is_active:
                raise forms.ValidationError('This user is not active')
        return super(UserLoginForm, self).clean(*args, **kwargs)


class UserRegisterForm(UserCreationForm):
    terms_and_cond = forms.BooleanField(required=True)
    email = forms.EmailField(label='Email address')
    coupon = forms.CharField(max_length=100, required=False, help_text='Optional.')
    country = forms.ChoiceField(choices=COUNTRY_LIST)
    email_news = forms.BooleanField(required=False)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password1',
            'password2',
            'coupon',
            'terms_and_cond',
            'country',
            'email_news'
        ]

    # def clean(self, *args, **kwargs):
    #     email = self.cleaned_data.get('email')
    #     email_qs = User.objects.filter(email=email)
    #     if email_qs.exists():
    #         raise forms.ValidationError(
    #             "This email has already been registered")
    #     return super(UserRegisterForm, self).clean(*args, **kwargs)

    def save(self, commit=True):
        user = super(UserRegisterForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user