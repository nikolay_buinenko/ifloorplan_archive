from django.urls import path, include
from . import views
from .models import OtherPage

urlpatterns = [
    path('', views.Public.index, name='main_page'),
    path('.well-known/apple-developer-merchantid-domain-association', views.Public.merch, name='merch'),
    path('contact/', views.Public.contact, name='Contact'),
    path('features/', views.Public.features, name='Features'),
    path('login/', views.Public.login, name='Login'),
    path('pricing/', views.Public.pricing, name='Pricing'),
    path('register/', views.Public.sign_up, name='Sign up'),
    path('logout/', views.Public.logout, name='logout'),
    path('ifloorplan/<int:pk>', views.Public.ifloorplan, name='ifloorplan'),
]

all_other_pages = OtherPage.objects.all()
for i in all_other_pages:
    urlpatterns.append(path(i.url, views.Public.content, name=i.title))