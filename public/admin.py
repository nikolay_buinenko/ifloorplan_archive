from django.contrib import admin
from .models import (
    MainPage,
    OtherPage,
    PurchaseCredits,
    PurchaseCreditsItem,
    ApiPrice,
    Feature,
    Coupon,
    Header,
    HeaderMenu,
    Footer,
    FooterFloorMenu,
    FooterAboutMenu,
    Feedback,
    ApiToken,
    Profile,
    SliderImage,
    SubscriptionPrice,
    Transaction,
    HomePage

)

# Register your models here.

admin.site.register(MainPage)
admin.site.register(OtherPage)

class TransactionAdmin(admin.ModelAdmin):
    list_display = ('user',
                    'transaction_hash_code',
                    'payment_method',
                    'paid',
                    'date'
                    )
    exclude = ['user']

admin.site.register(Transaction, TransactionAdmin)


admin.site.register(Feature)
admin.site.register(Coupon)

# admin.site.register(ApiPrice)
admin.site.register(Profile)
admin.site.register(SubscriptionPrice)

# =======================================================

@admin.register(ApiToken)
class ApiTokenAdmin(admin.ModelAdmin):
    list_display = ('user', 'token', 'data_enable','data_disable', 'active')
    search_fields = ('data_disable', 'data_enable',)


# ===========================================================
@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('name', 'email','date',)
    search_fields = ('name', 'email',)
    date_hierarchy = 'date'

    def has_add_permission(self, request):
        return False


# ============================================================
# class AnnualSubscriptionInline(admin.TabularInline):
#     fk_name = 'price'
#     model = AnnualSubscriptionItem
#     extra = 1
#
#
# @admin.register(AnnualSubscription)
# class AnnualSubscriptionAdmin(admin.ModelAdmin):
#     inlines = [AnnualSubscriptionInline]

# ============================================================
class PurchaseCreditsInline(admin.TabularInline):
    fk_name = 'price'
    model = PurchaseCreditsItem
    extra = 1


@admin.register(PurchaseCredits)
class PurchaseCreditsAdmin(admin.ModelAdmin):
    inlines = [PurchaseCreditsInline]
#=============================================================

class FooterMenuAboutInline(admin.TabularInline):
    fk_name = 'menu'
    model = FooterAboutMenu
    extra = 1


class FooterMenuFloorInline(admin.TabularInline):
    fk_name = 'menu2'
    model = FooterFloorMenu
    extra = 1


@admin.register(Footer)
class FooterMenuAdmin(admin.ModelAdmin):
    inlines = (FooterMenuAboutInline, FooterMenuFloorInline)

    def has_add_permission(self, request):
        # if there's already an entry, do not allow adding
        count = Footer.objects.all().count()
        if count == 0:
            return True

        return False
# ===============================================================

class MenuInline(admin.TabularInline):
    fk_name = 'menu'
    model = HeaderMenu
    extra = 1


@admin.register(Header)
class HeaderMenuAdmin(admin.ModelAdmin):
    inlines = [MenuInline]

    def has_add_permission(self, request):
        # if there's already an entry, do not allow adding
        count = Header.objects.all().count()
        if count == 0:
            return True

        return False
# ================================================================


class SliderImageInline(admin.TabularInline):
    fk_name = 'page'
    model = SliderImage
    extra = 1


@admin.register(HomePage)
class HomePageAdmin(admin.ModelAdmin):
    inlines = [SliderImageInline]

    def has_add_permission(self, request):
        # if there's already an entry, do not allow adding
        count = HomePage.objects.all().count()
        if count == 0:
            return True

        return False