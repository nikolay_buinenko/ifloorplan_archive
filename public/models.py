from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.

# ==================================================================
class ApiToken(models.Model):
    token = models.IntegerField(default=None)
    data_enable = models.DateField(auto_now_add=False, auto_now=False)
    data_disable = models.DateField(auto_now_add=False, auto_now=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='user_api')
    active = models.BooleanField(default=False)

# ===================================================================
class Feedback(models.Model):
    name = models.CharField(max_length=100)
    bisiness_name = models.CharField(max_length=100, blank=True)
    website = models.CharField(max_length=100, blank=True)
    email = models.EmailField(max_length=200)
    message = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "Feedback"

    def __str__(self):
        return self.name + "-" + self.email


# ===================================================================
class Footer(models.Model):
    name = models.CharField(max_length=30)
    icon = models.ImageField(upload_to='system_images/')
    discription_icon = RichTextField()
    name_menu1 = models.CharField(max_length=50)
    name_menu2 = models.CharField(max_length=50)
    name_menu3 = models.CharField(max_length=50)
    content_menu3 = RichTextField()
    copiright = RichTextField()

    def __str__(self):
        return self.name


class FooterAboutMenu(models.Model):
    title = models.CharField(max_length=100)
    link = models.CharField(max_length=200)
    menu = models.ForeignKey(Footer, on_delete=models.CASCADE, related_name='about_menu')


class FooterFloorMenu(models.Model):
    title = models.CharField(max_length=100)
    link = models.CharField(max_length=200)
    menu2 = models.ForeignKey(Footer, on_delete=models.CASCADE, related_name='floor_menu')

# ================================================================================================

class Header(models.Model):
    name = models.CharField(max_length=30)
    icon = models.ImageField(upload_to='system_images/')

    def __str__(self):
        return self.name


class HeaderMenu(models.Model):
    title = models.CharField(max_length=30)
    link = models.CharField(max_length=200)
    menu = models.ForeignKey(Header, on_delete=models.CASCADE, related_name='menu')
# ===============================================================================================

class Coupon(models.Model):
    name = models.CharField(max_length=30)
    credits = models.IntegerField(default=2)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name
# =================================================================================================
class SubscriptionPrice(models.Model):
    title = models.CharField(max_length=100)
    credit_count = models.IntegerField(default=0)
    cost_by_year = models.IntegerField(default=0)
    cost_by_mount = models.IntegerField(default=0)
    each = models.FloatField(default=0)


    def __str__(self):
        return self.title
# =================================================================================================

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    credits = models.IntegerField(default=2)
    subscription_credits = models.IntegerField(default=0)
    subscription = models.BooleanField(default=False)
    subscription_send_message = models.BooleanField(default=True)
    type_subscription = models.CharField(max_length=30, default="mount")
    date_over_subscription = models.FloatField(blank=True, default=0)
    date_start_subscription = models.FloatField(blank=True, default=0)
    date_alert_subscription_over = models.FloatField(blank=True, default=0)
    subscription_object = models.ForeignKey(SubscriptionPrice, on_delete=models.SET_NULL, null=True)
    frozen = models.BooleanField(default=False)
    coupon = models.CharField(max_length=30, blank=True)
    country = models.CharField(max_length=30, blank=True)
    email_notification = models.BooleanField(default=True)
    created_on = models.CharField(max_length=200)

    def __str__(self):
        return self.user.username

# ==============================================================================================

class Feature(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return self.title
# ===============================================================================================
class ApiPrice(models.Model):
    title = models.CharField(max_length=100)
    cost = models.IntegerField(default=2)
    discription = RichTextField()

# ================================================================================================

# class AnnualSubscription(models.Model):
#     title = models.CharField(max_length=100)
#     count_credits = models.IntegerField(default=2)
#     cost = models.IntegerField(default=2)
#     popular = models.BooleanField(default=False)
#
#     def __str__(self):
#         return self.title
#
#
# class AnnualSubscriptionItem(models.Model):
#     title = models.CharField(max_length=100)
#     price = models.ForeignKey(AnnualSubscription, on_delete=models.CASCADE, related_name='price')


# ================================================================================================

class PurchaseCredits(models.Model):
    SINGLE = "single"
    SUBSCRIPTION = "subscription"
    DOMAIN = "domain"
    TYPE = (
        (SINGLE, "single"),
        (SUBSCRIPTION, "subscription"),
        (DOMAIN, "domain"),
    )

    title = models.CharField(max_length=100)
    count_credits = models.IntegerField(default=2)
    cost = models.IntegerField(default=2)
    popular = models.BooleanField(default=False)
    type = models.CharField(max_length=20, choices=TYPE, default=SINGLE)

    def __str__(self):
        return self.title


class PurchaseCreditsItem(models.Model):
    title = models.CharField(max_length=100)
    price = models.ForeignKey(PurchaseCredits, on_delete=models.CASCADE, related_name='price')
# ====================================================================================================

class MainPage(models.Model):

    title = models.CharField(max_length=100, help_text="""The title for the Page 
        will be displayed in the title bar of the user's 
        browser, and at the top of the Page.""")
    description = models.TextField(help_text="""For SEO: Meta description.""", blank=True)
    keywords = models.TextField(help_text="""For SEO: Meta keywords.""", blank=True)
    published = models.BooleanField(default=True, help_text="""If this is not published, it will 
        not be visible anywhere on the website.
        However, if you are logged in as an Administrator and are 
        viewing the website as an Administrator, this item will be visible.""")
    content = RichTextUploadingField(help_text="""This content will be displayed on the 
        Page after the title. HTML is allowed.""")

    def __str__(self):
        return self.title
# =========================================================================================================

class OtherPage(models.Model):

    title = models.CharField(max_length=100, help_text="""The title for the Page 
        will be displayed in the title bar of the user's 
        browser, and at the top of the Page.""")
    url = models.CharField(max_length=100, help_text="""The URL (address) of the Page as seen in the address bar.
        Generally, the auto-generated URL is best.
        Avoid using only numbers. Only letters, numbers, and hyphens (-) are allowed.""")
    description = models.TextField(help_text="""For SEO: Meta description.""", blank=True)
    keywords = models.TextField(help_text="""For SEO: Meta keywords.""", blank=True)
    published = models.BooleanField(default=True, help_text="""If this is not published, it will 
        not be visible anywhere on the website.
        However, if you are logged in as an Administrator and are 
        viewing the website as an Administrator, this item will be visible.""")
    content = RichTextUploadingField(help_text="""This content will be displayed on the 
        Page after the title. HTML is allowed.""")

    def __str__(self):
        return self.title


class HomePage(models.Model):
    attantion_message = RichTextUploadingField()
    plan = models.IntegerField(default=0, help_text="""Enter Floor Plan id""")
    title_help_text1 = models.CharField(max_length=1000)
    help_text1 = models.CharField(max_length=1000)
    register_button_link = models.CharField(max_length=100)
    register_button_label = models.CharField(max_length=100)
    features_list = models.BooleanField(default=True)
    title_help_text2 = models.CharField(max_length=1000)
    help_text2 = models.CharField(max_length=1000)
    button_label1 = models.CharField(max_length=100)
    button_link1 = models.CharField(max_length=100)
    button_label2 = models.CharField(max_length=100)
    button_link2 = models.CharField(max_length=100)
    button_label3 = models.CharField(max_length=100)
    button_link3 = models.CharField(max_length=100)

class SliderImage(models.Model):
    image = models.ImageField(upload_to='system_images/')
    page = models.ForeignKey(HomePage, on_delete=models.CASCADE, related_name='slider_image')





class Transaction(models.Model):
    MOUNT = "mount"
    YEAR = "year"
    SIMPLE = "simple"
    PEAR = (
        (SIMPLE, "simple"),
        (MOUNT, "mount"),
        (YEAR, "year")
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    paid = models.BooleanField(default=False)
    transaction_hash_code = models.CharField(max_length=10)
    payment_method = models.CharField(max_length=10)
    pear = models.CharField(max_length=20, choices=PEAR, default=SIMPLE)
    product_subscription = models.ForeignKey(SubscriptionPrice, on_delete=models.SET_NULL, null=True)
    product_purchase = models.ForeignKey(PurchaseCredits, on_delete=models.SET_NULL, null=True)
    date = models.DateTimeField(auto_now_add=True)

    # def __str__(self):
    #     return self.user.username

