from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import FileResponse
from django.contrib.auth import login, authenticate, logout, get_user_model
from django.contrib.auth.forms import AuthenticationForm
from .forms import UserLoginForm, UserRegisterForm, FeedbackForm
from .models import (
    OtherPage,
    Profile,
    Coupon,
    Header,
    Footer,
    Feature,
    PurchaseCredits,
    HomePage,
    ApiPrice
)
from django import forms
from django.conf import settings
import json
from django.core.mail import send_mail, mail_admins
from api.models import IFloorPlan
from loguru import logger

logger.add("debug.log", format="{time} {level} {message}", level="DEBUG")

# Create your views here.
data_header = Header.objects.all().first()
data_footer = Footer.objects.all().first()

class Public:

    def merch(request):
        img = open('/home/django/ifloorplan/.well-known/apple-developer-merchantid-domain-association', 'rb')
        response = FileResponse(img)
        return response

    def index(request):
        data = HomePage.objects.all().first()
        features = Feature.objects.all()[:6]
        count_plans = IFloorPlan.objects.all().count()
        plans = IFloorPlan.objects.all()[count_plans - 5:count_plans - 1]
        # logger.debug(plans)
        return render(request, 'public/index.html', {"header": data_header,
                                                     "data" : data,
                                                     "plans" : plans,
                                                     "features" : features if data.features_list else [],
                                                     "footer": data_footer})

    def ifloorplan(request, pk):
        try:
            instance = IFloorPlan.objects.get(id=pk)
            count_view = instance.viewed
            instance.viewed = count_view + 1
            instance.save()
        except:
            pass
        return render(request, 'public/ifloorplan.html', {'pk': pk})

    def contact(request):
        page_name = request.resolver_match.url_name
        if request.method == "POST":
            ''' Begin reCAPTCHA validation '''
            recaptcha_response = request.POST.get('g-recaptcha-response')
            data = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            import requests
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = r.json()
            ''' End reCAPTCHA validation '''
            form = FeedbackForm(request.POST)
            if form.is_valid() and result['success']:
                name = form.cleaned_data['name']
                sender = form.cleaned_data['email']
                subject = "You have a new Feedback from {}:{}".format(name, sender)
                message = "Subject: iFloorPlan\n\nMessage: {}".format(form.cleaned_data['message'])
                mail_admins(subject, message)
                form.save()
                messages.add_message(request, messages.SUCCESS, 'Feedback Submitted.')
            else:
                messages.add_message(request, messages.ERROR, 'Error.')
        else:
            form = FeedbackForm()
        return render(request, 'public/contact_us.html', {"header": data_header, "footer": data_footer, "form": form, "page_name": page_name})

    def features(request):
        page_name = request.resolver_match.url_name
        obj = Feature.objects.all()
        data = []
        if obj.count() % 3 == 0:
            col = obj.count() // 3
        else:
            col = obj.count() // 3 + 1
        j = 0
        for i in range(col):
            list_empty = []
            for k in range(3):
                if j == obj.count():
                    break
                list_empty.append(obj[j])
                j += 1
            data.append(list_empty)

        return render(request, 'public/features.html', {"data": data, "header": data_header, "footer": data_footer, "page_name": page_name})

    def login(request):
        page_name = request.resolver_match.url_name
        if not request.user.is_authenticated:
            form = AuthenticationForm(request=request, data=request.POST)
            if form.is_valid():
                username = form.cleaned_data.get('username')
                password = form.cleaned_data.get('password')
                user = authenticate(username=username, password=password)
                login(request, user)
                return redirect('new')

            return render(request, 'public/login.html', {'form': form, "header": data_header, "footer": data_footer, "page_name": page_name})
        else:
            return redirect('new')

    def logout(request):
        logout(request)
        return redirect('/')

    def pricing(request):
        page_name = request.resolver_match.url_name
        purchase = PurchaseCredits.objects.all()
        api = ApiPrice.objects.all()
        return render(request, 'public/prices.html', {"header": data_header, "footer": data_footer, "data_purchase": purchase,
                                                      "data_api": api, "page_name": page_name})

    def sign_up(request):
        page_name = request.resolver_match.url_name
        form = UserRegisterForm(request.POST or None)
        if form.is_valid():
            ''' Begin reCAPTCHA validation '''
            recaptcha_response = request.POST.get('g-recaptcha-response')
            data = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            import requests
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = r.json()
            ''' End reCAPTCHA validation '''
            if result['success']:
                from django.utils import timezone
                user = form.save(commit=False)
                credits_default = 2
                password = form.cleaned_data.get('password')
                coupon = form.cleaned_data['coupon']
                try:
                    result_search_coupon = Coupon.objects.get(name=coupon)
                except:
                    result_search_coupon = False
                if result_search_coupon:
                    credits_default = int(result_search_coupon.credits)
                country = form.cleaned_data['country']

                if request.POST.get('email_news'):
                    email_notif = form.cleaned_data['email_news']
                else:
                    email_notif = False

                # user.set_password(password)
                user.save()
                profile_object = Profile(user=user, credits=credits_default, coupon=coupon, country=country,
                                         email_notification=email_notif, created_on=timezone.now())
                profile_object.save()

                return redirect('Login')
            else:
                raise forms.ValidationError(
                    "recapcha")
        return render(request, 'public/sign_up.html', {'form': form, "header": data_header, "footer": data_footer, "page_name": page_name})

    def content(request):
        page_name = request.resolver_match.url_name
        data = OtherPage.objects.get(title=page_name)
        return render(request, 'public/content.html', { 'data' : data, "header": data_header, "footer": data_footer, "page_name": page_name })