import React, {useState, useEffect} from 'react';

const Cam = (props) => {
    const camWrapStyle = {
        position: 'absolute',
        width: props.value.cam_width+"%",
        rotation: props.value.cam_rotation,
        top: props.value.cam_pos_y+"%",
        left: props.value.cam_pos_x+"%"
    };

    return (
        <div className="cam" style={camWrapStyle}>
            <img style={{transform: `rotate(${camWrapStyle.rotation}deg)`, width: "100%"}}
                 src={props.value.cam_img_hover}  className="arrowHover" alt=""/>
        </div>
    )
}

export default Cam;