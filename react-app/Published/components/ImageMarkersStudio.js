import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {
    changeCamPosition,
    changeImageKey,
    changeLevel,
    ifloorplanStateAdd,
    nextStep,
    setColor,
    setColorBackground,
    setColorText
} from '../redux/actions/actions';
import {getSystemIcons, getUserIcons, updateNewPlan} from "../serverRequests/requests";
import CustomButtons from './CustomButtons'


const ImageMarkersStudio = (props) => {

    const [loadedSystIcons, setLoadedSystIcons] = useState([]);
    const [loadedUserIcons, setLoadedUserIcons] = useState([]);

    const [showSystemIcons, setShowSystemIcons] = useState(true);
    const [markerScale, setMarkerScale] = useState(2);
    const [saveLastState, setSaveLastState] = useState({});

    const [swapMarkButt, setSwapMarkButt] = useState(false);
    const [markersButtons, setMarkersButtons] = useState(false);
    const [activeTab, setActiveTab] = useState({
        opacity1: 1,
        opacity2: -1,
        zIndex1: 1,
        zIndex2: -1
    })

    const Selector = () => {
        markersButtons
            ?
            setActiveTab({
                ...activeTab,
                opacity1: 1,
                opacity2: -1,
                zIndex1: 1,
                zIndex2: -1
            })
            :
            setActiveTab({
                ...activeTab,
                opacity1: -1,
                opacity2: 1,
                zIndex1: -1,
                zIndex2: 1
            })
    }

    // const [defaultButtons, setDefaultButtons] = useState({
    //     defaultButtonsColor: '#99bdff',
    //     defaultButtonsFontColor: '#515151'
    // })
    // const [userButtons, setUserButtons] = useState({
    //     userButtonColor: [],
    //     userButtonFontColor: []
    // })
    // const [buttonActive, setButtonActive] = useState({
    //     button: false,
    //     button_bg: defaultButtons.defaultButtonsColor,
    //     button_class: null,
    //     button_color_text: defaultButtons.defaultButtonsFontColor,
    //     button_label: 'Beach',
    //     button_scale: 2
    // });


    useEffect(() => {
        getSystemIcons()
            .then((data) => {
                const icons = [];
                for (let i = 0; i < data.length; i++) {
                    if (data[i].type == "IMAGE") {
                        icons.push(data[i]);
                    }
                }
                setLoadedSystIcons(icons);
            });
        getUserIcons()
            .then((data) => {
                const icons = [];
                for (let i = 0; i < data.length; i++) {
                    if (data[i].type == "IMAGE") {
                        icons.push(data[i]);
                    }
                }
                setLoadedUserIcons(icons);
            });
        const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        setSaveLastState(newState);

        function readCookie(name) {
            //function split cookie for get csrf token
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        // fetch("https://ifloorplan360.com/api/user_button/")
        //     .then((res) => {
        //         res.json()
        //             .then((data) => {
        //                 // If this is the first user input, then an empty array 'data' comes
        //                 if (data.length) {
        //                     setUserButtons({
        //                         ...userButtons,
        //                         userButtonColor: data[0].button_bk_color,
        //                         userButtonFontColor: data[0].button_lb_color
        //                     })
        //                 } else { // Setting default button colors
        //                     setButtonActive({
        //                         ...buttonActive,
        //                         button_bg: defaultButtons.defaultButtonsColor,
        //                         button_color_text: defaultButtons.defaultButtonsFontColor
        //                     })
        //                 }
        //             })
        //     })
    }, []);

    const chooseSysIcons = () => {
        setShowSystemIcons(true)
    }
    const chooseMyIcons = () => {
        setShowSystemIcons(false)
    }
    const setIcons = (index, type) => {
        const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        for (let j = 0; j < newState.level.length; j++) {
            for (let i = 0; i < newState.level[j].images.length; i++) {
                if (type == "system") {
                    newState.level[j].images[i].cam_img = loadedSystIcons[index].marker;
                    newState.level[j].images[i].cam_img_hover = loadedSystIcons[index].marker_hover;
                } else {
                    newState.level[j].images[i].cam_img = loadedUserIcons[index].marker;
                    newState.level[j].images[i].cam_img_hover = loadedUserIcons[index].marker_hover;
                }
            }
        }
        props.ifloorplanStateAdd(newState);
    }

    useEffect(() => {
        const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        for (let i = 0; i < newState.level.length; i++) {
            for (let j = 0; j < newState.level[i].images.length; j++) {
                newState.level[i].images[j].cam_width = markerScale;
            }
        }
        props.ifloorplanStateAdd(newState)
    }, [markerScale]);

    const CancelMarker = () => {
        props.ifloorplanStateAdd(saveLastState)
        setMarkerScale(2);
    }


    // const ToMyButtons = () => {
    //     setButtonActive({
    //         ...buttonActive,
    //         button_bg: userButtons.userButtonColor,
    //         button_color_text: userButtons.userButtonFontColor
    //     })
    // }

    // const ToSysButtons = () => {
    //     setButtonActive({
    //         ...buttonActive,
    //         button_bg: defaultButtons.defaultButtonsColor,
    //         button_color_text: defaultButtons.defaultButtonsFontColor
    //     })
    // }

    // const UpdateButton = () => {
    //     const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
    //     newState.level[props.levelNowId].images[props.image_key].button = true
    //     newState.level[props.levelNowId].images[props.image_key].button_bg = buttonActive.button_bg
    //     newState.level[props.levelNowId].images[props.image_key].button_class = buttonActive.button_class
    //     newState.level[props.levelNowId].images[props.image_key].button_color_text = buttonActive.button_color_text
    //     newState.level[props.levelNowId].images[props.image_key].button_label = buttonActive.button_label
    //     newState.level[props.levelNowId].images[props.image_key].button_scale = buttonActive.button_scale
    //     props.ifloorplanStateAdd(newState)
    //     console.log(buttonActive)
    // }

    // const HighlightButton = (event) => {
    //     let element = document.querySelectorAll('.selectMark__markers_item > button')
    //     for (let i = 0; i < element.length; i++) {
    //         if (element[i].classList.contains('active')) {
    //             element[i].classList.remove('active')
    //         } else if (element[i] == event) {
    //             element[i].classList.add('active')
    //         }
    //     }
    // }


    // const ButtonClick = (event) => {
    //     event => {
    //         setButtonActive({
    //             ...buttonActive,
    //             button: true,
    //             button_class: event.target.classList[0]
    //         });
    //         HighlightButton(event.target);
    //         UpdateButton();
    //         console.log('buttonActive', buttonActive)
    //         console.log('ifloorplanNowState', props.ifloorplanNowState)
    //     }
    // }


    // useEffect(() => {
    //     console.log('UpdateButton')
    // }, [buttonActive.button_scale]);

    return (
        <div className="selectMark_wrapper">
            <h4>Edit Image Markers</h4>
            <button className='chooseMarkButt'
                    onClick={() => {
                        setMarkersButtons(!markersButtons)
                        Selector()
                    }}>
                {markersButtons
                    ?
                    'Use markers'
                    :
                    'Use buttons'
                }
            </button>
            <div className="selectMark" style={{opasity: activeTab.opasity1, zIndex: activeTab.zIndex1}}>
                <div className="selectMark__action">
                    <h5>Select a Marker for all images</h5>
                </div>
                <div className="selectMark__select">
                    <a onClick={chooseSysIcons}>Markers</a>
                    <a onClick={chooseMyIcons}>My Markers</a>
                </div>
                <div className="selectMark__markers">

                    {showSystemIcons
                        ?
                        loadedSystIcons.map((el, index) => (
                            <div className="selectMark__markers_item markers" key={index} onClick={() => {
                                setIcons(index, "system")
                            }}>
                                <img src={el.marker} alt=""/>
                            </div>
                        ))
                        :
                        loadedUserIcons.map((el, index) => (
                            <div className="selectMark__markers_item markers" key={index} onClick={() => {
                                setIcons(index, "user")
                            }}>
                                <img src={el.marker} alt=""/>
                            </div>
                        ))
                    }

                </div>
                <div className="scale-wrapper">
                    <h3 id='test'>Scale</h3>
                    <input type="range"
                           id='markersSize'
                           step="0.05"
                           className="scale-image"
                           min="1"
                           max="3"
                           value={markerScale}
                           onChange={(event => setMarkerScale(event.target.value))}/>
                </div>
            </div>
            <CustomButtons opacity={activeTab.opacity2} zindex={activeTab.zIndex2}/>
            <div className="selectMark__btns swapMarkButt">
                <button className='chooseMarkButt'
                        onClick={() => {
                            setSwapMarkButt(!swapMarkButt)
                        }}>
                    {swapMarkButt
                        ?
                        'Swap Marker for a button'
                        :
                        'Swap with active marker'
                    }
                </button>
            </div>
            <div className="selectMark__btns">
                <button className="cancel" onClick={CancelMarker}>Cancel</button>
                <button className="done">Done</button>
            </div>
            {/*<div className="selectMark">*/}
            {/*    <div className="selectMark__action">*/}
            {/*        <h5>Swap Marker for a button</h5>*/}
            {/*        <h5>Swap with active marker</h5>*/}
            {/*    </div>*/}
            {/*    <div className="selectMark__select">*/}
            {/*        <a>Markers</a>*/}
            {/*        <a>My Markers</a>*/}
            {/*    </div>*/}

            {/*    <div className="selectMark__markers">*/}
            {/*        <div className="selectMark__markers_item">*/}
            {/*            <button className='rectangle'*/}
            {/*                    // onClick={(event)=>{*/}
            {/*                    //     ButtonClick(event)*/}
            {/*                    // }}*/}
            {/*                    style={{backgroundColor: '#99bdff', color: '#515151'}}*/}
            {/*            >Beach*/}
            {/*            </button>*/}
            {/*        </div>*/}
            {/*        <div className="selectMark__markers_item">*/}
            {/*            <button className='round-sides'*/}
            {/*                    // onClick={(event) => {*/}
            {/*                    //     setButtonActive({*/}
            {/*                    //         ...buttonActive,*/}
            {/*                    //         button: true,*/}
            {/*                    //         button_class: event.target.className*/}
            {/*                    //     });*/}
            {/*                    //     HighlightButton(event.target);*/}
            {/*                    //     UpdateButton();*/}
            {/*                    // }}*/}
            {/*                    style={{backgroundColor: '#99bdff', color: '#515151'}}*/}
            {/*            >Beach*/}
            {/*            </button>*/}
            {/*        </div>*/}
            {/*        <div className="selectMark__markers_item">*/}
            {/*            <button className='round'*/}
            {/*                    // onClick={(event) => {*/}
            {/*                    //     setButtonActive({*/}
            {/*                    //         ...buttonActive,*/}
            {/*                    //         button: true,*/}
            {/*                    //         button_class: event.target.className*/}
            {/*                    //     });*/}
            {/*                    //     HighlightButton(event.target);*/}
            {/*                    //     UpdateButton();*/}
            {/*                    // }}*/}
            {/*                    style={{backgroundColor: '#99bdff', color: '#515151'}}*/}
            {/*            >Beach*/}
            {/*            </button>*/}
            {/*        </div>*/}
            {/*        <div className="selectMark__markers_item">*/}
            {/*            <button className='big-rectangle'*/}
            {/*                    // onClick={(event) => {*/}
            {/*                    //     setButtonActive({*/}
            {/*                    //         ...buttonActive,*/}
            {/*                    //         button: true,*/}
            {/*                    //         button_class: event.target.className*/}
            {/*                    //     });*/}
            {/*                    //     HighlightButton(event.target);*/}
            {/*                    //     UpdateButton();*/}
            {/*                    // }}*/}
            {/*                    style={{backgroundColor: '#99bdff', color: '#515151'}}*/}
            {/*            >Beach*/}
            {/*            </button>*/}
            {/*        </div>*/}
            {/*        <div className="selectMark__markers_item">*/}
            {/*            <button className='big-round-sides'*/}
            {/*                    // onClick={(event) => {*/}
            {/*                    //     setButtonActive({*/}
            {/*                    //         ...buttonActive,*/}
            {/*                    //         button: true,*/}
            {/*                    //         button_class: event.target.className*/}
            {/*                    //     });*/}
            {/*                    //     HighlightButton(event.target);*/}
            {/*                    //     UpdateButton();*/}
            {/*                    // }}*/}
            {/*                    style={{backgroundColor: '#99bdff', color: '#515151'}}*/}
            {/*            >Beach*/}
            {/*            </button>*/}
            {/*        </div>*/}
            {/*    </div>*/}

            {/*    <div className="selectMark__label">*/}
            {/*        <label>Label<input*/}
            {/*            type="text"*/}
            {/*            value="Beach"*/}
            {/*            onChange={() => {*/}
            {/*                console.log('Rename button')*/}
            {/*            }}*/}
            {/*        /></label>*/}
            {/*    </div>*/}
            {/*    <div className="scale-wrapper">*/}
            {/*        <h3>Scale</h3>*/}
            {/*        <input type="range"*/}
            {/*               id='buttonSize'*/}
            {/*               step="0.05"*/}
            {/*               className="scale-image"*/}
            {/*               min="1"*/}
            {/*               max="5"*/}
            {/*               // value={buttonActive.button_scale}*/}
            {/*               onChange={()=>{*/}
            {/*                   console.log('Scale button')}}/>*/}
            {/*    </div>*/}
            {/*    <div className="selectMark__btns">*/}
            {/*        <button className="cancel">Cancel*/}
            {/*        </button>*/}
            {/*        <button className="done">*/}
            {/*            Done*/}
            {/*        </button>*/}
            {/*    </div>*/}
            {/*</div>*/}
        </div>
    )
}
const putState = (state) => {
    return {
        step: state.step,
        levelNowId: state.levelNowId,
        cam_key: state.cam_key,
        image_key: state.image_key,
        positionCams: state.positionCams,
        ifloorplanNowState: state.ifloorplanNowState,
        text_color: state.text_color,
        background_color: state.background_color,
        set_color: state.set_color,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        changeCamKey: (key) => dispatch(changeCamKey(key)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        changeCamPosition: (obj) => dispatch(changeCamPosition(obj)),
        setColor: (bool) => dispatch(setColor(bool)),
        changeLevel: (level) => dispatch(changeLevel(level)),
        changeImageKey: (key) => dispatch(changeImageKey(key)),
        setColorBackground: (color) => dispatch(setColorBackground(color)),
        setColorText: (color) => dispatch(setColorText(color))
    }
}


export default connect(putState, putActions)(ImageMarkersStudio);