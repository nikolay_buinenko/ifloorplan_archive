import React, {useState} from 'react';
import { connect } from 'react-redux';
import Draggable, {DraggableCore} from 'react-draggable';
import { nextStep, ifloorplanStateAdd, initScale } from '../redux/actions/actions';

import {updateNewPlan} from '../serverRequests/requests';
import KenburnsEditor from "./KenBurns/KenburnsEditor";
import KenburnsViewer from "./KenBurns/KenburnsViewer";
// import KenBurnsViewer from "./kenburns/KenBurnsViewer";
// import KenBurnsEditor from "./kenburns/KenBurnsEditor";
import raf from 'raf';

const KenBurns = (props) => {
    const ifloorplanObj = props.ifloorplanNowState; //last ifloor plan obj
    const [imageIndex, setImageIndex] = useState(0);

    const [width, setWidth] = useState(500);
    const [value, setValue] = useState({
        from: [0.8, [0.5, 0.5]],
        to: [1, [0.5, 0.5]]
    });
    const [stop, setStop] = useState(false);
    const image = ifloorplanObj.level[0].images[0].img;
    const [progress, setProgress] = useState(0);
    const onChange = (value) => {
        setValue(value);
    }

    const loop = (t) => {
        const progress_ = (t % 1500) / 1500;
        setProgress(progress_);
        raf(loop);
    };
    raf(loop);

    return(
        <div className="ken_burns_wrap">
            <div className="ken_title"><h2>KEN BURNS IMAGE STUDIO</h2></div>
            <div className="ken_done_btn"><button className="save">Done</button></div>
            <div className="ken_main_wrap">
                <div className="ken_tools_wrap">
                    <div className="ken_images">
                        <div className="image">
                            <img src={ifloorplanObj.level[props.levelNowId].images[imageIndex].img} alt=""/>
                        </div>
                        <div className="images_btn_group">
                            <button className="prev" onClick={()=>{
                                if(imageIndex != 0){
                                    setImageIndex(imageIndex - 1);
                                }
                            }}>Previous</button>
                            <button className="next" onClick={()=>{
                                if(ifloorplanObj.level[props.levelNowId].images.length - 1 > imageIndex){
                                    setImageIndex(imageIndex + 1);
                                }
                            }}>Next</button>
                        </div>
                    </div>
                    <div className="target_ken">
                        <img src="/media/system_images/Group17.png" alt=""/>
                        <p>Drag target on to the image</p>
                        <p>Then push play</p>
                    </div>
                    <div className="play_ken">
                        <img src="/media/system_images/Polygon2.png" alt=""/>
                        <p>Still working on this concept!!</p>
                    </div>
                </div>
                <div className="ken_wrap">
                    {/*<KenBurnsEditor*/}
                    {/*    image={ifloorplanObj.level[props.levelNowId].images[imageIndex].img}*/}
                    {/*/>*/}
                    <KenburnsEditor
                        image={ifloorplanObj.level[props.levelNowId].images[imageIndex].img}
                        onChange={onChange}
                        value={value}
                        width={width}
                        height={400}
                        progress={progress}
                        style={{display: "inline-block"}}
                    />

                    <KenburnsViewer
                        image={ifloorplanObj.level[props.levelNowId].images[imageIndex].img}
                        value={value}
                        width={width}
                        height={400}
                        progress={progress}
                        style={{display: "inline-block"}}
                    />
                </div>
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        LibraryImage: state.LibraryImage,
        LibraryPlans: state.LibraryPlans,
        ifloorplanNowState: state.ifloorplanNowState,
        levelNowId: state.levelNowId,
        scale: state.scale
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        dropFiles: (files, title) => dispatch(dropFiles(files, title)),
        initScale: (bool) => dispatch(initScale(bool)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan))
    }
}
export default connect(putState, putActions)(KenBurns);