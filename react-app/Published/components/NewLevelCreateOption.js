import React from 'react';
import { connect } from 'react-redux';
import { nextStep, ifloorplanStateAdd } from '../redux/actions/actions';
import { updateNewPlan } from '../serverRequests/requests';

const NewLevelCreateOption = (props) => {
    const ifloorplanObj = props.ifloorplanNowState;

    const onePlan = () => {
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        newState.level[props.levelNowId].plan_level.push({
            img: null,
        })
        updateNewPlan(newState)
            .then(data=>{
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                props.nextStep("single plan upload", "new level");
            });
    }

    const morePlans = () => {
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        newState.level[props.levelNowId].plan_level.push({
            img: null,
        })
        updateNewPlan(newState)
            .then(data=>{
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                props.nextStep("plan library", "new level");
            });
    }

    return (
        <div className="wrapNewLevelCreateOption">
            <div className="titleLevelOptionCreate">
                <h2>Creating new level</h2>
            </div>
            <div className="optionLevelCreate">
                <div onClick={morePlans}>Select a floor plan from library</div>
                <div className="text_title"><h2>Or</h2></div>
                <div onClick={onePlan}>Upload a floor plan</div>
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        levelNowId: state.levelNowId,
        ifloorplanNowState: state.ifloorplanNowState
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan))
    }
}

export default connect(putState, putActions)(NewLevelCreateOption);