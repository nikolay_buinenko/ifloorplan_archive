import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import {
    setColorBackground,
    nextStep,
    setColorText,
    set360ToolsAction,
    ifloorplanStateAdd,
    changeCamPosition } from '../redux/actions/actions';
import EditTextBlock from './EditTextBlock.js';
import PanChooseFile from './PanChooseFile';
import PanUploadFile from './PanUploadFile';
import Marker from './Marker';
import PanLibrary from './PanLibrary';
import {updateNewPlan, getSystemIcons } from '../serverRequests/requests';
import Draggable, {DraggableCore} from 'react-draggable';
import Cam from './Cam';
import Client from './Client';

const Panorama = (props) => {
    const ifloorplanObj = props.ifloorplanNowState; //last ifloor plan obj
    const panoramaNow = ifloorplanObj.level[props.levelNowId].panelum[ifloorplanObj.level[props.levelNowId].panelum.length - 1];

    const [toolsAction, setToolsAction] = useState("choose");
    const [activeDrags, setActiveDrags] = useState(0);
    const [tabsStyle, setTabsStyle] = useState(JSON.parse(ifloorplanObj.tabs_style));
    const [tabs, setTabs] = useState([]);
    const [cams, setCams] = useState([]); //array position active cams
    const [videoMarker, setVideoMarker] = useState([]);



    const [imageWrapStyle, setImageWrapStyle] = useState({ //style for image
        position: 'absolute',
        width: ifloorplanObj.level[props.levelNowId].img_width+'%',
        top: ifloorplanObj.level[props.levelNowId].img_pos_y+"%",
        left: ifloorplanObj.level[props.levelNowId].img_pos_x+"%"
    });
    const [planWrapStyle, setPlanWrapStyle] = useState({ //style for plan
        position: 'absolute',
        width: ifloorplanObj.level[props.levelNowId].plan_width+'%',
        top: ifloorplanObj.level[props.levelNowId].plan_pos_y+"%",
        left: ifloorplanObj.level[props.levelNowId].plan_pos_x+"%"
    });
    const [addressWrapStyle, setAdsressWrapStyle] = useState({ //address style
        position: 'absolute',
        color: props.text_color,
        top: ifloorplanObj.addressPosition_y+"%",
        left: ifloorplanObj.addressPosition_x+"%"
    });
    const [clientWrapStyle, setClientWrapStyle] = useState({
        position: "absolute",
        top: ifloorplanObj.client_block_y+"%",
        left: ifloorplanObj.client_block_x+"%",
        width: ifloorplanObj.client_width+"%"
    })
    //===============================================Markers=====================================
    const [markers, setMarkers] = useState([]);  //library markers
    const [doneMarkers, setDoneMarkers]  = useState([]); //array with markers is position done
    const panoramaLenght = ifloorplanObj.level[props.levelNowId].panelum.length; //length panorama array
    const [markerPosBool, setMarkerPosBool] = useState(false); //true - drag marker allowed
    const [markerWidth , setMarkerWidth] = useState(2);
    const [markerImg, setMarkerImg] = useState({});
    const [markerPosX, setMarkerPosX] = useState(0);
    const [markerPosY, setMarkerPosY] = useState(0);
    const [systemIcon, setSystemIcon] = useState([]);

    const startMarkersPosition = (value) => {
        //start drag marker
        console.log(ifloorplanObj);
        setMarkerImg(value);
        setMarkerPosBool(true);
    }

    const markersInit = () => {
        //init markers library
        let markers_ready = [];
        console.log(systemIcon);
        for(let i = 0; i < systemIcon.length; i++) {
            markers_ready.push(<div className="panelum_marker" key={i} onClick={() => {
                startMarkersPosition(systemIcon[i]);
            }}>
                <img src={systemIcon[i].marker} alt=""/>
            </div>)
        }
        setMarkers(markers_ready);
    }

    const myMarkersInit = () => {
        //init user markers library
        setMarkers([]);
        return;
    }

    const handleMarker = (event) => {
        //set position marker
        event.preventDefault();
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        const childBlock = document.getElementsByClassName("markerDrag")[0];

        const parrentBlock = document.getElementsByClassName("workPlace")[0];
        let childX = childBlock.getBoundingClientRect().left;
        let childY = childBlock.getBoundingClientRect().top;
        const { top, left, width, height } = parrentBlock.getBoundingClientRect();

        let x = (childX - left) / width * 100;
        let y = (childY - top) / height * 100;
        //change position properties
        setMarkerPosX(x);
        setMarkerPosY(y);
    }

    const saveMarker = (action) => {
        //save marker in global state json
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        newState.level[props.levelNowId].panelum[panoramaLenght - 1].cam_img = markerImg.marker;
        newState.level[props.levelNowId].panelum[panoramaLenght - 1].cam_img_hover = markerImg.marker_hover;
        newState.level[props.levelNowId].panelum[panoramaLenght - 1].cam_width = markerWidth;
        newState.level[props.levelNowId].panelum[panoramaLenght - 1].pos_x = markerPosX;
        newState.level[props.levelNowId].panelum[panoramaLenght - 1].pos_y = markerPosY;
        if(action === "add"){
            newState.level[props.levelNowId].panelum.push({
                img: "/media/system_images/broken-1.png",
                cam_img: null,
                cam_width: 10,
                pos_x: 0,
                pos_y: 0
            });
        }
        updateNewPlan(newState)
            .then((data)=>{
                //===========back default config marker before saving main json
                setMarkerImg("");
                setMarkerPosBool(false);
                setMarkerWidth(10);
                setMarkerPosY(0);
                setMarkerPosX(0);
                //==============================================================
                props.ifloorplanStateAdd(data[0]);
                if(action === "add"){
                    props.set360ToolsAction("choose");
                } else {
                    props.set360ToolsAction("choose");
                    if(ifloorplanObj.level[props.levelNowId].plan_level[0].img){
                        if(ifloorplanObj.level[props.levelNowId].images.length != 0){
                            props.nextStep("cams position", "_");
                        } else {
                            props.nextStep("upload images", "_");
                        }
                    } else {
                        props.nextStep("upload plans", "_");
                    }
                }
            })
    }

    useEffect(()=>{
        // render markers if their position is set
        const ifloorplan = props.ifloorplanNowState; //last ifloor plan obj
        const resultArr = [];
        for(let i = 0; i < panoramaLenght; i++){
            if(ifloorplan.level[props.levelNowId].panelum[i].pos_x != 0){
                resultArr.push(<Marker index={i}
                                       key={i}
                                       value={ifloorplan.level[props.levelNowId].panelum[i]}/>)
            }
        }
        setDoneMarkers(resultArr);
    }, [props.ifloorplanNowState]);

    //========================================End Markers ======================================

    const tabsInit = () => {
        //tab render
        let tabs_ready = [];
        ifloorplanObj.level.map((value, index) => (
            tabs_ready.push(<div className="tab" key={index}>{value.tabLabel}</div>)
        ));
        setTabs(tabs_ready);
    }

    useEffect(() => {
        //set position frontend blocks
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        setCams([]);
        const camsRender = [];
        for(let i = 0; i < ifloorplanObj.level[props.levelNowId].images.length; i++){
            if(ifloorplanObj.level[props.levelNowId].images[i].cam_pos_x !== 0){
                //init active cams
                camsRender.push(<Cam key={i} value={ifloorplanObj.level[props.levelNowId].images[i]}/>)
            }
        }
        setCams(camsRender);
        setVideoMarker([]);
        for(let i = 0; i < ifloorplanObj.level[props.levelNowId].video_marker.length; i++){
            setVideoMarker(state => [...state, <Marker key={i} index={i} value={ifloorplanObj.level[props.levelNowId].video_marker[i]}/>])
        }
        const resultArr = [];
        for(let i = 0; i < panoramaLenght; i++){
            if(ifloorplanObj.level[props.levelNowId].panelum[i].pos_x != 0){
                resultArr.push(<Marker index={i}
                                       key={i}
                                       value={ifloorplanObj.level[props.levelNowId].panelum[i]}/>)
            }
        }
        setDoneMarkers(resultArr);

        tabsInit(); //init tabs position

        const parrentBlock = document.getElementsByClassName("workPlace")[0];

        parrentBlock.style.color = ifloorplanObj.textColor;
        parrentBlock.style.background = ifloorplanObj.bgColor;

        //init colors
        props.setColorBackground(newState.bgColor);
        props.setColorText(newState.textColor);
        getSystemIcons()
            .then((data)=>{
                const icons = [];
                for(let i = 0; i < data.length; i++) {
                    if (data[i].type == "PANORAMA") {
                        icons.push(data[i]);
                    }
                }
                setSystemIcon(icons);
            })
    }, []);

    useEffect(()=>{
        markersInit();
    }, [props.tools_360_action])

    const onStart = () => {setActiveDrags(activeDrags + 1)};
    const onStop = () => {setActiveDrags(activeDrags - 1)};
    const dragHandlers = {onStart: onStart, onStop: onStop};
    return (
        <div className="workPlace">
            <div className="dnd-wrapper" style={{background:props.background_color}}>
                {!markerPosBool ? '' :
                    <Draggable onDrag={handleMarker} {...dragHandlers}>
                        <div className="markerDrag" style={{width: markerWidth+"%", position:'absolute', top:"50%", left:"50%"}}>
                            <img src={markerImg.marker} style={{width:"100%"}} alt=""/>
                        </div>
                    </Draggable>
                }
                <div className="panorama_tools">
                    { props.tools_360_action === "choose" ? <PanChooseFile/> : ''}
                    { props.tools_360_action === "new upload" ? <PanUploadFile/> : ''}
                    { props.tools_360_action === "library" ? <PanLibrary/> : ''}
                    {/*==================================================================*/}
                    { props.tools_360_action != "markers" ? '' :
                        <div className="markers_panelum">
                            <div className="title_markers">
                                <h3>Drag a marker on to the floor plan</h3>
                            </div>
                            <div className="markers_wrap">
                                <div className="markers_option">
                                    <a href="" onClick={(event)=>{
                                        event.preventDefault();
                                        event.currentTarget.className += "active";
                                        markersInit();
                                    }}>Markers</a>
                                    <a href="" onClick={(event) => {
                                        event.preventDefault();
                                        event.currentTarget.className += "active";
                                        myMarkersInit();
                                    }}>My Markers</a>
                                </div>
                                <div className="markers">
                                    {markers}
                                </div>
                            </div>
                            <div className="markers_keyboard">
                                <div className="scale">
                                    <label htmlFor="scale">Scale</label>
                                    <input type="range" id="scale" name="cowbell"
                                           min="0" max="10" value={markerWidth} step="0.5" onChange={(event) => {
                                        setMarkerWidth(event.target.value);
                                    }}/>
                                </div>
                                <div className="btn_group">
                                    <button className="add" onClick={()=>{
                                        saveMarker("add");
                                    }}>Add another 360</button>
                                    <button className="save" onClick={()=>{
                                        saveMarker("save");
                                    }}>Done</button>
                                </div>
                            </div>
                        </div>}
                    {/*========================================================================*/}
                </div>

                {doneMarkers}
                {cams}
                {videoMarker}
                {ifloorplanObj.client_data == "" ? '' : <div className="client_wrap_main" style={clientWrapStyle}>
                    <Client clientData={JSON.parse(ifloorplanObj.client_data)} clientPosition={JSON.parse(ifloorplanObj.client_position)}/>
                </div>}

                <div className="tabs" style={tabsStyle}>
                    {tabs}
                </div>
                {ifloorplanObj.brand_logo == null ? "" :
                    <div className="brandWrap" style={{position: 'absolute', width: ifloorplanObj.brand_width+'%', opacity: ifloorplanObj.brand_transparency, top:ifloorplanObj.brand_pos_y+"%", left:ifloorplanObj.brand_pos_x+"%"}}>
                        <img style={{width: '100%'}} src={ifloorplanObj.brand_logo} alt=""/>
                    </div>
                }
                <div className="imageWrap" style={imageWrapStyle}>
                    <img className='interior-image'
                         src={panoramaNow.img}
                         style={{width: '100%'}}/>
                </div>
                <div className="planWrap" style={planWrapStyle}>
                    <img className='floor-plan'
                         src={ifloorplanObj.level[props.levelNowId].plan_level[0].img}
                         style={{width: '100%'}}/>
                </div>
                <div className="addressWrap" style={addressWrapStyle}>
                    <EditTextBlock/>
                </div>
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        levelNowId: state.levelNowId,
        ifloorplanNowState: state.ifloorplanNowState,
        text_color: state.text_color,
        background_color: state.background_color,
        set_color: state.set_color,
        tools_360_action: state.tools_360_action
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        setColor: (bool) => dispatch(setColor(bool)),
        setColorBackground: (color) => dispatch(setColorBackground(color)),
        setColorText: (color) => dispatch(setColorText(color)),
        set360ToolsAction: (value) => dispatch(set360ToolsAction(value))
    }
}

export default connect(putState, putActions) (Panorama);