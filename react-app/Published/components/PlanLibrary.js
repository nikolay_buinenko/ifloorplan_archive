import React, {useState} from 'react';
import { connect } from 'react-redux';
import { nextStep, LibPlanAdd, ifloorplanStateAdd, setUpdatePlanImage } from '../redux/actions/actions';
import { deleteSinglePlan } from '../serverRequests/requests';

const PlanLibrary = (props) => {
    const [change, setChange] = useState(true);
    const [chackedId, setChackedId] = useState(null);
    let nowUsePlan = props.ifloorplanNowState.level[props.levelNowId].plan_level[0].img;
    const items = [];
    let button = true;
    console.log(props);
    let checked = "";
    const ifloorplanObj = props.ifloorplanNowState;

    const Select = () => {
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        for(let i = 0; i < props.LibraryPlans.length; i++){
            if(props.LibraryPlans[i].id === chackedId){
                newState.level[props.levelNowId].plan_level[0].img = props.LibraryPlans[i].plan_image;
            }
        }
        props.ifloorplanStateAdd(newState);
        if(props.prev_step === "new level"){
            props.nextStep("upload images", "new level");
        } else {
            props.nextStep("cams position", "_");
        }
    }

    const Cancel = () => {
        if(ifloorplanObj.level[props.levelNowId].images.length === 0){
            props.nextStep("upload images", "_");
        } else {
            props.nextStep("cams position", "_");
        }
    }

    const Chacked = (id) => {
        setChange(false);
        setChackedId(id);
    }

    const Upload = () => {
        props.nextStep("single plan upload", "_");
    }

    const deletePlan = (id) => {
        let form_data = {
            ifloorplan_id: ifloorplanObj.id,
            level_id: ifloorplanObj.level[props.levelNowId].id,
            id: id,
        }
        deleteSinglePlan(form_data)
            .then(data=>{
                props.LibPlanAdd(data);
            })
    }

    for (let i = 0; i <= 11; i++){
        if(props.LibraryPlans[i] == undefined){
            if (button) {
                items.push(
                    <div className="planItem btnAdd" key={i}>
                        <div className="image">
                            <button onClick={Upload}>Upload a floor plan</button>
                        </div>
                    </div>
                )
                button = false;
            } else {
                items.push(<div className="planItem" key={i}><div className="image"></div></div>)
            }
        } else {
            if((nowUsePlan === props.LibraryPlans[i].plan_image) && change){
                checked = <div className="checked"><i className="fa fa-check" aria-hidden="true"></i></div>;
            }
            if(!change && props.LibraryPlans[i].id === chackedId ){
                checked = <div className="checked"><i className="fa fa-check" aria-hidden="true"></i></div>;
            }
            items.push(<div className="planItem" onClick={()=>Chacked(props.LibraryPlans[i].id)} key={i}>{checked}<div className="image"><img src={props.LibraryPlans[i].plan_image} alt=""/></div>
                <div className="options">
                    <div><i className="fa fa-plus" aria-hidden="true" onClick={()=>{props.setUpdatePlanImage(props.LibraryPlans[i].id); props.nextStep("single plan update image", "_")}}></i></div>
                    <i className="fa fa-minus" aria-hidden="true" onClick={()=>deletePlan(props.LibraryPlans[i].id)}></i></div></div>)
            checked = "";
        }
    }

    return (
        <div className="planLibrary">
            <div className="titleBlock">
                <div className='planLibraryPanel'>
                    <h3>Floor plan Library</h3>
                    <div className="btnGroup">
                        <button onClick={Cancel}>Cancel</button>
                        <button onClick={Select}>Use selected Floor plan</button>
                    </div>
                </div>
                <p>Click a floor plan to select</p>
            </div>
            <div className="plansLib">
                {items}
            </div>

        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        LibraryPlans: state.LibraryPlans,
        ifloorplanNowState: state.ifloorplanNowState,
        prev_step : state.prev_step,
        levelNowId: state.levelNowId
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        LibPlanAdd: (array) => dispatch(LibPlanAdd(array)),
        setUpdatePlanImage: (id) => dispatch(setUpdatePlanImage(id))
    }
}

export default connect(putState, putActions) (PlanLibrary);