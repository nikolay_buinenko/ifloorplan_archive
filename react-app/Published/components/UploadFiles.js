import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { CSRFTOKEN } from '../redux/constant/constants';
import { uploadFilesToServer, updateNewPlan, updateSingleImage } from '../serverRequests/requests';
import { nextStep, LibImageAdd, LibPlanAdd, ifloorplanStateAdd, panelumLib } from '../redux/actions/actions';


const UploadFiles = (props) => {
    const [status, setStatus] = useState(0);
    const [filesStatus, setFilesStatus] = useState([]);
    const { ifloorplanNowState,
        levelNowId,
        ifloorplanStateAdd,
        nextStep,
        LibImageAdd,
        LibPlanAdd
    } = props;

    const initFiles = (files) => {
        const w = [];
        for (let i = 0; i < files.length; i++){
            w.push({ id: i, name: files[i].name, selected: true});
        }
        setFilesStatus(w);
    }


    const sendFileToServer = (files) => {

        const config = {
            onUploadProgress: (progressEvent) => {
                const {loaded, total} = progressEvent;
                let percents = Math.floor((loaded * 100) / total );
                setStatus(percents);
            },
            headers: {
                "X-CSRFToken": CSRFTOKEN,
                'content-type': 'multipart/form-data'
            }
        }

        if (props.dropFilesTitle === "360 lib") { //upload 360 images to library
            let form_data = new FormData();
            form_data.append('ifloorplan_id', ifloorplanNowState.id);
            form_data.append('many', true);
            for (var x = 0; x < files.length; x += 1){
                form_data.append('image_'+x, files[x], files[x].name);
            }
            uploadFilesToServer("https://ifloorplan360.com/api/panelum/", form_data, config)
                .then(res => {
                    props.panelumLib(res.data);
                    if(props.prev_step === "template"){
                        nextStep("cams position", "template");
                        return 1;
                    }
                    nextStep("360 library", "_");
                })
        }
        if (props.dropFilesTitle === "single 360") { //upload single file to 360  library
            let form_data = new FormData();
            form_data.append('ifloorplan_id', ifloorplanNowState.id);
            form_data.append('image', files[0], files[0].name);
            uploadFilesToServer("https://ifloorplan360.com/api/panelum/", form_data, config)
                .then(res => {
                    props.panelumLib(res.data);
                    nextStep("360 library", "_");
                })
        }
        if (props.dropFilesTitle === "PLANS") { //upload to plan library
            let form_data = new FormData();
            form_data.append('ifloorplan_id', ifloorplanNowState.id);
            for (var x = 0; x < files.length; x += 1){
                form_data.append('plan_image_'+x, files[x], files[x].name);
            }
            uploadFilesToServer("https://ifloorplan360.com/api/imageplanuload/", form_data, config)
                .then(res => {
                    console.log(res)
                    let newState = JSON.parse(JSON.stringify(ifloorplanNowState));
                    newState.level[props.levelNowId].plan_level[0].img = res.data[props.levelNowId].plan_image; //set first plan to ifloorplan
                    ifloorplanStateAdd(newState); //update state ifloorplan
                    LibPlanAdd(res.data);  //add to library
                    if(props.prev_step === "template"){
                        nextStep("upload images", "template");
                    } else {
                        nextStep("upload images", "_");
                    }

                })
        }
        if (props.dropFilesTitle === "IMAGES") {  //upload to image library
            let form_data = new FormData();
            form_data.append('ifloorplan_id', ifloorplanNowState.id);
            form_data.append('level_id', ifloorplanNowState.level[props.levelNowId].id);
            for (var x = 0; x < files.length; x += 1){
                form_data.append('image_'+x, files[x], files[x].name);
            }
            uploadFilesToServer("https://ifloorplan360.com/api/imageupload/", form_data, config)
                .then(res => {
                    LibImageAdd(res.data);  //add to library
                    if(props.prev_step === "new level"){
                        props.nextStep("image library", props.prev_step);
                        return 1;
                    }
                    if(props.prev_step === "exist images"){
                        props.nextStep("image library", props.prev_step);
                        return 1;
                    }
                    if(props.prev_step === 'library'){
                        props.nextStep("image library", "_");
                    } else {
                        if(props.prev_step === "template"){
                            props.nextStep("360 library upload", "template");
                        } else {
                            props.nextStep("360 library upload", "_");
                        }
                    }
                })
        }
        if (props.dropFilesTitle === "PLAN") {
            let form_data = new FormData();
            form_data.append('ifloorplan_id', ifloorplanNowState.id);
            for (var x = 0; x < files.length; x += 1){
                form_data.append('plan_image_'+x, files[x], files[x].name);
            }
            uploadFilesToServer("https://ifloorplan360.com/api/imageplanuload/", form_data, config)
                .then(res => {
                    LibPlanAdd(res.data);
                    nextStep("plan library", "_");
                })
        }
        if (props.dropFilesTitle === "IMAGE") {  //upload to image library
            let form_data = new FormData();
            form_data.append('ifloorplan_id', ifloorplanNowState.id);
            form_data.append('level_id', ifloorplanNowState.level[props.levelNowId].id);
            for (var x = 0; x < files.length; x += 1){
                form_data.append('image_'+x, files[x], files[x].name);
            }
            uploadFilesToServer("https://ifloorplan360.com/api/imageupload/", form_data, config)
                .then(res => {
                    LibImageAdd(res.data);  //add to library
                    props.nextStep("image library", "_");
                })
        }
        if (props.dropFilesTitle === "IMAGE SINGLE UPLOAD") {  //upload to image library
            let form_data = new FormData();
            form_data.append('ifloorplan_id', ifloorplanNowState.id);
            form_data.append('id', props.update_image_id);
            form_data.append('image', files[0], files[0].name);
            updateSingleImage("https://ifloorplan360.com/api/imageupload/", form_data, config)
                .then(res => {
                    LibImageAdd(res.data);  //add to library
                    props.nextStep("image library", "_");
                })
        }
        if (props.dropFilesTitle === "UPDATE SINGLE PLAN") {  //upload to image library
            let form_data = new FormData();
            form_data.append('ifloorplan_id', ifloorplanNowState.id);
            form_data.append('id', props.update_plan_image);
            form_data.append('plan', files[0], files[0].name);
            form_data.append('level_id', ifloorplanNowState.level[props.levelNowId].id);
            updateSingleImage("https://ifloorplan360.com/api/imageplanuload/", form_data, config)
                .then(res => {
                    LibPlanAdd(res.data);  //add to library
                    props.nextStep("plan library", "_");
                })
        }
    }

    useEffect(() => {
        initFiles(props.dropFiles);
        sendFileToServer(props.dropFiles);
    }, []);

    let title = "";
    switch (props.dropFilesTitle) {

        case "PLANS":
            title = "Floor plans files are uploading...";
            break;

        case "IMAGES":
            title = "Images files are uploading...";
            break;

        default:
            title = "";
            break;
    }

    return (
        <div className="status_upload">
            <div className="titleUpload">{title}</div>
            <div className="items">
                {
                    filesStatus.map((value, index) => {
                        return <div key={index} className="item">
                            <p>{value.name}</p>
                            <div className="progress"><div className="status" style={{width:status+'%'}}></div></div>
                        </div>
                    })
                }
            </div>
        </div>
    );
}

const putState = (state) => {
    return {
        step: state.step,
        dropFiles: state.dropFiles,
        dropFilesTitle: state.dropFilesTitle,
        levelNowId: state.levelNowId,
        ifloorplanNowState: state.ifloorplanNowState,
        prev_step: state.prev_step,
        update_image_id: state.update_image_id,
        update_plan_image: state.update_plan_image
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        LibPlanAdd: (array) => dispatch(LibPlanAdd(array)),
        LibImageAdd: (array) => dispatch(LibImageAdd(array)),
        panelumLib: (items) => dispatch(panelumLib(items)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan))
    }
}


export default connect(putState, putActions) (UploadFiles);