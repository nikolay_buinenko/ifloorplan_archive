import React, {useState} from 'react';
import Draggable, {DraggableCore} from 'react-draggable';

const KenBurnsEditor = (props) => {
    const [activeDrags, setActiveDrags] = useState(0);


    const handleStart = (event, ui) => {
        return null;
    }

    const handleFinish = (event, ui) => {
        return null
    }

    const onStart = () => {setActiveDrags(activeDrags + 1)};
    const onStop = () => {setActiveDrags(activeDrags - 1)};
    const dragHandlers = {onStart: onStart, onStop: onStop};
    return(
        <div className="editorBurns">
            <Draggable bounds="parent" onDrag={handleStart} {...dragHandlers}>
                <div className="start">
                    <p>start</p>
                </div>
            </Draggable>
            <Draggable bounds="parent" onDrag={handleFinish} {...dragHandlers}>
                <div className="finish">
                    <p>finish</p>
                </div>
            </Draggable>
            <img src={props.image} alt=""/>
        </div>
    )
}
export default KenBurnsEditor;