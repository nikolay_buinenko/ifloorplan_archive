import React, {useState} from 'react';
import { connect } from 'react-redux';
import { nextStep, ifloorplanStateAdd } from '../redux/actions/actions';
import { updateNewPlan } from '../serverRequests/requests';

const Media = (props) => {
    const [media, setMedia] = useState("");
    const [label, setLabel] = useState("");
    const ifloorplanObj = props.ifloorplanNowState;

    const Save = () => {
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        if(media != "" && label != ""){
            let type = "";
            try {
                const url = new URL(media);
                if (url.hostname == "youtube.com" || url.hostname == "www.youtube.com" ){
                    type = "video";
                } else {
                    type = "walk";
                }
            } catch (e) {
                return 1;
            }

            const new_media = {
                index: newState.media.length,
                type: type,
                url: media,
                label: label
            }
            newState.media.push(new_media);
            updateNewPlan(newState)
                .then((data)=>{
                    props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                    props.nextStep("cams position", "_");
                })
        }
    }

    return(
        <div className="video">
            <div className="video_title">
                <div className="first_title"><h3>Add external media such as a video or 360 walk through</h3></div>
                <div className="second_title"><h3>As a navigation tab - Media plays at full window width</h3></div>
            </div>
            <div className="video_inputs">
                <div className="inputs">
                    <label htmlFor="video">URL for the video or 360 walk through (Matterport etc)</label>
                    <input type="text" onChange={event => {
                        setMedia(event.target.value);
                    }} id="video" placeholder="https://youtu.be"/>
                </div>
                <div className="inputs">
                    <label htmlFor="label">Label for a navigation tab</label>
                    <input type="text" id="label" onChange={event => {
                        setLabel(event.target.value);
                    }} placeholder="Property video"/>
                </div>
                <div className="button_group">
                    <button className="cancel" onClick={()=>props.nextStep("cams position", "_")}>Cancel</button>
                    <button className="save" onClick={Save}>Save</button>
                </div>
            </div>
            <div className="video_comment">
                <h2>
                    As a symbol - Video plays smaller with floor plan still visible.
                    Viewer has option to expand to full window width
                </h2>
            </div>
        </div>
    )
}



const putState = (state) => {
    return {
        ifloorplanNowState: state.ifloorplanNowState,
    }
}
const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
    }
}
export default connect(putState, putActions)(Media);