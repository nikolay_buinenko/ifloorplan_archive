import React from 'react';
import { connect } from 'react-redux';
import { nextStep, dropFiles } from '../redux/actions/actions';
import Dropzone from "react-dropzone";

const SingleUploadPlan = (props) => {

    const handleDrop = (files) => {
        props.dropFiles(files, "PLAN");
        props.nextStep("upload files", "_");
    }
    const Cancel = () => {
        props.nextStep("plan library", "_")
    }

    return (
        <div className="uploadPlans">
            <Dropzone onDrop={handleDrop}>
                {({ getRootProps, getInputProps }) => (
                    <div {...getRootProps({ className: "dropzone" })}>
                        <h3>Drag & Drop your floor plan for this level here.</h3><br/>
                        <h3>Or</h3><br/>
                        <input {...getInputProps()} />
                        <button>Choose file</button><br/>
                        <h3>Single file upload</h3>
                    </div>

                )}
            </Dropzone>
            <div className="cancelBtnBlue"><button onClick={Cancel}>Cancel</button></div>
        </div>
    );
}

const putState = (state) => {
    return {
        step: state.step
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        dropFiles: (files, title) => dispatch(dropFiles(files, title))
    }
}

export default connect(putState, putActions) (SingleUploadPlan);