import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import {updateNewPlan} from '../serverRequests/requests';
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import Popup from "reactjs-popup";
import arrayMove from 'array-move';
import {
    nextStep,
    changeLevel,
    ifloorplanStateAdd,} from '../redux/actions/actions';

const ManageLevels = (props) => {
    const ifloorplanObj = props.ifloorplanNowState;
    const [renameIndex, setRenameIndex] = useState(0);
    const [deleteIndex, setDeleteIndex] = useState(0);
    const [deletePopup, setDeletePopup] = useState(false);
    const [renamePopup, setRenamePopup] = useState(false);
    const [levelHasDeleted, setLevelHasDeleted] = useState(false);
    const [tabsStyle, setTabsStyle] = useState(JSON.parse(ifloorplanObj.tabs_style));
    const [levels, setLevels] = useState({
        items: ifloorplanObj.level,
    });
    const [tabs, setTabs] = useState([]);
    const [newNameLevel, setNewNameLevel] = useState("");
    const [popupError, setPopupError] = useState(false);

    const Top = () => {
        setTabsStyle({
            top: "0%",
            left: "50%",
            transform: "rotate(0deg)",
        })
    }
    const Right = () => {
        setTabsStyle({
            right: "0%",
            transformOrigin: "right bottom",
            transform: "rotate(-90deg)",
        })
    }
    const Left = () => {
        setTabsStyle({
            left: "0%",
            transformOrigin: "left bottom",
            transform: "rotate(90deg)",
        })
    }
    const Bottom = () => {
        setTabsStyle({
            left: "50%",
            bottom: "0%",
            transform: "rotate(0deg)",
        })
    }

    const Rename = () => {
        setRenamePopup(false);
        let newArr = JSON.parse(JSON.stringify(levels));
        for(let i = 0; i < levels.items.length; i++){
            if(newArr.items[i].index == renameIndex){
                newArr.items[i].tabLabel = newNameLevel;
            }
        }
        setLevels(newArr);
    }

    const Delete = () => {
        setDeletePopup(false);
        if(levels.items.length == 1){
            setPopupError(true);
            return
        }
        let newArr = JSON.parse(JSON.stringify(levels));
        for(let i = 0; i < newArr.items.length; i++){
            if(newArr.items[i].index == deleteIndex){
                newArr.items.splice(i, 1);
            }
        }
        for(let i = 0; i < newArr.items.length; i++){
            newArr.items[i].index = i; //change index level for position
        }
        setLevelHasDeleted(true);
        setDeleteIndex(0);
        setLevels(newArr);
    }

    const SortableItem = SortableElement(({value}) => <div className="level active">
        <div className="tlt">{value.tabLabel}</div>
        <div className="cntr_btn">
            <button onClick={()=>{
                setDeletePopup(false);
                setRenamePopup(true);
                setRenameIndex(value.index);
            }}>Rename</button>
            <button onClick={()=>{
                setRenamePopup(false);
                setDeletePopup(true);
                setDeleteIndex(value.index);
            }}>Delete</button>
            <div className="plm_icon">:::</div>
        </div>
    </div>);

    const SortableList = SortableContainer(({items}) => {
        return (
            <div className="wrap_levels">
                {items.map((value, index) => (
                    <SortableItem key={`item-${value.id}`} index={index} value={value} />
                ))}
            </div>
        );
    });

    const tabsInit = () => {
        let tabs_ready = [];
        levels.items.map((value, index) => (
            tabs_ready.push(<div className="tab" key={index}>{value.tabLabel}</div>)
        ));
        setTabs(tabs_ready);
    }
    useEffect(()=>{
        tabsInit();
    }, []);

    useEffect(()=>{
        tabsInit();
    }, [levels]);

    const Save = () => {
        let newArr = JSON.parse(JSON.stringify(levels));
        let Style_tab = JSON.stringify(tabsStyle);
        console.log(Style_tab);
        for(let i = 0; i < newArr.items.length; i++){
            newArr.items[i].index = i; //change index level for position
        }
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        newState.level = newArr.items;
        newState.tabs_style = Style_tab;
        updateNewPlan(newState)
            .then(data=>{
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                if(levelHasDeleted){
                    props.changeLevel(levels.items.length - 1);
                }
                props.nextStep("cams position", "_");
            })
    }

    const onSortEnd = ({oldIndex, newIndex}) => {
        setLevels(({items}) => ({
            items: arrayMove(items, oldIndex, newIndex),
        }));
    }

    return(
        <div className="manage_levels">
            <div className="tabs" style={tabsStyle}>
                {tabs}
                <div className="tab">Video</div>
            </div>
            <div className="manage_title">
                <div className="desc_tabs">Re-arrenge Tabs</div>
            </div>
            <div className="manage_body">
                <div className="levels_manage">
                    <div className="title_manage_levels">
                        <h3>Manage Levels and Tabs</h3>
                        <h4>Arrange Tabs and order of Levels</h4>
                    </div>
                    <div className="description_manage">
                        <p>Drag blocks up or down to arrange the order of appereance when images are in slideshow mode</p>
                    </div>
                    <SortableList items={levels.items} onSortEnd={onSortEnd} />
                </div>
                <div className="manage_control_panel">
                    <div className="control_buttons">
                        <button>Position tab strip</button>
                        <button onClick={Top}>Tabs at Top</button>
                        <button onClick={Right}>Tabs at Right</button>
                        <button onClick={Bottom}>Tabs at Bottom</button>
                        <button onClick={Left}>Tabs at Left</button>
                    </div>
                </div>
            </div>
            <div className="button_group">
                <button className="save" onClick={Save}>Done</button>
            </div>

            <Popup
                open={popupError}
                closeOnDocumentClick
                onClose={()=> {
                    setPopupError(false);
                }}
            >
                <div className="modal">
                    <h3>Error! You have only one level. You not can delete this level!</h3>
                    <div className="button_group">
                        <button className="save" onClick={()=>{
                            setPopupError(false);
                        }}>OK</button>
                    </div>

                </div>
            </Popup>

            <Popup
                open={deletePopup}
                closeOnDocumentClick
                onClose={()=> {
                    setDeletePopup(false);
                }}
            >
                <div className="modal">
                    <h3>Are you sure you want to delete {levels.items[deleteIndex].tabLabel}</h3>
                    <h3>This cannot be undone!</h3>
                    <div className="button_group">
                        <button className="cancel" onClick={()=>{
                            setDeletePopup(false);
                        }}>Cancel</button>
                        <button className="save" onClick={Delete}>Delete</button>
                    </div>

                </div>
            </Popup>


            <Popup
                open={renamePopup}
                closeOnDocumentClick
                onClose={()=> {
                    setRenamePopup(false);
                }}
            >
                <div className="modal">
                    <h3>Rename this level</h3>
                    <div className="inputs_rename">
                        <label htmlFor="rename">Give this level a new name</label>
                        <input type="text" id="rename" onChange={(event)=>{
                            setNewNameLevel(event.target.value);
                        }} placeholder="New name for this level"/>
                    </div>
                    <div className="button_group">
                        <button className="cancel" onClick={()=>{
                            setRenamePopup(false);
                            setNewNameLevel("");
                        }}>Cancel</button>
                        <button className="save" onClick={Rename}>Done</button>
                    </div>

                </div>
            </Popup>
        </div>
    )
}

const putState = (state) => {
    return {
        ifloorplanNowState: state.ifloorplanNowState,
        levelNowId: state.levelNowId
    }
}
const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        changeLevel: (level) => dispatch(changeLevel(level)),
    }
}
export default connect(putState, putActions)(ManageLevels);