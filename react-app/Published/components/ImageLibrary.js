import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import Popup from "reactjs-popup";
import {deleteSimpleImage, deleteMultiImage, updateNewPlan} from '../serverRequests/requests';
import { nextStep, LibImageAdd, ifloorplanStateAdd, clearIfloorState, updateSingleImageAction, setImageLibSort, changeImageKey, changeLevel,} from '../redux/actions/actions';
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import arrayMove from 'array-move';


const ImageLibrary = (props) => {
    const ifloorplanObj = props.ifloorplanNowState;
    const [items, setItems] = useState([]);
    const [newState, setNewState] = useState(JSON.parse(JSON.stringify(ifloorplanObj)));
    const [replacePopup, setReplacePopup] = useState(false);
    const [coverPopup, setCoverPopup] = useState(false);
    const [coverPopupError1, setCoverPopupError1] = useState(false);
    const [coverPopupError2, setCoverPopupError2] = useState(false);

    const [deletePopup, setDeletePopup] = useState(false);
    const [deleteIdImage, setDeleteIdImage] = useState();

    const [libImage, setLibImage] = useState([]);
    const [deleteArr, setDeleteArr] = useState([]);
    const [coverImg, setCoverImg] = useState("");

    const [updateImageId, setUpdateImageId] = useState(null);
    const [updateImagePopup, setUpdateImagePopup] = useState(false);

    const [imagesState, setImagesState] = useState(ifloorplanObj.level[props.levelNowId].images);

    useEffect(()=>{
        initSort();
    }, [props.LibraryImage])


    const initSort = () => {
        //sort images by plan
        setLibImage([]);
        if(props.prev_step == "first"){
            setLibImage(props.LibraryImage);
            return 1;
        }
        const preparateDataImages = [];
        for(let i = 0; i < ifloorplanObj.level.length; i++){
            for(let k = 0; k < ifloorplanObj.level[i].images.length; k++) {
                preparateDataImages.push(ifloorplanObj.level[i].images[k])
            }
        }
        preparateDataImages.sort((a, b)=>{
            if(a.index < b.index) return -1;
            if(a.index > b.index) return 1;
            return 0;
        });
        const newImageSort = [];
        for(let i = 0; i < preparateDataImages.length; i++){
            let result = props.LibraryImage.find((item, index)=>{
                if(preparateDataImages[i].img_id === item.id){
                    return true;
                }
            });
            if(typeof result != "undefined"){
                result.selected = false
                newImageSort.push(result);
            }
        }
        for(let i = 0; i < props.LibraryImage.length; i++){
            let found = false;
            let result = preparateDataImages.find((item, index)=>{
                if(item.img_id === props.LibraryImage[i].id){
                    found = true;
                }
            });
            if(!found){
                newImageSort.push(props.LibraryImage[i]);
            }
        }
        setLibImage(newImageSort);
    }


    const addImage = () => {
        props.nextStep("simple upload image", "_");
    }

    const deselectAll = () => {
        const lib = [...libImage];
        for(let i = 0; i < lib.length; i++){
            lib[i].selected = false;
        }
        setLibImage(lib);
    }

    const Cover = () => {
        //method for change first image
        const coverImg = [];
        const lib = [...libImage];
        const coverobj = {...newState};
        for(let i = 0; i < lib.length; i++){ //searched selected image and push to array
            if(lib[i].selected){
                coverImg.push(lib[i]);
            }
        }
        if(coverImg.length === 1){ //valid array if lenght == 1 -> set image
            coverobj.splash_screen = coverImg[0].image;
            setCoverImg(coverImg[0].image);
            setNewState(coverobj);
            setCoverPopup(false);
        } else if(coverImg.length >= 1){ //if lenght > 1 error
            setCoverPopupError1(true);
        } else {                         // if lenght = 0 error
            setCoverPopupError2(true);
        }
    }

    const deleteAll = () => {
        props.changeImageKey(0);
        //delete all images and delete all cam position releted him
        let form_data = {
            ifloorplan_id: ifloorplanObj.id,
            level_id: ifloorplanObj.level[props.levelNowId].id,
            multi_delete: [],
        }
        const lib = [...libImage];
        for(let i = 0; i < lib.length; i++){ //mapping images to delete
            lib[i].deleted = true;
        }
        for(let i = 0; i < lib.length; i++){
            if(lib[i].deleted){
                form_data.multi_delete.push(lib[i].id);
            }
        }
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        for(let i = 0; i < newState.level.length; i++){ //delete all images obj
            newState.level[i].images = [];
        }
        updateNewPlan(newState)
            //update ifloor plan object mai state
            .then(data=>{
                props.changeLevel(0);
                props.changeImageKey(0);
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
            });
        deleteMultiImage(form_data)
            //delete all images and upload new images
            .then(res=> {
                props.nextStep("upload images", 'library');
            });
    }

    const useSelectedImages = () => {
        const chekedImages = [];
        for(let i = 0; i < libImage.length; i++){ //find selected image and add to array
            if(libImage[i].selected){
                chekedImages.push(libImage[i]);
            }
        }
        // -------------------------------------------------------------------------------------
        //algorithm for sort images in library with support deleted, change index and create new objects
        const oldImagesState = JSON.parse(JSON.stringify(ifloorplanObj.level[props.levelNowId].images));
        const newImagesState = [];
        for(let i = 0; i < chekedImages.length; i++){
            let founded = false;
            for(let j = 0; j < oldImagesState.length; j++){
                if(chekedImages[i].id == oldImagesState[j].img_id){
                    oldImagesState[j].img = chekedImages[i].image;
                    oldImagesState[j].img_id = chekedImages[i].id;
                    // newImagesState.push(oldImagesState[j]);
                    founded = true;
                }
            }
            if(!founded){ //this part create new objects if they exist
                const newImageObject = {
                    img: chekedImages[i].image,
                    img_id: chekedImages[i].id,
                    index: i
                }
                oldImagesState.push(newImageObject);
            }
            //if array chekedImages not have items which are in array oldImagesState then they delete
        }
        //------------------------------------------end-------------------
        return oldImagesState;
    }

    const Chacked = (id) => {
        //maping images like "selected"
        const lib = [...libImage];
        for(let i = 0; i < lib.length; i++){
            if(lib[i].id == id){
                lib[i].selected ? lib[i].selected = false : lib[i].selected = true;
                setLibImage(lib);
                break;
            }
        }
    }

    const updateImages = (dataArray) => {
        props.LibImageAdd(dataArray);  //add to library
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        let oldImages = newState.level[props.levelNowId].images;
        let newStateImages = [];
        for(let i = 0; i < dataArray.length; i++){
            for(let j  = 0; j < oldImages.length; j++){
                if(dataArray[i].id === oldImages[j].img_id){
                    oldImages[j].img = dataArray[i].image;
                    newStateImages.push(oldImages[j]);
                }
            }
        }
        newState.images = newStateImages;
    }

    const Save = () => {
        const newImageState = useSelectedImages();
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        newState.level[props.levelNowId].images = newImageState;
        let new_index = 0;
        for(let i = 0; i < libImage.length; i++){
            for(let j = 0; j < newState.level.length; j++){
                let result = newState.level[j].images.find((item, index) => {
                    if(item.img_id === libImage[i].id){
                        return true;
                    }
                });
                if(typeof result != "undefined"){
                    result.index = new_index;
                    result.img = libImage[i].image;
                    new_index++;
                }
            }
        }
        updateNewPlan(newState)
            //update ifloor plan object main state
            .then(data => {
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                if(data[0].level[props.levelNowId].images.length == 0){
                    props.changeImageKey(0);
                    props.nextStep("upload images", "_");
                    return 1;
                }
                props.changeImageKey(0);
                // props.changeImageKey(countImage - 1);
                if(props.prev_step === "new level"){
                    props.nextStep("scale layout", "new level");
                    return 1;
                }
                props.nextStep("cams position", "_");
            });
    }

    const handleSingleUploadImage = () => {
        props.updateSingleImageAction(updateImageId);
        props.nextStep("simple upload image", "update single image");
    }

    const simpleDelete = () => {
        //delete one image and delete cam position releted him
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        for(let i = 0; i < newState.level.length; i++){
            for(let j = 0; j < newState.level[i].images.length; j++){
                if(newState.level[i].images[j].img_id === deleteIdImage){
                    newState.level[i].images.splice(j, 1);
                }
            }
        }
        const lib = [...libImage];
        const data = {
            ifloorplan_id: ifloorplanObj.id,
            multi_delete: [deleteIdImage],
        }
        deleteMultiImage(data)
            .then((data)=>{
                console.log(data);
                props.LibImageAdd(data);
                setDeletePopup(false);
                setDeleteIdImage('');
                updateNewPlan(newState)
                    .then((data)=>{
                        props.ifloorplanStateAdd(data[0]);
                    })
            })
    }
    //===================================================================
    const SortableItem = SortableElement(({value}) => {
            let in_this_level = false;
            for(let i = 0; i < ifloorplanObj.level[props.levelNowId].images.length; i++){
                if(ifloorplanObj.level[props.levelNowId].images[i].img_id == value.id){
                    in_this_level = true;
                }
            }

            return <div className="imageItem_sub">
                { value.selected ? <div className="checked"><i className="fa fa-check" aria-hidden="true"></i></div> : ''}
                <div className="image"  onClick={()=>{Chacked(value.id)}}>
                    <img src={value.image} alt=""/>
                </div>
                {in_this_level ? <div className="now_level"><p>In use on this level</p></div> : ""}
                <div className="options">
                    <i className="fa fa-plus" onClick={()=>{setUpdateImageId(value.id);setUpdateImagePopup(true)}} aria-hidden="true"></i>
                    <i className="fa fa-minus" onClick={()=>{setDeleteIdImage(value.id);setDeletePopup(true);}} aria-hidden="true"></i>
                </div>
            </div>
        }
    );

    const SortableList = SortableContainer(({items}) => {
        return (
            <div className="plansLib">

                {items.map((value, index) => (
                    <SortableItem key={`item-${value.id}`} index={index} value={value} />
                ))}
            </div>
        );
    });
    //=============================================================================



    useEffect(()=>{
        // start after render component

        const items_img = document.getElementsByClassName('image'); // disable standart drag callback
        for(let i = 0; i < items_img.length; i++){
            items_img[i].ondragstart = function() {
                return false;
            };
        }
    }, []);

    useEffect(()=>{
        props.setImageLibSort(libImage);
    }, [libImage])


    const onSortEnd = ({oldIndex, newIndex}) => {
        setLibImage(arrayMove(libImage, oldIndex, newIndex));
    }

    return (
        <div className="planLibrary">
            <div className="btnGroup">
                <div className="btn_lib_left">
                    <div><h2>Image Library</h2></div>
                    <div>
                        <div className="addImageBlock">
                            <button onClick={addImage}>Add Image <i className="fa fa-plus" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
                <div className="btn_lib_right">
                    <div className="btn_link_lib">
                        <div>
                            <button onClick={()=>setReplacePopup(true)}>Replace all images</button>
                        </div>
                        <div><button onClick={()=>setCoverPopup(true)}>Use selected for Cover</button></div>
                        <div>
                            <button onClick={()=>{
                                if(props.prev_step == "exist images"){
                                    return 1;
                                }
                                props.nextStep("cams position", "_")
                            }}>Cancel</button>
                        </div>
                    </div>
                    <div className="btn_orange_lib">
                        <div>
                            <button onClick={useSelectedImages}>Use selected images</button>
                        </div>
                        <div>
                            <button onClick={Save}>Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="titleBlock">
                <div className="info">
                    <p>Re-arrange the order</p>
                    <p>Replace Image</p>
                    <p>Add Image</p>
                    <p>Remove Image</p>
                    <p>Replace all images</p>
                    <p>Choose Cover image</p>
                </div>
                <div className="midBlockTitle">
                    <div><h2>Click an image to select</h2></div>
                </div>

            </div>
            <SortableList axis={"xy"} distance={5} items={libImage} onSortEnd={onSortEnd} />

            <Popup
                open={deletePopup}
                closeOnDocumentClick
                onClose={()=> {
                    setDeletePopup(false);
                }}
            >
                <div className="modal">
                    <p className="title_modal">Remove this image?</p>
                    <p className="description_modal">Image and Marker will be deleted</p>
                    <div className="btnGroup">
                        <button onClick={()=>setDeletePopup(false)}>Cancel</button>
                        <button onClick={simpleDelete}>OK</button>
                    </div>
                </div>
            </Popup>


            <Popup
                open={coverPopupError2}
                closeOnDocumentClick
                onClose={()=> {
                    setCoverPopupError2(false);
                    setCoverPopup(false);
                }}
            >
                <div className="modal">
                    <p className="title_modal">You must select one image</p><br/>

                </div>
            </Popup>

            <Popup
                open={coverPopupError1}
                closeOnDocumentClick
                onClose={()=> setCoverPopupError1(false)}
            >
                <div className="modal">
                    <p className="title_modal">Deselect all images and select 1 image for the splash screen.</p><br/>
                    <div className="btnGroup">
                        <button onClick={()=> {
                            setCoverPopupError1(false);
                            seCoverPopupError2(false);
                            setCoverPopup(false);
                        }}>Cancel</button>
                        <button onClick={()=> {
                            setCoverPopupError1(false);
                            setCoverPopupError2(false);
                            setCoverPopup(false);
                            deselectAll();
                        }
                        }>Deselect all</button>
                    </div>
                </div>
            </Popup>

            <Popup
                open={updateImagePopup}
                closeOnDocumentClick
                onClose={()=> setUpdateImagePopup(false)}
            >
                <div className="modal">
                    <h2 className="title_modal">Update this image?</h2>
                    <p className="description_modal">The Marker for this image will remain in position and will use the updated image.</p>
                    <div className="btnGroup">
                        <button onClick={()=> setUpdateImagePopup(false)}>Cancel</button>
                        <button onClick={()=> handleSingleUploadImage()}>OK</button>
                    </div>
                </div>
            </Popup>


            <Popup
                open={replacePopup}
                closeOnDocumentClick
                onClose={()=> setReplacePopup(false)}
            >
                <div className="modal">
                    <p className="title_modal">Are you sure you want to delete all images?</p><br/>
                    <p className="description_modal">This will delete all images and take you to the image uploader where you can upload new images.</p>
                    <p className="description_modal">You will have to place each marker again.</p>
                    <div className="btnGroup">
                        <button onClick={()=> setReplacePopup(false)}>Cancel</button>
                        <button onClick={()=> deleteAll()}>Delete all and re-upload</button>
                    </div>
                </div>
            </Popup>


            <Popup
                open={coverPopup}
                closeOnDocumentClick
                onClose={()=> setCoverPopup(false)}
            >
                <div className="modal">
                    <p className="title_modal">Use selected image for the splash screen?</p><br/>
                    <div className="btnGroup">
                        <button onClick={()=> setCoverPopup(false)}>Cancel</button>
                        <button onClick={()=> {
                            Cover();
                            setCoverPopup(false);
                        }}>OK</button>
                    </div>
                </div>
            </Popup>

        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        LibraryImage: state.LibraryImage,
        levelNowId: state.levelNowId,
        prev_step: state.prev_step,
        ifloorplanNowState: state.ifloorplanNowState,
        sort_image_lib: state.sort_image_lib
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        LibImageAdd: (array) => dispatch(LibImageAdd(array)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        clearIfloorState: () => dispatch(clearIfloorState()),
        changeImageKey: (key) => dispatch(changeImageKey(key)),
        setImageLibSort: (array) => dispatch(setImageLibSort(array)),
        changeLevel: (level) => dispatch(changeLevel(level)),
        updateSingleImageAction: (id) => dispatch(updateSingleImageAction(id)),
    }
}

export default connect(putState, putActions) (ImageLibrary);