import React, { Component, useState} from 'react';
import { connect } from 'react-redux';
import Popup from "reactjs-popup";
import ColorSet from "./ColorSet";
import ImageMarkersStudio from "./ImageMarkersStudio";
import {setColor,
    changeLevel,
    nextStep,
    setColorBackground,
    setOpenImageMarkers,
    setColorText,
    changeCamKey,
    ifloorplanStateAdd,
    setVideosSidebar,
    changeImageKey,
    changeCamPosition } from '../redux/actions/actions';
import {updateNewPlan} from '../serverRequests/requests';

const TopPanel = (props) => {
    const [showMenu, setShowMenu] = useState('none');
    const [newLevelName, setNewLevelName] = useState('');
    const [addLevelPopup, setAddLevelPopup] = useState(false);
    const [saveAsTemplatePopup, setSaveAsTemplatePopup] = useState(false);
    const [prevStepSet, setPrevStepSet] = useState(false);
    const [newTemplateName, setNewTemplateName] = useState('');

    const handleClickTools = () => {
        if (showMenu === 'none'){
            setShowMenu('block');
        } else {
            setShowMenu('none');
        }
    }

    const ifloorplanObj = props.ifloorplanNowState; //last ifloor plan obj
    const levelNow = ifloorplanObj.level[props.levelNowId].tabLabel;

    const handleClickItem = (stepName, key = "_") => {
        handleClickTools(); //close menu
        props.nextStep(stepName, key);
    }

    const handleNext = (e) => {//next image to set position cam
        e.preventDefault();
        const images = props.positionCams.level[props.levelNowId].images; //all images current level
        let newState = JSON.parse(JSON.stringify(props.positionCams)); //main object floor plan
        updateNewPlan(newState)
            .then(data => {
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                props.changeCamPosition(data[0]);  //update second state ifloorplan
                if(props.image_key !== images.length - 1){ //if now index image last when not upper index
                    props.changeImageKey(props.image_key + 1); //up index
                }
            });
    }


    const handlePrev = (e) => {//previos index image to set position cam
        e.preventDefault();
        if(props.image_key !== 0){ //if now index image first when not lower index
            props.changeImageKey(props.image_key - 1) //lower index
        }
    }

    const newLevel = () => { //new level create function
        const newLevelObj = {  //new object level
            tabLabel: newLevelName,
            index: ifloorplanObj.level.length //set last index
        }
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState)); //prev obj ifloorplan
        newState.level.push(newLevelObj); //add new level to array
        setAddLevelPopup(false); //disable level popup
        handleClickTools(); //disable main menu tools
        updateNewPlan(newState)
            .then(data=>{
                props.changeImageKey(0);
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                props.changeCamPosition(data[0]); //update second ifloorplan objecct
                props.nextStep("new level create option", "_"); //next step
                props.changeLevel(data[0].level.length - 1); //set new index level to edit
            });

    }

    const newPanelum = () => {//new panorama
        const panoramObj = { //new object panellum panorama
            img: "/media/system_images/broken-1.png",
            cam_img: null,
            cam_width: 10,
            pos_x: 0,
            pos_y: 0
        }
        let newState = JSON.parse(JSON.stringify(ifloorplanObj)); //last state ifloorplan
        newState.level[props.levelNowId].panelum.push(panoramObj); //append new panorama object
        updateNewPlan(newState)
            .then(data=>{
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                handleClickItem("panorama"); //next step
            });
    }

    const newTemplate = () => { // don`t change
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        newState.template_name = newTemplateName;
        newState.address = "";
        newState.end_live = parseInt(newState.end_live) + 31536000;
        newState.level = [{tabLabel: "First Floor", index: 0}];
        updateNewPlan(newState)
            .then(data=>{
                handleClickItem("");
            });
    }

    const Publish = () => { // don`t change
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        if(props.type_action === 'new'){
            newState.end_live = parseInt(newState.end_live) + 31536000;
            newState.published = true;
        }
        updateNewPlan(newState)
            .then(data => {
                console.log('done! data:', data);
                // if(props.type_action === 'edit'){
                //     return 1;
                // } else {
                //     handleClickItem("");
                // }
            });
    }

    return (
        <div className="topPanelWrap">
            <div className="firstOpt">
                <i className="fa fa-caret-up" aria-hidden="true"></i>
                <div className="toolsBtn">
                    <div className="toolsBtnWrapp"
                         onClick={handleClickTools //open the Tools (changed by Liza)
                         }>
                        <p>Tools</p>

                        <i className="fa fa-caret-down" aria-hidden="true"></i>
                    </div>
                    <div className="MenuWrap" style={{display: showMenu}}>
                        <div className="itemMenu SubMenuWrap" >
                            <p>Floor Plan</p>
                            <i className="fa fa-caret-right" aria-hidden="true"></i>
                            <div className="SubItems">
                                <div className="itemMenu" onClick={()=>handleClickItem("plan library")}><p>Floor plan Library</p></div>
                                <div className="itemMenu" onClick={()=>handleClickItem("scale and repos", "scale plan")}><p>Scale and Reposition</p></div>
                            </div>
                        </div>
                        <div className="itemMenu SubMenuWrap"><p>Images</p><i className="fa fa-caret-right" aria-hidden="true"></i>
                            <div className="SubItems">
                                <div className="itemMenu" onClick={()=>handleClickItem("image library")}><p>Image Library</p></div>
                                <div className="itemMenu" onClick={()=>handleClickItem("ken burns")}><p>Ken Burns Studio...</p></div>
                            </div>
                        </div>
                        <div className="itemMenu" onClick={()=> {
                            handleClickTools(); //close menu
                            newPanelum(); //new panorama object
                        }}><p>360 Images</p></div>
                        <div className="itemMenu" onClick={()=>{handleClickItem("position videos", "_")}}><p>Videos</p></div>
                        <div className="itemMenu" onClick={()=>handleClickItem("layout")}><p>Layout</p></div>
                        <div className="itemMenu" onClick={()=>handleClickItem("clients")}><p>Client Library</p></div>
                        <div className="itemMenu" style={{position: 'relative'}} onClick={()=>setAddLevelPopup(true)}>
                            <p>Add Levels</p>
                        </div>
                        <div className="addLevelWrap" style={{display: addLevelPopup ? 'block' : 'none'}}>
                            <h3>Add new level</h3>
                            <p>Write a name for this level</p>
                            <input onChange={(e)=>{setNewLevelName(e.target.value)}}  type="text" placeholder="Text hint..."/>
                            <p>This name appears on the navigation tab</p>
                            <div className="btnGroup">
                                <button onClick={()=>setAddLevelPopup(false)}>Cancel</button>
                                <button onClick={newLevel}>Add level</button>
                            </div>
                        </div>
                        <div className="itemMenu" onClick={()=> {
                            handleClickTools(); //close menu
                            handleClickItem("cams position");
                            props.setColor(true); //open menu set color
                        }}><p>Colours</p></div>
                        <div className="itemMenu" onClick={()=>{
                            handleClickTools(); //close menu
                            handleClickItem("manage levels");
                        }}><p>Manage Levels</p></div>
                        <div className="itemMenu" onClick={()=>{handleClickItem("cams position"); props.setOpenImageMarkers(true)}}>
                            <p>Image Markers</p>
                        </div>
                        <div className="itemMenu" onClick={()=>{
                            handleClickTools(); //close menu
                            handleClickItem("add media");
                        }}><p>Add external media</p></div>
                        <div className="itemMenu" onClick={()=>handleClickItem("text editor")}><p>Add Text Information</p></div>
                        <div className="itemMenu" onClick={()=>{
                            setSaveAsTemplatePopup(true);
                        }}><p>Save as Template</p></div>
                        <div className="saveAsTemplateWrap" style={{display: saveAsTemplatePopup ? 'block' : 'none'}}>
                            <h3>Save as a Template</h3>
                            <p>Write a name for your Template</p>
                            <input onChange={(e)=>{setNewTemplateName(e.target.value)}}  type="text" placeholder="Text hint..."/>
                            <p>Qualifying text</p>
                            <div className="btnGroup">
                                <button onClick={()=>setSaveAsTemplatePopup(false)}>Cancel</button>
                                <button onClick={newTemplate}>Save</button>
                            </div>
                        </div>
                        <div className="itemMenu" onClick={()=>{
                            handleClickTools(); //close menu
                            handleClickItem("add lat and long");
                        }}><p>Add Lat and Long</p></div>
                    </div>
                </div>
                {/*=====================================*/}
                {/*======btn preev next ============*/}
                <a href="" onClick={(event) => {
                    if(props.step === "cams position"){
                        handlePrev(event);
                    }
                }} className={props.step === "cams position" ? "prevBtn" : "prevBtn not"}>
                    <i className="fa fa-caret-left" aria-hidden="true"></i>
                    <p>Previous</p>
                </a>

                <a href="" onClick={(event) => {
                    if(props.step === "cams position"){
                        handleNext(event);
                    }
                }} className={props.step === "cams position" ? "nextBtn" : "nextBtn not"}>
                    <p>Next</p><i className="fa fa-caret-right" aria-hidden="true"></i></a>
                {/*    =======================================*/}
            </div>
            <div className="secondOpt">
                Creating new. {levelNow}
                {console.log('props:', props,'state',  props.state, 'ifloorplanNowState', props.ifloorplanNowState)}
            </div>
            <div className="thirthOpt">
                <a href="" className={props.step === "cams position" ? "saveNowBtn" : "saveNowBtn not"}
                   onClick={(event)=>{
                       event.preventDefault();
                       if(props.step === "cams position"){

                       }
                   }}
                ><p>Save now...</p></a>
                <a href={'https://ifloorplan360.com/ifloorplan/'+ifloorplanObj.id}
                   onClick={(event)=>{
                       if(props.step === "cams position"){

                       } else {
                           event.preventDefault();
                       }
                   }}
                   target="_blank"
                   className={props.step === "cams position" ? "previewBtn" : "previewBtn not"}>
                    <p>Preview</p></a>
                <a href="" className={props.step === "cams position" ? "publishBtn" : "publishBtn not"}
                   onClick={(event)=>{
                       event.preventDefault();
                       if(props.step === "cams position"){
                           Publish();
                       }
                   }}
                ><p>{props.type_action === "edit" ? "Update" : "Publish..."}</p></a>
            </div>
            {props.set_color ? <ColorSet/> : ''}
            {props.image_markers_layout ? <ImageMarkersStudio/> : ''}
        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        positionCams: state.positionCams,
        ifloorplanNowState: state.ifloorplanNowState,
        levelNowId: state.levelNowId,
        image_key: state.image_key,
        cam_key: state.cam_key,
        set_color: state.set_color,
        prev_step: state.prev_step,
        type_action: state.type_action,
        image_markers_layout: state.image_markers_layout
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        changeCamKey: (key) => dispatch(changeCamKey(key)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        changeCamPosition: (obj) => dispatch(changeCamPosition(obj)),
        changeLevel: (level) => dispatch(changeLevel(level)),
        changeImageKey: (key) => dispatch(changeImageKey(key)),
        setColor: (bool) => dispatch(setColor(bool)),
        setColorBackground: (color) => dispatch(setColorBackground(color)),
        setColorText: (color) => dispatch(setColorText(color)),
        setOpenImageMarkers: (bool) => dispatch(setOpenImageMarkers(bool)),
    }
}

export default connect(putState, putActions) (TopPanel);