import React from 'react';
import { connect } from 'react-redux';
import { nextStep, dropFiles } from '../redux/actions/actions';
import Dropzone from "react-dropzone";

const UploadPanoramaLib = (props) => {

    const handleDrop = (files) => {
        props.dropFiles(files, "360 lib");
        if(props.prev_step === "template"){
            props.nextStep("upload files", "template");
        } else {
            props.nextStep("upload files", "_");
        }
    }

    return (
        <div className="uploadImages">
            <Dropzone onDrop={handleDrop}>
                {({ getRootProps, getInputProps }) => (
                    <div {...getRootProps({ className: "dropzone" })}>
                        <h3>Drop your 360 images here...</h3><br/>
                        <h3>Or</h3><br/>
                        <input {...getInputProps()} />
                        <button>Choose files</button><br/>
                    </div>

                )}
            </Dropzone>
            <div className="cancel">
                <h3>Or</h3><br/>
                <button className="cancel_btn" onClick={()=>{
                    if(props.prev_step === "template"){
                        props.nextStep("cams position", "template");
                    } else {
                        props.nextStep('scale layout', "_");
                    }
                }}>No thanks - Continue</button>
            </div>

        </div>
    );
}

const putState = (state) => {
    return {
        step: state.step,
        prev_step: state.prev_step,
        ifloorplanNowState: state.ifloorplanNowState
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        dropFiles: (files, title) => dispatch(dropFiles(files, title))
    }
}

export default connect(putState, putActions) (UploadPanoramaLib);