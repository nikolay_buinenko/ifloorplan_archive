import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Draggable, {DraggableCore} from 'react-draggable';
import { nextStep, ifloorplanStateAdd, initScale } from '../redux/actions/actions';
import {updateNewPlan} from '../serverRequests/requests';
import Client from './Client';
import DragMeIcon from './otherResources/DragMeIcon';
import EditTextBlock from './EditTextBlock.js';

const Layout = (props) => {

    const ifloorplanObj = props.ifloorplanNowState;
    const { LibraryImage, LibraryPlans } = props;
    const [activeDrags, setActiveDrags] = useState(0)
    const [inputStyle, setInputStyle] = useState({
        position_x: ifloorplanObj.addressPosition_x,
        position_y: ifloorplanObj.addressPosition_y
    });

    const levelObj = ifloorplanObj.level[props.levelNowId];

    const [imageStyle, setStyle] = useState({
        img: levelObj.images[0].img || null,
        img_width: ifloorplanObj.level[props.levelNowId].img_width,
        position_x: ifloorplanObj.level[props.levelNowId].img_pos_x,
        position_y: ifloorplanObj.level[props.levelNowId].img_pos_y
    });
    const [planLevel, setPlanLevel] = useState({
        img: levelObj.plan_level[0].img || null,
        img_width: ifloorplanObj.level[props.levelNowId].plan_width,
        position_x: ifloorplanObj.level[props.levelNowId].plan_pos_x,
        position_y: ifloorplanObj.level[props.levelNowId].plan_pos_y
    });
    const [brandStyle, setBrandStyle] = useState({
        img: ifloorplanObj.brand_logo || null,
        img_width: ifloorplanObj.brand_width,
        position_x: ifloorplanObj.brand_pos_x,
        position_y: ifloorplanObj.brand_pos_y
    });
    const [clientWrapStyle, setClientWrapStyle] = useState({
        position: "absolute",
        position_y: ifloorplanObj.client_block_y,
        position_x: ifloorplanObj.client_block_x,
        width: ifloorplanObj.client_width
    })



    if (props.LibraryImage.length === 0 && props.type_action === 'new'){
        props.nextStep("upload images", "_");
    }

    const handleScale = (event) => {
        //change size block images
        if (event.target.className === 'scale-floor') {
            setPlanLevel({...planLevel,
                img_width: event.target.value
            });
        } else if (event.target.className === 'scale-client'){
            setClientWrapStyle({
                ...clientWrapStyle,
                width: event.target.value
            });
        } else {
            setStyle({ ...imageStyle,
                img_width: event.target.value
            });
        };
    };

    const dragHandleImage = (event) => {
        //set position block images
        event.preventDefault();
        const childBlock = document.getElementsByClassName("interior-image_wrap")[0];
        const parrentBlock = document.getElementsByClassName("dnd-wrapper")[0];
        let childX = childBlock.getBoundingClientRect().left;
        let childY = childBlock.getBoundingClientRect().top;
        const { top, left, width, height } = parrentBlock.getBoundingClientRect();
        let x = (childX - left) / width * 100; // get position at parsent
        let y = (childY - top) / height * 100;
        setStyle({
            ...imageStyle,
            position_y: y,
            position_x: x
        });
    };

    const dragHandlePlan = (event) => {
        event.preventDefault();
        const childBlock = document.getElementsByClassName("floor-plan-wrap")[0];
        const parrentBlock = document.getElementsByClassName("dnd-wrapper")[0];
        let childX = childBlock.getBoundingClientRect().left;
        let childY = childBlock.getBoundingClientRect().top;
        const { top, left, width, height } = parrentBlock.getBoundingClientRect();
        let x = (childX - left) / width * 100; // get position at parsent
        let y = (childY - top) / height * 100;
        setPlanLevel({
            ...planLevel,
            position_x: x,
            position_y: y
        });
    };
    const dragHandleBrand = (event) => {
        event.preventDefault();
        const childBlock = document.getElementsByClassName("interior-brand_wrap")[0];
        const parrentBlock = document.getElementsByClassName("dnd-wrapper")[0];
        let childX = childBlock.getBoundingClientRect().left;
        let childY = childBlock.getBoundingClientRect().top;
        const { top, left, width, height } = parrentBlock.getBoundingClientRect();
        let x = (childX - left) / width * 100; // get position at parsent
        let y = (childY - top) / height * 100;
        setBrandStyle({
            ...brandStyle,
            position_x: x,
            position_y: y
        });
    };

    const dragHandleClient = (event) => {
        event.preventDefault();
        const childBlock = document.getElementsByClassName("client_reposition")[0];
        const parrentBlock = document.getElementsByClassName("dnd-wrapper")[0];
        let childX = childBlock.getBoundingClientRect().left;
        let childY = childBlock.getBoundingClientRect().top;
        const { top, left, width, height } = parrentBlock.getBoundingClientRect();
        let x = (childX - left) / width * 100; // get position at parsent
        let y = (childY - top) / height * 100;
        setClientWrapStyle({
            ...clientWrapStyle,
            position_x: x,
            position_y: y
        });
    };

    const dragHandleText = (event) => {
        event.preventDefault();
        const childBlock = document.getElementsByClassName("edit-text-block")[0];

        const parrentBlock = document.getElementsByClassName("dnd-wrapper")[0];
        let childX = childBlock.getBoundingClientRect().left;
        let childY = childBlock.getBoundingClientRect().top;
        const { top, left, width, height } = parrentBlock.getBoundingClientRect();

        let x = (childX - left) / width * 100;
        let y = (childY - top) / height * 100;
        setInputStyle({
            ...inputStyle,
            position_x: x,
            position_y: y
        })
    }

    const handlerSubmit = () => {
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));

        newState.level[props.levelNowId].plan_width = planLevel.img_width;
        newState.level[props.levelNowId].plan_pos_x = planLevel.position_x;
        newState.level[props.levelNowId].plan_pos_y = planLevel.position_y;

        newState.brand_width = brandStyle.img_width;
        newState.brand_pos_x = brandStyle.position_x;
        newState.brand_pos_y = brandStyle.position_y;

        newState.addressPosition_x = inputStyle.position_x;
        newState.addressPosition_y = inputStyle.position_y;

        newState.client_block_x = clientWrapStyle.position_x;
        newState.client_block_y = clientWrapStyle.position_y;
        newState.client_width = clientWrapStyle.width;

        for(let i = 0; i < newState.level.length; i++){
            newState.level[i].img_pos_x = imageStyle.position_x;
            newState.level[i].img_pos_y = imageStyle.position_y;
        }

        newState.level[props.levelNowId].img_width = imageStyle.img_width;

        updateNewPlan(newState)
            .then(data => {
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                // props.initScale(false);
                props.nextStep("cams position", "_");
            });
    };

    useEffect(()=>{
        //set colors
        const parrentBlock = document.getElementsByClassName("workPlace")[0];
        parrentBlock.style.color = ifloorplanObj.textColor;
        parrentBlock.style.background = ifloorplanObj.bgColor;
    }, [])



    const onStart = () => {setActiveDrags(activeDrags + 1)};
    const onStop = () => {setActiveDrags(activeDrags - 1)};
    const dragHandlers = {onStart: onStart, onStop: onStop};

    return (
        <div className="scale-layout workPlace">
            <div className="scales-panel">
                <div>
                    <div className="scale-wrapper">
                        <h3>Scale images</h3>
                        <input type="range"
                               step="1"
                               className="scale-image"
                               min="10"
                               max="90"
                               onChange={handleScale}
                               value={imageStyle.img_width}/>
                    </div>
                    <div className="scale-wrapper">
                        <h3>Scale clients</h3>
                        <input type="range"
                               step="1"
                               className="scale-client"
                               min="10"
                               max="90"
                               onChange={handleScale}
                               value={clientWrapStyle.width}/>
                    </div>
                    <div className="scale-wrapper">
                        <h3>Scale floor plans</h3>
                        <input type="range"
                               step="1"
                               className="scale-floor"
                               min="10"
                               max="90"
                               onChange={handleScale}
                               value={planLevel.img_width}/>
                    </div>
                    <button className="save_layout" onClick={handlerSubmit}>Hide Layout Controls</button>
                </div>
            </div>


            <div className='dnd-wrapper' style={{position: 'relative'}}>


                {ifloorplanObj.client_data == "" ? '' :
                    <Draggable
                        handle=".icon_reposition_client"
                        onDrag={dragHandleClient}
                        {...dragHandlers}>
                        <div className='client_reposition'
                             style={{
                                 width: clientWrapStyle.width+"%",
                                 height: "20%",
                                 position: 'absolute',
                                 top: ifloorplanObj.client_block_y+"%", //add top pos from DB
                                 left: ifloorplanObj.client_block_x+"%", //add left pos from DB
                             }}>
                            <div className="wrap_layout_item" style={{width: '100%', position: 'relative'}}>
                                <Client clientData={JSON.parse(ifloorplanObj.client_data)} clientPosition={JSON.parse(ifloorplanObj.client_position)}/>
                                <div className="icon_reposition_client">
                                    <DragMeIcon/>
                                </div>
                            </div>
                        </div>
                    </Draggable>
                }
                {ifloorplanObj.brand_width == 0 ? "" :
                    <Draggable
                        onDrag={dragHandleBrand}
                        handle=".icon_reposition_brand"
                        {...dragHandlers}>
                        <div className='interior-brand_wrap'
                             style={{
                                 position: 'absolute',
                                 opacity: ifloorplanObj.brand_transparency,
                                 top: ifloorplanObj.brand_pos_y+"%", //add top pos from DB
                                 left: ifloorplanObj.brand_pos_x+"%", //add left pos from DB
                                 width: `${ifloorplanObj.brand_width}%`
                             }}>
                            <div className="wrap_layout_item" style={{width: '100%', position: 'relative'}}>
                                <img className='interior-brand'
                                     src={ifloorplanObj.brand_logo}
                                     style={{width: '100%'}}/>
                                <div className="icon_reposition_brand">
                                    <DragMeIcon/>
                                </div>
                            </div>
                        </div>
                    </Draggable>

                }


                <Draggable
                    onDrag={dragHandleImage}
                    handle=".icon_reposition_image"
                    {...dragHandlers}>
                    <div className='interior-image_wrap'
                         style={{
                             position: 'absolute',
                             top: ifloorplanObj.level[props.levelNowId].img_pos_y+"%", //add top pos from DB
                             left: ifloorplanObj.level[props.levelNowId].img_pos_x+"%", //add left pos from DB
                             width: `${imageStyle.img_width}%`
                         }}>
                        <div className="wrap_layout_item" style={{width: '100%', position: 'relative'}}>
                            <img className='interior-image'
                                 src={imageStyle.img}
                                 style={{width: '100%'}}/>
                            <div className="icon_reposition_image">
                                <DragMeIcon/>
                            </div>
                        </div>
                    </div>
                </Draggable>

                <Draggable
                    onDrag={dragHandlePlan}
                    handle=".icon_reposition_plan"
                    {...dragHandlers}>
                    <div className='floor-plan-wrap'
                         style={{
                             position: 'absolute',
                             top: ifloorplanObj.level[props.levelNowId].plan_pos_y+"%", //add top pos from DB
                             left: ifloorplanObj.level[props.levelNowId].plan_pos_x+"%", //add left pos from DB
                             width: `${planLevel.img_width}%`
                         }}>

                        <div className="wrap_layout_item" style={{width: '100%', position: 'relative'}}>
                            <img className='floor-plan'
                                 src={planLevel.img}
                                 style={{width: '100%'}}/>
                            <div className="icon_reposition_plan">
                                <DragMeIcon/>
                            </div>
                        </div>

                    </div>
                </Draggable>
                <Draggable
                    onDrag={dragHandleText}
                    {...dragHandlers}>
                    <div className='edit-text-block'
                         style={{
                             position: 'absolute',
                             top: ifloorplanObj.addressPosition_y+"%",
                             left: ifloorplanObj.addressPosition_x+"%"
                         }}>

                        <div className="wrap_layout_item" style={{width: '100%', position: 'relative'}}>
                            <EditTextBlock/>
                            <div className="icon_reposition_address">
                                <DragMeIcon/>
                            </div>
                        </div>
                    </div>
                </Draggable>
            </div>
            {/*<EditTextBlock/>*/}
        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        LibraryImage: state.LibraryImage,
        LibraryPlans: state.LibraryPlans,
        ifloorplanNowState: state.ifloorplanNowState,
        levelNowId: state.levelNowId,
        scale: state.scale,
        type_action: state.type_action
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        dropFiles: (files, title) => dispatch(dropFiles(files, title)),
        initScale: (bool) => dispatch(initScale(bool)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan))
    }
}
export default connect(putState, putActions)(Layout);