import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import { updateNewPlan } from '../serverRequests/requests';
import { nextStep, set360ToolsAction, ifloorplanStateAdd, panelumLib } from '../redux/actions/actions';
import Dropzone from "react-dropzone";

const PanLibrary = (props) => {
    const ifloorplanObj = props.ifloorplanNowState; //last ifloor plan obj
    const panoramaLenght = ifloorplanObj.level[props.levelNowId].panelum.length;
    const [images, setImages] = useState([]);
    const [selectedPoint, setSelectedPoint] = useState('');
    const [selectedImage, setSelectedImage] = useState("");
    const [selectedId, setSelectedId] = useState('');

    useEffect(()=>{
        const img = [];
        for(let i = 0; i < props.panelum_lib.length; i++){
            let img_active = props.panelum_lib[i].image;
            img.push(<div className="pan_image_wrap" key={i} onClick={()=>{
                setSelectedImage(img_active);
                setSelectedId(props.panelum_lib[i].id);
                setSelectedPoint(i);
            }}>
                <img src={img_active} alt=""/>
            </div>)
        }
        setImages(img);
    }, []);

    useEffect(()=>{
        setImages([]);
        const img = [];
        for(let i = 0; i < props.panelum_lib.length; i++){
            let img_active = props.panelum_lib[i].image;
            if(i == selectedPoint){
                img.push(<div className="pan_image_wrap active" key={i} onClick={()=>{
                    setSelectedImage(img_active);
                    setSelectedId(props.panelum_lib[i].id);
                    setSelectedPoint(i);
                }}>
                    <div className="checked"><i className="fa fa-check" aria-hidden="true"></i></div>
                    <img src={img_active} alt=""/>
                </div>)
            } else {
                img.push(<div className="pan_image_wrap" key={i} onClick={()=>{
                    setSelectedImage(img_active);
                    setSelectedId(props.panelum_lib[i].id);
                    setSelectedPoint(i);
                }}>
                    <img src={img_active} alt=""/>
                </div>)
            }

        }
        setImages(img);
    }, [selectedPoint]);

    const Save = () => {
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        newState.level[props.levelNowId].panelum[panoramaLenght - 1].img = selectedImage;
        newState.level[props.levelNowId].panelum[panoramaLenght - 1].id_lib_img = setSelectedId;
        updateNewPlan(newState)
            .then((data)=>{
                props.ifloorplanStateAdd(data[0]);
                props.set360ToolsAction("markers");
            })
    }


    return (
        <div className="panorama_simple_lib">
            <h3>Select an image </h3>
            <div className="panorama_images_wrap">
                {images}
            </div>
            <div className="pan_lib_btn_group">
                <a href="" className="link_360_lib" onClick={(event)=>{
                    event.preventDefault();
                    props.nextStep("360 library", "_");
                }}>View 360 Image Library</a>
                <button className="select_btn" onClick={Save}>Use Selected</button>
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        levelNowId: state.levelNowId,
        ifloorplanNowState: state.ifloorplanNowState,
        tools_360_action: state.tools_360_action,
        panelum_lib: state.panelum_lib,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        panelumLib: (items) => dispatch(panelumLib(items)),
        set360ToolsAction: (value) => dispatch(set360ToolsAction(value))
    }
}

export default connect(putState, putActions) (PanLibrary);