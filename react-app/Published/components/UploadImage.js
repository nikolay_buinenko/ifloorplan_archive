import React from 'react';
import { connect } from 'react-redux';
import { nextStep, dropFiles } from '../redux/actions/actions';
import Dropzone from "react-dropzone";


const UploadImage = (props) => {

    const handleDrop = (files) => {
        props.dropFiles(files, "IMAGES");
        props.nextStep("upload files", props.prev_step);
    }

    return (
        <div className="uploadImages">
            {props.prev_step === "new level" || props.prev_step === "exist images" ?
                <div className="other_case_tool"><a href="" onClick={(event)=>{
                    event.preventDefault();
                    props.nextStep("image library", "new level");
                }}>Choose images from library</a></div> : ""}

            <Dropzone onDrop={handleDrop}>
                {({ getRootProps, getInputProps }) => (
                    <div {...getRootProps({ className: "dropzone" })}>
                        <h3>Drag & Drop your Images here...</h3>
                        <h3>Or</h3>
                        <input {...getInputProps()} />
                        <button>Choose files</button>
                        <p>Creating a multi level iFloorPlan</p>
                        <p>You can upload all your images for this property. Access them from the Image library</p>
                    </div>

                )}
            </Dropzone>
        </div>
    );
}

const putState = (state) => {
    return {
        step: state.step,
        prev_step: state.prev_step,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        dropFiles: (files, title) => dispatch(dropFiles(files, title))
    }
}

export default connect(putState, putActions) (UploadImage);