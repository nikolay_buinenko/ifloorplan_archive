import React, {useState} from 'react';
import { connect } from 'react-redux';
import { ChromePicker } from 'react-color';
import {updateNewPlan} from '../serverRequests/requests';
import {setColor,
    nextStep,
    setColorBackground,
    setColorText,
    ifloorplanStateAdd } from '../redux/actions/actions';
const ColorSet = (props) => {
    const ifloorplanObj = props.ifloorplanNowState; //last ifloor plan obj
    const [textColors, setTextColors] = useState(ifloorplanObj.textColor);
    const [backgroundColor, setBackgroundColor] = useState(ifloorplanObj.bgColor);

    return (
        <div className="colorPanel">
            <div className="colorPickerTitle">
                <h3>Colours</h3>
                <p>Choose colours for text and the background</p>
            </div>
            <div className="wrapColorPicker">
                <h3><b>Text</b></h3>
                <ChromePicker
                    color={textColors}
                    onChangeComplete={(color, event)=> {
                        setTextColors(color.hex);
                        props.setColorText(color.hex);
                    }}
                />
            </div>
            <div className="wrapColorPicker">
                <h3><b>Background</b></h3>
                <ChromePicker
                    color={backgroundColor}
                    onChangeComplete={(color, event)=> {
                        setBackgroundColor(color.hex);
                        props.setColorBackground(color.hex);
                    }}
                />
            </div>
            <div className="btnWrap">
                <button onClick={()=>{
                    let newState = JSON.parse(JSON.stringify(ifloorplanObj));
                    newState.textColor = textColors;
                    newState.bgColor = backgroundColor;
                    updateNewPlan(newState)
                        .then(data=>{
                            props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                            props.setColor(false); //close set menu color
                        });
                }}>Done</button>
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        ifloorplanNowState: state.ifloorplanNowState,
        set_color: state.set_color,
        text_color: state.text_color,
        background_color: state.background_color,
    }
}
const putActions = (dispatch) => {
    return {
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        setColor: (bool) => dispatch(setColor(bool)),
        setColorBackground: (color) => dispatch(setColorBackground(color)),
        setColorText: (color) => dispatch(setColorText(color))
    }
}

export default connect(putState, putActions)(ColorSet);