import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import { CSRFTOKEN } from '../redux/constant/constants';
import Popup from "reactjs-popup";
import Dropzone from "react-dropzone";
import { updateNewPlan, deletePanelumImage, uploadFilesToServerPanelum } from '../serverRequests/requests';
import { nextStep, set360ToolsAction, ifloorplanStateAdd, panelumLib } from '../redux/actions/actions';

const PanoramaLibrary = (props) => {
    const ifloorplanObj = props.ifloorplanNowState; //last ifloor plan obj
    const panoramaLenght = ifloorplanObj.level[props.levelNowId].panelum.length;
    const [images, setImages] = useState([]);
    const [selectedPoint, setSelectedPoint] = useState('');
    const [selectedImage, setSelectedImage] = useState("");
    const [selectedId, setSelectedId] = useState('');

    const [boolHoverOption, setBoolHoverOption] = useState(false);
    const [blockHoverId , setBlockHoverId] = useState('');
    const [blockText, setBlockText] = useState('');

    const [replaceId, setReplaceId] = useState('');
    const [replasePopup, setReplasePopup] = useState(false);

    const simpleDelete = (id) => {
        const data = {
            simple: id,
            ifloorplan_id: ifloorplanObj.id
        }
        deletePanelumImage(data)
            .then((res)=>{
                props.panelumLib(res); //update library
                const newPanelumArr = [];
                let newState = JSON.parse(JSON.stringify(ifloorplanObj));
                //if removed image in panelum object -> delete panelum obj
                for(let i = 0; i < newState.level[props.levelNowId].panelum.length; i++){
                    const instance_id = newState.level[props.levelNowId].panelum[i].id_lib_img;
                    if(instance_id != id){
                        newPanelumArr.push(newState.level[props.levelNowId].panelum[i]);
                    }
                }
                newState.level[props.levelNowId].panelum = newPanelumArr;
                updateNewPlan(newState)
                    .then((data)=>{
                        props.ifloorplanStateAdd(data[0]);
                    })
            })
    }

    const replaceOne = (files) => {
        setReplasePopup(false);
        const config = {
            headers: {
                "X-CSRFToken": CSRFTOKEN,
                'content-type': 'multipart/form-data'
            }
        }
        let form_data = new FormData();
        form_data.append('ifloorplan_id', ifloorplanObj.id);
        form_data.append('image', files[0], files[0].name);
        form_data.append('id', replaceId);
        uploadFilesToServerPanelum("https://ifloorplan360.com/api/panelum/", form_data, config)
            .then(res => {
                props.panelumLib(res.data);
                let newState = JSON.parse(JSON.stringify(ifloorplanObj));
                //update image
                for(let i = 0; i < newState.level[props.levelNowId].panelum.length; i++){
                    const instance_id = newState.level[props.levelNowId].panelum[i].id_lib_img;
                    for(let j = 0; j < res.data.length; j++){
                        if(instance_id === res.data[j].id){
                            newState.level[props.levelNowId].panelum[i].img = res.data[j].image;
                        }
                    }
                }
                updateNewPlan(newState)
                    .then((data)=>{
                        props.ifloorplanStateAdd(data[0]);
                    })
            })
    }

    const initImage = () => {
        setImages([]);
        const img = [];
        for(let i = 0; i < props.panelum_lib.length; i++){
            let img_active = props.panelum_lib[i].image;
            img.push(<div className="pan_image_wrap active" key={i} >
                {i === selectedPoint ? <div className="checked"><i className="fa fa-check" aria-hidden="true"></i></div> : ''}
                <img src={img_active} alt="" onClick={()=>{
                    setSelectedImage(img_active);
                    setSelectedId(props.panelum_lib[i].id);
                    setSelectedPoint(i);
                }}/>
                {blockHoverId === i ? <div className="hover__lib_text">{blockText}</div> : ''}
                <div className="options">
                    <div><i
                        onClick={()=>{
                            setReplaceId(props.panelum_lib[i].id);
                            setReplasePopup(true);
                        }}
                        onMouseOut={()=>{
                            setBlockHoverId('');
                            setBlockText("");
                        }}
                        onMouseEnter={()=>{
                            setBlockHoverId(i);
                            setBlockText("Replace this image");
                        }}
                        className="fa fa-plus" aria-hidden="true"></i></div>
                    <div><i
                        onClick={()=>{
                            simpleDelete(props.panelum_lib[i].id);
                        }}
                        onMouseOut={()=>{
                            setBlockHoverId('');
                            setBlockText("");
                        }}
                        onMouseEnter={()=>{
                            setBlockHoverId(i);
                            setBlockText("Remove this image");
                        }}
                        className="fa fa-minus" aria-hidden="true"></i></div>
                </div>
            </div>)

        }
        setImages(img);
    }

    useEffect(()=>{
        initImage();
    }, []);

    useEffect(()=>{
        initImage();
    }, [selectedPoint, blockHoverId, props.panelum_lib]);


    const Save = () => {
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        newState.level[props.levelNowId].panelum[panoramaLenght - 1].img = selectedImage;
        newState.level[props.levelNowId].panelum[panoramaLenght - 1].id_lib_img = setSelectedId;
        updateNewPlan(newState)
            .then((data)=>{
                props.ifloorplanStateAdd(data[0]);
                props.nextStep("panorama", "_");
                props.set360ToolsAction("markers");
            })
    }

    const replaceAll = () => {
        const id_to_delete = [];
        for(let i = 0; i < props.panelum_lib.length; i++){
            id_to_delete.push(props.panelum_lib[i].id);
        }
        const data = {
            many: id_to_delete,
            ifloorplan_id: ifloorplanObj.id
        }
        deletePanelumImage(data)
            .then((res)=>{
                props.panelumLib(res);
                let newState = JSON.parse(JSON.stringify(ifloorplanObj));
                newState.level[props.levelNowId].panelum = [];
                updateNewPlan(newState)
                    .then((data)=>{
                        props.ifloorplanStateAdd(data[0]);
                        props.nextStep("360 library upload", "_");
                    })
            })
    }

    return (
        <div className="panorama_lib">
            <div className="panorama_tools_lib">
                <div className="rep_add_tools_lib">
                    <h3>360 Image Library</h3>
                    <button className="add360" onClick={()=>{
                        props.nextStep("panorama single upload file", "_");
                    }}>Add 360 Image +</button>
                    <button className="replace360all"
                            onClick={replaceAll}
                    >Replace all images</button>
                </div>
                <div className="can_save_tools_lib">
                    <button className="cancel" onClick={()=>{
                        props.nextStep("panorama", "_");
                        props.set360ToolsAction("choose");
                    }}>Cancel</button>
                    <button className="selected" onClick={Save}>Use selected</button>
                </div>
            </div>
            <div className="panorama_title_wrap">
                <div className="description_pan">
                    <p>Replace Image</p>
                    <p>Add image</p>
                    <p>Remove image</p>
                    <p>Replace all image</p>
                </div>
                <div className="main_pan_title">
                    <h3>Click an image to select</h3>
                </div>
            </div>
            <div className="panorama_img_wrap_lib">
                {images}
            </div>

            <Popup
                open={replasePopup}
                closeOnDocumentClick
                onClose={()=> {
                    setReplasePopup(false);
                }}
            >
                <div className="modal">
                    <Dropzone onDrop={replaceOne}>
                        {({ getRootProps, getInputProps }) => (
                            <div {...getRootProps({ className: "dropzone" })}>
                                <h3>Drop a single image file here...</h3><br/>
                                <h3>Or</h3><br/>
                                <input {...getInputProps()} />
                                <button>Choose file</button><br/>
                                <p>Single file upload</p>
                            </div>

                        )}
                    </Dropzone>
                    <button onClick={()=>{
                        setReplasePopup(false);
                    }}>Cancel</button>
                </div>
            </Popup>

        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        ifloorplanNowState: state.ifloorplanNowState,
        levelNowId: state.levelNowId,
        panelum_lib: state.panelum_lib,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        panelumLib: (items) => dispatch(panelumLib(items)),
        set360ToolsAction: (value) => dispatch(set360ToolsAction(value))
    }
}

export default connect(putState, putActions) (PanoramaLibrary);
