import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {
    changeCamPosition,
    changeImageKey,
    changeLevel,
    ifloorplanStateAdd,
    nextStep,
    setColor,
    setColorBackground,
    setColorText
} from '../redux/actions/actions';
import {ChromePicker} from 'react-color';
import {getSystemIcons, getUserIcons, updateNewPlan} from "../serverRequests/requests";

const CustomButtons = ({opacity, zindex}) => {


    return (
        <div className='customButtons' style={{opacity: opacity, zIndex: zindex}}>
            <ChromePicker
                color={'#16d43f'}
                // onChangeComplete={(colorBG) => {
                //     setColorBG(colorBG.hex)
                // }}
            />
            <div className="selectButton__label">
                <h3>Label</h3>
                <input
                    id='labelButton'
                    type="text"
                    value="Bla-bla"
                    onChange={() => {
                        console.log('Rename button')
                    }}/>
            </div>
            <div className="scale-wrapper">
                <h3>Button wigth</h3>
                <input type="range"
                       id='buttonSize'
                       step="0.05"
                       className="scale-image"
                       min="1"
                       max="5"
                       onChange={() => {
                           console.log('Scale button')
                       }}/>
            </div>
            <div className="scale-wrapper">
                <h3>Button height</h3>
                <input type="range"
                       id='buttonSize'
                       step="0.05"
                       className="scale-image"
                       min="1"
                       max="5"
                       onChange={() => {
                           console.log('Scale button')
                       }}/>
            </div>
            <div className="scale-wrapper">
                <h3>Corner rounding</h3>
                <input type="range"
                       id='buttonSize'
                       step="0.05"
                       className="scale-image"
                       min="1"
                       max="5"
                       onChange={() => {
                           console.log('Scale button')
                       }}/>
            </div>
            <div className="scale-wrapper">
                <h3>Button scale</h3>
                <input type="range"
                       id='buttonSize'
                       step="0.05"
                       className="scale-image"
                       min="1"
                       max="5"
                       onChange={() => {
                           console.log('Scale button')
                       }}/>
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        levelNowId: state.levelNowId,
        cam_key: state.cam_key,
        image_key: state.image_key,
        positionCams: state.positionCams,
        ifloorplanNowState: state.ifloorplanNowState,
        text_color: state.text_color,
        background_color: state.background_color,
        set_color: state.set_color,
    }
}
const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        changeCamKey: (key) => dispatch(changeCamKey(key)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        changeCamPosition: (obj) => dispatch(changeCamPosition(obj)),
        setColor: (bool) => dispatch(setColor(bool)),
        changeLevel: (level) => dispatch(changeLevel(level)),
        changeImageKey: (key) => dispatch(changeImageKey(key)),
        setColorBackground: (color) => dispatch(setColorBackground(color)),
        setColorText: (color) => dispatch(setColorText(color))
    }
}
export default connect(putState, putActions)(CustomButtons);