import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import { nextStep, set360ToolsAction, ifloorplanStateAdd } from '../redux/actions/actions';

const PanChooseFile = (props) => {
    return (
        <div className="choose">
            <div className="choose_upload_wrap_pan">
                <a href="" onClick={(event)=>{
                    event.preventDefault();
                    props.set360ToolsAction("new upload");
                }}>Upload a 360 image</a>
                <h3>Or</h3>
                <a href="" onClick={(event)=>{
                    event.preventDefault();
                    props.set360ToolsAction("library");
                }}>Choose a 360 image from library</a>
            </div>
            <div className="pan_lib_link">
                <a style={{marginTop:"100px"}} href="" onClick={(event)=>{
                    event.preventDefault();
                    props.nextStep("360 library", "_");
                }}>View 360 Image Library</a>
            </div>
            <div className="btn_done_pan">
                <button className="done">Done</button>
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        levelNowId: state.levelNowId,
        ifloorplanNowState: state.ifloorplanNowState,
        tools_360_action: state.tools_360_action
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        set360ToolsAction: (value) => dispatch(set360ToolsAction(value))
    }
}


export default connect(putState, putActions) (PanChooseFile);