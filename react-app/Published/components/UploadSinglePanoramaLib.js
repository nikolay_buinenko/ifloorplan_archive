import React from 'react';
import { connect } from 'react-redux';
import { nextStep, dropFiles } from '../redux/actions/actions';
import Dropzone from "react-dropzone";


const UploadSinglePanoramaLib = (props) => {

    const handleDrop = (files) => {
        props.dropFiles(files, "single 360");
        props.nextStep("upload files", "_");
    }

    return (
        <div className="uploadImages">
            <Dropzone onDrop={handleDrop}>
                {({ getRootProps, getInputProps }) => (
                    <div {...getRootProps({ className: "dropzone" })}>
                        <h3>Drop a single image file here...</h3><br/>
                        <h3>Or</h3><br/>
                        <input {...getInputProps()} />
                        <button>Choose file</button><br/>
                        <p>Single file upload</p>
                    </div>

                )}
            </Dropzone>
            <div className="cancel">
                <button className="cancel_btn" onClick={()=>{
                    props.nextStep('360 library', "_");
                }}>Cancel</button>
            </div>
        </div>
    );
}

const putState = (state) => {
    return {
        step: state.step
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        dropFiles: (files, title) => dispatch(dropFiles(files, title))
    }
}

export default connect(putState, putActions) (UploadSinglePanoramaLib);