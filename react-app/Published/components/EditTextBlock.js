import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Draggable, {DraggableCore} from 'react-draggable';
import { ifloorplanStateAdd } from '../redux/actions/actions';

const EditTextBlock = (props) => {
    const ifloorplanObj = props.ifloorplanNowState;
    const [text, setText] = useState("");
    const [textSize, setSize] = useState([1.5, 2, 2.5, 3, 3.5, 4]);
    const [inputStyle, setInputStyle] = useState({
        fontWeight: ifloorplanObj.addressFontWeight,
        fontStyle: ifloorplanObj.addressFontStyle,
        fontSize: `${ifloorplanObj.addressFontSize}vh`,
        fontFamily: ifloorplanObj.addressFontFamily,
        color: props.text_color
    });

    const [opacitySize, setOpacitySize] = useState('none');
    const [opacityFont, setOpacityFont] = useState('none');
    const [isBlockVisible, setBlockVisible] = useState(false); //is text editor block visible or not
    const [activeButton, setActive] = useState(inputStyle.fontStyle === 'normal' ? '' : 'active');

    const updateMainState = () => {
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        newState.addressFontFamily = inputStyle.fontFamily;
        newState.addressFontSize = +inputStyle.fontSize.slice(0, -2); //without "vh" in the end
        newState.addressFontStyle = inputStyle.fontStyle;
        newState.addressFontWeight = inputStyle.fontWeight;
        newState.address = text;
        props.ifloorplanStateAdd(newState);
    };

    useEffect(()=>{
        setText(props.ifloorplanNowState.address);
    }, [props.ifloorplanNowState])

    useEffect(()=>{
        updateMainState();
    }, [text, textSize, inputStyle]);

    const changeSize = (value) => {
        setInputStyle({
            ...inputStyle,
            fontSize: `${value}vh`
        });
        setOpacitySize('none');
    };

    const changeFont = (value) => {
        setInputStyle({
            ...inputStyle,
            fontFamily: value
        });

        setOpacityFont('none');
    };

    const handleChange = (event) => {
        setText(event.target.value);
    };

    useEffect(()=>{
        setInputStyle({
            ...inputStyle,
            color: props.text_color
        });
    }, [props.text_color]);

    return (
        <div className='edit-text-block'>
            <div className="wrap_text_editor_main"
                 style={{opacity: isBlockVisible ? 1 : 0}}>
                <div className='edit-font-wrapper'>
                    <div className='font-style'>
                        <div onClick={() => setOpacityFont(opacityFont === 'none' ? 'block' : 'none')}>
                            {inputStyle.fontFamily.split(',')[0]}
                            <i className="fas fa-caret-down"></i>
                        </div>
                        <ul style={{display: opacityFont}}>
                            <li onClick = {() => changeFont('\'IBM Plex Serif\', serif')}>Plex Serif</li>
                            <li onClick = {() => changeFont('\'Roboto\', sans-serif')}>Roboto</li>
                            <li onClick = {() => changeFont('\'DM Sans\', sans-serif')}>Sans</li>
                        </ul>
                    </div>

                    <div className='font-size'>
                        <div onClick={() => setOpacitySize(opacitySize === 'none' ? 'block' : 'none')}>
                            <i className="fas fa-text-height"></i>
                            <i className="fas fa-caret-down"></i>
                        </div>
                        <ul style={{display: opacitySize}}>
                            {textSize.map(size => {
                                return (
                                    <li key = {size} onClick = {() => changeSize(size)}> {size} </li>
                                )
                            })}
                        </ul>
                    </div>

                    <div className={inputStyle.fontWeight === 'bold' ? 'bold-style active' : 'bold-style'}
                         onClick={() => setInputStyle({
                             ...inputStyle,
                             fontWeight: inputStyle.fontWeight === 'bold' ? 'normal' : 'bold'
                         })}>
                        <i className="fas fa-bold"></i>
                    </div>
                    <div className={inputStyle.fontStyle === 'italic' ? 'italic-style active' : 'italic-style'}
                         onClick={() => setInputStyle({
                             ...inputStyle,
                             fontStyle: inputStyle.fontStyle === 'italic' ? 'normal' : 'italic'
                         })}>
                        <i className="fas fa-italic"></i>
                    </div>

                </div>
            </div>

            <form>
                <input id="text_input" style={inputStyle}
                       onClick={() => setBlockVisible(pre => !pre)}
                       type = 'text'
                       placeholder = 'Type address of this property here'
                       onChange = {handleChange}
                       value={text}/>
            </form>

        </div>
    )
}

const putState = (state) => {
    return {
        ifloorplanNowState: state.ifloorplanNowState,
        text_color: state.text_color,
    }
}

const putActions = (dispatch) => {
    return {
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan))
    }
}

export default connect(putState, putActions)(EditTextBlock);