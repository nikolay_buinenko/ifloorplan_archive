export const sizeBlock = (classParent) => {
    // return width and height component
    let element = document.getElementsByClassName(classParent);
    return {
        width: element[0].offsetWidth,
        height: element[0].offsetHeight
    }
}