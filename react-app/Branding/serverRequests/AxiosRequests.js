import {
    CSRFTOKEN
}
    from '../redux/constants/BrandConst';

export async function getBrand( userId ){
    const config ={
        method: 'GET',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            'content-type': 'application/json'
        }
    }
    const response = await fetch('https://ifloorplan360.com/api/getbrand/'+userId+'/',config);
    return await response.json();
}

export async function sendImage(image) {
    let form_data = new FormData();
    form_data.append('logo', image[0], image[0].name);
    form_data.append('width',  25);
    form_data.append('transparency',  1);
    form_data.append('position_x',  0);
    form_data.append('position_y',  0);
    form_data.append('url',  '');
    const config = {
        method: 'POST',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
        },
        body: form_data
    }
    const response = await fetch('https://ifloorplan360.com/api/brand/', config);
    return await response.json();
}

export async function sendProperties(data) {
    let form_data = new FormData();
    if(typeof(data.logo) !== "string"){
        form_data.append('logo', data.logo[0], data.logo[0].name);
    }
    form_data.append('brand_id',  data.id);
    form_data.append('width',  data.width);
    form_data.append('transparency',  data.transparency);
    form_data.append('position_x',  data.position_x);
    form_data.append('position_y',  data.position_y);
    form_data.append('url',  data.url);
    const config = {
        method: 'PUT',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
        },
        body: form_data
    }
    const response = await fetch('https://ifloorplan360.com/api/brand/', config);
    return await response.json();
}

export async function deleteBrand(id) {
    let form_data = new FormData();
    form_data.append('id', id);
    const config = {
        method: 'DELETE',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
        },
        body: form_data
    }
    const response = await fetch('https://ifloorplan360.com/api/brand/', config);
    return await response;
}