import React from 'react';
import Dropzone from "react-dropzone";
import { connect } from 'react-redux';
import {uploadDataImageBrand, nextStep, changeParamsBrand} from '../redux/actions/ActionsTypes';
import {sendImage, sendProperties} from '../serverRequests/AxiosRequests';



const Create = (props) => {

    const handleDrop = (acceptedFiles) => {
        if (props.roleType === "CREATE"){
            sendImage(acceptedFiles)
            .then((data) => {
                props.uploadImage(data); //put data response server to store
                props.nextStep("scaled logo"); //go to nrext step
            });
        } else {
            let newState = JSON.parse(JSON.stringify(props.newBrandCreateJson[props.newBrandCreateJson.length - 1]));
            newState.logo = acceptedFiles;
            sendProperties(newState)
                .then(data => {
                    props.changeParamsBrand(data);
                    props.nextStep("update buttons navigate");
                });
        }

    }


    return (
        <div className='onDropBlock'>
            <Dropzone onDrop={handleDrop}>
                {({ getRootProps, getInputProps }) => (
                    <div {...getRootProps({ className: "dropzone" })}>
                        <h3>Drag & Drop your business logo here...</h3><br/>
                        <h3>Or</h3><br/>
                        <input {...getInputProps()} />
                        <button>Choose files</button><br/>
                    </div>

                )}
            </Dropzone>
            {props.roleType === "CREATE" ? "" :
                <div className="cancel">
                    <button onClick={()=>{
                        props.nextStep("update buttons navigate");
                    }}>Cancel</button>
                </div>
            }
        </div>
    )
}

const putState = (state) => {
    return {
        roleType: state.roleType,
        newBrandCreateJson: state.newBrandCreateJson
    }
}

const putDispatch = (dispatch) => {
    return {
        uploadImage: (dataArray) => dispatch(uploadDataImageBrand(dataArray)),
        changeParamsBrand: (data) => dispatch(changeParamsBrand(data)),
        nextStep: (value) => dispatch(nextStep(value))
    }
}

export default connect(putState, putDispatch)(Create);