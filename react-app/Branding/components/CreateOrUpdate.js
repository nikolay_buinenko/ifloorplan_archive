import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import {getBrand} from '../serverRequests/AxiosRequests';
import Create from './Create';
import Update from './Update';
import Drag from '../containers/Drag';
import Scale from '../containers/Scale';
import Url from '../containers/Url';
import Finish from '../containers/Finish';
import NavUpdateButtons from '../containers/NavigationButtons/NavUpdateButtons';
import MainNavButtonsUpdate from '../containers/NavigationButtons/MainNavButtonsUpdate';
import {putBrandData, nextStep, changeParamsBrand} from '../redux/actions/ActionsTypes';


class CreateOrUpdate extends Component {
    constructor(props){
        super(props)
        const userId = document.getElementById("user_id").value; //get user id
        getBrand(userId)
            .then((data) => {
                if(data.length > 0) {
                    props.putBrandData(data[0]); //put data response server to store
                    props.changeParamsBrand(data[0]);
                    props.nextStep("update");
                } else {
                    props.nextStep("create");
                }
            });
    }

    render(){
        let step = this.props.step; //now value step

        return (
            <div>
                <div className="titleBlock">
                    <div className="title">Branding</div>
                    {step === "update" ? <NavUpdateButtons/> : ''}
                    {step === "update buttons navigate" ? <MainNavButtonsUpdate/> : ''}
                </div>
                <div className="wrap">
                    {step === "create" ? <Create/> : ''}
                    {step === "update" ? <Update/> : step === "update buttons navigate" ? <Update/> : ''}

                    {/*{step === "drag image" ? <Drag/> : ''}*/}
                    {step === "scaled logo" ? <Scale/> : ''}
                    {step === "set url" ? <Url/> : ''}
                    {step === "finish" ? <Finish/> : ''}
                </div>
            </div>

        )
    }

}

const putState = (state) => {
    return {
        brand: state.brand,
        step: state.step,
        newBrandCreateJson: state.newBrandCreateJson
    }
}

const putActions = (dispatch) => {
    return {
        putBrandData: (dataBrand) => dispatch(putBrandData(dataBrand)),
        changeParamsBrand: (data) => dispatch(changeParamsBrand(data)),
        nextStep: (value) => dispatch(nextStep(value))
    }
}


export default connect(putState, putActions) (CreateOrUpdate);