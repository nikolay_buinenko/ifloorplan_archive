import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {changeParamsBrand, nextStep, deleteBrandAction} from '../redux/actions/ActionsTypes';
import { sendProperties, deleteBrand } from '../serverRequests/AxiosRequests';
import {sizeBlock} from '../helperFunction';


const Update = (props) => {
    const brandJson = props.newBrandCreateJson[props.newBrandCreateJson.length - 1];

    const logo = props.newBrandCreateJson[props.newBrandCreateJson.length - 1].logo; // get last logo
    const idBrand = props.newBrandCreateJson[props.newBrandCreateJson.length - 1].id;
    const scale = props.newBrandCreateJson[props.newBrandCreateJson.length - 1].width;
    const transparency = props.newBrandCreateJson[props.newBrandCreateJson.length - 1].transparency;

    const [hover, setHover] = useState(transparency);


    const makeChanges = (e) => {
        e.preventDefault();
        props.nextStep("test");
    }


    return(
        <div className="updateBlock contain">
            <div className="imageWrap" onMouseEnter={() => setHover(1)}
        onMouseLeave={() => setHover(transparency)} style={{position: 'absolute', width: scale+'%', opacity: hover, top:brandJson.position_y+"%", left:brandJson.position_x+"%"}}>
                <img style={{width: '100%'}} src={logo} alt=""/>
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        newBrandCreateJson: state.newBrandCreateJson
    }
}

const putDispatch = (dispatch) => {
    return {
        nextStep: (value) => dispatch(nextStep(value))
    }
}
export default connect(putState, putDispatch)(Update);