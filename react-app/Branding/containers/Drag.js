import React, { useState } from 'react';
import { connect } from 'react-redux';
import {changeParamsBrand, nextStep} from '../redux/actions/ActionsTypes';
import Draggable, {DraggableCore} from 'react-draggable';
import { sendProperties } from '../serverRequests/AxiosRequests';
import {sizeBlock} from '../helperFunction';



const Drag = (props) => {

    const [position, setPosition] = useState({
        x: 0,
        y: 0
    })

    const Submit = () => {
        let newState = JSON.parse(JSON.stringify(props.newBrandCreateJson[props.newBrandCreateJson.length - 1]));
        newState.position_x = position.x;
        newState.position_y = position.y;
        sendProperties(newState)
            .then(data => {
                props.changeParamsBrand(data);
                props.nextStep("scaled logo");
            });
    }

    const [activeDrags, setActiveDrags] = useState(0);
    const [isShown, setIsShown] = useState(false);

    const dragHandleImage = (event) => {
        event.preventDefault();
        // const block = document.querySelector(`.${event.target.className}`);
        const childBlock = document.getElementsByClassName("imageWrap")[0];

        const parrentBlock = document.getElementsByClassName("dragWrap")[0];
        let childX = childBlock.getBoundingClientRect().left;
        let childY = childBlock.getBoundingClientRect().top;
        const { top, left, width, height } = parrentBlock.getBoundingClientRect();

        let x = (childX - left) / width * 100; // get position at parsent
        let y = (childY - top) / height * 100;
        setPosition({x:x, y:y})
    }

    const onStart = () => {setActiveDrags(activeDrags + 1)}
    const onStop = () => {setActiveDrags(activeDrags - 1)}
    const logo = props.newBrandCreateJson[props.newBrandCreateJson.length - 1].logo; // get last logo

    const dragHandlers = {onStart: onStart, onStop: onStop};
    return (
        <div className="dragWrap contain">
            <button onClick={Submit}>Submit</button>
            <Draggable bounds="parent" onDrag={dragHandleImage} defaultPosition={{x: 0, y: 0}} {...dragHandlers}>
                <div className="imageWrap" onMouseEnter={() => setIsShown(true)}
                        onMouseLeave={() => setIsShown(false)} style={{position: 'absolute', width: '25%',}}>
                    {/*{isShown && (*/}
                    {/*    <h3>Drag me</h3>*/}
                    {/*)}*/}
                    <img style={{width: '100%'}} src={logo} alt=""/>
                </div>
            </Draggable>
        </div>
    )
}

const putState = (state) => {
    return {
        newBrandCreateJson: state.newBrandCreateJson
    }
}

const putDispatch = (dispatch) => {
    return {
        changeParamsBrand: (data) => dispatch(changeParamsBrand(data)),
        nextStep: (step) => dispatch(nextStep(step))
    }
}

export default connect(putState, putDispatch)(Drag);