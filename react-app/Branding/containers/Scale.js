import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {changeParamsBrand, nextStep} from '../redux/actions/ActionsTypes';
import Draggable, {DraggableCore} from 'react-draggable';
import { sendProperties } from '../serverRequests/AxiosRequests';
import {sizeBlock} from '../helperFunction';



const Scale = (props) => {
    // изменено - теперь область перетаскивания работает только здесь

    const brandJson = props.newBrandCreateJson[props.newBrandCreateJson.length - 1];
    const scaleOLD = props.newBrandCreateJson[props.newBrandCreateJson.length - 1].width;
    const transparencyOLD = props.newBrandCreateJson[props.newBrandCreateJson.length - 1].transparency;
    const [scale, setScale] = useState(scaleOLD);
    const [transparency, setTransparency] = useState(transparencyOLD);
    const logo = props.newBrandCreateJson[props.newBrandCreateJson.length - 1].logo; // get last logo
    const [position, setPosition] = useState({
        x: 0,
        y: 0
    })


    const Save = () => {
        let newState = JSON.parse(JSON.stringify(props.newBrandCreateJson[props.newBrandCreateJson.length - 1]));
        newState.position_x = position.x;
        newState.position_y = position.y;
        newState.width = scale;
        newState.transparency = transparency;
        sendProperties(newState)
            .then(data => {
                props.changeParamsBrand(data);
                if (props.roleType === "UPDATE"){
                    props.nextStep("update buttons navigate");
                } else {
                    props.nextStep("set url");
                }

            });
    }

    const [activeDrags, setActiveDrags] = useState(0);
    const [isShown, setIsShown] = useState(false);

    const dragHandleImage = (event) => {
        event.preventDefault();
        // const block = document.querySelector(`.${event.target.className}`);
        const childBlock = document.getElementsByClassName("imageWrap")[0];

        const parrentBlock = document.getElementsByClassName("scaleWrap")[0];
        let childX = childBlock.getBoundingClientRect().left;
        let childY = childBlock.getBoundingClientRect().top;
        const { top, left, width, height } = parrentBlock.getBoundingClientRect();

        let x = (childX - left) / width * 100; // get position at parsent
        let y = (childY - top) / height * 100;
        setPosition({x:x, y:y})
    }

    const onStart = () => {setActiveDrags(activeDrags + 1)}
    const onStop = () => {setActiveDrags(activeDrags - 1)}

    const dragHandlers = {onStart: onStart, onStop: onStop};
    return  (
        <div className="scaleWrap contain">
                <div className="scalePoint">
                    <div className="scale">
                        <p>Scale</p>
                        <input type="range" onChange={(e) => setScale(e.target.value)} step="1" id="scale" defaultValue={scaleOLD} name="scale" min="0" max="100" />
                    </div>
                    <div className="transparency">
                        <p>Transparency</p>
                        <input type="range" onChange={(e) => setTransparency(e.target.value)} step="0.1" id="transparency" defaultValue={transparencyOLD} name="transparency" min="0" max="1" />
                    </div>
                    <div className="save"><button onClick={Save}>Save</button></div>
                </div>
                {/*<div className="imageWrap" style={{position: 'absolute', width: scale+'%', opacity: transparency, top:brandJson.position_y+"%", left:brandJson.position_x+"%"}}>*/}
                {/*    <img style={{width: '100%'}} src={logo} alt=""/>*/}
                {/*</div>*/}
                <Draggable bounds="parent" onDrag={dragHandleImage} defaultPosition={{x: 0, y: 0}} {...dragHandlers}>
                    <div className="imageWrap" onMouseEnter={() => setIsShown(true)}
                         onMouseLeave={() => setIsShown(false)} style={{position: 'absolute', width: scale+'%', opacity: transparency}}>
                        <img style={{width: '100%'}} src={logo} alt=""/>
                    </div>
                </Draggable>
        </div>
    )
}

const putState = (state) => {
    return {
        newBrandCreateJson: state.newBrandCreateJson,
        roleType: state.roleType,
    }
}

const putDispatch = (dispatch) => {
    return {
        changeParamsBrand: (data) => dispatch(changeParamsBrand(data)),
        nextStep: (value) => dispatch(nextStep(value))
    }
}
export default connect(putState, putDispatch)(Scale);