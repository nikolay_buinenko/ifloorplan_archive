import React from 'react';
import { connect } from 'react-redux';
import {nextStep, changeRole} from '../../redux/actions/ActionsTypes';


const NavUpdateButtons = (props) => {

    const Edit = () => {
        props.nextStep("update buttons navigate");
        props.changeRole("UPDATE");
    }

    return (
        <div className="navUpdateButtons">
            <div>Branding is set up for this account.</div>
            <button className="btn" onClick={Edit}>Edit</button>
        </div>
    )
}


const putDispatch = (dispatch) => {
    return {
        nextStep: (value) => dispatch(nextStep(value)),
        changeRole: (role) => dispatch(changeRole(role))
    }
}
export default connect(null, putDispatch)(NavUpdateButtons);