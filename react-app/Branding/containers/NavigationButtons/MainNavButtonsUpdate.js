import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {nextStep, changeParamsBrand, changeRole } from '../../redux/actions/ActionsTypes';
import { sendProperties, deleteBrand } from '../../serverRequests/AxiosRequests';
import Popup from "reactjs-popup";

const MainNavButtonsUpdate = (props) => {
    const [popup, setPopup] = useState(false);
    const Save = (e) => {
        e.preventDefault();
        props.nextStep("update");
    }

    const Cancel = (e) => {
        e.preventDefault();
        sendProperties(props.brand)
            .then(data => {
                props.changeParamsBrand(data);
                props.nextStep("update");
            });

    }

    const Delete = (e) => {
        e.preventDefault();
        const idBrand = props.brand.id;
        deleteBrand(idBrand)
            .then(()=>{
                props.nextStep("create");
                props.changeRole("CREATE");
            });
    }
    return (
        <div className="navMainUpdateButtons">
            <div className="wrapOptions">
                <a href="" onClick={(e) => {e.preventDefault();props.nextStep("scaled logo")}}>Edit this Logo</a>
                <a href="" onClick={(e) => {e.preventDefault();props.nextStep("set url")}}>Edit clickthrough URL</a>
                <a href="" onClick={(e) => {e.preventDefault();props.nextStep("create")}}>Upload a new Logo</a>
                <a href="" onClick={(e) => {e.preventDefault();setPopup(true)}}>Delete Branding</a>
            </div>
            <div className="wrapButtons">
                <a className="navCancel" href="" onClick={Cancel}>Cancel</a>
                <a className="btn navSave" href="" onClick={Save}>Save</a>
            </div>
            <Popup
                open={popup}
                closeOnDocumentClick
                onClose={()=> setPopup(false)}
            >
                <div className="modal">
                    <p>Are you sure you want to delete this Branding.</p><br/>
                    <p>Branding will disappear from your published iFloorPlans and this cannot be undone.</p><br/>
                    <p>You can upload a new logo and start again.</p>
                    <div className="btnGroup">
                        <button onClick={()=> setPopup(false)}>Cancel</button>
                        <button onClick={Delete}>Delete</button>
                    </div>
                </div>
            </Popup>
        </div>
    )
}

const putState = (state) => {
    return {
        brand: state.brand,
    }
}


const putDispatch = (dispatch) => {
    return {
        deleteBrandAction: (id) => dispatch(deleteBrandAction(id)),
        nextStep: (value) => dispatch(nextStep(value)),
        changeParamsBrand: (data) => dispatch(changeParamsBrand(data)),
        changeRole: (role) => dispatch(changeRole(role))
    }
}
export default connect(putState, putDispatch)(MainNavButtonsUpdate);