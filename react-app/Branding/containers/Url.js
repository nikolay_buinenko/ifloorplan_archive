import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {changeParamsBrand, nextStep, deleteBrandAction} from '../redux/actions/ActionsTypes';
import { sendProperties, deleteBrand } from '../serverRequests/AxiosRequests';
import {sizeBlock} from '../helperFunction';

const Url = (props) => {
    const brandJson = props.newBrandCreateJson[props.newBrandCreateJson.length - 1];
    const logo = props.newBrandCreateJson[props.newBrandCreateJson.length - 1].logo; // get last logo
    const scale = props.newBrandCreateJson[props.newBrandCreateJson.length - 1].width;
    const transparency = props.newBrandCreateJson[props.newBrandCreateJson.length - 1].transparency;

    const Cancel = () => {
        props.nextStep("finish");
    }


    const Save = () => {
        let newState = JSON.parse(JSON.stringify(props.newBrandCreateJson[props.newBrandCreateJson.length - 1]));
        const fieldValue = document.getElementById("urlBrandField").value;
        newState.url = fieldValue;
        sendProperties(newState)
            .then(data => {
                props.changeParamsBrand(data);
                if(props.roleType === "UPDATE"){
                    props.nextStep("update buttons navigate")
                } else {
                    props.nextStep("finish");
                }
            });
    }

    return (
        <div className="urlWrap contain">
            <div className="formUrl">
                <div className="inputField">
                    <label htmlFor="url">Enter a clickthrough link</label>
                    <input type="text" id="urlBrandField" name="url" placeholder="https://www.yourWebsite.com"/>
                </div>
                <div className="buttonGroup">
                    <button onClick={Cancel} className="cancel">Cancel</button>
                    <button onClick={Save} className="save">Save</button>
                </div>
            </div>

            <div className="imageWrap" style={{position: 'absolute', width: scale+'%', opacity: transparency, top:brandJson.position_y+"%", left:brandJson.position_x+"%"}}>
                <img style={{width: '100%'}} src={logo} alt=""/>
            </div>
        </div>
    )
}


const putState = (state) => {
    return {
        newBrandCreateJson: state.newBrandCreateJson,
        roleType: state.roleType,
    }
}

const putDispatch = (dispatch) => {
    return {
        changeParamsBrand: (data) => dispatch(changeParamsBrand(data)),
        nextStep: (value) => dispatch(nextStep(value))
    }
}
export default connect(putState, putDispatch)(Url);