import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import  rootReducer from './redux/reducers/rootReducer';
import { connect } from 'react-redux';
import CreateOrUpdate from './components/CreateOrUpdate';

const store = createStore(rootReducer, applyMiddleware(thunk));


class App extends Component {
    render() {
        return (
            <div className="brendingWrapper">
                <Provider store={store}>
                    <CreateOrUpdate/>
                </Provider>
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('app'));