import {
    UPLOAD_IMAGE_BRAND,
    ADD_LINK_BRAND,
    PUT_BRAND_DATA,
    CSRFTOKEN,
    UPLOAD_IMAGE,
    NEXT_STEP,
    CHANGE_PARAMS_BRAND,
    DELETE_BRAND,
    CHANGE_ROLE,
} from '../constants/BrandConst';

export function changeRole(role){
    return {
        type: CHANGE_ROLE,
        role
    }
}


export function deleteBrandAction(id) {
    return {
        type: DELETE_BRAND,
        id
    }
}


export function nextStep(value) {
    return {
        type: NEXT_STEP,
        value
    }
}


export function uploadImage(file) {
    return {
        type: UPLOAD_IMAGE_BRAND,
        file
    }
}


export function addLink(link) {
    return {
        type: ADD_LINK_BRAND,
        link
    }
}


export function putBrandData(brand){
    return {
        type: PUT_BRAND_DATA,
        brand
    }
}


export function uploadDataImageBrand(dataArray){
    return {
        type: UPLOAD_IMAGE,
        dataArray
    }
}

export function changeParamsBrand(data) {
    return {
        type: CHANGE_PARAMS_BRAND,
        data
    }
}