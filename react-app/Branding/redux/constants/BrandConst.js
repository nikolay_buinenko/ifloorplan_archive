export const PUT_BRAND_DATA = "PUT_BRAND_DATA";
export const UPLOAD_IMAGE = "UPLOAD_IMAGE";
export const UPLOAD_IMAGE_BRAND = "UPLOAD_IMAGE_BRAND";
export const NEXT_STEP = "NEXT_STEP";
export const ADD_LINK_BRAND = "ADD_LINK_BRAND";
export const CHANGE_PARAMS_BRAND = "CHANGE_PARAMS_BRAND";
export const DELETE_BRAND = "DELETE_BRAND";
export const CHANGE_ROLE = "CHANGE_ROLE";


//====================================================
function readCookie(name) {
    //function split cookie for get csrf token
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
export const CSRFTOKEN = readCookie('csrftoken'); //get csrftoken
//======================================================

