import * as types from '../constants/BrandConst';

const initialState = {
    step: 'create',
    image  : null,
    roleType : "CREATE", //type CREATE or UPDATE
    brand: {},
    newBrandCreateJson: []
}


const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.PUT_BRAND_DATA:
            return { ...state, brand: action.brand };
            break;

        case types.CHANGE_ROLE:
            return { ...state, roleType: action.role };
            break;

        case types.UPLOAD_IMAGE:
            return { ...state, newBrandCreateJson: [...state.newBrandCreateJson, action.dataArray] };
            break;

        case types.NEXT_STEP:
            return { ...state, step: action.value };
            break;

        case types.CHANGE_PARAMS_BRAND:
            return { ...state, newBrandCreateJson: [...state.newBrandCreateJson, action.data] };
            break;

        case types.DELETE_BRAND:
            return initialState;
            break;

        default:
           return state;
    }
}

export default rootReducer;