
import axios from 'axios';

//====================================================
function readCookie(name) {
    //function split cookie for get csrf token
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
const CSRFTOKEN = readCookie('csrftoken'); //get csrftoken
//======================================================

export async function deletePlan( data ){
    const config = {
        method: 'DELETE',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            'content-type': 'application/json'
        },
        body: JSON.stringify(data)
    }
    const response = await fetch('https://ifloorplan360.com/api/delete/', config);
    return await response.json();
}


export async function updateNewPlan( data ){
    const config = {
        method: 'PUT',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            'content-type': 'application/json'
        },
        body: JSON.stringify(data)
    }
    const response = await fetch('https://ifloorplan360.com/api/update/', config);
    return await response.json();
}

export async function getPlan( data ){
    const config = {
        method: 'GET',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            'content-type': 'application/json'
        },
    }
    const response = await fetch('https://ifloorplan360.com/api/get/?token=1234567&ifloorplan='+data.plan_id, config);
    return await response.json();
}