import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { CSRFTOKEN } from '../../New/redux/constant/constants';
import { nextStep, ifloorplanStateAdd, setType, LibImageAdd, LibPlanAdd, panelumLib, clearIfloorState } from '../../New/redux/actions/actions';
import UploadPlans from "../../New/components/UploadPlans";
import SingleUploadPlan from "../../New/components/SingleUploadPlan";
import SingleUploadImage from "../../New/components/SingleUploadImage";
import UploadFiles from "../../New/components/UploadFiles";
import UploadImage from "../../New/components/UploadImage";
import TopPanel from "../../New/components/TopPanel";
import ScaleLayout from "../../New/components/ScaleLayout";
import PlanLibrary from "../../New/components/PlanLibrary";
import ImageLibrary from "../../New/components/ImageLibrary";
import CamsPosition from "../../New/components/CamsPosition";
import TextEditor from "../../New/components/TextEditor";
import Panorama from "../../New/components/Panorama";
import Media from "../../New/components/Media";
import Video from "../components/Video";
import ManageLevels from "../../New/components/ManageLevels";
import LatLong from "../../New/components/LatLong";
import UploadSinglePanoramaLib from "../../New/components/UploadSinglePanoramaLib";
import UploadPanoramaLib from "../../New/components/UploadPanoramaLib";
import PanoramaLibrary from "../../New/components/PanoramaLibrary";
import NewLevelCreateOption from "../../New/components/NewLevelCreateOption";
import Clients from "../../New/components/Clients";
import Layout from "../../New/components/Layout";
import KenBurns from "../../New/components/KenBurns";
import UpdateSinglePlan from "../../New/components/UpdateSinglePlan";
import PositionVideos from "../../New/components/PositionVideos";
import { createNewPlan, getTemplates, getBrand, getImages } from "../../New/serverRequests/requests";
import { getPlan } from './../requests';


const NewOrTemp = (props) => {
        const step = props.step;
        const [animationSaved, setAnimationSaved] = useState(0);

        useEffect(()=>{
            const ifloorplan_id = document.getElementById("ifloorplan_id");
            getPlan({plan_id: ifloorplan_id.value})
                .then(data=>{
                    props.ifloorplanStateAdd(data[0]);
                    props.setType('edit');
                    props.nextStep("cams position", "edit");
                })
            const config = {
                headers: {
                    "X-CSRFToken": CSRFTOKEN,
                    'content-type': 'application/json'
                }
            }
        getImages("https://ifloorplan360.com/api/imageplanuload/?id="+ifloorplan_id.value, {ifloorplan_id:ifloorplan_id.value}, config)
            .then((res)=>{
                props.LibPlanAdd(res.data);  //add to library
            })

        getImages("https://ifloorplan360.com/api/imageupload/?id="+ifloorplan_id.value, {ifloorplan_id:ifloorplan_id.value}, config)
            .then((res)=>{
                props.LibImageAdd(res.data);  //add to library
            })

        getImages("https://ifloorplan360.com/api/panelum/?id="+ifloorplan_id.value, {ifloorplan_id:ifloorplan_id.value}, config)
            .then((res)=>{
                props.panelumLib(res.data);  //add to library
            })


        }, [])


        return (
            <div className={step === "" ? "wrap" : step}>
                {step !== "" ? <TopPanel setAnimation={setAnimationSaved} animation={animationSaved}/> : ''}
                {step === "upload plans" ? <UploadPlans setAnimation={setAnimationSaved}/> : ''}
                {step === "upload files" ? <UploadFiles setAnimation={setAnimationSaved}/> : ''}
                {step === "upload images" ? <UploadImage setAnimation={setAnimationSaved}/> : ''}
                {step === "scale layout" ? <ScaleLayout setAnimation={setAnimationSaved}/> : ''}
                {step === "scale and repos" ? <ScaleLayout setAnimation={setAnimationSaved}/> : ''}
                {step === "plan library" ? <PlanLibrary setAnimation={setAnimationSaved}/> : ''}
                {step === "image library" ? <ImageLibrary setAnimation={setAnimationSaved}/> : ''}
                {step === "single plan upload" ? <SingleUploadPlan setAnimation={setAnimationSaved}/> : ''}
                {step === "cams position" ? <CamsPosition setAnimation={setAnimationSaved}/> : ''}
                {step === "simple upload image" ? <SingleUploadImage setAnimation={setAnimationSaved}/> : ''}
                {step === "new level create option" ? <NewLevelCreateOption setAnimation={setAnimationSaved}/> : ''}
                {step === "text editor" ? <TextEditor setAnimation={setAnimationSaved}/> : ''}
                {step === "add video" ? <Video setAnimation={setAnimationSaved}/> : ''}
                {step === "add media" ? <Media setAnimation={setAnimationSaved}/> : ''}
                {step === "panorama" ? <Panorama setAnimation={setAnimationSaved}/> : ''}
                {step === "manage levels" ? <ManageLevels setAnimation={setAnimationSaved}/> : ''}
                {step === "add lat and long" ? <LatLong setAnimation={setAnimationSaved}/> : ''}
                {step === "360 library" ? <PanoramaLibrary setAnimation={setAnimationSaved}/> : ''}
                {step === "single 360" ? <UploadSinglePanoramaLib setAnimation={setAnimationSaved}/> : ''}
                {step === "360 library upload" ? <UploadPanoramaLib setAnimation={setAnimationSaved}/> : ''}
                {step === "panorama single upload file" ? <UploadSinglePanoramaLib setAnimation={setAnimationSaved}/> : ''}
                {step === "clients" ? <Clients setAnimation={setAnimationSaved}/> : ''}
                {step === "layout" ? <Layout setAnimation={setAnimationSaved}/> : ''}
                {step === "ken burns" ? <KenBurns setAnimation={setAnimationSaved}/> : ''}
                {step === "single plan update image" ? <UpdateSinglePlan setAnimation={setAnimationSaved}/> : ''}
                {step === "position videos" ? <PositionVideos setAnimation={setAnimationSaved}/> : ''}

            </div>
        )
}

const putState = (state) => {
    return {
        step: state.step,
        dropFiles: state.dropFiles,
        dropFilesTitle: state.dropFilesTitle,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        LibPlanAdd: (array) => dispatch(LibPlanAdd(array)),
        LibImageAdd: (array) => dispatch(LibImageAdd(array)),
        panelumLib: (items) => dispatch(panelumLib(items)),
        setType: (key) => dispatch(setType(key)),
        clearIfloorState: () => dispatch(clearIfloorState())
    }
}


export default connect(putState, putActions) (NewOrTemp);