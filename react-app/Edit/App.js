import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { connect } from 'react-redux';
// import Published from './components/Published';
import rootReducer from '../New/redux/reducer/rootReducer';
import NewOrTemp from './containers/NewOrTemp';

const store = createStore(rootReducer, applyMiddleware(thunk));

class App extends Component {
    render() {
        return (
            <div className="PublishedWrapper">
                <Provider store={store}>
                    <NewOrTemp/>
                </Provider>
            </div>
        );
    }
}
ReactDOM.render(<App />, document.getElementById('app'));