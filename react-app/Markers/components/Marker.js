import React, {useState, useEffect} from 'react';


const Marker = (props) => {
    const [marker, setMarker] = useState(props.data.marker);
    return (
        <div className="item"
             onMouseEnter={()=>{setMarker(props.data.marker_hover)}}
             onMouseLeave={()=>{setMarker(props.data.marker)}}
             onClick={()=>{props.setIdToDelete(props.data.id);props.setDeletePopup(true)}}>
            <img src={marker} alt=""/>
            <div className="del">
                <i className="fas fa-times"></i>
            </div>
        </div>
    )
}
export default Marker;