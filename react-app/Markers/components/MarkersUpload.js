import React from 'react';

export default function MarkersUpload({appointment}) {
    return (
        <div className="markers__upload">
            <h4>Upload Markers for {appointment}</h4>
            <div className="markers__upload_wrap">
                <p>Drop 2 files here for {appointment} Markers</p>
                <p>Or</p>
                <button>Choose files</button>
            </div>
        </div>
    )
}