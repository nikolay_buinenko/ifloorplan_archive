import React, {useCallback, useState} from 'react';
import {useDropzone} from 'react-dropzone';
import axios from 'axios';
import Popup from 'reactjs-popup';


const UploadMarkers = ({appointment, uploadType, updateData}) => {

    // Functions for Modal
    const [open, setOpen] = useState(false);
    const closeModal = () => setOpen(false);
    const tooManyFiles = () => setOpen(val => !val);
    // Maximum upload file size in bytes
    const maxUploadSize = 5242880;

    const onDrop = useCallback((acceptedFiles) => {

        let form_data = new FormData();
        const CSRFTOKEN = readCookie('csrftoken'); //get csrftoken
        // Selecting the type of the loaded Marker
        let typeOfUpload;
        if (appointment === 'Images') {
            typeOfUpload = "IMAGE"
        } else if (appointment === 'Video') {
            typeOfUpload = "VIDEO"
        } else if (appointment === '360 Images') {
            typeOfUpload = "PANORAMA"
        }

        function readCookie(name) {
            //function split cookie for get csrf token
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        if (acceptedFiles.length > 2) {
            tooManyFiles();
        } else if (acceptedFiles.length == 2) {
            form_data.append('marker', acceptedFiles[0], acceptedFiles[0].name);
            form_data.append('marker_hover', acceptedFiles[1], acceptedFiles[1].name);
            form_data.append('type', typeOfUpload);

            // console.log(form_data)
            // console.log(acceptedFiles)
            // console.log(acceptedFiles[0])
            // console.log(acceptedFiles[0].name)
            // console.log(acceptedFiles[1])
            // console.log(acceptedFiles[1].name)

            axios({
                url: 'https://ifloorplan360.com/api/user_markers/',
                method: 'POST',
                data: form_data,
                headers: {
                    "X-CSRFToken": CSRFTOKEN,
                    'Content-Type': 'multipart/form-data'
                }
            })
                .then(data => {
                    // We create an object with data and transfer it to the App
                    let dataApp = new Object();
                    dataApp.id = data.data.id
                    dataApp.marker = data.data.marker
                    dataApp.marker_hover = data.data.marker_hover
                    dataApp.type = data.data.type
                    dataApp.openSwap = true
                    dataApp.files = acceptedFiles
                    dataApp.appointment = appointment
                    updateData(dataApp)
                })
        }
    }, []);

    const {isDragActive, getRootProps, getInputProps, isDragReject, acceptedFiles, rejectedFiles} = useDropzone({
        onDrop,
        accept: "image/png, image/svg+xml, image/gif",
        minSize: 0,
        maxSize: maxUploadSize,
    });
    // const isFileTooLarge = rejectedFiles.length > 0 && rejectedFiles[0].size > maxUploadSize;

    return (
        <div className="markers__upload">

            <Popup
                open={open}>
                <div>
                    <p>You can only upload 2 files for each Marker</p>
                    <a className="close" onClick={closeModal}>Ok</a>
                </div>
            </Popup>
            <h4>Upload Markers for {appointment}</h4>
            <div {...getRootProps()} className="markers__upload_wrap">
                <p>{!isDragActive && 'Drop 2 files here for' + ' ' + appointment + ' ' + 'Markers'}
                    {isDragActive && !isDragReject && "Drop it like it's "}
                    {isDragReject && "File type not accepted, "}</p>
                <p>{!isDragActive && 'Or'}
                    {isDragActive && !isDragReject && "hot!"}
                    {isDragReject && "sorry!"}</p>
                <input {...getInputProps()} id="uploadFile"/>
                <button>Choose files</button>
                {/*{!isDragActive && 'Choose files'}*/}
                {/*{isDragActive && !isDragReject && "Drop it like it's hot!"}*/}
                {/*{isDragReject && "File type not accepted, sorry!"}*/}
                {/*{isFileTooLarge && (*/}
                {/*    <div className="text-danger mt-2">*/}
                {/*        File is too large.*/}
                {/*    </div>*/}
                {/*)}*/}
            </div>
        </div>
    );
};

export default UploadMarkers;