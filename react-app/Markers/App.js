import React, {useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import {ChromePicker} from 'react-color';
import axios from 'axios';
import UploadMarkers from './components/UploadMarkers.js';
import Popup from 'reactjs-popup';
import Marker from './components/Marker';
import {getUserIcons} from './../New/serverRequests/requests';

export default function App() {

  const [swapMarkersOpen, setSwapMarkersOpen] = useState(false);
  const [swapMarkersMarker, setSwapMarkersMarker] = useState([]);
  const [swapMarkersMarkerHov, setSwapMarkersMarkerHov] = useState([]);
  const [swapMarkersType, setSwapMarkersType] = useState([]);

  const [idToDelete, setIdToDelete] = useState();
  const [deletePopup, setDeletePopup] = useState(false);

  const [listMarkers, setListMarkers] = useState({
    image: [],
    video: [],
    panelum: []
  })

  const [uploadFilesMarker, setUploadFilesMarker] = useState([]);
  const [uploadFilesMarkerHov, setUploadFilesMarkerHov] = useState([]);
  const [uploadFilesId, setUploadFilesId] = useState([]);
  const [markersAppointment, setMarkersAppointment] = useState([]);

  const [openPopup, setOpenPopup] = useState(false);

  const CSRFTOKEN = readCookie('csrftoken'); //get csrftoken
    function readCookie(name) {
      //function split cookie for get csrf token
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
      }
      return null;
    }

  // The function gets the data object from the UploadMarkers
  const updateData = (value) => {
    setSwapMarkersOpen(value.openSwap)
    setSwapMarkersMarker(value.marker)
    setSwapMarkersMarkerHov(value.marker_hover)
    setSwapMarkersType(value.type)
    setUploadFilesId(value.id)
    setUploadFilesMarker(value.files[0])
    setUploadFilesMarkerHov(value.files[1])
    setMarkersAppointment(value.appointment)
  }

  // Function swaps Markers and MarkersHover
  const swapper = () => {
    let bufer = swapMarkersMarkerHov
    setSwapMarkersMarkerHov(swapMarkersMarker)
    setSwapMarkersMarker(bufer)
    let buferFiles = uploadFilesMarker
    setUploadFilesMarker(uploadFilesMarkerHov)
    setUploadFilesMarkerHov(buferFiles)

  }
  // The function updates tokens on the server
  const updateMarkers = () => {
    let form_data = new FormData();

    form_data.append('id', uploadFilesId)
    form_data.append('marker', uploadFilesMarker, uploadFilesMarker.name);
    form_data.append('marker_hover', uploadFilesMarkerHov, uploadFilesMarkerHov.name);
    axios({
      url: 'https://ifloorplan360.com/api/user_markers/',
      method: 'PUT',
      data: form_data,
      headers: {
        "X-CSRFToken": CSRFTOKEN,
        'Content-Type': 'multipart/form-data'
      }
    })
    setOpenPopup(true)
  }

  // The function removes tokens on the server
  const deleteMarkers = () => {
    let form_data = new FormData();

    form_data.append('id', uploadFilesId)
    axios({
      url: 'https://ifloorplan360.com/api/user_markers/',
      method: 'DELETE',
      data: form_data,
      headers: {
        "X-CSRFToken": CSRFTOKEN,
        'Content-Type': 'multipart/form-data'
      }
    })
    setSwapMarkersOpen(!swapMarkersOpen)
  }

// The function closes the PopUp
  const closePopup = () => {
    setSwapMarkersOpen(!swapMarkersOpen)
    setOpenPopup(false)
  }

  const initMarkers = () => {
      const imgMark = [];
      const vidMark = [];
      const panMark = [];
      getUserIcons()
          .then(data=>{
            for(let i = 0; i < data.length; i++){
              let marker = <Marker
                  key={i}
                  setIdToDelete={setIdToDelete}
                  setDeletePopup={setDeletePopup}
                  data={data[i]}
              />
              if(data[i].type == "IMAGE"){
                imgMark.push(marker)
              } else if(data[i].type == "VIDEO"){
                vidMark.push(marker)
              } else {
                panMark.push(marker)
              }
            }
            setListMarkers({image: imgMark, video: vidMark, panelum: panMark});
          })
  }
  console.log(idToDelete)
  const DeleteMarker = () => {
    setDeletePopup(false);
    let form_data = new FormData();

    form_data.append('id', idToDelete);
    axios({
      url: 'https://ifloorplan360.com/api/user_markers/',
      method: 'DELETE',
      data: form_data,
      headers: {
        "X-CSRFToken": CSRFTOKEN,
        'Content-Type': 'multipart/form-data'
      }
    }).then(()=>{
      initMarkers();
    })
  }

  //After the component is mounted, load the previously saved colors of the pop-ups from the server. If this is the first user login, then load the default colors
  useEffect(() => {
    initMarkers()
  }, [])
  useEffect(()=>{
    initMarkers()
  }, [swapMarkersOpen]);



  if (swapMarkersOpen == false) {
    return (
      <div className="markersWrapper">
        <div className="markers__title">
          <h2>Markers</h2>
          <p>Upload your own Markers set for images, video or 360 images. Choose colours for you buttons</p>
        </div>
        <div className="markers__upload_instructions">
          <p>To create you own Markers to replace the default blue arrow and red active arrow marker, upload 2
             files.</p>
          <p>The first file shows where the image was taken from. The second file is for the hover and shows
             the active Marker.</p>
          <p>The files can be uploaded in the following formats: .png, .gif or .svg. The files must be on a
             transparent background. The files should be about 80 x 80 pixels.</p>
        </div>
        <div className="markers__uploads">
          <UploadMarkers appointment={'Images'} updateData={updateData}/>
          <UploadMarkers appointment={'Video'} updateData={updateData}/>
          <UploadMarkers appointment={'360 Images'} updateData={updateData}/>
        </div>

        <div className="markers_list">
          <div className="images_list markers__list_item">
            {listMarkers.image}
          </div>
          <div className="videos_list markers__list_item">
            {listMarkers.video}
          </div>
          <div className="panelum_list markers__list_item">
            {listMarkers.panelum}
          </div>
        </div>
        <Popup open={deletePopup}
               closeOnDocumentClick={()=>setDeletePopup(false)}
               className='popup_done'>
          <div>
            <p>If you delete a marker, then all published plans associated with it will change the marker to the default.</p>
            <p>You can change them on the published tab.</p>
            <button className="cancel" onClick={()=>{setDeletePopup(false);setIdToDelete("")}}>Cancel</button>
            <button className="but_delete_marker" onClick={DeleteMarker}>Delete</button>
          </div>
        </Popup>
      </div>
    )
  } else if (swapMarkersOpen == true) {
    return (

      <div className="markersWrapper">

        <Popup open={openPopup}
               closeOnDocumentClick={false}
               className='popup_done'>
          <div>
            <p>The {markersAppointment} Marker set has been saved.</p>
            <p>To use these Markers when creating a new iFloorPlan:</p>
            <p>Tools menu > {markersAppointment} > My Markers</p>
            <a className='done' onClick={closePopup}>Done</a>
          </div>
        </Popup>

        <div className="markers__title">
          <h2>Markers</h2>
          <p>Upload your own Markers set for images, video or 360 images. Choose colours for you buttons</p>
        </div>
        <div className="swapMarkers">
          <div className="swapMarkers__title">
            <h4>New {markersAppointment} Markers</h4>
          </div>
          <div className="swapMarkers__swaper">
            <div className="swapMarkers__swaper_pic">
              <h4>Normal</h4>
              <img src={swapMarkersMarker} alt="marker"/>
            </div>
            <div className="swapMarkers__swaper_pic swap">
              <div className="pic" onClick={swapper}>
                <h4>Swap</h4>
                <svg xmlns="http://www.w3.org/2000/svg" width="53" height="12" viewBox="0 0 53 12" fill="none">
                  <path d="M53 11L3 11L15 1" stroke="#1276EB" strokeWidth="2"/>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="53" height="12" viewBox="0 0 53 12" fill="none">
                  <path d="M0 1H50L38 11" stroke="#1276EB" strokeWidth="2"/>
                </svg>
              </div>
            </div>
            <div className="swapMarkers__swaper_pic">
              <h4>Active/Hover</h4>
              <img src={swapMarkersMarkerHov} alt="marker"/>
            </div>
          </div>
          <div className="swapMarkers__btns">
            <button className='discard'
                    onClick={deleteMarkers}>Discard
            </button>
            <button className='save'
                    onClick={updateMarkers}>Save
            </button>
          </div>
        </div>
      </div>
    )
  }
}

ReactDOM.render(<App/>, document.getElementById('app'));