import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { connect } from 'react-redux';
import Main from './containers/Main';
import rootReducer from './redux/reducers/rootReducer';

const store = createStore(rootReducer, applyMiddleware(thunk));


class App extends Component {
    render() {
        return (
            <div className="NewWrapper">
                <Provider store={store}>
                    <Main />
                </Provider>
            </div>
        );
    }
}
ReactDOM.render(<App />, document.getElementById('app'));