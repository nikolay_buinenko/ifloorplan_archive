import * as types from '../constants/constants';

export function setMarkerActivity(bool) {
    return {
        type: types.MARKERS_ACTIVITY,
        bool
    }
}


export function setMarkerNow(index) {
    return {
        type: types.MARKER_NOW,
        index
    }
}


export function initImage(image) {
    return {
        type: types.IMAGE,
        image
    }
}

export function initLevel(level) {
    return {
        type: types.LEVEL,
        level
    }
}


export function initStateIFL(obj) {
    return {
        type: types.IFL_STATE,
        obj
    }
}

export function changeStep(step) {
    return {
        type: types.NOW_STEP,
        step
    }
}