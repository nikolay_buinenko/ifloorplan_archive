import * as types from '../constants/constants';

const initialState = {
    step: '',
    now_image: 0,
    now_level: 0,
    now_marker: 0,
    marker_activity: false,
    ifl_state: {},
}


const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.MARKER_NOW:
            return { ...state, now_marker: action.index };
            break;

        case types.MARKERS_ACTIVITY:
            return { ...state, marker_activity: action.bool };
            break;

        case types.LEVEL:
            return { ...state, now_level: action.level };
            break;

        case types.IMAGE:
            return { ...state, now_image: action.image };
            break;

        case types.IFL_STATE:
            return { ...state, ifl_state: action.obj };
            break;

        case types.NOW_STEP:
            return { ...state, step: action.step };
            break;


        default:
           return state;
    }
}

export default rootReducer;