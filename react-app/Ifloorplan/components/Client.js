import React, {useState} from 'react';

const Client = (props) => {
    if(Object.keys(props.clientPosition).length === 0){
        return null;
    }
    const clientNameStyle = {
        top: props.clientPosition.position_brand_y+"%",
        left: props.clientPosition.position_brand_x+"%"
    }
    const contactNameStyle = {
        top: props.clientPosition.position_name_y+"%",
        left: props.clientPosition.position_name_x+"%"
    }
    const contactPhoneStyle = {
        top: props.clientPosition.position_phone_y+"%",
        left: props.clientPosition.position_phone_x+"%"
    }
    const contactEmailStyle = {
        top: props.clientPosition.position_contact_y+"%",
        left: props.clientPosition.position_contact_x+"%"
    }
    const clientLogoStyle = {
        width: props.clientPosition.width+"%",
        top: props.clientPosition.position_logo_y+"%",
        left: props.clientPosition.position_logo_x+"%"
    }



    return(
        <div className="client_wrap">
            <div className="logo-wrapper">
                {/*<p className="client_name" style={clientNameStyle}>{props.clientData.client_name}</p>*/}
                <p className="contact_name" style={contactNameStyle}>{props.clientData.contact_name}</p>
                <p className="contact_phone" style={contactPhoneStyle}>{props.clientData.contact_phone}</p>
                <p className="contact_email" style={contactEmailStyle}>{props.clientData.contact_email}</p>
                <img className="client-logo" src={props.clientPosition.logo} style={clientLogoStyle}/>
            </div>
        </div>
    )
}
export default Client;