import React, {useState, useEffect} from 'react';


const Media = (props) => {
    const [frame, setFrame] = useState('');

    const init = () => {
        const url = new URL(props.mediaData.url);
        let parametr;
        if(props.mediaData.type == "video"){
            parametr = url.searchParams.get('v');
            setFrame(<iframe id="ytplayer" type="text/html"
                               src={"https://www.youtube.com/embed/"+parametr}
                               frameBorder="0"/>);
        } else {
            parametr = url.searchParams.get('m');
            setFrame(<iframe src={"https://my.matterport.com/show/?m="+parametr} frameBorder="0"
                        allowFullScreen allow="xr-spatial-tracking"></iframe>);
        }
    }

    useEffect(()=>{
        init();
    }, [props.index]);

    useEffect(()=>{
        init();
    }, [])

    return (
        <div className="media_wrap">
            <div className="address_slide_wrap">
                <div className="address_slide">{props.address}</div>
            </div>
            <div className="media">
                {frame}
            </div>
        </div>
    )
}
// https://my.matterport.com/show/?m=roWLLMMmPL8
export default Media;