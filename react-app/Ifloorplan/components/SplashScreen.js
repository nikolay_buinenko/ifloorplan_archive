import React, {useState, useEffect} from 'react';


const SplashScreen = (props) => {
    const [countImages, setCountImages] = useState(0);
    const [countVideo, setCountVideo] = useState(0);

    useEffect(()=>{
        let img_counter = 0;
        let vid_counter = 0;
        for(let i = 0; i < props.data.level.length; i++){
            for(let j = 0; j < props.data.level[i].images.length; j++){
                img_counter++;
            }
        }
        for(let i = 0; i < props.data.level.length; i++){
            for(let j = 0; j < props.data.level[i].video_marker.length; j++){
                vid_counter++;
            }
        }
        setCountImages(img_counter);
        setCountVideo(vid_counter);
    }, [])

    return (
        <div className="splash_screen">
            <div className="splash_img">
                <img src={props.data.splash_screen} alt=""/>
            </div>
            <div className="info_for_ifloorplan">
                <div className="address_ifi">{props.data.address}</div>
                <div className="other_info_ifi">{countImages} images | {countVideo} Videos</div>
            </div>
        </div>
    )
}
export default SplashScreen;