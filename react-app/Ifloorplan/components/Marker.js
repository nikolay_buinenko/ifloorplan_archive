import React, {useState, useEffect} from 'react';
import {sizeBlock} from '../helperFunction';
import {setMarkerNow, setMarkerActivity} from '../redux/actions/actions';
import {connect} from 'react-redux';

const Marker = (props) => {
    let style = {};
    let rotation = 0;
    if (props.action === "image marker") {
        style = {
            position: 'absolute',
            width: props.data.cam_width + "%",
            top: props.data.cam_pos_y + "%",
            left: props.data.cam_pos_x + "%",
            zIndex: '123'
        };
        rotation = props.data.cam_rotation;
    }
    if (props.action === "video marker") {
        style = {
            position: 'absolute',
            width: props.data.cam_width + "%",
            top: props.data.pos_y + "%",
            left: props.data.pos_x + "%",
            zIndex: '123'
        };
        rotation = 0;
    }
    if (props.action === "panelum marker") {
        style = {
            position: 'absolute',
            width: props.data.cam_width + "%",
            top: props.data.pos_y + "%",
            left: props.data.pos_x + "%",
            zIndex: '123'
        };
        rotation = 0;
    }

    console.log('index', props.index)

    return (
        <div className="markerWrap"
             style={style}
             onClick={() => {
                 const heigth_image = document.getElementsByClassName("ifloorPlanSlider")[0].offsetHeight;
                 if (props.action === "video marker") {
                     props.setMedia(props.data.video);
                     props.setIndexMarker(props.index);
                     props.setMediaType("video");
                     props.setHeigthImage(heigth_image);
                 }
                 if (props.action === "panelum marker") {
                     props.setMedia(props.data.img);
                     props.setIndexMarker(props.index);
                     props.setMediaType("panorama");
                     props.setHeigthImage(heigth_image);
                 }
                 if (props.action === "image marker" && props.ifl_state.level[props.now_level].images[props.index].button) {
                     props.setMedia(props.data.img);
                     props.setIndexMarker(props.index);
                     props.setMediaType("image");
                 }
             }}
             onMouseEnter={() => {
                 if (props.action === "image marker" && !props.ifl_state.level[props.now_level].images[props.index].button) {
                     props.setMedia(props.data.img);
                     props.setIndexMarker(props.index);
                     props.setMediaType("image");
                 }
             }}
        >

            {
                props.action === "image marker" && props.ifl_state.level[props.now_level].images[props.index].button
                    ?
                    <button style={{
                        position: "absolute",
                        background: props.ifl_state.level[props.now_level].images[props.index].button_bg,
                        color: props.ifl_state.level[props.now_level].images[props.index].button_color_text,
                        transform: `scale(${props.ifl_state.level[props.now_level].images[props.index].button_scale})`,
                        padding: `${props.ifl_state.level[props.now_level].images[props.index].button_padding_vert}rem ${props.ifl_state.level[props.now_level].images[props.index].button_padding_gor}rem`,
                        borderRadius: props.ifl_state.level[props.now_level].images[props.index].button_radius+"rem"
                    }}>
                        {props.ifl_state.level[props.now_level].images[props.index].button_label}
                    </button>
                    :
                    <img style={{width: '100%', transform: `rotate(${rotation}deg)`}}
                         src={props.active ? props.data.cam_img_hover : props.data.cam_img} className="marker" alt=""/>
            }
        </div>
    )
}

const putState = (state) => {
    return {
        ifl_state: state.ifl_state,
        now_level: state.now_level,
        now_image: state.now_image
    }
}

const putActions = (dispatch) => {
    return {
        changeStep: (step) => dispatch(changeStep(step)),
        initStateIFL: (obj) => dispatch(initStateIFL(obj)),
        initImage: (image) => dispatch(initImage(image)),
        setMarkerActivity: (bool) => dispatch(setMarkerActivity(bool)),
        setMarkerNow: (index) => dispatch(setMarkerNow(index)),
    }
}

export default connect(putState, putActions)(Marker);