import React, {useState, useEffect} from 'react';
import Popup from "reactjs-popup";
import parse from 'html-react-parser';

const Video = (props) => {

    const [popup, setPopup] = useState(props.action);
    const [videoLink, setVideoLink] = useState(props.videoList[0].video);
    const [active, setActive] = useState(0);

    const [listVideo, setListVideo] = useState([]);
    useEffect(()=>{
        const arr = [];
        for(let i = 0; i < props.videoList.length; i++){
            arr.push(
                <div className={`video_item ${active == i ? "active" : ""}`} key={i}
                     onClick={()=>{
                         setActive(i);
                         setVideoLink(props.videoList[i].video);
                     }}
                >
                    {props.videoList[i].label}
                </div>
            )
        }
        setListVideo(arr);
    }, [])

    useEffect(()=>{
        const arr = [];
        for(let i = 0; i < props.videoList.length; i++){
            arr.push(
                <div className={`video_item ${active == i ? "active" : ""}`} key={i}
                     onClick={()=>{
                         setActive(i);
                         setVideoLink(props.videoList[i].video);
                     }}
                >
                    {props.videoList[i].label}
                </div>
            )
        }
        setListVideo(arr);
    }, [active]);

    return(
        <div className="video_popup">
            <Popup
                open={props.action}
                closeOnDocumentClick
                onClose={()=> {
                    return
                }}
            >
                <div className="modal">
                    <div className="frame_video">
                        {parse(videoLink)}
                    </div>
                    <div className="list_video">
                        {listVideo}
                    </div>
                </div>
            </Popup>
        </div>
    )
}

export  default Video;