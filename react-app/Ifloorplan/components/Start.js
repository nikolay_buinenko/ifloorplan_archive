import React, {useEffect, useState, useRef} from 'react';
import {connect} from 'react-redux';
import {initLevel, setMarkerActivity} from '../redux/actions/actions';
import Marker from './Marker';
import {Pannellum} from "pannellum-react";
import Client from './Client';
import SlidePanelMain from './SlidePanelMain';
import SplashScreen from './SplashScreen';
import MapWrapPopup from './MapWrapPopup';
import ZoomWindow from './ZoomWindow';
import Media from './Media';
import IfloorplanSlider from './IfloorplanSlider';
import Popup from 'reactjs-popup';
import parse from 'html-react-parser';

const Start = (props) => {
    const [activeButtonMore, setActiveButtonMore] = useState(false); //button more action enable/disable
    const [activeMarkers, setActiveMarkers] = useState(true);   //arrows hide/visible

    const [slidePlay, setSlidePlay] = useState(false);

    const [zoom, setZoom] = useState(false);

    const [splashScreen, setSplashScreen] = useState(true);

    const [heigthImage, setHeigthImage] = useState(0);
    const [findedHeight, setFindedHeight] = useState(false);

    const [media, setMedia] = useState("");
    const [mediaType, setMediaType] = useState("image");   // image/video/panorama
    const [indexMarker, setIndexMarker] = useState(0);
    const [markersData, setMarkersData] = useState();  //  [{action:string, data:{}}]

    const [markers, setMarkers] = useState([]);  //render markers

    const [mapStart, setMapStart] = useState(false);  //action map enable/disable

    const [slideToolsHover, setSlideToolsHover] = useState(false); // hidden/visible icons slider

    const [tabsStyle, setTabsStyle] = useState(JSON.parse(props.ifl_state.tabs_style));
    const [tabs, setTabs] = useState([]);  //render tabs array
    const [mediaTabs, setMediaTabs] = useState([]);

    const [sliderData, setSliderData] = useState([]);

    const [mediaIndex, setMediaIndex] = useState();  // index active media screen
    const [mediaScreen, setMediaScreen] = useState(false); // enable/disable media screen

    const [textStyle, setTextStyle] = useState({
        fontWeight: props.ifl_state.addressFontWeight,
        fontStyle: props.ifl_state.addressFontStyle,
        fontSize: props.ifl_state.addressFontSize + 'vh',
        fontFamily: props.ifl_state.addressFontFamily,
        color: props.ifl_state.textColor,
        position: 'absolute',
        top: props.ifl_state.addressPosition_y + "%",
        left: props.ifl_state.addressPosition_x + "%",
        transition: "0.5s"
    });
    const [imageWrapStyle, setImageWrapStyle] = useState({
        position: 'absolute',
        width: props.ifl_state.level[props.now_level].img_width + '%',
        top: props.ifl_state.level[props.now_level].img_pos_y + "%",
        left: props.ifl_state.level[props.now_level].img_pos_x + "%"
    });
    const [planWrapStyle, setPlanWrapStyle] = useState({
        position: 'absolute',
        width: props.ifl_state.level[props.now_level].plan_width + '%',
        top: props.ifl_state.level[props.now_level].plan_pos_y + "%",
        left: props.ifl_state.level[props.now_level].plan_pos_x + "%"
    });
    const [clientWrapStyle, setClientWrapStyle] = useState({
        position: "absolute",
        top: props.ifl_state.client_block_y + "%",
        left: props.ifl_state.client_block_x + "%",
        transform: 'scale(' + props.ifl_state.client_scale + ')'
    })

    const [showInfoModal, setShowInfoModal] = useState(false);
    const [contentInfoModal, setContentInfoModal] = useState(props.ifl_state.textBlock)

    useEffect(() => {
        setPlanWrapStyle({
            ...planWrapStyle,
            width: props.ifl_state.level[props.now_level].plan_width + '%',
            top: props.ifl_state.level[props.now_level].plan_pos_y + "%",
            left: props.ifl_state.level[props.now_level].plan_pos_x + "%"
        })
    }, [props.now_level]);


    useEffect(() => {
        zoom
            ?
            setTextStyle({...textStyle, left: "15%", position: "fixed", top: "1%"})
            :
            setTextStyle({
                ...textStyle,
                left: props.ifl_state.addressPosition_x + "%",
                position: "absolute",
                top: props.ifl_state.addressPosition_y + "%"
            })
    }, [zoom])

    const tabsInit = () => {
        const tabs_ready = [];
        const media_tabs = [];
        if (props.ifl_state.level.length == 1 && props.ifl_state.media.length == 0) {
            return 1;
        }
        props.ifl_state.level.map((value, index) => {
            if (index == props.now_level) {
                tabs_ready.push(<div className="tab active_tab" key={index}>{value.tabLabel}</div>)
            } else {
                tabs_ready.push(<div className="tab" onClick={() => {
                    props.initLevel(index);
                }} key={index}>{value.tabLabel}</div>)
            }
        });
        props.ifl_state.media.map((value, index) => {
            media_tabs.push(<div className={mediaIndex == index ? "tab active_tab" : "tab"} onClick={event => {
                if (event.target.className != "tab active_tab") {
                    setMediaIndex(index);
                    setMediaScreen(true);
                } else {
                    setMediaScreen(false);
                    setMediaIndex('null');
                }
            }} key={index}>{value.label}</div>)
        })
        setMediaTabs(media_tabs);
        setTabs(tabs_ready);
    }

    const buttonMore = (e) => {
        e.preventDefault();
        if (activeButtonMore) {
            setActiveButtonMore(false);
        } else {
            setActiveButtonMore(true);
        }
    }


    const initMarkers = () => {
        //init all markers
        const imagesArray = props.ifl_state.level[props.now_level].images;
        const videoArray = props.ifl_state.level[props.now_level].video_marker;
        const panelumArray = props.ifl_state.level[props.now_level].panelum;
        const data = [];
        for (let i = 0; i < imagesArray.length; i++) {
            if (imagesArray[i].cam_pos_x !== 0) {
                data.push({action: "image marker", data: imagesArray[i]});
            }
        }
        for (let i = 0; i < videoArray.length; i++) {
            if (videoArray[i].pos_x !== 0) {
                data.push({action: "video marker", data: videoArray[i]});
            }
        }
        for (let i = 0; i < panelumArray.length; i++) {
            if (panelumArray[i].pos_x !== 0) {
                data.push({action: "panelum marker", data: panelumArray[i]});
            }
        }
        const render = [];
        for (let i = 0; i < data.length; i++) {
            render.push(<Marker key={i}
                                index={i}
                                action={data[i].action}
                                setHeigthImage={setHeigthImage}
                                data={data[i].data}
                                active={indexMarker == i ? true : false}
                                setIndexMarker={setIndexMarker}
                                setMedia={setMedia}
                                setMediaType={setMediaType}
            />)
        }
        setMarkersData(data);
        setMarkers(render);
    }

    useEffect(() => {
        initMarkers();
    }, [indexMarker])


    useEffect(() => {
        const splash = setTimeout(() => {
            setSplashScreen(splashScreen => false);
            clearTimeout(splash);
        }, 2000);
        initMarkers();
        tabsInit(); //init tabs position

        const preparateData = [];
        for (let i = 0; i < props.ifl_state.level.length; i++) {
            for (let j = 0; j < props.ifl_state.level[i].images.length; j++) {
                preparateData.push({data: props.ifl_state.level[i].images[j], l_index: i, m_index: j});
            }
        }
        preparateData.sort((a, b) => {
            if (a.data.index > b.data.index) return 1;
            if (a.data.index < b.data.index) return -1;
            return 0;
        })
        setSliderData(preparateData);

        const background = document.getElementsByClassName("mainWrapIfloorplan")[0]
        background.style.background = props.ifl_state.bgColor;
        document.body.style.background = props.ifl_state.bgColor;

        setMedia(props.ifl_state.level[props.now_level].images[0].img);
    }, []);

    useEffect(() => {
        if (zoom) {
            setActiveMarkers(false);
        } else {
            setActiveMarkers(true);
        }
    }, [zoom]);


    useEffect(() => {
        tabsInit();
    }, [mediaIndex])

    useEffect(() => {
        if (!slidePlay) setIndexMarker(0);
        setMediaType("image");
        setMedia(props.ifl_state.level[props.now_level].images[0].img);
        initMarkers();
        tabsInit(); //init tabs position
    }, [props.now_image, props.now_level]);


    return (
        <div className="start" style={{background: props.ifl_state.bgColor}}>
            <div className="more" style={{display: splashScreen ? "none" : "block"}}>
                <svg onMouseDown={(e) => buttonMore(e)}
                    // width="26"
                    // height="18"
                     width="5%"
                     viewBox="0 0 26 18"
                     fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd"
                          clipRule="evenodd"
                          d="M0.25 17.5H25.75V14.6667H0.25V17.5ZM0.25 10.4167H25.75V7.58333H0.25V10.4167ZM0.25 0.5V3.33333H25.75V0.5H0.25Z"
                          fill="#8D8888"/>
                </svg>
                <div className="moreMenu" style={{display: activeButtonMore ? 'block' : 'none'}}>
                    {props.ifl_state.textBlock
                        ?
                        <button
                            onClick={() => {
                                setShowInfoModal(true)
                            }}>
                            Plan Info</button>
                        : ""}
                    <button>Share this</button>
                    <br/>
                    <button
                        onClick={() => {
                            if (mapStart) {
                                setMapStart(false);
                            } else {
                                setMapStart(true);
                            }
                        }}
                    >Map
                    </button>
                    <br/>
                    <button onClick={() => setActiveMarkers(!activeMarkers)}>{activeMarkers ? "Hide" : "Show"} arrows
                    </button>
                </div>
                <Popup
                    open={showInfoModal}
                    closeOnDocumentClick={false}>
                        <div className="contentTextInfo">
                            {parse(contentInfoModal)}
                        </div>
                    <a className="closeTextInfo" onClick={() => {
                        setShowInfoModal(false)
                    }}>Close</a>
                </Popup>
            </div>

            {props.ifl_state.client_data == "" ? '' : !zoom ? <div className="client_wrap_main" style={clientWrapStyle}>
                <Client clientData={JSON.parse(props.ifl_state.client_data)}
                        clientPosition={JSON.parse(props.ifl_state.client_position)}/>
            </div> : ""}

            {!splashScreen
                ?
                <div className="tabs" style={tabsStyle}>{tabs}{mediaTabs}</div>
                :
                ""}


            {splashScreen ? <SplashScreen data={props.ifl_state}/> : ""}

            {mediaScreen ? <Media index={mediaIndex}
                                  mediaData={props.ifl_state.media[mediaIndex]}
                                  address={props.ifl_state.address}/> : ""}

            {props.ifl_state.lat_long === "" ? "" :
                <MapWrapPopup latLong={props.ifl_state.lat_long} action={mapStart}/>}
            {activeMarkers ? markers : ""}
            {false ? <ZoomWindow media={media}
                                 address={props.ifl_state.address}
                                 data={markersData}
                                 setZoom={setZoom}
                                 nowIndex={indexMarker}
                                 setMediaType={setMediaType}
                                 setIndexMarker={setIndexMarker}
                                 setMedia={setMedia}
            /> : ""}
            <div className="images"
                 onMouseEnter={() => setSlideToolsHover(true)}
                 onMouseLeave={() => setSlideToolsHover(false)}
                 style={imageWrapStyle}>
                {mediaType === "panorama" ?
                    <Pannellum
                        width="100%"
                        image={media}
                        pitch={10}
                        height={heigthImage + 'px'}
                        yaw={180}
                        hfov={110}
                        autoLoad
                        onLoad={() => {
                            console.log("panorama loaded");
                        }}
                    >
                    </Pannellum>
                    :
                    ""
                }
                {mediaType === "image" ? <IfloorplanSlider src={sliderData}
                                                           index={indexMarker}
                                                           setIndexMarker={setIndexMarker}
                                                           setLevelIndex={props.initLevel}
                                                           setZoom={setZoom}
                                                           setSlidePlayStatement={setSlidePlay}
                                                           nowLevel={props.now_level}
                                                           count={props.ifl_state.level[props.now_level].images.length}/> : ""}
                {mediaType === "image" && slideToolsHover ? <SlidePanelMain data={markersData}
                                                                            setZoom={setZoom}
                                                                            nowIndex={indexMarker}
                                                                            setMediaType={setMediaType}
                                                                            setIndexMarker={setIndexMarker}
                                                                            setMedia={setMedia}/> : ""}
                {mediaType === "video" ? <iframe height={heigthImage + 'px'} id="ytplayer" type="text/html"
                                                 src={"https://www.youtube.com/embed/" + media + "?autoplay=1&origin=https://ifloorplan360.com"}
                                                 frameBorder="0"/> : ""}
            </div>


            <div className="plan" style={planWrapStyle}>
                <img src={props.ifl_state.level[props.now_level].plan_level[0].img} alt=""/>
            </div>
            <div className="addressBlock" style={textStyle}>{props.ifl_state.address}</div>
            <div className="brandWrap"
                 style={{
                     position: 'absolute',
                     width: props.ifl_state.brand_width + '%',
                     opacity: props.ifl_state.brand_transparency,
                     top: props.ifl_state.brand_pos_y + "%",
                     left: props.ifl_state.brand_pos_x + "%"
                 }}>
                <img style={{width: '100%'}} src={props.ifl_state.brand_logo} alt=""/>
            </div>
        </div>
    )
}


const putState = (state) => {
    return {
        ifl_state: state.ifl_state,
        now_level: state.now_level,
        now_image: state.now_image,
        now_marker: state.now_marker,
        marker_activity: state.marker_activity
    }
}

const putActions = (dispatch) => {
    return {
        changeStep: (step) => dispatch(changeStep(step)),
        initStateIFL: (obj) => dispatch(initStateIFL(obj)),
        initLevel: (level) => dispatch(initLevel(level)),
        setMarkerActivity: (bool) => dispatch(setMarkerActivity(bool))
    }
}

export default connect(putState, putActions)(Start);

