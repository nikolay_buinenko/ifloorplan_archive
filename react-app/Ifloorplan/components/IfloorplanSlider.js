import React, {useEffect, useRef, useState} from 'react';
import Slider from 'react-slick';

const IfloorplanSlider = ({src, refObj, setSlidePlayStatement, index, setIndexMarker, setZoom, nowLevel, count, setLevelIndex}) => {

  const [sliderPlay, setSliderPlay] = useState(false)
  const [indexSlider, setIndexSlider] = useState(0)
  const [sliderZoom, setSliderZoom] = useState(false)
  const [nextSlide, setNextSlide] = useState(0);

  const [settings, setSettings] = useState({
    dots: false,
    arrows: false,
    accessibility: false,
    fade: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    pauseOnHover: true,
    pauseOnFocus: true,
    beforeChange: function (currentSlide, nextSlide) {
      setNextSlide(nextSlide);
    }
  })


  useEffect(()=>{
    if(src.length != 0 && sliderPlay){
      setIndexMarker(src[nextSlide].m_index);
      setLevelIndex(src[nextSlide].l_index)
    }
  }, [nextSlide]);

  useEffect(() => {
    for(let i = 0; i < src.length; i++){
      if(nowLevel == src[i].l_index && index == src[i].m_index && !sliderPlay){
        slider.current.slickGoTo(i, true)
      }
    }
  }, [index])

  useEffect(() => {
    for(let i = 0; i < src.length; i++){
      if(nowLevel == src[i].l_index && src[i].m_index == 0 && !sliderPlay){
        slider.current.slickGoTo(i, true)
      }
    }
  }, [nowLevel])


  const slider = useRef();

  const Play = () => {
    setSlidePlayStatement(true);
    slider.current.slickPlay();
    setSliderPlay(!sliderPlay)
  }

  const Pause = () => {
    setSlidePlayStatement(false);
    slider.current.slickPause();
    setSliderPlay(!sliderPlay)
  }

  const Next = () => {
    slider.current.slickNext();
  }

  const Prev = () => {
    slider.current.slickPrev();
  }

  var zoomSlider = document.querySelector('.ifloorPlanSlider')

  const Zoom = () => {

    let slider = document.querySelector('.ifloorPlanSlider')
    slider.style.opacity = 0

    setTimeout(() => {
      slider.style.opacity = 1
    }, 500);

    setZoom(zoom => !zoom)
    zoomSlider.classList.toggle('zoom')
    setSliderZoom(sliderZoom => !sliderZoom)
  }


  return (
    <div className='ifloorPlanSlider ' style={{display:"block"}}>
      <Slider ref={slider} {...settings}>
        {src.map(el => (
          <div key={el.data.id}>
            <img src={el.data.img}/>
          </div>
        ))}
      </Slider>
      <div className="sliderPanel">
        <svg xmlns="http://www.w3.org/2000/svg"
             width="8%" viewBox="0 0 34 66"
             fill="none"
             onClick={Prev}>
          <path d="M30 3L6 33L30 63"
                stroke="white"
                strokeOpacity="0.8"
                strokeWidth="9"/>
        </svg>
        {sliderPlay
          ?
          <svg xmlns="http://www.w3.org/2000/svg"
               width="15%"
               viewBox="4 4 18 18"
               fill="none"
               onClick={Pause}>
            <path fill-rule="evenodd"
                  clip-rule="evenodd"
                  d="M14 19h4V5h-4M6 19h4V5H6v14z"
                  fill="#FFFEFE"
                  stroke="#fff"
                  opacity="0.8">
            </path>
          </svg>
          :
          <svg xmlns="http://www.w3.org/2000/svg"
               width='15%'
               viewBox="0 0 63 78"
               fill="none"
               onClick={Play}>
            <path opacity="0.8"
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1 1V77L62 39L1 1Z"
                  fill="#FFFEFE"
                  stroke="#979797"
                  strokeOpacity="0.01"/>
          </svg>
        }

        <svg xmlns="http://www.w3.org/2000/svg"
             width="8%"
             viewBox="0 0 34 66"
             fill="none"
             onClick={Next}>>
          <path d="M4 3L28 33L4 63"
                stroke="white"
                strokeOpacity="0.8"
                strokeWidth="9"/>
        </svg>
      </div>
      <div className="zoom__btn">


        {sliderZoom
          ?
          <svg xmlns="http://www.w3.org/2000/svg"
               width="10%"
               viewBox="0 0 30 31"
               fill="none"
               onClick={Zoom}>
            <path fillRule="evenodd"
                  opacity="0.8"
                  clipRule="evenodd"
                  d="M20.0831 19.0673H21.438L29.9961 27.5587L27.4407 30.0892L18.8655 21.6147V20.2731L18.4024 19.7976C16.4473 21.4619 13.909 22.4639 11.1478 22.4639C4.99077 22.4639 0 17.5219 0 11.4251C0 5.32823 4.99077 0.38623 11.1478 0.38623C17.3048 0.38623 22.2956 5.32823 22.2956 11.4251C22.2956 14.1593 21.2837 16.6727 19.6029 18.6088L20.0831 19.0673ZM3.43008 11.4251C3.43008 15.6538 6.87732 19.0673 11.1478 19.0673C15.4182 19.0673 18.8655 15.6538 18.8655 11.4251C18.8655 7.19635 15.4182 3.7828 11.1478 3.7828C6.87732 3.7828 3.43008 7.19635 3.43008 11.4251Z"
                  fill="white"/>
            <path d="M15.5979 12.5052H12.2383H10.5585H7.19897V10.8418H10.5585H12.2383H15.5979V12.5052Z"
                  opacity="0.8"
                  fill="white"/>
          </svg>
          :
          <svg xmlns="http://www.w3.org/2000/svg"
               width="10%"
               viewBox="0 0 50 50"
               fill="none"
               onClick={Zoom}>
            <g opacity="0.8">
              <path fillRule="evenodd"
                    clipRule="evenodd"
                    d="M33.4763 31.4465H35.7347L50 45.7404L45.7404 50L31.4465 35.7347V33.4763L30.6747 32.6758C27.4157 35.4774 23.1847 37.1641 18.582 37.1641C8.31904 37.1641 0 28.8451 0 18.582C0 8.31904 8.31904 0 18.582 0C28.8451 0 37.1641 8.31904 37.1641 18.582C37.1641 23.1847 35.4774 27.4157 32.6758 30.6747L33.4763 31.4465ZM5.71756 18.582C5.71756 25.7004 11.4637 31.4465 18.582 31.4465C25.7004 31.4465 31.4465 25.7004 31.4465 18.582C31.4465 11.4637 25.7004 5.71755 18.582 5.71755C11.4637 5.71755 5.71756 11.4637 5.71756 18.582Z"
                    fill="white"/>
              <path d="M26 20.4H20.4V26H17.6V20.4H12V17.6H17.6V12H20.4V17.6H26V20.4Z" fill="white"/>
            </g>
          </svg>
        }
      </div>
    </div>
  )
}

export default IfloorplanSlider;