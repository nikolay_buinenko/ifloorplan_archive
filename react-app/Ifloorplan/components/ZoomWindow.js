import React, {useState} from 'react';
import SlidePanelMini from './SlidePanelMini';

const ZoomWindow = (props) => {
    const [hover, setHover] = useState(false);
    return (
        <div className="zoomWrap">
            <div className="address_slide_wrap">
                <div className="address_slide">{props.address}</div>
            </div>
            <div className="image_wrap">
                <div className="image_zoom">
                    <img src={props.media} alt=""/>
                </div>
            </div>
            <div className="slide_zoom_tools"
                 onMouseEnter={()=>setHover(true)}
                 onMouseLeave={()=> setHover(false)}
            >
                {hover ? <SlidePanelMini
                    data={props.data}
                    setZoom={props.setZoom}
                    nowIndex={props.nowIndex}
                    setMediaType={props.setMediaType}
                    setIndexMarker={props.setIndexMarker}
                    setMedia={props.setMedia}
                /> : ""}
            </div>
        </div>
    )
}
export default ZoomWindow;