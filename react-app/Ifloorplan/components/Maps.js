import React from 'react';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';

export const Maps = (props) => {
    console.log({ lat: props.lat, lng: props.lng })
    return(
        <Map
            google={props.google}
            zoom={15}
            initialCenter={{ lat: props.lat, lng: props.lng }}
        >
            <Marker position={{ lat: props.lat, lng: props.lng }} />
        </Map>
    )
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyBj_5jx0TD50Wy1FFqsq9Q7zxiGT6Vvcv4'
})(Maps);