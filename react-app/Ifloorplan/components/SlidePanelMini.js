import React, {useEffect, useState} from 'react';

const SlidePanelMini = (props) => {
  const [click, setClick] = useState(0);
  const [sliderPlay, setSliderPlay] = useState(false);
  const [timer, setTimer] = useState();

  const Prev = () => {
    if (props.nowIndex != 0) {
      let index = 0;
      for (let i = props.nowIndex - 1; i => 0; i--) {
        if (props.data[i].action === "image marker") {
          index = i;
          break;
        }
      }
      props.setMedia(props.data[index].data.img);
      props.setIndexMarker(index);
      props.setMediaType("image");
    }
  }

  const Next = () => {
    if (props.nowIndex != props.data.length - 1) {
      let index = 0;
      for (let i = props.nowIndex + 1; i < props.data.length; i++) {
        if (props.data[i].action === "image marker") {
          index = i;
          break;
        }
      }
      props.setMedia(props.data[index].data.img);
      props.setIndexMarker(index);
      props.setMediaType("image");
    }
  }


  const TogglePlayPause = () => {
    setSliderPlay(!sliderPlay);
  }

  useEffect(()=>{
    sliderPlay ? Play() : Stop()
  }, [sliderPlay]);

  const Stop = () => {
    clearInterval(timer);
  }

  const Play = () => {
    props.setMediaType("image");
    const Start = () => {
      let mark = 0;
      props.setIndexMarker((indexMarker) => {
        let index = 0;
        if (indexMarker != props.data.length - 1) {
          for (let i = indexMarker + 1; i < props.data.length; i++) {
            if (props.data[i].action === "image marker") {
              index = i;
              break;
            }
          }
        } else {
          Stop();
        }
        mark = index;
        return index;
      });
      props.setMedia(media => props.data[mark].data.img);
    }
    setTimer(setInterval(Start, 2000));
  }

  const Zoom = () => {
    props.setZoom(zoom => !zoom);
  }

  return (
    <div className="slide_tools">
      <div className="play_next_prev">
        <div className="prev" onClick={Prev}>
          <svg xmlns="http://www.w3.org/2000/svg" width="22" height="36" viewBox="0 0 22 36" fill="none">
            <path d="M18 3L6 18L18 33" stroke="white" strokeWidth="9"/>
          </svg>
        </div>
        <div className="play" onClick={TogglePlayPause}>
          {
            sliderPlay
              ?
              <svg xmlns="http://www.w3.org/2000/svg"
                             aria-hidden="true"
                             width="22%"
                             preserveAspectRatio="xMidYMid meet"
                             viewBox="5 5 50 80">
              <path fillRule="evenodd"
                    clipRule="evenodd"
                    d="M14 19h4V5h-4M6 19h4V5H6v14z"
                    fill="#FFFEFE"
                    stroke="#979797"
                    strokeOpacity="0.01"/>
            </svg>
            :
            <svg xmlns="http://www.w3.org/2000/svg"
                 width="23"
                 viewBox="5 5 70 90"
                 fill="none">
              <path fillRule="evenodd"
                    clipRule="evenodd"
                    d="M1 1.38623V31.0892L22 16.2377L1 1.38623Z"
                    fill="#FFFEFE"
                    stroke="#979797"
                    strokeOpacity="0.01"/>
            </svg>
          }
        </div>
        <div className="next" onClick={Next}>
          <svg xmlns="http://www.w3.org/2000/svg" width="22" height="36" viewBox="0 0 22 36" fill="none">
            <path d="M4 3L16 18L4 33" stroke="white" strokeWidth="9"/>
          </svg>
        </div>
      </div>
      <div className="zoom">
        <div onClick={Zoom}>
          <svg xmlns="http://www.w3.org/2000/svg" width="30" height="31" viewBox="0 0 30 31" fill="none">
            <path fillRule="evenodd"
                  clipRule="evenodd"
                  d="M20.0831 19.0673H21.438L29.9961 27.5587L27.4407 30.0892L18.8655 21.6147V20.2731L18.4024 19.7976C16.4473 21.4619 13.909 22.4639 11.1478 22.4639C4.99077 22.4639 0 17.5219 0 11.4251C0 5.32823 4.99077 0.38623 11.1478 0.38623C17.3048 0.38623 22.2956 5.32823 22.2956 11.4251C22.2956 14.1593 21.2837 16.6727 19.6029 18.6088L20.0831 19.0673ZM3.43008 11.4251C3.43008 15.6538 6.87732 19.0673 11.1478 19.0673C15.4182 19.0673 18.8655 15.6538 18.8655 11.4251C18.8655 7.19635 15.4182 3.7828 11.1478 3.7828C6.87732 3.7828 3.43008 7.19635 3.43008 11.4251Z"
                  fill="white"/>
            <path d="M15.5979 12.5052H12.2383H10.5585H7.19897V10.8418H10.5585H12.2383H15.5979V12.5052Z" fill="white"/>
          </svg>
        </div>
      </div>
    </div>
  )
}
export default SlidePanelMini;