import React, {useState, useEffect} from 'react';
import Popup from "reactjs-popup";
import Maps from './Maps';

const MapWrapPopup = (props) => {

    if(props.latLong  === null){
        return null;
    }

    const lat_long_arr = props.latLong.split(" ");

    return(
        <div className="map_wrap_popup">
            <Popup
                open={props.action}
                closeOnDocumentClick
                onClose={()=> {
                    return
                }}
            >
                <div className="modal">
                    <Maps lat={parseFloat(lat_long_arr[0])} lng={parseFloat(lat_long_arr[1])}/>
                </div>
            </Popup>
        </div>
    )
}
export  default MapWrapPopup;