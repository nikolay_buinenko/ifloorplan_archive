import React, {useEffect, useState} from 'react';

const SlidePanelMain = (props) => {
  const [click, setClick] = useState(0);
  const [sliderPlay, setSliderPlay] = useState(false);
  const [timer, setTimer] = useState();

  const Prev = () => {
    if (props.nowIndex != 0) {
      let index = 0;
      for (let i = props.nowIndex - 1; i => 0; i--) {
        if (props.data[i].action === "image marker") {
          index = i;
          break;
        }
      }
      props.setMedia(props.data[index].data.img);
      props.setIndexMarker(index);
      props.setMediaType("image");
    }
  }

  const Next = () => {
    if (props.nowIndex != props.data.length - 1) {
      let index = 0;
      for (let i = props.nowIndex + 1; i < props.data.length; i++) {
        if (props.data[i].action === "image marker") {
          index = i;
          break;
        }
      }
      props.setMedia(props.data[index].data.img);
      props.setIndexMarker(index);
      props.setMediaType("image");
    }
  }


  const TogglePlayPause = () => {
    setSliderPlay(!sliderPlay);
  }

  useEffect(()=>{
    sliderPlay ? Play() : Stop()
  }, [sliderPlay]);

  const Stop = () => {
    clearInterval(timer);
  }

  const Play = () => {
    props.setMediaType("image");
    const Start = () => {
      let mark = 0;
      props.setIndexMarker((indexMarker) => {
        let index = 0;
        if (indexMarker != props.data.length - 1) {
          for (let i = indexMarker + 1; i < props.data.length; i++) {
            if (props.data[i].action === "image marker") {
              index = i;
              break;
            }
          }
        } else {
          Stop();
        }
        mark = index;
        return index;
      });
      props.setMedia(media => props.data[mark].data.img);
    }
    setTimer(setInterval(Start, 2000));
  }

  const Zoom = () => {
    props.setZoom(zoom => !zoom);
  }

  return (
    <div className="slide_tools">
      <div className="slide_tools_wrap">
        <div className="play_next_prev">
          <div className="prev" onClick={Prev}>
            <svg xmlns="http://www.w3.org/2000/svg" width="11%" viewBox="0 0 34 66" fill="none">
              <path d="M30 3L6 33L30 63" stroke="white" strokeOpacity="0.8" strokeWidth="9"/>
            </svg>
          </div>
          <div className="play" onClick={TogglePlayPause}>
            {
              sliderPlay
                ?
                <svg xmlns="http://www.w3.org/2000/svg"
                     width="23"
                     viewBox="0 0 21 31"
                     fill="none">
                  <path fillRule="evenodd"
                        clipRule="evenodd"
                        d="M14 19h4V5h-4M6 19h4V5H6v14z"
                        fill="#FFFEFE"
                        stroke="#979797"
                        strokeOpacity="0.01"/>
                </svg>
                :
                <svg xmlns="http://www.w3.org/2000/svg"
                     width='22%'
                     viewBox="0 0 63 78"
                     fill="none">
                  <path opacity="0.8"
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M1 1V77L62 39L1 1Z"
                        fill="#FFFEFE"
                        stroke="#979797"
                        strokeOpacity="0.01"/>
                </svg>
            }
          </div>
          <div className="next" onClick={Next}>
            <svg xmlns="http://www.w3.org/2000/svg" width="11%" viewBox="0 0 34 66" fill="none">
              <path d="M4 3L28 33L4 63" stroke="white" strokeOpacity="0.8" strokeWidth="9"/>
            </svg>
          </div>
        </div>
        <div className="zoom">
          <div onClick={Zoom}>
            <svg xmlns="http://www.w3.org/2000/svg" width="10%" viewBox="0 0 50 50" fill="none">
              <g opacity="0.8">
                <path fillRule="evenodd"
                      clipRule="evenodd"
                      d="M33.4763 31.4465H35.7347L50 45.7404L45.7404 50L31.4465 35.7347V33.4763L30.6747 32.6758C27.4157 35.4774 23.1847 37.1641 18.582 37.1641C8.31904 37.1641 0 28.8451 0 18.582C0 8.31904 8.31904 0 18.582 0C28.8451 0 37.1641 8.31904 37.1641 18.582C37.1641 23.1847 35.4774 27.4157 32.6758 30.6747L33.4763 31.4465ZM5.71756 18.582C5.71756 25.7004 11.4637 31.4465 18.582 31.4465C25.7004 31.4465 31.4465 25.7004 31.4465 18.582C31.4465 11.4637 25.7004 5.71755 18.582 5.71755C11.4637 5.71755 5.71756 11.4637 5.71756 18.582Z"
                      fill="white"/>
                <path d="M26 20.4H20.4V26H17.6V20.4H12V17.6H17.6V12H20.4V17.6H26V20.4Z" fill="white"/>
              </g>
            </svg>
          </div>
        </div>
      </div>
    </div>
  )
}
export default SlidePanelMain;