import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import {initStateIFL, changeStep} from '../redux/actions/actions';
import Start from '../components/Start';
import Empty from '../components/Empty';
import {getIfloorplan} from '../serverRequests/requests';


const Main = (props) => {

    useEffect(()=>{
        let ifloorplan_id = document.getElementById('ifloorplan_id').value;
        getIfloorplan(ifloorplan_id)
            .then((res)=>{
                props.initStateIFL(res[0]);
                if(res.length === 0){
                    props.changeStep('empty');
                } else {
                    props.changeStep('start');
                }
            })
    }, []);
    return (
        <div className="mainWrapIfloorplan">
            {props.step === "" ? '' : ''}
            {props.step === "start" ? <Start/> : ''}
            {props.step === "empty" ? <Empty/> : ''}
        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
    }
}

const putActions = (dispatch) => {
    return {
        changeStep: (step)=>dispatch(changeStep(step)),
        initStateIFL: (obj)=>dispatch(initStateIFL(obj)),
    }
}

export default connect(putState, putActions)(Main);