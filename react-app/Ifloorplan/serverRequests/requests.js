function readCookie(name) {
    //function split cookie for get csrf token
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
const CSRFTOKEN = readCookie('csrftoken'); //get csrftoken

export async function getIfloorplan( id ){
    const config = {
        method: 'GET',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            'content-type': 'application/json'
        },
    }
    const response = await fetch('https://ifloorplan360.com/api/get?token=1234567&ifloorplan='+id, config);
    return await response.json();
}