import { CSRFTOKEN } from "../redux/constant/constants";
import axios from 'axios';

export async function getSystemIcons(){
    const config ={
        method: 'GET',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            'content-type': 'application/json'
        }
    }
    const response = await fetch('https://ifloorplan360.com/api/system_markers/',config);
    return await response.json();
}

export async function getUserIcons(){
    const config ={
        method: 'GET',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            'content-type': 'application/json'
        }
    }
    const response = await fetch('https://ifloorplan360.com/api/user_markers/',config);
    return await response.json();
}

export async function getBrand( userId ){
    const config ={
        method: 'GET',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            'content-type': 'application/json'
        }
    }
    const response = await fetch('https://ifloorplan360.com/api/getbrand/'+userId+'/',config);
    return await response.json();
}

export async function getLogoClients( data ){
    const config = {
        method: 'POST',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            'content-type': 'application/json'
        },
        body: JSON.stringify(data)
    }
    const response = await fetch('https://ifloorplan360.com/api/clientimgdownload/', config);
    return await response.json();
}

export async function getClients( data ){
    const config = {
        method: 'POST',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            'content-type': 'application/json'
        },
        body: JSON.stringify(data)
    }
    const response = await fetch('https://ifloorplan360.com/api/clientdownload/', config);
    return await response.json();
}

export async function deletePanelumImage( data ){
    const config = {
        method: 'DELETE',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            "Content-type": "application/json"
        },
        body: JSON.stringify(data)
    }
    const response = await fetch('https://ifloorplan360.com/api/panelum/', config);
    return await response.json();
}


export async function deleteMultiImage( data ){
    const config = {
        method: 'DELETE',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            "Content-type": "application/json"
        },
        body: JSON.stringify(data)
    }
    const response = await fetch('https://ifloorplan360.com/api/imageupload/', config);
    return await response.json();
}

export async function deleteSinglePlan( data ){
    const config = {
        method: 'DELETE',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            "Content-type": "application/json"
        },
        body: JSON.stringify(data)
    }
    const response = await fetch('https://ifloorplan360.com/api/imageplanuload/', config);
    return await response.json();
}

export async function deleteSimpleImage( data ){
    const config = {
        method: 'DELETE',
        headers: {
            "X-CSRFToken": CSRFTOKEN
        },
        body: data
    }
    const response = await fetch('https://ifloorplan360.com/api/imageupload/', config);
    return await response.json();
}

export async function createNewPlan( data ){
    const config = {
        method: 'POST',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            'content-type': 'application/json'
        },
        body: JSON.stringify(data)
    }
    const response = await fetch('https://ifloorplan360.com/api/create/?token=1234567', config);
    return await response.json();
}

export async function updateNewPlan( data ){
    const config = {
        method: 'PUT',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            'content-type': 'application/json'
        },
        body: JSON.stringify(data)
    }
    const response = await fetch('https://ifloorplan360.com/api/update/', config);
    return await response.json();
}

export async function uploadFilesToServer(url, file, config){
    const response = await axios.post(url, file, config);
    return await response;
}

export async function getImages(url, file, config){
    const response = await axios.get(url, file, config);
    return await response;
}

export async function uploadFilesToServerPanelum(url, file, config){
    const response = await axios.put(url, file, config);
    return await response;
}

export async function updateSingleImage(url, file, config){
    const response = await axios.put(url, file, config);
    return await response;
}


export async function getTemplates( data ){
    const config = {
        method: 'GET',
        headers: {
            "X-CSRFToken": CSRFTOKEN,
            'content-type': 'application/json'
        },
    }
    const response = await fetch('https://ifloorplan360.com/api/get/?token=1234567&ifloorplan=0&user_id='+data.user_id, config);
    return await response.json();
}