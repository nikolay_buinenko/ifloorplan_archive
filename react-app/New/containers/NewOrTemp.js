import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {CSRFTOKEN} from '../redux/constant/constants';
import { nextStep, ifloorplanStateAdd, setType, LibImageAdd, LibPlanAdd, panelumLib, clearIfloorState } from '../redux/actions/actions';
import UploadPlans from "../components/UploadPlans";
import SingleUploadPlan from "../components/SingleUploadPlan";
import SingleUploadImage from "../components/SingleUploadImage";
import UploadFiles from "../components/UploadFiles";
import UploadImage from "../components/UploadImage";
import TopPanel from "../components/TopPanel";
import ScaleLayout from "../components/ScaleLayout";
import PlanLibrary from "../components/PlanLibrary";
import ImageLibrary from "../components/ImageLibrary";
import CamsPosition from "../components/CamsPosition";
import TextEditor from "../components/TextEditor";
import Panorama from "../components/Panorama";
import Media from "../components/Media";
import ManageLevels from "../components/ManageLevels";
import LatLong from "../components/LatLong";
import UploadSinglePanoramaLib from "../components/UploadSinglePanoramaLib";
import UploadPanoramaLib from "../components/UploadPanoramaLib";
import PanoramaLibrary from "../components/PanoramaLibrary";
import NewLevelCreateOption from "../components/NewLevelCreateOption";
import Clients from "../components/Clients";
import Layout from "../components/Layout";
import KenBurns from "../components/KenBurns";
import UpdateSinglePlan from "../components/UpdateSinglePlan";
import PositionVideos from "../components/PositionVideos";
import LibOrCont from "../components/LibOrCont";
import { createNewPlan, getTemplates, getBrand, getImages } from "../serverRequests/requests";
import {START_JSON} from "../redux/constant/constants";


const NewOrTemp = (props) => {
        const [templates, setTemplates] = useState([]);
        const [templatesWrap, setTemplatesWrap] = useState(false);
        const step = props.step;
        const [animationSaved, setAnimationSaved] = useState(0);

        const[showTools, setShowTools] = useState(false)
        


        const startNewPlan = () => {
            setTemplatesWrap(false);
            let brand;
            getBrand(document.getElementById("user_id").value)
                .then((data)=>{
                    brand = data[0];
                    createNewPlan(START_JSON)
                        .then((data_res) => {
                                let data_res_new = Object.assign({}, data_res[0]);
                                if(brand){
                                    data_res_new.brand_logo = brand.logo;
                                    data_res_new.brand_width = brand.width;
                                    data_res_new.brand_transparency = brand.transparency;
                                    data_res_new.brand_pos_x = brand.position_x;
                                    data_res_new.brand_pos_y = brand.position_y;
                                    data_res_new.brand_url = brand.url;
                                }
                                props.ifloorplanStateAdd(data_res_new);
                                props.nextStep("upload plans", "_");
                        });
                })
        }

        const setTemplate = (data) => {
            let newState = JSON.parse(JSON.stringify(data));
            delete newState.author;
            delete newState.id;
            delete newState.videos;
            newState.template_name = "";
            newState.start_live = new Date().getTime()/1000;
            newState.end_live = new Date().getTime()/1000+3600;
            newState.level = [{plan_level: {img: null},images:[],tabLabel: "First Floor", index: 0}];
            createNewPlan(newState)
                .then((data_res) => {
                    setTemplatesWrap(false);
                    props.ifloorplanStateAdd(data_res[0]);
                    props.nextStep("upload plans", "template");
                });
        }

        const startTemplate = () => {
            const data_request = {
                user_id: document.getElementById("user_id").value,
            }
            setTemplatesWrap(true);
            getTemplates(data_request)
                .then(data=>{
                    const temp = [];
                    for(let i = 0; i < data.length; i++){
                        if(data[i].template_name != ""){
                            temp.push(
                                <div className="template" onClick={()=>{setTemplate(data[i])}} key={i}>
                                    <h3>{data[i].template_name}</h3>
                                </div>
                            )
                        }
                    }
                    setTemplates(temp);
                });
        }

        return (
            <div className={step === "" ? "wrap" : step}>
                {step !== "" ? <TopPanel setAnimation={setAnimationSaved} animation={animationSaved} showTools={showTools}/> : ''}
                {step === "" ? <div className="subHeader"><div className="title">New</div></div> : '' }
                {step === "" ? <div className="mainApp"><button onClick={startNewPlan}>Start new iFloorPlan</button><p>Or</p><button onClick={startTemplate}>Start new from Template</button></div> : '' }

                {step === "upload plans" ? <UploadPlans setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}//
                {step === "upload files" ? <UploadFiles setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}//
                {step === "upload images" ? <UploadImage setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}//
                {step === "scale layout" ? <ScaleLayout setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "scale and repos" ? <ScaleLayout setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "plan library" ? <PlanLibrary setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "image library" ? <ImageLibrary setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "single plan upload" ? <SingleUploadPlan setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}//
                {step === "cams position" ? <CamsPosition setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "simple upload image" ? <SingleUploadImage setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}//
                {step === "new level create option" ? <NewLevelCreateOption setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "text editor" ? <TextEditor setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "add media" ? <Media setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "panorama" ? <Panorama setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "manage levels" ? <ManageLevels setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "add lat and long" ? <LatLong setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "360 library" ? <PanoramaLibrary setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "single 360" ? <UploadSinglePanoramaLib setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "360 library upload" ? <UploadPanoramaLib setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}//
                {step === "panorama single upload file" ? <UploadSinglePanoramaLib setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}//
                {step === "clients" ? <Clients setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "layout" ? <Layout setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "ken burns" ? <KenBurns setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "single plan update image" ? <UpdateSinglePlan setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}//
                {step === "position videos" ? <PositionVideos setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}
                {step === "lib or cont" ? <LibOrCont setAnimation={setAnimationSaved} setShowTools={setShowTools}/> : ''}

                <div className="tempWrap" style={{ display: templatesWrap ? "block" : "none"}}>
                    <h3 className="template_title">Choose a Template</h3>
                    {templates}
                </div>
            </div>
        )
}

const putState = (state) => {
    return {
        step: state.step,
        dropFiles: state.dropFiles,
        dropFilesTitle: state.dropFilesTitle
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        LibPlanAdd: (array) => dispatch(LibPlanAdd(array)),
        LibImageAdd: (array) => dispatch(LibImageAdd(array)),
        panelumLib: (items) => dispatch(panelumLib(items)),
        setType: (key) => dispatch(setType(key)),
        clearIfloorState: () => dispatch(clearIfloorState())
    }
}


export default connect(putState, putActions) (NewOrTemp);