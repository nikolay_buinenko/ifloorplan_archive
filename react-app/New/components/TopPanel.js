import React, {Component, useState, useEffect} from 'react';
import {connect} from 'react-redux';
import Popup from "reactjs-popup";
import ColorSet from "./ColorSet";
import ImageMarkersStudio from "./ImageMarkersStudio";
import SaveAnimation from "./otherResources/SaveAnimation"
import {
    setColor,
    changeLevel,
    nextStep,
    setColorBackground,
    setOpenImageMarkers,
    setColorText,
    changeCamKey,
    ifloorplanStateAdd,
    setVideosSidebar,
    changeImageKey,
    changeCamPosition
} from '../redux/actions/actions';
import {updateNewPlan, createNewPlan} from '../serverRequests/requests';

const TopPanel = (props) => {
    const [showMenu, setShowMenu] = useState('none'); //Main menu tools enable/disable
    const [newLevelName, setNewLevelName] = useState(''); //New Level name
    const [addLevelPopup, setAddLevelPopup] = useState(false);  //add level popup enable/disable
    const [saveAsTemplatePopup, setSaveAsTemplatePopup] = useState(false); //save as template popup enable/disable
    const [newTemplateName, setNewTemplateName] = useState(''); //new template name

    const [nowLevelName, setNowLevelName] = useState("");

    const [lastColorBtn, setLastColorBtn] = useState(false);

    const handleClickTools = () => {// enable/disable main menu tools toggle function
        if (showMenu === 'none') {
            setShowMenu('block');
        } else {
            setShowMenu('none');
        }
    }
    const ifloorplanObj = props.ifloorplanNowState; //last ifloor plan obj

    useEffect(() => {
        setNowLevelName(props.ifloorplanNowState.level[props.levelNowId].tabLabel)
    }, [])

    useEffect(() => {
        setLastColorBtn(false);
    }, [props.levelNowId])

    useEffect(() => {
        setNowLevelName(props.ifloorplanNowState.level[props.levelNowId].tabLabel)
    }, [props.levelNowId, props.ifloorplanNowState])

    const handleClickItem = (stepName, key = "_") => {
        handleClickTools(); //close main menu tools
        props.nextStep(stepName, key); //next step
    }

    const handleNext = (e) => {//next image to set position cam
        e.preventDefault();
        const images = props.ifloorplanNowState.level[props.levelNowId].images; //all images current level
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState)); //main object floor plan
        if (props.image_markers_layout) { //if open image markers dont send data to server
            if (props.image_key !== images.length - 1) { //if now index image last when not upper index
                props.changeImageKey(props.image_key + 1); //up index
            } else {
                setLastColorBtn(true);
            }
            return 1;
        }
        updateNewPlan(newState)
            .then(data => {
                props.setAnimation(animationSaved => animationSaved + 1);
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                props.changeCamPosition(data[0]);  //update second state ifloorplan
                if (props.image_key !== images.length - 1) { //if now index image last when not upper index
                    props.changeImageKey(props.image_key + 1); //up index
                } else {
                    setLastColorBtn(true);
                }
            });
    }


    const handlePrev = (e) => {//previos index image to set position cam
        e.preventDefault();
        setLastColorBtn(false)
        if (props.image_key !== 0) { //if now index image first when not lower index
            props.changeImageKey(props.image_key - 1) //lower index
        }
    }

    const newLevel = () => { //new level create function
        const newLevelObj = {  //new object level
            tabLabel: newLevelName,
            index: ifloorplanObj.level.length //set last index
        }
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState)); //prev obj ifloorplan
        newState.level.push(newLevelObj); //add new level to array
        setAddLevelPopup(false); //disable level popup
        handleClickTools(); //disable main menu tools
        updateNewPlan(newState)
            .then(data => {
                props.setAnimation(animationSaved => animationSaved + 1);
                props.changeImageKey(0);
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                props.changeCamPosition(data[0]); //update second ifloorplan objecct
                props.nextStep("new level create option", "new level"); //next step
                props.changeLevel(data[0].level.length - 1); //set new index level to edit
            });

    }

    const newPanelum = () => {//new panorama
        const panoramObj = { //new object panellum panorama
            img: "/media/system_images/broken-1.png",
            cam_img: null,
            cam_width: 10,
            pos_x: 0,
            pos_y: 0
        }
        let newState = JSON.parse(JSON.stringify(ifloorplanObj)); //last state ifloorplan
        newState.level[props.levelNowId].panelum.push(panoramObj); //append new panorama object
        updateNewPlan(newState)
            .then(data => {
                props.setAnimation(animationSaved => animationSaved + 1);
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                handleClickItem("panorama"); //next step
            });
    }
    // const newTemplate = () => {
    //     //new template create function
    //     let newState = JSON.parse(JSON.stringify(ifloorplanObj)); //last state ifloorplan
    //     newState.template_name = newTemplateName; //set template name
    //     newState.address = ""; //remove address
    //     newState.end_live = parseInt(newState.end_live) + 31536000; //add time delay to delete
    //     newState.level = [{tabLabel: "First Floor", index: 0}]; //reset levels
    //     createNewPlan(newState)
    //         .then((data_res) => {
    //             return 1;
    //         });
    // }

    const newTemplate = () => {
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        newState.template_name = newTemplateName;
        newState.address = "";
        newState.end_live = parseInt(newState.end_live) + 31536000;

        for (let i = 0; ifloorplanObj.level.length > i; i++) {
            newState.level[i].images = [];
            delete newState.level[i].id
            delete newState.level[i].plan
            delete newState.level[i].panelum
            newState.level[i].plan_level = {
                img: null
            }
            delete newState.level[i].video_marker
        }
        delete newState.id
        delete newState.author
        delete newState.media
        delete newState.order
        createNewPlan(newState)
            .then((data_res) => {
                return 1;
            });
        setSaveAsTemplatePopup(false)
    }

    const Publish = () => {
        //publish function
        let newState = JSON.parse(JSON.stringify(ifloorplanObj)); //last state ifloorplan
        if (props.type_action === 'new') { //if create new plan but not edit
            newState.end_live = parseInt(newState.end_live) + 31536000; //add time delay to delete
            newState.published = true; //set active status publish
        }
        updateNewPlan(newState)
            .then(data => {
                props.setAnimation(animationSaved => animationSaved + 1);
                if (props.type_action === 'edit') { //if edit - not change step
                    return 1;
                } else {
                    handleClickItem("");
                }
            });
    }

    return (
        <div className="topPanelWrap">
            {/*{console.log('level: ', props.ifloorplanNowState.level[0])}*/}
            <div className="firstOpt">
                <i className="fa fa-caret-up" aria-hidden="true"></i>
                <div className="toolsBtn"
                     // style={!props.showTools ? {backgroundColor: "red"} : {""};}
                    >
                    <div className="toolsBtnWrapp"
                         onClick={handleClickTools //open the Tools (changed by Liza)
                         }>
                        <p>Tools</p>

                        <i className="fa fa-caret-down" aria-hidden="true"></i>
                    </div>
                    <div className="MenuWrap" style={{display: showMenu}}>
                        <div className="itemMenu SubMenuWrap">
                            <p>Floor Plan</p>
                            <i className="fa fa-caret-right" aria-hidden="true"></i>
                            <div className="SubItems">
                                <div className="itemMenu" onClick={() => handleClickItem("plan library")}><p>Floor plan
                                    Library</p></div>


                                {/*<div className="itemMenu"*/}
                                {/*     onClick={() => handleClickItem("scale and repos", "scale plan")}><p>Scale and*/}
                                {/*    Reposition</p></div>*/}


                            </div>
                        </div>
                        <div className="itemMenu SubMenuWrap"><p>Images</p><i className="fa fa-caret-right"
                                                                              aria-hidden="true"></i>
                            <div className="SubItems">
                                <div className="itemMenu" onClick={() => handleClickItem("image library")}><p>Image
                                    Library</p></div>
                                <div className="itemMenu" onClick={() => handleClickItem("ken burns")}><p>Ken Burns
                                    Studio...</p></div>
                            </div>
                        </div>
                        <div className="itemMenu" onClick={() => {
                            handleClickTools(); //close menu
                            newPanelum(); //new panorama object
                        }}><p>360 Images</p></div>
                        <div className="itemMenu" onClick={() => {
                            handleClickItem("position videos", "_")
                        }}><p>Videos</p></div>
                        <div className="itemMenu" onClick={() => handleClickItem("layout")}><p>Layout</p></div>
                        <div className="itemMenu" onClick={() => handleClickItem("clients")}><p>Client Library</p></div>
                        <div className="itemMenu" style={{position: 'relative'}} onClick={() => setAddLevelPopup(true)}>
                            <p>Add Levels</p>
                        </div>
                        <div className="addLevelWrap" style={{display: addLevelPopup ? 'block' : 'none'}}>
                            <h3>Add new level</h3>
                            <p>Write a name for this level</p>
                            <input onChange={(e) => {
                                setNewLevelName(e.target.value)
                            }} type="text" placeholder="Text hint..."/>
                            <p>This name appears on the navigation tab</p>
                            <div className="btnGroup">
                                <button onClick={() => setAddLevelPopup(false)}>Cancel</button>
                                <button onClick={newLevel}>Add level</button>
                            </div>
                        </div>
                        <div className="itemMenu" onClick={() => {
                            handleClickTools(); //close menu
                            handleClickItem("cams position");
                            props.setColor(true); //open menu set color
                        }}><p>Colours</p></div>
                        <div className="itemMenu" onClick={() => {
                            handleClickTools(); //close menu
                            handleClickItem("manage levels");
                        }}><p>Manage Levels</p></div>
                        <div className="itemMenu" onClick={() => {
                            handleClickItem("cams position");
                            props.setOpenImageMarkers(true)
                        }}>
                            <p>Image Markers</p>
                        </div>
                        <div className="itemMenu" onClick={() => {
                            handleClickTools(); //close menu
                            handleClickItem("add media");
                        }}><p>Add external media</p></div>
                        <div className="itemMenu" onClick={() => handleClickItem("text editor")}><p>Add Text
                            Information</p></div>
                        <div className="itemMenu" onClick={() => {
                            setSaveAsTemplatePopup(true);
                        }}><p>Save as Template</p></div>
                        <div className="saveAsTemplateWrap" style={{display: saveAsTemplatePopup ? 'block' : 'none'}}>
                            <h3>Save as a Template</h3>
                            <p>Write a name for your Template</p>
                            <input onChange={(e) => {
                                setNewTemplateName(e.target.value)
                            }} type="text" placeholder="Text hint..."/>
                            <p>Qualifying text</p>
                            <div className="btnGroup">
                                <button onClick={() => setSaveAsTemplatePopup(false)}>Cancel</button>
                                <button onClick={newTemplate}>Save</button>
                            </div>
                        </div>
                        <div className="itemMenu" onClick={() => {
                            handleClickTools(); //close menu
                            handleClickItem("add lat and long");
                        }}><p>Add Lat and Long</p></div>
                    </div>
                </div>
                {/*=====================================*/}

                <div className="secondOpt">
                    Creating new. {nowLevelName}
                    {/*{console.log('props:', props,'state',  props.state, 'ifloorplanNowState', props.ifloorplanNowState.level[0].tabLabel)}*/}
                </div>
                {/*    =======================================*/}
            </div>
            <div className="secondOptBtn">
                {/*======btn preev next ============*/}
                <a href="" onClick={(event) => {
                    event.preventDefault();
                    if (props.step === "cams position") {
                        handlePrev(event);
                    }
                }} className={props.step === "cams position" ? "prevBtn" : "prevBtn not"}
                   style={{color: "blue"}}>
                    <i className="fa fa-caret-left" aria-hidden="true"></i>
                    <p style={{color: "blue"}}>Previous</p>
                </a>

                <a href="" onClick={(event) => {
                    event.preventDefault();
                    if (props.step === "cams position") {
                        handleNext(event);
                    }
                }} className={props.step === "cams position" ? "nextBtn" : "nextBtn not"}
                   style={{color: lastColorBtn ? "blue" : "orange"}}>
                    <p style={{color: lastColorBtn ? "blue" : "orange"}}>Next</p><i className="fa fa-caret-right"
                                                                                    aria-hidden="true"></i></a>
            </div>

            <div className="thirthOpt">
                <SaveAnimation animation={props.animation} state={props.ifloorplanNowState}/>
                <a href="" className={props.step === "cams position" ? "saveNowBtn" : "saveNowBtn not"}
                   onClick={(event) => {
                       event.preventDefault();
                       if (props.step === "cams position") {

                       }
                   }}
                >
                    {/*<p>Save now...</p>*/}
                </a>
                <a href={'https://ifloorplan360.com/ifloorplan/' + ifloorplanObj.id}
                   onClick={(event) => {
                       if (props.step === "cams position") {

                       } else {
                           event.preventDefault();
                       }
                   }}
                   target="_blank"
                   className={props.step === "cams position" ? "previewBtn" : "previewBtn not"}>
                    <p>Preview</p></a>
                <a href="" className={props.step === "cams position" ? "publishBtn" : "publishBtn not"}
                   onClick={(event) => {
                       event.preventDefault();
                       if (props.step === "cams position") {
                           Publish();
                       }
                   }}
                ><p>{props.type_action === "edit" ? "Update" : "Publish..."}</p></a>
            </div>
            {props.set_color ? <ColorSet/> : ''}
            {props.image_markers_layout ? <ImageMarkersStudio setAnimation={props.setAnimation}/> : ''}
        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        positionCams: state.positionCams,
        ifloorplanNowState: state.ifloorplanNowState,
        levelNowId: state.levelNowId,
        image_key: state.image_key,
        cam_key: state.cam_key,
        set_color: state.set_color,
        prev_step: state.prev_step,
        type_action: state.type_action,
        image_markers_layout: state.image_markers_layout
    }
}
const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        changeCamKey: (key) => dispatch(changeCamKey(key)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        changeCamPosition: (obj) => dispatch(changeCamPosition(obj)),
        changeLevel: (level) => dispatch(changeLevel(level)),
        changeImageKey: (key) => dispatch(changeImageKey(key)),
        setColor: (bool) => dispatch(setColor(bool)),
        setColorBackground: (color) => dispatch(setColorBackground(color)),
        setColorText: (color) => dispatch(setColorText(color)),
        setOpenImageMarkers: (bool) => dispatch(setOpenImageMarkers(bool)),
    }
}

export default connect(putState, putActions)(TopPanel);