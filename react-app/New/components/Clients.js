import React, {useState, useEffect, useRef} from 'react';
import { connect } from 'react-redux';
import {updateNewPlan, getClients, getLogoClients } from '../serverRequests/requests';
import {
    nextStep,
    ifloorplanStateAdd,} from '../redux/actions/actions';
import Client from './Client';

const LatLong = (props) => {
    const ifloorplanObj = props.ifloorplanNowState;
    const [clients, setClients] = useState([]);
    const [listRenderClients, setListRenderClients] = useState([]);
    const [clientsLogo, setClientsLogo] = useState([]);
    const [clientRender, setClientRender] = useState({});
    const [selectClient, setSelectClient] = useState({
        id: "",
        name: "",
        contact: "",
        client: ""
    });

    useEffect(()=>{
        const data_request = {
            user_id: document.getElementById("user_id").value
        }
        getClients(data_request)
            .then(data=>{
                setClients(data);
            })
        getLogoClients(data_request)
            .then(data=>{
                setClientsLogo(data);
            })
    }, []);

    console.log(clientsLogo);

    useEffect(()=>{
        const render = [];

        for(let i = 0; i < clients.length; i++){
            render.push(
                <div className={clients[i].id == selectClient.id ? "active_client" : "client"} key={i} onClick={()=>{
                    console.log(clients[i].id)
                    setSelectClient({id:clients[i].id, name:clients[i].client_name, contact:clients[i].contact_name, client:clients[i]})
                }}><h3>{clients[i].client_name} - {clients[i].contact_name}</h3></div>
            )
        }
        setListRenderClients(render);

        for(let i = 0; i < clientsLogo.length; i++){
            if(clientsLogo[i].client == selectClient.id){
                setClientRender(clientsLogo[i]);
            }
        }

    }, [clients, selectClient])

    const Save = () => {
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        newState.client_data = JSON.stringify(selectClient.client);
        newState.client_position = JSON.stringify(clientRender);
        updateNewPlan(newState)
            .then(data => {
                props.setAnimation(animationSaved=>animationSaved+1);
                props.ifloorplanStateAdd(data[0]);
                props.nextStep("cams position", "_");
            })
    }

    console.log(clientRender, selectClient)
    return(
        <div className="clients_wrap">
            <div className="client_list_wrap">
                <h3 className="title_client_list">Choose a Client for this iFloorPlan</h3>
                <div className="client_items">
                    {listRenderClients}
                </div>
            </div>
            <div className="clientWrap">
                <h3 className="name_client">{selectClient.name} - {selectClient.contact}</h3>
                <div className="client_preview">
                    <Client clientData={selectClient.client} clientPosition={clientRender}/>
                </div>
                <div className="inputs_client">
                    <input type="text" placeholder="Change contact name"/>
                    <input type="text" placeholder="Change contact phone number"/>
                    <input type="text" placeholder="Change contact email"/>
                </div>
                <div className="btn_group_client">
                    <button className="cancel" onClick={()=>props.nextStep("cams position", "_")}>Cancel</button>
                    <button className="rem_client">Remove this Client</button>
                    <button className="done" onClick={Save}>Done</button>
                </div>
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        ifloorplanNowState: state.ifloorplanNowState,
    }
}
const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
    }
}
export default connect(putState, putActions)(LatLong);