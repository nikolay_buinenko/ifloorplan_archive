import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import { getSystemIcons } from "../serverRequests/requests";
import { nextStep, set360ToolsAction, ifloorplanStateAdd, panelumLib } from '../redux/actions/actions';
import Dropzone from "react-dropzone";

const PanMarkers = (props) => {
    const { ifloorplanNowState } = props;
    const [markers, setMarkers] = useState([]);
    const [systemIcon, setSystemIcon] = useState([]);
    // const systemIcon = [
    //     "/media/system_images/panelum_icon1.png",
    //     "/media/system_images/panelum_icon2.png",
    //     "/media/system_images/panelum_icon3.png",
    // ];

    const markersInit = () => {
        let markers_ready = [];
        systemIcon.map((value, index) => (
            markers_ready.push(<div className="panelum_marker" key={index}>
                <img src={value.marker} alt=""/>
            </div>)
        ));
        setMarkers(markers_ready);
    }

    const myMarkersInit = () => {
        setMarkers([]);
        return;
    }

    useEffect(()=>{
        getSystemIcons()
            .then((data)=>{
                const icons = [];
                for(let i = 0; i < data.length; i++) {
                    if (data[i].type == "PANORAMA") {
                        icons.push(data[i]);
                    }
                }
                setSystemIcon(icons);
                markersInit();
            })
    }, []);

    return (
        <div className="markers_panelum">
            <div className="title_markers">
                <h3>Drag a marker on to the floor plan</h3>
            </div>
            <div className="markers_wrap">
                <div className="markers_option">
                    <a href="" onClick={(event)=>{
                        event.preventDefault();
                        event.currentTarget.className += "active";
                        markersInit();
                    }}>Markers</a>
                    <a href="" onClick={(event) => {
                        event.preventDefault();
                        event.currentTarget.className += "active";
                        myMarkersInit();
                    }}>My Markers</a>
                </div>
                <div className="markers">
                    {markers}
                </div>
            </div>
            <div className="markers_keyboard">
                <div className="scale">
                    <label htmlFor="scale">Scale</label>
                    <input type="range" id="scale" name="cowbell"
                           min="0" max="100" value="50" step="10"/>
                </div>
                <div className="btn_group">
                    <button className="add">Add another 360</button>
                    <button className="save">Done</button>
                </div>
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        levelNowId: state.levelNowId,
        ifloorplanNowState: state.ifloorplanNowState,
        tools_360_action: state.tools_360_action,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        set360ToolsAction: (value) => dispatch(set360ToolsAction(value))
    }
}


export default connect(putState, putActions) (PanMarkers);