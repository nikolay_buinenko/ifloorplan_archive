import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {
    changeCamPosition,
    changeImageKey,
    changeLevel,
    ifloorplanStateAdd,
    nextStep,
    setColor,
    setColorBackground,
    setColorText
} from '../redux/actions/actions';
import {ChromePicker} from 'react-color';
import {getSystemIcons, getUserIcons, updateNewPlan} from "../serverRequests/requests";

const CustomButtons = (props) => {

    const [toggleColors, setToggleColors] = useState(true)
    const [buttonLabel, setButtonLabel] = useState('Button')
    const [colorBg, setColorBg] = useState([])
    const [colorFont, setColorFont] = useState([])
    const [butWidth, setButWidth] = useState(props.ifloorplanNowState.level[props.levelNowId].images[props.image_key].button_padding_gor)
    const [butHeight, setButHeight] = useState(props.ifloorplanNowState.level[props.levelNowId].images[props.image_key].button_padding_vert)
    const [butRounding, setButRounding] = useState(props.ifloorplanNowState.level[props.levelNowId].images[props.image_key].button_radius)
    const [butScale, setButScale] = useState(props.ifloorplanNowState.level[props.levelNowId].images[props.image_key].button_scale)


    // Pressing the button 'Swap Marker for a button'/'Swap with active marker' changes the marker to the button
    useEffect(() => {

        const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));

        if (props.swapMarkButt) {
            newState.level[props.levelNowId].images[props.image_key].button = true
            newState.level[props.levelNowId].images[props.image_key].button_label = buttonLabel
        } else {
            newState.level[props.levelNowId].images[props.image_key].button = false
        }

        props.ifloorplanStateAdd(newState)
    }, [props.swapMarkButt])

    return (
        <div className='customButtons' style={{opacity: props.opacity, zIndex: props.zindex}}>
            <div className="customButtons__example">
                <button className="customButtons__example_btn"
                        style={{
                            backgroundColor: colorBg,
                            color: colorFont,
                            padding: butHeight + 'rem' + ' ' + butWidth + 'rem',
                            borderRadius: butRounding + 'rem'
                        }}>
                    {buttonLabel}
                </button>
            </div>
            <ChromePicker
                color={toggleColors ? colorBg : colorFont}
                onChangeComplete={

                    toggleColors
                        ?
                        (color) => {
                            setColorBg(color.hex)
                            const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
                            newState.level[props.levelNowId].images[props.image_key].button_bg = color.hex
                            props.ifloorplanStateAdd(newState)
                        }
                        :
                        (color) => {
                            setColorFont(color.hex)
                            const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
                            newState.level[props.levelNowId].images[props.image_key].button_color_text = color.hex
                            props.ifloorplanStateAdd(newState)
                        }

                }
            />
            <div className="selectButton__colorSelector">
                <h3 className="bg_color">Background color</h3>
                <div className={toggleColors
                    ?
                    "switch-btn"
                    :
                    "switch-btn switch-on"}
                     onClick={
                         () => {
                             setToggleColors(!toggleColors)
                         }
                     }
                ></div>
                <h3 className="font_color">Font color</h3>
            </div>
            <div className="selectButton__label">
                <h3>Label</h3>
                <input
                    type="text"
                    value={buttonLabel}
                    onChange={(event) => {
                        setButtonLabel(event.target.value)
                        const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
                        newState.level[props.levelNowId].images[props.image_key].button_label = event.target.value
                        props.ifloorplanStateAdd(newState)
                    }}/>
            </div>
            <div className="scale-wrapper">
                <h3>Button wigth</h3>
                <input type="range"
                       id='buttonSize'
                       step="0.05"
                       className="scale-image"
                       min="0.2"
                       max="5"
                       value={butWidth}
                       onChange={(event) => {
                           setButWidth(event.target.value)
                           const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
                           newState.level[props.levelNowId].images[props.image_key].button_padding_gor = event.target.value
                           props.ifloorplanStateAdd(newState)
                       }}/>
            </div>
            <div className="scale-wrapper">
                <h3>Button height</h3>
                <input type="range"
                       id='buttonSize'
                       step="0.05"
                       className="scale-image"
                       min="0.1"
                       max="3"
                       value={butHeight}
                       onChange={(event) => {
                           setButHeight(event.target.value)
                           const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
                           newState.level[props.levelNowId].images[props.image_key].button_padding_vert = event.target.value
                           props.ifloorplanStateAdd(newState)
                       }}/>
            </div>
            <div className="scale-wrapper">
                <h3>Corner rounding</h3>
                <input type="range"
                       id='buttonSize'
                       step="0.05"
                       className="scale-image"
                       min="0.5"
                       max="4.1"
                       value={butRounding}
                       onChange={(event) => {
                           setButRounding(event.target.value)
                           const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
                           newState.level[props.levelNowId].images[props.image_key].button_radius = event.target.value
                           props.ifloorplanStateAdd(newState)
                       }}/>
            </div>
            <div className="scale-wrapper">
                <h3>Button scale</h3>
                <input type="range"
                       id='buttonSize'
                       step="0.05"
                       className="scale-image"
                       min="0.5"
                       max="3"
                       value={butScale}
                       onChange={(event) => {
                           setButScale(event.target.value)
                           const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
                           newState.level[props.levelNowId].images[props.image_key].button_scale = event.target.value
                           props.ifloorplanStateAdd(newState)
                       }}/>
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        levelNowId: state.levelNowId,
        cam_key: state.cam_key,
        image_key: state.image_key,
        positionCams: state.positionCams,
        ifloorplanNowState: state.ifloorplanNowState,
        text_color: state.text_color,
        background_color: state.background_color,
        set_color: state.set_color,
    }
}
const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        changeCamKey: (key) => dispatch(changeCamKey(key)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        changeCamPosition: (obj) => dispatch(changeCamPosition(obj)),
        setColor: (bool) => dispatch(setColor(bool)),
        changeLevel: (level) => dispatch(changeLevel(level)),
        changeImageKey: (key) => dispatch(changeImageKey(key)),
        setColorBackground: (color) => dispatch(setColorBackground(color)),
        setColorText: (color) => dispatch(setColorText(color))
    }
}
export default connect(putState, putActions)(CustomButtons);