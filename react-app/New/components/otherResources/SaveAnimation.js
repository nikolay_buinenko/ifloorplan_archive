import React, {useState, useEffect} from 'react'


const SaveAnimation = (props) => {

    const [save, setSave] = useState(false)

    useEffect(() => {

        console.log(props)

        setSave(true)
        let saveBlock = document.querySelector('.save')
        saveBlock.style.opacity = 1

        const timer = setTimeout(() => {
            setSave(false)

            const timer = setTimeout(() => {
                let saveBlock = document.querySelector('.save')
                saveBlock.style.opacity = 0
            }, 1000);
            return () => clearTimeout(timer);

        }, 1000);
        return () => clearTimeout(timer);

    }, [props.animation]);


    return (
        <div className='saveAnimation'>
            {save
                ?
                <div className="save">
                    <h3>Save</h3>
                    <div className="preloader"></div>
                </div>
                :
                <div className="save">
                    <h3>Saved </h3>
                    <span> &#10004;</span>
                </div>
            }
        </div>
    )
}

export default SaveAnimation;