import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {nextStep, dropFiles} from '../redux/actions/actions';
import Dropzone from "react-dropzone";
import ScaleLayout from "./ScaleLayout";


const UploadPlans = (props) => {

    const handleDrop = (files) => {
        props.dropFiles(files, "PLANS");
        if (props.prev_step === "template") {
            props.nextStep("upload files", "template");
        } else {
            props.nextStep("upload files", "_");
        }
    }

    // useEffect(() => {
    //     props.setShowTools(true)
    // }, [])

    return (
        <div className="uploadPlans">

            <Dropzone onDrop={handleDrop}>
                {({getRootProps, getInputProps}) => (
                    <div {...getRootProps({className: "dropzone"})}>
                        <h3>Drag & Drop your floor plan(s) here...</h3>
                        <h3>Or</h3>
                        <input {...getInputProps()} />
                        <button>Choose files</button>
                        <p>Creating a multi level iFloorPlan</p>
                        <p>You can upload all your floor plans for this property. Access them from the Floor plan
                            library</p>
                    </div>

                )}
            </Dropzone>
        </div>
    );
}

const putState = (state) => {
    return {
        step: state.step,
        prev_step: state.prev_step,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        dropFiles: (files, title) => dispatch(dropFiles(files, title))
    }
}

export default connect(putState, putActions)(UploadPlans);