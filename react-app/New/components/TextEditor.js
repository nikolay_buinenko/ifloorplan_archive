import React, {useState} from 'react';
import { connect } from 'react-redux';
import {updateNewPlan} from '../serverRequests/requests';
import { nextStep, ifloorplanStateAdd} from '../redux/actions/actions';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment';

const TextEditor = (props) => {
    const [textData, setTextData] = useState(props.ifloorplanNowState.textBlock);

    const Save = () => {
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        newState.textBlock = textData;
        updateNewPlan(newState)
            .then(data=>{
                props.setAnimation(animationSaved=>animationSaved+1);
                console.log('Text editor data:', data);
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                props.nextStep("cams position", "_");
            });
    }

    return (
        <div className="textEditor">
                <CKEditor
                    editor={ ClassicEditor }
                    data={textData}
                    onInit={ editor => {
                        // You can store the "editor" and use when it is needed.
                        // console.log( 'Editor is ready to use!', editor );
                    } }
                    // config={ {
                    //     plugins: [ Alignment, ],
                    //     toolbar: [ 'alignment', ]
                    // } }
                    onChange={ ( event, editor ) => {
                        const data = editor.getData();
                        setTextData(data);
                    } }
                    onBlur={ ( event, editor ) => {
                        // console.log( 'Blur.', editor );
                    } }
                    onFocus={ ( event, editor ) => {
                        // console.log( 'Focus.', editor );
                    } }
                />

                <div className="buttonGroup_textEditor">
                    <button className="cancel" onClick={()=>props.nextStep("cams position", "_")}>Cancel</button>
                    <button className="save" onClick={Save}>Save</button>
                </div>
        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        ifloorplanNowState: state.ifloorplanNowState,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
    }
}


export default connect(putState, putActions) (TextEditor);