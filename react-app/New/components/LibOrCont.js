import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Draggable, {DraggableCore} from 'react-draggable';
import { nextStep, ifloorplanStateAdd, initScale } from '../redux/actions/actions';
import {updateNewPlan} from '../serverRequests/requests';

import EditTextBlock from './EditTextBlock.js';

const LibOrCont = (props) => {

    const ifloorplanObj = props.ifloorplanNowState;
    const [inputStyle, setInputStyle] = useState({
        position_x: ifloorplanObj.addressPosition_x,
        position_y: ifloorplanObj.addressPosition_y
    });

    const levelObj = ifloorplanObj.level[props.levelNowId];

    const [imageStyle, setStyle] = useState({
        img_width: ifloorplanObj.level[props.levelNowId].img_width,
        position_x: ifloorplanObj.level[props.levelNowId].img_pos_x,
        position_y: ifloorplanObj.level[props.levelNowId].img_pos_y
    });
    const [planLevel, setPlanLevel] = useState({
        img: levelObj.plan_level[0].img || null,
        img_width: ifloorplanObj.level[props.levelNowId].plan_width,
        position_x: ifloorplanObj.level[props.levelNowId].plan_pos_x,
        position_y: ifloorplanObj.level[props.levelNowId].plan_pos_y
    });

    const Continue = () => {
            let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));

            for(let i = 0; i < props.LibraryImage.length; i++){
                newState.level[props.levelNowId].images.push({
                    img: props.LibraryImage[i].image,
                    img_id: props.LibraryImage[i].id,
                    index: i
                });
            }
            newState.splash_screen = props.LibraryImage[0].image;
            updateNewPlan(newState)
                .then(data => {
                    props.setAnimation(animationSaved=>animationSaved+1);
                    props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                    props.nextStep("cams position", "_");
                })
    }



    useEffect(()=>{
        const parrentBlock = document.getElementsByClassName("workPlace")[0];

        parrentBlock.style.color = ifloorplanObj.textColor;
        parrentBlock.style.background = ifloorplanObj.bgColor;
    }, [])


    return (
        <div className="scale-layout workPlace">
            <div className="scales-panel">
                <div className="notification">
                    <p>
                        Displaing the first uploaded image.
                        Select images fro this level  by opening the
                        <a href="" onClick={(event) => {
                            event.preventDefault();
                            props.nextStep("image library", "first");
                        }}> Image Library</a> or
                        <a href="" onClick={(event) => {
                            event.preventDefault();
                            Continue();
                        }}> Continue</a> to use all uploads on this level.
                    </p>
                </div>
            </div>

            <div className='dnd-wrapper' style={{overflow: "hidden"}}>

                <div className='interior-image'
                     style={{
                         position: 'absolute',
                         top: ifloorplanObj.level[0].img_pos_y+"%", //add top pos from DB
                         left: ifloorplanObj.level[0].img_pos_x+"%", //add left pos from DB
                         width: `${imageStyle.img_width}%`,
                         zIndex: -1
                     }}>
                    <img className='interior-image'
                         src={ifloorplanObj.level[props.levelNowId].images.length == 0 ?
                                props.LibraryImage[0].image
                                   :
                                ifloorplanObj.level[props.levelNowId].images[0].img
                            }
                         style={{width: '100%'}}/>
                </div>

                <div className='floor-plan'
                     style={{
                         position: 'absolute',
                         top: ifloorplanObj.level[props.levelNowId].plan_pos_y+"%", //add top pos from DB
                         left: ifloorplanObj.level[props.levelNowId].plan_pos_x+"%", //add left pos from DB
                         width: `${planLevel.img_width}%`,
                         zIndex: -1
                     }}>
                    <img className='floor-plan'
                         src={planLevel.img}
                         style={{width: '100%'}}/>

                </div>
                <div className='edit-text-block'
                     style={{
                         position: 'absolute',
                         top: ifloorplanObj.addressPosition_y+"%",
                         left: ifloorplanObj.addressPosition_x+"%"
                     }}>
                    <EditTextBlock/>

                </div>

                {ifloorplanObj.brand_logo == null ? "" :
                    <div className="brandWrap" style={{position: 'absolute', width: ifloorplanObj.brand_width+'%', opacity: ifloorplanObj.brand_transparency, top:ifloorplanObj.brand_pos_y+"%", left:ifloorplanObj.brand_pos_x+"%"}}>
                        <img style={{width: '100%'}} src={ifloorplanObj.brand_logo} alt=""/>
                    </div>
                }
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        prev_step: state.prev_step,
        LibraryImage: state.LibraryImage,
        LibraryPlans: state.LibraryPlans,
        ifloorplanNowState: state.ifloorplanNowState,
        levelNowId: state.levelNowId,
        scale: state.scale
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        dropFiles: (files, title) => dispatch(dropFiles(files, title)),
        initScale: (bool) => dispatch(initScale(bool)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan))
    }
}
export default connect(putState, putActions)(LibOrCont);