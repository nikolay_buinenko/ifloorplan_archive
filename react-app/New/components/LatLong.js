import React, {useState} from 'react';
import { connect } from 'react-redux';
import {updateNewPlan} from '../serverRequests/requests';
import {
    nextStep,
    ifloorplanStateAdd,} from '../redux/actions/actions';

const LatLong = (props) => {
    const ifloorplanObj = props.ifloorplanNowState;
    const [latLong , setLatLong] = useState("");

    const Save = () => {

        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        if(latLong != ""){
            newState.lat_long = latLong;
            updateNewPlan(newState)
                .then((data)=>{
                    props.setAnimation(animationSaved=>animationSaved+1);
                    console.log('LatLong data: ', data);
                    props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                    props.nextStep("cams position", "_");
                })
        }
    }

    return(
        <div className="lat_long">
            <div className="title_lat">
                <h3 className="first_title_lat">if we can't find the property address you have entered you can still have a map view</h3>
                <h3>You can find the property on this map, get the Latitude and Longitude co-ordinates</h3>
                <div className="map_btn">
                    <a href="http://www.ifloorplan.com/map" target="_blank" className="map">Map</a>
                </div>
            </div>
            <div className="input_lat">
                <div className="input_lat_long">
                    <label htmlFor="lat_long">Paste the co-ordinates in the field below</label>
                    <input type="text" id="lat_long" onChange={event => {
                        setLatLong(event.target.value);
                    }} placeholder="-34.42872250867193, 150.5506162109375"/>
                </div>
                <div className="button_group">
                    <button className="cancel" onClick={()=>props.nextStep("cams position", "_")}>Cancel</button>
                    <button className="save" onClick={Save}>Save</button>
                </div>
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        ifloorplanNowState: state.ifloorplanNowState,
    }
}
const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
    }
}
export default connect(putState, putActions)(LatLong);