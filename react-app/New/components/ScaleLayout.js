import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Draggable, {DraggableCore} from 'react-draggable';
import { nextStep, ifloorplanStateAdd, initScale } from '../redux/actions/actions';
import {updateNewPlan} from '../serverRequests/requests';

import EditTextBlock from './EditTextBlock.js';

const ScaleLayout = (props) => {

    const ifloorplanObj = props.ifloorplanNowState;
    const { LibraryImage, LibraryPlans } = props;
    const [activeDrags, setActiveDrags] = useState(0);
    const [notification, setNotification] = useState(false);
    const [inputStyle, setInputStyle] = useState({
        position_x: ifloorplanObj.addressPosition_x,
        position_y: ifloorplanObj.addressPosition_y
    });

    const levelObj = ifloorplanObj.level[props.levelNowId];

    const [imageStyle, setStyle] = useState({
        img_width: ifloorplanObj.level[0].img_width,
        position_x: ifloorplanObj.level[props.levelNowId].img_pos_x,
        position_y: ifloorplanObj.level[props.levelNowId].img_pos_y
    });
    const [planLevel, setPlanLevel] = useState({
        img: levelObj.plan_level[0].img || null,
        img_width: ifloorplanObj.level[props.levelNowId].plan_width,
        position_x: ifloorplanObj.level[props.levelNowId].plan_pos_x,
        position_y: ifloorplanObj.level[props.levelNowId].plan_pos_y
    });

    if (props.LibraryImage.length === 0){
        props.nextStep("upload images", "_");
    }


    const handleScale = (event) => {
        //change size block images
        if (event.target.className === 'scale-floor') {
            setPlanLevel({...planLevel,
                img_width: event.target.value
            });
        } else {
            setStyle({ ...imageStyle,
                img_width: event.target.value
            });
        };
    };

    const dragHandleImage = (event) => {
        //set position block images
        event.preventDefault();
        // const block = document.querySelector(`.${event.target.className}`);
        const childBlock = document.getElementsByClassName(event.target.className)[0];

        const parrentBlock = document.getElementsByClassName("dnd-wrapper")[0];
        let childX = childBlock.getBoundingClientRect().left;
        let childY = childBlock.getBoundingClientRect().top;
        const { top, left, width, height } = parrentBlock.getBoundingClientRect();

        let x = (childX - left) / width * 100; // get position at parsent
        let y = (childY - top) / height * 100;

        switch (event.target.className) {
            case 'interior-image':
                setStyle({
                    ...imageStyle,
                    position_y: y,
                    position_x: x
                });
                break;
            case 'floor-plan':
                setPlanLevel({
                    ...planLevel,
                    position_x: x,
                    position_y: y
                });
                break;

        };
    };

    const dragHandleText = (event) => {
        event.preventDefault();
        const childBlock = document.getElementsByClassName("edit-text-block")[0];

        const parrentBlock = document.getElementsByClassName("dnd-wrapper")[0];
        let childX = childBlock.getBoundingClientRect().left;
        let childY = childBlock.getBoundingClientRect().top;
        const { top, left, width, height } = parrentBlock.getBoundingClientRect();

        let x = (childX - left) / width * 100;
        let y = (childY - top) / height * 100;
        setInputStyle({
            ...inputStyle,
            position_x: x,
            position_y: y
        })
    }

    const handlerSubmit = () => {
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));

        newState.level[props.levelNowId].plan_width = planLevel.img_width;
        newState.level[props.levelNowId].plan_pos_x = planLevel.position_x;
        newState.level[props.levelNowId].plan_pos_y = planLevel.position_y;

        newState.addressPosition_x = inputStyle.position_x;
        newState.addressPosition_y = inputStyle.position_y;

        newState.level[props.levelNowId].img_width = imageStyle.img_width;
        newState.level[props.levelNowId].img_pos_x = props.prev_step === "new level" ? ifloorplanObj.level[0].img_pos_x : imageStyle.position_x;
        newState.level[props.levelNowId].img_pos_y = props.prev_step === "new level" ? ifloorplanObj.level[0].img_pos_y : imageStyle.position_y;

        updateNewPlan(newState)
            .then(data => {
                props.setAnimation(animationSaved=>animationSaved+1);
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                if(props.prev_step === "new level"){
                    props.nextStep("cams position", 'new level');
                } else {
                    props.nextStep("lib or cont", "_")
                }
            });
    };

    useEffect(()=>{
        const parrentBlock = document.getElementsByClassName("workPlace")[0];

        parrentBlock.style.color = ifloorplanObj.textColor;
        parrentBlock.style.background = ifloorplanObj.bgColor;
    }, [])

    const onStart = () => {setActiveDrags(activeDrags + 1)};
    const onStop = () => {setActiveDrags(activeDrags - 1)};
    const dragHandlers = {onStart: onStart, onStop: onStop};

    return (
        <div className="scale-layout workPlace">
            <div className="scales-panel">
                {props.prev_step === "_" ?
                    <div>
                        <div className="scale-wrapper">
                            <h3>Scale images</h3>
                            <input type="range"
                                   step="1"
                                   className="scale-image"
                                   min="10"
                                   max="90"
                                   onChange={handleScale}
                                   value={imageStyle.img_width}/>
                        </div>
                        <button onClick={handlerSubmit}>
                            Continue
                        </button>
                        <div className="scale-wrapper">
                            <h3>Scale floor plans</h3>
                            <input type="range"
                                   step="1"
                                   className="scale-floor"
                                   min="10"
                                   max="90"
                                   onChange={handleScale}
                                   value={planLevel.img_width}/>
                        </div>
                    </div>
                    :
                        <div>
                            <div className="scale-wrapper">
                                <h3>Scale floor plans</h3>
                                <input type="range"
                                       step="1"
                                       className="scale-floor"
                                       min="10"
                                       max="90"
                                       onChange={handleScale}
                                       value={planLevel.img_width}/>
                            </div>
                            <button onClick={handlerSubmit}>
                                Continue
                            </button>
                        </div>
                }


            </div>

            <div className='dnd-wrapper'>

                <Draggable
                    onDrag={dragHandleImage}
                    disabled={props.prev_step === "_" ? false : true}
                    {...dragHandlers}>
                    <div className='interior-image'
                         style={{
                             position: 'absolute',
                             top: ifloorplanObj.level[0].img_pos_y+"%", //add top pos from DB
                             left: ifloorplanObj.level[0].img_pos_x+"%", //add left pos from DB
                             width: `${imageStyle.img_width}%`
                          }}>
                        <img className='interior-image'
                            src={ifloorplanObj.level[props.levelNowId].images.length == 0 ?
                                props.LibraryImage[0].image
                                   :
                                ifloorplanObj.level[props.levelNowId].images[0].img
                            }
                             style={{width: '100%'}}/>
                    </div>
                </Draggable>

                <Draggable
                    onDrag={dragHandleImage}
                    {...dragHandlers}>
                    <div className='floor-plan'
                         style={{
                             position: 'absolute',
                             top: ifloorplanObj.level[props.levelNowId].plan_pos_y+"%", //add top pos from DB
                             left: ifloorplanObj.level[props.levelNowId].plan_pos_x+"%", //add left pos from DB
                             width: `${planLevel.img_width}%`
                         }}>
                        <img className='floor-plan'
                             src={planLevel.img}
                             style={{width: '100%'}}/>

                    </div>
                </Draggable>
                <Draggable
                    bounds="parent"
                    onDrag={dragHandleText}
                    disabled={props.prev_step === "_" ? false : true}
                    {...dragHandlers}>
                    <div className='edit-text-block'
                        style={{
                             position: 'absolute',
                             top: ifloorplanObj.addressPosition_y+"%",
                             left: ifloorplanObj.addressPosition_x+"%"
                          }}>
                        <EditTextBlock/>

                    </div>
                </Draggable>

                {ifloorplanObj.brand_logo == null ? "" :
                    <div className="brandWrap" style={{position: 'absolute', width: ifloorplanObj.brand_width+'%', opacity: ifloorplanObj.brand_transparency, top:ifloorplanObj.brand_pos_y+"%", left:ifloorplanObj.brand_pos_x+"%"}}>
                        <img style={{width: '100%'}} src={ifloorplanObj.brand_logo} alt=""/>
                    </div>
                }
            </div>
            {/*<EditTextBlock/>*/}
        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        prev_step: state.prev_step,
        LibraryImage: state.LibraryImage,
        LibraryPlans: state.LibraryPlans,
        ifloorplanNowState: state.ifloorplanNowState,
        levelNowId: state.levelNowId,
        scale: state.scale
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        dropFiles: (files, title) => dispatch(dropFiles(files, title)),
        initScale: (bool) => dispatch(initScale(bool)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan))
    }
}
export default connect(putState, putActions)(ScaleLayout);