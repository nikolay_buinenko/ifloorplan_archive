import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import { ChromePicker } from 'react-color';
import {updateNewPlan} from '../serverRequests/requests';
import {getSystemIcons, getUserIcons, } from '../serverRequests/requests';
import {setColor,
    nextStep,
    setColorBackground,
    setColorText,
    ifloorplanStateAdd } from '../redux/actions/actions';

const VideoSideBar = (props) => {
    const ifloorplanObj = props.ifloorplanNowState; //last ifloor plan obj
    const [tools, setTools] = useState(false); // now action
    const [link, setLink] = useState("");
    const [clearLink, setClearLink] = useState("");
    const [btnGroup, setBtnGroup] = useState(false);
    const [markersRender, setMarkersRender] = useState([]);
    const [systemIcon, setSystemIcon] = useState([]);
    const [userIcon, setUserIcon] = useState([]);

    const SystemMarkersInit = () => {
        const render = [];
        systemIcon.map((item, index) => {
            render.push(
                <div className="marker"
                     onClick={()=>{props.setMarkerImg(item); setBtnGroup(true)}}
                     key={index}>
                    <img src={item.marker} alt=""/>
                </div>
            )
        })
        setMarkersRender(render);
    }

    const UserMarkersInit = () => {
        const render = [];
        userIcon.map((item, index) => {
            render.push(
                <div className="marker"
                     onClick={()=>{props.setMarkerImg(item); setBtnGroup(true)}}
                     key={index}>
                    <img src={item.marker} alt=""/>
                </div>
            )
        })
        setMarkersRender(render);
    }

    useEffect(()=>{
        if(tools){
            getSystemIcons()
                .then((data)=>{
                    const icons = [];
                    for(let i = 0; i < data.length; i++) {
                        if (data[i].type == "VIDEO") {
                            icons.push(data[i]);
                        }
                    }
                    setSystemIcon(icons);
                    SystemMarkersInit();
                })
            getUserIcons()
                .then((data)=>{
                    const icons = [];
                    for(let i = 0; i < data.length; i++) {
                        if (data[i].type == "VIDEO") {
                            icons.push(data[i]);
                        }
                    }
                    setUserIcon(icons);
                })
        }
    }, [tools]);

    useEffect(() => {
        if (systemIcon && !markersRender.length) {
            SystemMarkersInit();
        }
    }, [systemIcon.length]);

    const Get = () => {
        if (link !== '') {
            setTools(true);
            let url = new URL(link);
            let v = url.searchParams.get("v");
            console.log(v)
            if(v == null){
                v = url.pathname;
            }
            props.setLink(v);
            setClearLink(v);
        }
    }

    const addAnother = () => {
        setTools(false);
        setBtnGroup(false);
        props.addAnother();
    }

    return (
        <div className="video_marker">
            <div className="linkAdd_video_marker">
                <h2 className="title">Add a Video</h2>
                <label htmlFor="urlLink">Link a video</label>
                <input type="text" id="urlLink" placeholder="Enter URL" value={link} onChange={(event)=>setLink(event.target.value)}/>
                <button onClick={Get}>Get</button>
            </div>
            {!tools ? "" :
                <div className="tools_video_marker">
                    <div className="preview_video_marker">
                        <iframe id="ytplayer" type="text/html"
                                src={clearLink === '' ? '' : "https://www.youtube.com/embed/"+clearLink}
                                frameBorder="0"/>
                    </div>
                    <div className="type_markers_video">
                        <button onClick={SystemMarkersInit}>Markers</button>
                        <button onClick={UserMarkersInit}>My Markers</button>
                    </div>
                    <div className="markers_video_marker">{markersRender}</div>
                    <div className="scale_video_marker">
                        <label htmlFor="scale">Scale</label>
                        <input type="range" min="0" max="25" value={props.scale} onChange={(event) => {
                            props.setScale(event.target.value);
                        }}/>
                    </div>
                    {!btnGroup ? "" :
                        <div className="btn_group_video_marker">
                            <button className="add_another" onClick={addAnother}>Add another video</button>
                            <button className="done" onClick={props.done}>Done</button>
                        </div>
                    }
                </div>
            }
        </div>
    )
}

const putState = (state) => {
    return {
        ifloorplanNowState: state.ifloorplanNowState,
        set_color: state.set_color,
        text_color: state.text_color,
        background_color: state.background_color,
    }
}
const putActions = (dispatch) => {
    return {
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        setColor: (bool) => dispatch(setColor(bool)),
        setColorBackground: (color) => dispatch(setColorBackground(color)),
        setColorText: (color) => dispatch(setColorText(color))
    }
}

export default connect(putState, putActions)(VideoSideBar);