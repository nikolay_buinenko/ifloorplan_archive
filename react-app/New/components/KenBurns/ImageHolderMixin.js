let ImageHolderMixin = {

  fetchImage: function (props) {
    let img = new window.Image();
    if (!props.image) return;
    this.curImgSrc = img.src = props.image;
    let self = this;
    img.onload = function () {
      if (self.onImageLoaded) self.onImageLoaded(img);
      if (self.isMounted())
        self.forceUpdate();
    };
    this.img = img;
  },

  componentDidMount: function () {
    this.fetchImage(this.props);
  },

  componentDidUpdate: function (props) {
    if (!this.curImgSrc || this.curImgSrc !== props.image) {
      this.fetchImage(props);
    }
  }

};

module.exports = ImageHolderMixin;