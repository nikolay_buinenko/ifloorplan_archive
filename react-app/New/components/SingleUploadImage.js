import React from 'react';
import { connect } from 'react-redux';
import { nextStep, dropFiles } from '../redux/actions/actions';
import Dropzone from "react-dropzone";

const SingleUploadImage = (props) => {

    const handleDrop = (files) => {
        if(props.prev_step === "update single image"){
            props.dropFiles(files, "IMAGE SINGLE UPLOAD");
            props.nextStep("upload files", "update single image");
            return null;
        }
        props.dropFiles(files, "IMAGE");
        props.nextStep("upload files", "_");
    }
    const Cancel = () => {
        props.nextStep("image library", "_")
    }

    return (
        <div className="uploadPlans">
            <Dropzone onDrop={handleDrop}>
                {({ getRootProps, getInputProps }) => (
                    <div {...getRootProps({ className: "dropzone" })}>
                        <h3>Drag & Drop your singleimage for this level here.</h3><br/>
                        <h3>Or</h3><br/>
                        <input {...getInputProps()} />
                        <button>Choose file</button><br/>
                        <h3>Single file upload</h3>
                    </div>

                )}
            </Dropzone>
            <div className="cancelBtnBlue"><button onClick={Cancel}>Cancel</button></div>
        </div>
    );
}

const putState = (state) => {
    return {
        step: state.step,
        prev_step: state.prev_step
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        dropFiles: (files, title) => dispatch(dropFiles(files, title))
    }
}

export default connect(putState, putActions) (SingleUploadImage);