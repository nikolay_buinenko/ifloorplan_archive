import React, {useState, useEffect} from 'react';
import {sizeBlock} from '../helperFunctions';
import { connect } from 'react-redux';

const VideoMarker = (props) => {
    const [wrapStyle, setWrapStyle] = useState({
        position: 'absolute',
        width: props.value.cam_width+'%',
        top: props.value.pos_y+"%",
        left: props.value.pos_x+"%"
    });


    return (
        <div className="markerDrag" style={wrapStyle}>
            <img src={props.value.cam_img} style={{width:"100%"}} alt=""/>
        </div>
    )
}

const putState = (state) => {
    return {}
}

const putActions = (dispatch) => {
    return {}
}

export default connect(putState, putActions)(VideoMarker);