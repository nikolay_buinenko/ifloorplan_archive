import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {
    changeCamPosition,
    changeImageKey,
    changeLevel,
    ifloorplanStateAdd,
    setOpenImageMarkers,
    nextStep,
    setColor,
    setColorBackground,
    setColorText
} from '../redux/actions/actions';
import {getSystemIcons, getUserIcons, updateNewPlan} from "../serverRequests/requests";
import CustomButtons from './CustomButtons'


const ImageMarkersStudio = (props) => {

    const [loadedSystIcons, setLoadedSystIcons] = useState([]);
    const [loadedUserIcons, setLoadedUserIcons] = useState([]);

    const [showSystemIcons, setShowSystemIcons] = useState(true);
    const [markerScale, setMarkerScale] = useState(2);
    const [saveLastState, setSaveLastState] = useState({});

    const [swapMarkButt, setSwapMarkButt] = useState(false);
    const [markersButtons, setMarkersButtons] = useState(false);
    const [activeTab, setActiveTab] = useState({
        opacity1: 1,
        opacity2: -1,
        zIndex1: 1,
        zIndex2: -1
    })

    const Selector = () => {
        markersButtons
            ?
            setActiveTab({
                ...activeTab,
                opacity1: 1,
                opacity2: -1,
                zIndex1: 1,
                zIndex2: -1
            })
            :
            setActiveTab({
                ...activeTab,
                opacity1: -1,
                opacity2: 1,
                zIndex1: -1,
                zIndex2: 1
            })
    }

    useEffect(() => {
        getSystemIcons()
            .then((data) => {
                const icons = [];
                for (let i = 0; i < data.length; i++) {
                    if (data[i].type == "IMAGE") {
                        icons.push(data[i]);
                    }
                }
                setLoadedSystIcons(icons);
            });
        getUserIcons()
            .then((data) => {
                const icons = [];
                for (let i = 0; i < data.length; i++) {
                    if (data[i].type == "IMAGE") {
                        icons.push(data[i]);
                    }
                }
                setLoadedUserIcons(icons);
            });
        const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        setSaveLastState(newState);

        function readCookie(name) {
            //function split cookie for get csrf token
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }
    }, []);

    const chooseSysIcons = () => {
        setShowSystemIcons(true)
    }
    const chooseMyIcons = () => {
        setShowSystemIcons(false)
    }
    const setIcons = (index, type) => {
        const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        for (let j = 0; j < newState.level.length; j++) {
            for (let i = 0; i < newState.level[j].images.length; i++) {
                if (type == "system") {
                    newState.level[j].images[i].cam_img = loadedSystIcons[index].marker;
                    newState.level[j].images[i].cam_img_hover = loadedSystIcons[index].marker_hover;
                } else {
                    newState.level[j].images[i].cam_img = loadedUserIcons[index].marker;
                    newState.level[j].images[i].cam_img_hover = loadedUserIcons[index].marker_hover;
                }
            }
        }
        props.ifloorplanStateAdd(newState);
    }

    useEffect(() => {
        const newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        for (let i = 0; i < newState.level.length; i++) {
            for (let j = 0; j < newState.level[i].images.length; j++) {
                newState.level[i].images[j].cam_width = markerScale;
            }
        }
        props.ifloorplanStateAdd(newState)
    }, [markerScale]);

    const CloseMarkStudio = () => {
        let markStidio = document.querySelector('.selectMark_wrapper')
        markStidio.style.left = '-30%'
        const timer = setTimeout(() => {
            props.setOpenImageMarkers(false)
        }, 500);
        return () => clearTimeout(timer);
    }

    const CancelMarker = () => {
        props.ifloorplanStateAdd(saveLastState)
        setMarkerScale(2);
        CloseMarkStudio();
    }

    return (
        <div className="selectMark_wrapper">
            <h4>Edit Image Markers</h4>
            <button className='chooseMarkButt'
                    onClick={() => {
                        setMarkersButtons(!markersButtons)
                        Selector()
                    }}
                    disabled={!markersButtons}
                    style={!markersButtons ? {backgroundColor: "#71a9eb", cursor: "unset"} : {}}>
                Replace all markers
            </button>
            <button className='chooseMarkButt'
                    onClick={() => {
                        setMarkersButtons(!markersButtons)
                        Selector()
                    }}
                    disabled={markersButtons}
                    style={markersButtons ? {backgroundColor: "#71a9eb", cursor: "unset"} : {}}>
                Swap Marker for a button
            </button>
            <h5>{markersButtons ? "Create a button and swap with a marker" : "Select a Marker to use for all images"}</h5>
            <div className="selectMark" style={{opasity: activeTab.opasity1, zIndex: activeTab.zIndex1}}>
                {/*<div className="selectMark__action">*/}
                {/*    <h5>Select a Marker for all images</h5>*/}
                {/*</div>*/}
                <div className="selectMark__select">
                    <a onClick={chooseSysIcons}>Markers</a>
                    <a onClick={chooseMyIcons}>My Markers</a>
                </div>
                <div className="selectMark__markers">
                    {showSystemIcons
                        ?
                        loadedSystIcons.map((el, index) => (
                            <div className="selectMark__markers_item markers" key={index} onClick={() => {
                                setIcons(index, "system")
                            }}>
                                <img src={el.marker} alt=""/>
                            </div>
                        ))
                        :
                        loadedUserIcons.map((el, index) => (
                            <div className="selectMark__markers_item markers" key={index} onClick={() => {
                                setIcons(index, "user")
                            }}>
                                <img src={el.marker} alt=""/>
                            </div>
                        ))
                    }
                </div>
                <div className="scale-wrapper">
                    <h3 id='test'>Scale</h3>
                    <input type="range"
                           id='markersSize'
                           step="0.05"
                           className="scale-image"
                           min="1"
                           max="3"
                           value={markerScale}
                           onChange={(event => setMarkerScale(event.target.value))}/>
                </div>
            </div>
            <CustomButtons opacity={activeTab.opacity2} zindex={activeTab.zIndex2} swapMarkButt={swapMarkButt}/>
            <div className="selectMark__btns swapMarkButt">
                {markersButtons ?
                    <button className='chooseMarkButt'
                            onClick={() => {
                                setSwapMarkButt(!swapMarkButt)
                            }}>
                        {swapMarkButt
                            ?
                            'Swap Marker for a button'
                            :
                            'Swap with active marker'
                        }
                    </button>
                    :
                    ""
                }
            </div>
            <div className="selectMark__btns">
                <button className="cancel"
                        onClick={() => {
                            CancelMarker();
                            CloseMarkStudio();
                        }}>
                    Cancel
                </button>
                <button className="done" onClick={() => {
                    let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
                    updateNewPlan(newState)
                        .then(data => {
                            props.setAnimation(animationSaved => animationSaved + 1);
                            props.ifloorplanStateAdd(data[0]);
                        });
                    CloseMarkStudio();
                }}>
                    Done
                </button>
            </div>
        </div>
    )
}
const putState = (state) => {
    return {
        step: state.step,
        levelNowId: state.levelNowId,
        cam_key: state.cam_key,
        image_key: state.image_key,
        positionCams: state.positionCams,
        ifloorplanNowState: state.ifloorplanNowState,
        text_color: state.text_color,
        background_color: state.background_color,
        set_color: state.set_color,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        changeCamKey: (key) => dispatch(changeCamKey(key)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        changeCamPosition: (obj) => dispatch(changeCamPosition(obj)),
        setColor: (bool) => dispatch(setColor(bool)),
        changeLevel: (level) => dispatch(changeLevel(level)),
        changeImageKey: (key) => dispatch(changeImageKey(key)),
        setColorBackground: (color) => dispatch(setColorBackground(color)),
        setColorText: (color) => dispatch(setColorText(color)),
        setOpenImageMarkers: (bool) => dispatch(setOpenImageMarkers(bool))
    }
}


export default connect(putState, putActions)(ImageMarkersStudio);
