import React, {useState} from 'react';
import { connect } from 'react-redux';
import { ChromePicker, SketchPicker } from 'react-color';
import {updateNewPlan} from '../serverRequests/requests';
import {setColor,
    nextStep,
    setColorBackground,
    setColorText,
    ifloorplanStateAdd } from '../redux/actions/actions';
const ColorSet = (props) => {
    const ifloorplanObj = props.ifloorplanNowState; //last ifloor plan obj
    const [textColors, setTextColors] = useState(ifloorplanObj.textColor);
    const [backgroundColor, setBackgroundColor] = useState(ifloorplanObj.bgColor);
    const [colorStyle, setStyle] = useState(ifloorplanObj.bgColor);

    // const changeColor = (color) => {
    //     setBackgroundColor(color.hex);
    //     console.log('onChange color:', color);
    //
    // };
    //
    // const changeColorComplete = (color) => {
    //     props.setColorBackground(color.hex);
    //     setOpacityBg(data.hsl.a);
    // };

    const changeTextComplete = (color) => {
        const newColor = `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a})`;
        props.setColorText(newColor);
    };

    const changeBackgroundComplete = (color) => {
        const newColor = `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a})`;
        props.setColorBackground(newColor);
    };

    const saveColors = () => {
        let newState = JSON.parse(JSON.stringify(ifloorplanObj));
        // newState.textColor = textColors;
        // newState.bgColor = backgroundColor;
        newState.textColor = props.text_color;
        newState.bgColor = props.background_color;
        updateNewPlan(newState)
            .then(data=>{
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                props.setColor(false); //close set menu color
            });
    };

    return (
        <div className="colorPanel" style={{backgroundColor: colorStyle}}>
            <div className="colorPickerTitle">
                <h3>Colours</h3>
                <p>Choose colours for text and the background</p>
            </div>
            <div className="wrapColorPicker">
                <h3><b>Text</b></h3>
                <ChromePicker
                    color={textColors}
                    onChange = {color => setTextColors(color.rgb)}
                    onChangeComplete = {color => changeTextComplete(color.rgb)}
                />
            </div>
            <div className="wrapColorPicker">
                <h3><b>Background</b></h3>
                <ChromePicker
                    color={backgroundColor}
                    onChange={color => setBackgroundColor(color.rgb)}
                    onChangeComplete = {color => changeBackgroundComplete(color.rgb)}
                />
            </div>
            <div className="btnWrap">
                <button onClick={()=> saveColors()}>Done</button>
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        ifloorplanNowState: state.ifloorplanNowState,
        set_color: state.set_color,
        text_color: state.text_color,
        background_color: state.background_color,
    }
}
const putActions = (dispatch) => {
    return {
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        setColor: (bool) => dispatch(setColor(bool)),
        setColorBackground: (color) => dispatch(setColorBackground(color)),
        setColorText: (color) => dispatch(setColorText(color))
    }
}

export default connect(putState, putActions)(ColorSet);