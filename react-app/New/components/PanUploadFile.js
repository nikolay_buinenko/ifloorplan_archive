import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import { CSRFTOKEN } from '../redux/constant/constants';
import { uploadFilesToServer, updateNewPlan } from '../serverRequests/requests';
import { nextStep, set360ToolsAction, ifloorplanStateAdd, panelumLib } from '../redux/actions/actions';
import Dropzone from "react-dropzone";

const PanUploadFile = (props) => {
    const [status, setStatus] = useState(0);
    const [uploadProcess, setUploadProcess] = useState(false);
    const [fileName, setFileName] = useState("");

    const handleDrop = (files) => {
        const config = {
            onUploadProgress: (progressEvent) => {
                setUploadProcess(true);
                const {loaded, total} = progressEvent;
                let percents = Math.floor((loaded * 100) / total );
                setStatus(percents);
            },
            headers: {
                "X-CSRFToken": CSRFTOKEN,
                'content-type': 'multipart/form-data'
            }
        }
        setFileName(files[0].name);
        let form_data = new FormData();
        form_data.append('ifloorplan_id', props.ifloorplanNowState.id);
        form_data.append('image', files[0], files[0].name);

        uploadFilesToServer("https://ifloorplan360.com/api/panelum/", form_data, config)
            .then(res => {
                props.panelumLib(res.data);
                let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
                const panoramaLenght = props.ifloorplanNowState.level[props.levelNowId].panelum.length;
                newState.level[props.levelNowId].panelum[panoramaLenght - 1].img = res.data[res.data.length - 1].image;
                newState.level[props.levelNowId].panelum[panoramaLenght - 1].id_lib_img = res.data[res.data.length - 1].id_lib_img;
                updateNewPlan(newState)
                    .then((data)=>{
                        props.ifloorplanStateAdd(data[0]);
                        props.set360ToolsAction("markers");
                    })
            })
    }

    return (
        <div>
            {uploadProcess ? <div className="process_panelum">
                <div className="name_file"><p>{fileName}</p></div>
                <div className="line_process_panelum">
                    <div style={{width: status+"%"}}></div>
                </div>
            </div> : <div className="upload_panelum">
                <Dropzone onDrop={handleDrop}>
                    {({ getRootProps, getInputProps }) => (
                        <div {...getRootProps({ className: "dropzone" })}>
                            <h3>Drop 360 image here</h3>
                            <h3>Or</h3>
                            <div className="btn_group_panelum">
                                <input {...getInputProps()} />
                                <button className="choose_btn">Choose file</button>
                            </div>
                        </div>

                    )}
                </Dropzone>
                <button className="cansel" onClick={()=>{
                    props.set360ToolsAction("choose");
                }}>Cancel</button>
            </div>}
        </div>
    )
}

const putState = (state) => {
    return {
        levelNowId: state.levelNowId,
        ifloorplanNowState: state.ifloorplanNowState,
        tools_360_action: state.tools_360_action,
        panelum_lib: state.panelum_lib,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        panelumLib: (items) => dispatch(panelumLib(items)),
        set360ToolsAction: (value) => dispatch(set360ToolsAction(value))
    }
}


export default connect(putState, putActions) (PanUploadFile);