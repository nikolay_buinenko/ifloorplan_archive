import React from 'react';
import { connect } from 'react-redux';
import { nextStep, dropFiles } from '../redux/actions/actions';
import Dropzone from "react-dropzone";


const UploadSinglePlan = (props) => {

    const handleDrop = (files) => {
        props.dropFiles(files, "UPDATE SINGLE PLAN");
        props.nextStep("upload files", "_");
    }

    return (
        <div className="uploadImages">
            <Dropzone onDrop={handleDrop}>
                {({ getRootProps, getInputProps }) => (
                    <div {...getRootProps({ className: "dropzone" })}>
                        <h3>Drag & Drop your replacement floor plan</h3>
                        <h3>Or</h3>
                        <input {...getInputProps()} />
                        <button>Choose files</button>
                        <h3>Single file upload</h3>
                    </div>

                )}
            </Dropzone>
            <div className="cancel"><button onClick={()=>{props.nextStep("plan library", "_")}}>Cancel</button></div>
        </div>
    );
}

const putState = (state) => {
    return {
        step: state.step,
        prev_step: state.prev_step,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        dropFiles: (files, title) => dispatch(dropFiles(files, title))
    }
}

export default connect(putState, putActions) (UploadSinglePlan);