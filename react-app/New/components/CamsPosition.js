import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';

import {setColor,
    setColorBackground,
    nextStep,
    changeLevel,
    changeImageKey,
    setColorText,
    ifloorplanStateAdd,
    changeCamPosition } from '../redux/actions/actions';
import EditTextBlock from './EditTextBlock.js';
import Cam from './Cam';
import Client from './Client';
import Marker from './Marker';
import {DraggableCore} from 'react-draggable';

const CamsPosition = (props) => {
    const ifloorplanObj = props.ifloorplanNowState; //last ifloor plan obj
    const [hoverBlockLeft, setHoverBlockLeft] = useState(false);  //hover cams
    const [hoverBlockRight, setHoverBlockRight] = useState(false); //hover cams
    const [camPointerVisible, setCamPointerVisible] = useState(true);
    const [rotation, setRotation] = useState(0);  //cam rotation
    const [cams, setCams] = useState([]); //array position active cams
    const [videoMarker, setVideoMarker] = useState([]);
    const [doneMarkers, setDoneMarkers]  = useState([]); //array with markers is position done
    const [tabsStyle, setTabsStyle] = useState(JSON.parse(ifloorplanObj.tabs_style));
    const [mediaTabs, setMediaTabs] = useState([]);
    const [tabs, setTabs] = useState([]);
    const [imageWrapStyle, setImageWrapStyle] = useState({
        position: 'absolute',
        width: ifloorplanObj.level[props.levelNowId].img_width+'%',
        top: ifloorplanObj.level[props.levelNowId].img_pos_y+"%",
        left: ifloorplanObj.level[props.levelNowId].img_pos_x+"%"
    });
    const [planWrapStyle, setPlanWrapStyle] = useState({
        position: 'absolute',
        width: ifloorplanObj.level[props.levelNowId].plan_width+'%',
        top: ifloorplanObj.level[props.levelNowId].plan_pos_y+"%",
        left: ifloorplanObj.level[props.levelNowId].plan_pos_x+"%"
    });
    const [addressWrapStyle, setAdsressWrapStyle] = useState({
        position: 'absolute',
        color: props.text_color,
        top: ifloorplanObj.addressPosition_y+"%",
        left: ifloorplanObj.addressPosition_x+"%"
    });
    const [clientWrapStyle, setClientWrapStyle] = useState({
        position: "absolute",
        top: ifloorplanObj.client_block_y+"%",
        left: ifloorplanObj.client_block_x+"%",
        transform: 'scale('+ifloorplanObj.client_scale+')',
        width: '35vw'
    })

    const [camStyle, setCamStyle] = useState({
        position: "absolute",
        width: 2+"%",
        top: "50%",
        left: "50%",
    })

    const [correction, setCorrection] = useState({
        x: 3,
        y: 3
    })

    const tabsInit = () => {
        let tabs_ready = [];
        let media_tabs = [];
        if(ifloorplanObj.level.length == 1 && ifloorplanObj.media.length == 0){
            return 1;
        }
        ifloorplanObj.level.map((value, index) => {
            if (index == props.levelNowId) {
                tabs_ready.push(<div className="tab active_tab" key={index}>{value.tabLabel}</div>)
            } else {
                tabs_ready.push(<div className="tab" onClick={() => {
                    props.changeImageKey(0); //reset image key
                    if(ifloorplanObj.level[index].images.length == 0){
                        props.nextStep("upload images", "exist images");
                    }
                    props.changeLevel(index);
                }} key={index}>{value.tabLabel}</div>)
            }
        });
        ifloorplanObj.media.map((value, index) => {
            media_tabs.push(<div className="tab" key={index}>{value.label}</div>)
        })
        setMediaTabs(media_tabs);
        setTabs(tabs_ready);
    }

    const rotateright = (e) => {
        /*
        * when cam chenged rotate -> change json obj ifloorplan in global state
        * */
        let newRotation = rotation;
        if (e.shiftKey) {
            newRotation = rotation + 90;
        } else {
            newRotation = rotation + 5;
        }
        if(newRotation >= 360){
            newRotation =- 360;
        }
        setRotation(newRotation);
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        newState.level[props.levelNowId].images[props.image_key].cam_rotation = newRotation;
        props.ifloorplanStateAdd(newState);
    }

    const rotateleft = (e) => {
        /*
        * when cam chenged rotate -> change json obj ifloorplan in global state
        * */
        let newRotation = rotation;
        if (e.shiftKey) {
            newRotation = rotation - 90;
        }else{
            newRotation = rotation - 5;
        }
        if(newRotation >= 360){
            newRotation =- 360;
        }
        setRotation(newRotation);
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        newState.level[props.levelNowId].images[props.image_key].cam_rotation = newRotation;
        props.ifloorplanStateAdd(newState);
    }

    const imagesArray = ifloorplanObj.level[props.levelNowId].images;

    const otherMarkerInit = () => {
        const resultArr = []; //init markers 360
        for(let i = 0; i < ifloorplanObj.level[props.levelNowId].panelum.length; i++){
            if(ifloorplanObj.level[props.levelNowId].panelum[i].pos_x != 0){
                resultArr.push(<Marker index={i}
                                       key={i}
                                       value={ifloorplanObj.level[props.levelNowId].panelum[i]}/>)
            }
        }
        setDoneMarkers(resultArr);

        setVideoMarker([]);
        for(let i = 0; i < ifloorplanObj.level[props.levelNowId].video_marker.length; i++){
            setVideoMarker(state => [...state, <Marker key={i} index={i} value={ifloorplanObj.level[props.levelNowId].video_marker[i]}/>])
        }
    }

    useEffect(() => {
        //set position frontend blocks
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        props.changeCamPosition(newState);
        setCams([]);
        for(let i = 0; i < imagesArray.length; i++){
            if(imagesArray[i].cam_pos_x !== 0 && props.image_key !== i){
                //init active cams
                if(imagesArray[i].button){
                    setCams(state => [...state, <button className={imagesArray[i].button_class}
                                                        style={{
                                                            position: "absolute",
                                                            top: imagesArray[i].cam_pos_y+"%",
                                                            left: imagesArray[i].cam_pos_x+"%",
                                                            background: imagesArray[i].button_bg,
                                                            color: imagesArray[i].button_color_text,
                                                            transform: `scale(${imagesArray[i].button_scale})`,
                                                            fontSize: imagesArray[i].button_font_size+"rem",
                                                            padding: `${imagesArray[i].button_padding_vert}rem ${imagesArray[i].button_padding_gor}rem`,
                                                            borderRadius: imagesArray[i].button_radius+"rem"
                                                        }}
                    >{imagesArray[i].button_label}</button>]);
                } else {
                    setCams(state => [...state, <Cam key={i} value={imagesArray[i]}/>]);
                }
            }
        }
        tabsInit(); //init tabs position

        //correction value
        const parrentBlock = document.getElementsByClassName("workPlace")[0];

        parrentBlock.style.color = ifloorplanObj.textColor;
        parrentBlock.style.background = ifloorplanObj.bgColor;

        const { top, left, width, height } = parrentBlock.getBoundingClientRect();
        // const width = parrentBlock.offsetWidth;
        // const height = parrentBlock.offsetHeight;
        // const { top, left, width, height } = parrentBlock.getBoundingClientRect();
        if(imagesArray[props.image_key].cam_pos_x == 0){
            const childBlock = document.getElementsByClassName("camPosSet")[0];
            childBlock.classList.add('bounces');
        }

        otherMarkerInit();

        const arrowL = document.getElementsByClassName("hoverBlockLeft")[0];
        const arrowR = document.getElementsByClassName("hoverBlockRight")[0];
        const arrow = document.getElementsByClassName("handle")[0];
        arrowL.ondragstart = function() {   //disable drag in arrows
            return false;
        };
        arrowR.ondragstart = function() {
            return false;
        };
        arrow.ondragstart = function() {
            return false;
        };

        //init colors
        props.setColorBackground(newState.bgColor);
        props.setColorText(newState.textColor);

    }, []);

    useEffect(()=> {
        //start when cams changed positions
        //1 hook - when changed global state array ifloorplan
        //2 hook - when changed global state cam key
        setCams([]);
        for(let i = 0; i < imagesArray.length; i++){
            if(imagesArray[i].cam_pos_x !== 0 && props.image_key !== i){
                if(imagesArray[i].button){
                    setCams(state => [...state, <button className={imagesArray[i].button_class}
                                                        style={{
                                                            position: "absolute",
                                                            top: imagesArray[i].cam_pos_y+"%",
                                                            left: imagesArray[i].cam_pos_x+"%",
                                                            background: imagesArray[i].button_bg,
                                                            color: imagesArray[i].button_color_text,
                                                            transform: `scale(${imagesArray[i].button_scale})`,
                                                            fontSize: imagesArray[i].button_font_size+"rem",
                                                            padding: `${imagesArray[i].button_padding_vert}rem ${imagesArray[i].button_padding_gor}rem`,
                                                            borderRadius: imagesArray[i].button_radius+"rem"
                                                        }}
                    >{imagesArray[i].button_label}</button>]);
                } else {
                    setCams(state => [...state, <Cam key={i} value={imagesArray[i]}/>]);
                }
            }
        }
        if(imagesArray[props.image_key].cam_pos_x == 0){
            setCamStyle({
                position: "absolute",
                top: "50%",
                left: "50%",
                width: props.ifloorplanNowState.level[props.levelNowId].images[props.image_key].cam_width+"%"
            })
            const childBlock = document.getElementsByClassName("camPosSet")[0];
            childBlock.classList.add('bounces');
            setRotation(0);
        } else {
            const parrentBlock = document.getElementsByClassName("workPlace")[0];
            const { top, left, width, height } = parrentBlock.getBoundingClientRect();
            let x = imagesArray[props.image_key].cam_pos_x / 100 * width - 2;
            let y = imagesArray[props.image_key].cam_pos_y / 100 * height - 2;
            setCamStyle({
                position: "absolute",
                transform: `translate(${x}px, ${y}px)`,
                width: props.ifloorplanNowState.level[props.levelNowId].images[props.image_key].cam_width+"%"
            })
            setRotation(ifloorplanObj.level[props.levelNowId].images[props.image_key].cam_rotation);
        }

    }, [props.image_key, props.ifloorplanNowState]);

    useEffect(()=>{
        //start when cams changed positions
        //1 hook - when changed global state array ifloorplan
        //2 hook - when changed global state cam key
        setCams([]);
        for(let i = 0; i < imagesArray.length; i++){
            if(imagesArray[i].cam_pos_x !== 0 && props.image_key !== i){
                if(imagesArray[i].button){
                    setCams(state => [...state, <button className={imagesArray[i].button_class}
                                                        style={{
                                                            position: "absolute",
                                                            top: imagesArray[i].cam_pos_y+"%",
                                                            left: imagesArray[i].cam_pos_x+"%",
                                                            background: imagesArray[i].button_bg,
                                                            color: imagesArray[i].button_color_text,
                                                            transform: `scale(${imagesArray[i].button_scale})`,
                                                            fontSize: imagesArray[i].button_font_size+"rem",
                                                            padding: `${imagesArray[i].button_padding_vert}rem ${imagesArray[i].button_padding_gor}rem`,
                                                            borderRadius: imagesArray[i].button_radius+"rem"
                                                        }}
                    >{imagesArray[i].button_label}</button>]);
                } else {
                    setCams(state => [...state, <Cam key={i} value={imagesArray[i]}/>]);
                }
            }
        }
        if(imagesArray[props.image_key].cam_pos_x == 0){
            setCamStyle({
                position: "absolute",
                top: "50%",
                left: "50%",
                width: props.ifloorplanNowState.level[props.levelNowId].images[props.image_key].cam_width+"%"
            })
        } else {
            const parrentBlock = document.getElementsByClassName("workPlace")[0];
            const { top, left, width, height } = parrentBlock.getBoundingClientRect();
            let x = imagesArray[props.image_key].cam_pos_x / 100 * width - 2;
            let y = imagesArray[props.image_key].cam_pos_y / 100 * height - 2;
            setCamStyle({
                position: "absolute",
                transform: `translate(${x}px, ${y}px)`,
                width: props.ifloorplanNowState.level[props.levelNowId].images[props.image_key].cam_width+"%"
            })
        }
        setImageWrapStyle({
            ...imageWrapStyle,
            width: ifloorplanObj.level[props.levelNowId].img_width+'%',
            top: ifloorplanObj.level[props.levelNowId].img_pos_y+"%",
            left: ifloorplanObj.level[props.levelNowId].img_pos_x+"%"
        });
        setPlanWrapStyle({
            ...planWrapStyle,
            width: ifloorplanObj.level[props.levelNowId].plan_width+'%',
            top: ifloorplanObj.level[props.levelNowId].plan_pos_y+"%",
            left: ifloorplanObj.level[props.levelNowId].plan_pos_x+"%"
        });
        setRotation(ifloorplanObj.level[props.levelNowId].images[props.image_key].cam_rotation);
        tabsInit(); //init tabs position
        otherMarkerInit();
    }, [props.levelNowId])


    const [startPos, setStartPos] = useState({
        x: 0,
        y: 0
    })
    const startDrag = (event, {node}) => {
        const clientRect = node.getBoundingClientRect(),
            parentRect = node.offsetParent.getBoundingClientRect();

        const relativePositionX = event.clientX - clientRect.left,
            relativePositionY = event.clientY - clientRect.top;

        const height = clientRect.bottom - clientRect.top,
            width = clientRect.right - clientRect.left;

        const transformOrigin = {
            x: Math.round(relativePositionX / width * 100),
            y: Math.round(relativePositionY / height * 100)
        };
        setStartPos({
            x: clientRect.left - parentRect.left + node.offsetParent.scrollLeft,
            y: clientRect.top - parentRect.top + node.offsetParent.scrollTop
        })
    }


    const drag = (event, { deltaX, deltaY }) => {
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        const childBlock = document.getElementsByClassName("camPosSet")[0];

        const parrentBlock = document.getElementsByClassName("workPlace")[0];
        let childX = childBlock.getBoundingClientRect().left;
        let childY = childBlock.getBoundingClientRect().top;
        const { top, left, width, height } = parrentBlock.getBoundingClientRect();


        let x = (childX - left) / width * 100;
        let y = (childY - top) / height * 100;

        //change position properties
        newState.level[props.levelNowId].images[props.image_key].cam_pos_x = x;
        newState.level[props.levelNowId].images[props.image_key].cam_pos_y = y;
        newState.level[props.levelNowId].images[props.image_key].cam_rotation = rotation;
        props.ifloorplanStateAdd(newState); //update main ifloorplan in global state

        setStartPos({
            x:startPos.x + deltaX,
            y:startPos.y + deltaY
        })

        setCamStyle({
            position: "absolute",
            transform: `translate(${startPos.x}px, ${startPos.y}px)`,
            width: props.ifloorplanNowState.level[props.levelNowId].images[props.image_key].cam_width+"%"
        })
    }

    const stop = () => {
        const childBlock = document.getElementsByClassName("camPosSet")[0];
        childBlock.classList.remove('bounces');
    }

    const ifl_image = ifloorplanObj.level[props.levelNowId].images[props.image_key];

    return (


        <div className="workPlace" style={{background:props.background_color}}>
            <div className="dnd-wrapper">

                {ifloorplanObj.client_data == "" ? '' : <div className="client_wrap_main" style={clientWrapStyle}>
                    <Client clientData={JSON.parse(ifloorplanObj.client_data)} clientPosition={JSON.parse(ifloorplanObj.client_position)}/>
                </div>}

                <div className="tabs" style={tabsStyle}>
                    {tabs}
                    {mediaTabs}
                </div>
                <div className="imageWrap" style={imageWrapStyle}>
                    <img className='interior-image'
                                src={ifloorplanObj.level[props.levelNowId].images[props.image_key].img || null}
                                style={{width: '100%'}}/>
                </div>
                <div className="planWrap" style={planWrapStyle}>
                    <img className='floor-plan'
                                 src={ifloorplanObj.level[props.levelNowId].plan_level[0].img || null}
                                 style={{width: '100%'}}/>
                </div>
                {ifloorplanObj.brand_logo == null ? "" :
                    <div className="brandWrap" style={{position: 'absolute', width: ifloorplanObj.brand_width+'%', opacity: ifloorplanObj.brand_transparency, top:ifloorplanObj.brand_pos_y+"%", left:ifloorplanObj.brand_pos_x+"%"}}>
                        <img style={{width: '100%'}} src={ifloorplanObj.brand_logo || null} alt=""/>
                    </div>
                }
                <div className="addressWrap" style={addressWrapStyle}>
                    <EditTextBlock/>
                </div>
                {cams}
                {doneMarkers}
                {videoMarker}

                <DraggableCore handle=".handle" bounds="parent" onStop={stop} onDrag={drag} onStart={startDrag}>
                    {ifl_image.button ?
                        <div className="camPosSet" style={camStyle}><button className="handle" style={{
                                                    position: "absolute",
                                                    top: ifl_image.cam_pos_y+"%",
                                                    left: ifl_image.cam_pos_x+"%",
                                                    background: ifl_image.button_bg,
                                                    color: ifl_image.button_color_text,
                                                    transform: `scale(${ifl_image.button_scale})`,
                                                    fontSize: ifl_image.button_font_size+"rem",
                                                    padding: `${ifl_image.button_padding_vert}rem ${ifl_image.button_padding_gor}rem`,
                                                    borderRadius: ifl_image.button_radius+"rem"
                                                }}
                        >{ifl_image.button_label}</button></div>

                        :

                        <div className="camPosSet" style={camStyle}>
                            <img className="handle" style={{
                                width: "100%",
                                transform: `rotate(${rotation}deg)`,
                                zIndex: 12,
                                position: "relative"
                            }}
                                 src={ifloorplanObj.level[props.levelNowId].images[props.image_key].cam_img}
                                 alt=""/>

                            <div style={{
                                display: "flex",
                                position: "absolute",
                                height: "300%",
                                width: "150%",
                                top: "-100%",
                                left: "-100%"
                            }}
                                 onMouseDown={(e) => rotateleft(e)}
                                 className="hoverBlockLeft"
                                 onMouseOut={() => {
                                     setHoverBlockLeft(false)
                                 }}
                                 onMouseEnter={() => {
                                     setHoverBlockLeft(true)
                                 }}>
                                {hoverBlockLeft ? (
                                    <img src="/media/ifloorplans_source/icon-rotater-left_over.svg" alt=""
                                         style={{width: "100%", height: "100%"}}/>
                                ) : (
                                    <img src="/media/ifloorplans_source/icon-rotater-left.svg" alt=""
                                         style={{width: "100%", height: "100%"}}/>
                                )
                                }
                            </div>
                            <div style={{
                                display: "flex",
                                position: "absolute",
                                height: "300%",
                                width: "150%",
                                top: "-100%",
                                left: "50%"
                            }}
                                 onMouseDown={(e) => rotateright(e)}
                                 className="hoverBlockRight"
                                 onMouseOut={() => {
                                     setHoverBlockRight(false)
                                 }}
                                 onMouseEnter={() => {
                                     setHoverBlockRight(true)
                                 }}>
                                {hoverBlockRight ? (
                                    <img src="/media/ifloorplans_source/icon-rotater-right_over.svg" alt=""
                                         style={{width: "100%", height: "100%"}}/>
                                ) : (
                                    <img src="/media/ifloorplans_source/icon-rotater-right.svg" alt=""
                                         style={{width: "100%", height: "100%"}}/>
                                )
                                }
                            </div>

                        </div>
                    }
              </DraggableCore>


            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        levelNowId: state.levelNowId,
        cam_key: state.cam_key,
        image_key: state.image_key,
        positionCams: state.positionCams,
        ifloorplanNowState: state.ifloorplanNowState,
        text_color: state.text_color,
        background_color: state.background_color,
        set_color: state.set_color,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        changeCamKey: (key) => dispatch(changeCamKey(key)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        changeCamPosition: (obj) => dispatch(changeCamPosition(obj)),
        setColor: (bool) => dispatch(setColor(bool)),
        changeLevel: (level) => dispatch(changeLevel(level)),
        changeImageKey: (key) => dispatch(changeImageKey(key)),
        setColorBackground: (color) => dispatch(setColorBackground(color)),
        setColorText: (color) => dispatch(setColorText(color))
    }
}


export default connect(putState, putActions) (CamsPosition);