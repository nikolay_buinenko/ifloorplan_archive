import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import { setColor,
    setColorBackground,
    nextStep,
    changeLevel,
    changeImageKey,
    setColorText,
    ifloorplanStateAdd,
    changeCamPosition } from '../redux/actions/actions';
import EditTextBlock from './EditTextBlock.js';
import { updateNewPlan } from '../serverRequests/requests';
import Cam from './Cam';
import Marker from './Marker';
import Client from './Client';
import VideoSideBar from "./VideoSideBar";
import Draggable from 'react-draggable';

const PositionVideos = (props) => {
    const ifloorplanObj = props.ifloorplanNowState; //last ifloor plan obj
    const [cams, setCams] = useState([]); //array position active cams
    const [videoMarker, setVideoMarker] = useState([]);
    const [doneMarkers, setDoneMarkers]  = useState([]); //array with markers is position done
    const [tabsStyle, setTabsStyle] = useState(JSON.parse(ifloorplanObj.tabs_style));
    const [tabs, setTabs] = useState([]);
    const [activeDrags, setActiveDrags] = useState(0);

    const [scale, setScale] = useState(2);
    const [link, setLink] = useState("");
    const [markerImg, setMarkerImg] = useState({});
    const [position, setPosition] = useState({x:0,y:0});

    const [imageWrapStyle, setImageWrapStyle] = useState({
        position: 'absolute',
        width: ifloorplanObj.level[props.levelNowId].img_width+'%',
        top: ifloorplanObj.level[props.levelNowId].img_pos_y+"%",
        left: ifloorplanObj.level[props.levelNowId].img_pos_x+"%"
    });
    const [planWrapStyle, setPlanWrapStyle] = useState({
        position: 'absolute',
        width: ifloorplanObj.level[props.levelNowId].plan_width+'%',
        top: ifloorplanObj.level[props.levelNowId].plan_pos_y+"%",
        left: ifloorplanObj.level[props.levelNowId].plan_pos_x+"%"
    });
    const [addressWrapStyle, setAdsressWrapStyle] = useState({
        position: 'absolute',
        color: props.text_color,
        top: ifloorplanObj.addressPosition_y+"%",
        left: ifloorplanObj.addressPosition_x+"%"
    });
    const [clientWrapStyle, setClientWrapStyle] = useState({
        position: "absolute",
        top: ifloorplanObj.client_block_y+"%",
        left: ifloorplanObj.client_block_x+"%"
    })


    const tabsInit = () => {
        let tabs_ready = [];
        ifloorplanObj.level.map((value, index) => {
            if (index == props.levelNowId) {
                tabs_ready.push(<div className="tab active_tab" key={index}>{value.tabLabel}</div>)
            } else {
                tabs_ready.push(<div className="tab" onClick={() => {
                    props.changeLevel(index);
                }} key={index}>{value.tabLabel}</div>)
            }
        });
        setTabs(tabs_ready);
    }

    const imagesArray = ifloorplanObj.level[props.levelNowId].images;

    useEffect(() => {
        //set position frontend blocks
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        props.changeCamPosition(newState);
        setCams([]);
        for(let i = 0; i < imagesArray.length; i++){
            if(imagesArray[i].cam_pos_x !== 0){
                //init active cams
                setCams(state => [...state, <Cam key={i} value={imagesArray[i]}/>]);
            }
        }
        setVideoMarker([]);
        for(let i = 0; i < ifloorplanObj.level[props.levelNowId].video_marker.length; i++){
            setVideoMarker(state => [...state, <Marker key={i} index={i} value={ifloorplanObj.level[props.levelNowId].video_marker[i]}/>])
        }
        const resultArr = []; //init markers 360
        for(let i = 0; i < ifloorplanObj.level[props.levelNowId].panelum.length; i++){
            if(ifloorplanObj.level[props.levelNowId].panelum[i].pos_x != 0){
                resultArr.push(<Marker index={i}
                                       key={i}
                                       value={ifloorplanObj.level[props.levelNowId].panelum[i]}/>)
            }
        }
        setDoneMarkers(resultArr);

        tabsInit(); //init tabs position

        const parrentBlock = document.getElementsByClassName("workPlace")[0];

        parrentBlock.style.color = ifloorplanObj.textColor;
        parrentBlock.style.background = ifloorplanObj.bgColor;

        //init colors
        props.setColorBackground(newState.bgColor);
        props.setColorText(newState.textColor);

    }, []);

    useEffect(()=> {
        //start when cams changed positions
        //1 hook - when changed global state array ifloorplan
        //2 hook - when changed global state cam key
        setCams([]);
        for(let i = 0; i < imagesArray.length; i++){
            if(imagesArray[i].cam_pos_x !== 0){
                setCams(state => [...state, <Cam key={i} value={imagesArray[i]}/>]);
            }
        }
        setVideoMarker([]);
        for(let i = 0; i < ifloorplanObj.level[props.levelNowId].video_marker.length; i++){
            setVideoMarker(state => [...state, <Marker key={i} index={i} value={ifloorplanObj.level[props.levelNowId].video_marker[i]}/>])
        }
    }, [props.image_key, props.ifloorplanNowState]);

    useEffect(()=>{
        //start when cams changed positions
        //1 hook - when changed global state array ifloorplan
        //2 hook - when changed global state cam key
        setCams([]);
        for(let i = 0; i < imagesArray.length; i++){
            if(imagesArray[i].cam_pos_x !== 0){
                setCams(state => [...state, <Cam key={i} value={imagesArray[i]}/>]);
            }
        }
        tabsInit(); //init tabs position
        props.changeImageKey(0); //reset image key
    }, [props.levelNowId])

    const Done = () => {
        const newVideoMarker = {
            video : link,
            cam_img : markerImg.marker,
            cam_img_hover : markerImg.marker_hover,
            cam_width : scale,
            pos_x : position.x,
            pos_y : position.y
        }
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        newState.level[props.levelNowId].video_marker.push(newVideoMarker);
        updateNewPlan(newState)
            .then(data=>{
                props.setAnimation(animationSaved=>animationSaved+1);
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                props.nextStep("cams position", "_"); //next step
            });
    }

    const AddAnother = () => {
        const newVideoMarker = {
            video : link,
            cam_img : markerImg.marker,
            cam_img_hover : markerImg.marker_hover,
            cam_width : scale,
            pos_x : position.x,
            pos_y : position.y
        }
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        newState.level[props.levelNowId].video_marker.push(newVideoMarker);
        updateNewPlan(newState)
            .then(data=>{
                props.setAnimation(animationSaved=>animationSaved+1);
                props.ifloorplanStateAdd(data[0]); //update state ifloorplan
                setLink("");
                setMarkerImg({});
                setScale(15);
            });
    }

    const handleMarker = (event) => {
        //set position marker
        event.preventDefault();
        let newState = JSON.parse(JSON.stringify(props.ifloorplanNowState));
        const childBlock = document.getElementsByClassName("markerDrag")[0];

        const parrentBlock = document.getElementsByClassName("workPlace")[0];
        let childX = childBlock.getBoundingClientRect().left;
        let childY = childBlock.getBoundingClientRect().top;
        const { top, left, width, height } = parrentBlock.getBoundingClientRect();

        let x = (childX - left) / width * 100;
        let y = (childY - top) / height * 100;

        //change position properties
        setPosition({x:x, y:y});
    }


    const onStart = () => {setActiveDrags(activeDrags + 1)};
    const onStop = () => {setActiveDrags(activeDrags - 1)};
    const dragHandlers = {onStart: onStart, onStop: onStop};
    return (
        <div className="workPlace">
            <div className="dnd-wrapper" style={{background:props.background_color}}>
                <VideoSideBar
                    scale={scale}
                    setScale={setScale}
                    link={link}
                    setLink={setLink}
                    markerImg={markerImg}
                    setMarkerImg={setMarkerImg}
                    done={Done}
                    addAnother={AddAnother}
                />
                {markerImg.marker === '' ? '' :
                    <Draggable onDrag={handleMarker} {...dragHandlers}>
                        <div className="markerDrag" style={{width: scale+"%", position:'absolute', top:"50%", left:"50%"}}>
                            <img src={markerImg.marker} style={{width:"100%"}} alt=""/>
                        </div>
                    </Draggable>
                }
                {ifloorplanObj.client_data == "" ? '' : <div className="client_wrap_main" style={clientWrapStyle}>
                    <Client clientData={JSON.parse(ifloorplanObj.client_data)} clientPosition={JSON.parse(ifloorplanObj.client_position)}/>
                </div>}

                <div className="tabs" style={tabsStyle}>
                    {tabs}
                    <div className="tab">Video</div>
                </div>
                <div className="imageWrap" style={imageWrapStyle}>
                    <img className='interior-image'
                                src={ifloorplanObj.level[props.levelNowId].images[props.image_key].img || null}
                                style={{width: '100%'}}/>
                </div>
                <div className="planWrap" style={planWrapStyle}>
                    <img className='floor-plan'
                                 src={ifloorplanObj.level[props.levelNowId].plan_level[0].img || null}
                                 style={{width: '100%'}}/>
                </div>
                {ifloorplanObj.brand_logo == null ? "" :
                    <div className="brandWrap" style={{position: 'absolute', width: ifloorplanObj.brand_width+'%', opacity: ifloorplanObj.brand_transparency, top:ifloorplanObj.brand_pos_y+"%", left:ifloorplanObj.brand_pos_x+"%"}}>
                        <img style={{width: '100%'}} src={ifloorplanObj.brand_logo || null} alt=""/>
                    </div>
                }
                <div className="addressWrap" style={addressWrapStyle}>
                    <EditTextBlock/>
                </div>
                {cams}
                {videoMarker}
                {doneMarkers}
            </div>
        </div>
    )
}

const putState = (state) => {
    return {
        step: state.step,
        levelNowId: state.levelNowId,
        cam_key: state.cam_key,
        image_key: state.image_key,
        positionCams: state.positionCams,
        ifloorplanNowState: state.ifloorplanNowState,
        text_color: state.text_color,
        background_color: state.background_color,
        set_color: state.set_color,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        changeCamKey: (key) => dispatch(changeCamKey(key)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        changeCamPosition: (obj) => dispatch(changeCamPosition(obj)),
        setColor: (bool) => dispatch(setColor(bool)),
        changeLevel: (level) => dispatch(changeLevel(level)),
        changeImageKey: (key) => dispatch(changeImageKey(key)),
        setColorBackground: (color) => dispatch(setColorBackground(color)),
        setColorText: (color) => dispatch(setColorText(color))
    }
}

export default connect(putState, putActions) (PositionVideos);