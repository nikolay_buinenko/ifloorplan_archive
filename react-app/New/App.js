import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { connect } from 'react-redux';
import NewOrTemp from './containers/NewOrTemp';
import rootReducer from './redux/reducer/rootReducer';

const store = createStore(rootReducer, applyMiddleware(thunk));


export class App extends Component {
    render() {
        return (
            <div className="NewWrapper">
                <Provider store={store}>
                    <NewOrTemp />
                </Provider>
            </div>
        );
    }
}
ReactDOM.render(<App />, document.getElementById('app'));