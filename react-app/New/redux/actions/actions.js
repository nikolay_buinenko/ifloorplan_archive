import * as types from '../constant/constants';

export function setImageLibSort(array){
    return {
        type: types.IMAGE_LIBRARY_SORTABLE_LIST,
        array
    }
}

export function setVideosSidebar(bool){
    return {
        type: types.VIDEOS,
        bool
    }
}

export function setOpenImageMarkers(bool){
    return {
        type: types.IMAGE_MARKERS_LAYOUT,
        bool
    }
}

export function setUpdatePlanImage(id) {
    return {
        type: types.UPDATE_SINGLE_PLAN_IMAGE,
        id
    }
}

export function setType(key) {
    return {
        type: types.TYPE,
        key
    }
}

export function updateSingleImageAction(id) {
    return {
        type: types.UPDATE_SINGLE_IMAGE,
        id
    }
}

export function clearIfloorState() {
    return {
        type: types.CLEAR_IFLOORPLAN_STATE,
    }
}

export function panelumLib(items) {
    return {
        type: types.PANELUM_LIB,
        items
    }
}

export function set360ToolsAction(value) {
    return {
        type: types.SET_360_TOOLS_ACTION,
        value
    }
}

export function setColorText(color) {
    return {
        type: types.SET_TEXT_COLOR,
        color
    }
}

export function setColorBackground(color) {
    return {
        type: types.SET_BACKGROUND_COLOR,
        color
    }
}

export function setColor(bool) {
    return {
        type: types.SET_COLOR,
        bool
    }
}


export function changeLevel(level) {
    return {
        type: types.LEVEL,
        level
    }
}

export function initScale(bool) {
    return {
        type: types.SCALE,
        bool
    }
}


export function changeImageKey(key) {
    return {
        type: types.IMAGE_NOW_KEY,
        key
    }
}

export function changeCamPosition(obj) {
    return {
        type: types.CHANGE_CAMS_POSITION,
        obj
    }
}

export function changeCamKey(key) {
    return {
        type: types.CAM_NOW_KEY,
        key
    }
}

export function LibPlanAdd(array) {
    return {
        type: types.LIBRARY_PLAN_ADD,
        array
    }
}

export function LibImageAdd(array) {
    return {
        type: types.LIBRARY_IMAGE_ADD,
        array
    }
}


export function nextStep(step, prevStep) {
    return {
        type: types.NEXT_STEP,
        step,
        prevStep
    }
}

export function dropFiles(files, title) {
    return {
        type: types.DROP_FILES,
        files,
        title
    }
}

export function ifloorplanStateAdd(plan) {
    return {
        type: types.IFLOORPLAN_STATE,
        plan
    }
}