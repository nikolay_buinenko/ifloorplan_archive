import * as types from '../constant/constants';

const initialState = {
    step: '',  //now step
    prev_step: '',  // previous step or key
    scale: true,
    dropFiles: [],   //droped files
    LibraryPlans: [],  //all plans images
    LibraryImage: [],  //all images
    ifloorplanNowState: {}, //main state
    dropFilesTitle: "",  //type droped files
    levelNowId: 0,  //now level index
    cam_key: 0,   //now cam marker index
    set_color: false,  //enable/disable colors panel
    text_color: '#000000',
    background_color: '#ffffff',
    image_key: 0, //now cam marker index
    panelum_lib: [],  //all panelum library image
    update_image_id: null,  //now image id for update
    tools_360_action: "choose",  //360 action tools
    positionCams: {},
    type_action: "new",  //type creation plans
    update_plan_image: null,
    image_markers_layout: false,
    videos: false,
    sort_image_lib: [],  //library image after sorting
}


const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.IMAGE_LIBRARY_SORTABLE_LIST:
            return { ...state, sort_image_lib: action.array};
            break;

        case types.VIDEOS:
            return { ...state, videos: action.bool};
            break;

        case types.IMAGE_MARKERS_LAYOUT:
            return { ...state, image_markers_layout: action.bool};
            break;

        case types.UPDATE_SINGLE_PLAN_IMAGE:
            return { ...state, update_plan_image: action.id};
            break;

        case types.TYPE:
            return { ...state, type_action: action.key};
            break;

        case types.UPDATE_SINGLE_IMAGE:
            return { ...state, update_image_id: action.id};
            break;

        case types.CLEAR_IFLOORPLAN_STATE:
            return { ...state, ifloorplanNowState: [], positionCams: {} };
            break;

        case types.PANELUM_LIB:
            return { ...state, panelum_lib: action.items };
            break;

        case types.SET_360_TOOLS_ACTION:
            return { ...state, tools_360_action: action.value };
            break;

        case types.SET_BACKGROUND_COLOR:
            return { ...state, background_color: action.color };
            break;

        case types.SET_TEXT_COLOR:
            return { ...state, text_color: action.color };
            break;

        case types.SET_COLOR:
            return { ...state, set_color: action.bool };
            break;

        case types.LEVEL:
            return { ...state, levelNowId: action.level };
            break;

        case types.SCALE:
            return { ...state, scale: action.bool };
            break;

        case types.IMAGE_NOW_KEY:
            return { ...state, image_key: action.key };
            break;

        case types.CHANGE_CAMS_POSITION:
            return { ...state, positionCams: action.obj };
            break;

        case types.CAM_NOW_KEY:
            return { ...state, cam_key: action.key };
            break;

        case types.LIBRARY_PLAN_ADD:
            return { ...state, LibraryPlans: action.array };
            break;

        case types.LIBRARY_IMAGE_ADD:
            return { ...state, LibraryImage: action.array };
            break;

        case types.NEXT_STEP:
            return { ...state, step: action.step, prev_step: action.prevStep };
            break;

        case types.DROP_FILES:
            return { ...state, dropFiles: action.files, dropFilesTitle: action.title };
            break;

        case types.IFLOORPLAN_STATE:
            return { ...state, ifloorplanNowState: action.plan };
            break;

        default:
           return state;
    }
}

export default rootReducer;