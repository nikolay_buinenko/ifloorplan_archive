export const NEXT_STEP = "NEXT_STEP";
export const LEVEL = "LEVEL";
export const DROP_FILES = "DROP_FILES";
export const IFLOORPLAN_STATE = "IFLOORPLAN_STATE";
export const LIBRARY_PLAN_ADD = "LIBRARY_PLAN_ADD";
export const LIBRARY_IMAGE_ADD = "LIBRARY_IMAGE_ADD";
export const CAM_NOW_KEY = "CAM_NOW_KEY";
export const CHANGE_CAMS_POSITION = "CHANGE_CAMS_POSITION";
export const IMAGE_NOW_KEY = "IMAGE_NOW_KEY";
export const SCALE = "SCALE";
export const SET_COLOR = "SET_COLOR";
export const SET_BACKGROUND_COLOR = "SET_BACKGROUND_COLOR";
export const SET_TEXT_COLOR = "SET_TEXT_COLOR";
export const SET_360_TOOLS_ACTION = "SET_360_TOOLS_ACTION";
export const PANELUM_LIB = "PANELUM_LIB";
export const CLEAR_IFLOORPLAN_STATE = "CLEAR_IFLOORPLAN_STATE";
export const UPDATE_SINGLE_IMAGE = "UPDATE_SINGLE_IMAGE";
export const TYPE = "TYPE";
export const UPDATE_SINGLE_PLAN_IMAGE = "UPDATE_SINGLE_PLAN_IMAGE";
export const IMAGE_MARKERS_LAYOUT = "IMAGE_MARKERS_LAYOUT";
export const VIDEOS = "VIDEOS";
export const IMAGE_LIBRARY_SORTABLE_LIST = "IMAGE_LIBRARY_SORTABLE_LIST";

export const START_JSON = {
    level: [
        {
            plan_level: {
                img: null,
            },
            images: [],
            tabLabel: "First Floor",
            index: 0
        },

    ],
    media: [],
    order: [],
    tabs_style: JSON.stringify({top: "0%", left: "50%", transform: "rotate(0deg)"}),
    textColor: "#000000",
    bgColor: "#ffffff",
    camColor: "#0babe6",
    address: ""
};

//====================================================
function readCookie(name) {
    //function split cookie for get csrf token
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
export const CSRFTOKEN = readCookie('csrftoken'); //get csrftoken
//======================================================