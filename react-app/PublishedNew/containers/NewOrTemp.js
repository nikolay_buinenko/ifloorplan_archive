import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { CSRFTOKEN, START_JSON } from '../../New/redux/constant/constants';
import { nextStep, ifloorplanStateAdd, setType, LibImageAdd, LibPlanAdd, panelumLib, clearIfloorState } from '../../New/redux/actions/actions';
import UploadPlans from "../../New/components/UploadPlans";
import SingleUploadPlan from "../../New/components/SingleUploadPlan";
import SingleUploadImage from "../../New/components/SingleUploadImage";
import UploadFiles from "../../New/components/UploadFiles";
import UploadImage from "../../New/components/UploadImage";
import TopPanel from "../components/TopPanel";
import ScaleLayout from "../../New/components/ScaleLayout";
import PlanLibrary from "../../New/components/PlanLibrary";
import ImageLibrary from "../../New/components/ImageLibrary";
import CamsPosition from "../../New/components/CamsPosition";
import TextEditor from "../../New/components/TextEditor";
import Panorama from "../../New/components/Panorama";
import Media from "../../New/components/Media";
import Video from "../components/Video";
import ManageLevels from "../../New/components/ManageLevels";
import LatLong from "../../New/components/LatLong";
import UploadSinglePanoramaLib from "../../New/components/UploadSinglePanoramaLib";
import UploadPanoramaLib from "../../New/components/UploadPanoramaLib";
import PanoramaLibrary from "../../New/components/PanoramaLibrary";
import NewLevelCreateOption from "../../New/components/NewLevelCreateOption";
import Clients from "../../New/components/Clients";
import Layout from "../../New/components/Layout";
import KenBurns from "../../New/components/KenBurns";
import UpdateSinglePlan from "../../New/components/UpdateSinglePlan";
import PositionVideos from "../../New/components/PositionVideos";
import { createNewPlan, getTemplates, getBrand, getImages } from "../../New/serverRequests/requests";


const NewOrTemp = (props) => {
        const [templates, setTemplates] = useState([]);
        const [templatesWrap, setTemplatesWrap] = useState(false);
        const step = props.step;


        const startNewPlan = () => {
            setTemplatesWrap(false);
            let brand;
            getBrand(document.getElementById("user_id").value)
                .then((data)=>{
                    brand = data[0];
                    createNewPlan(START_JSON)
                        .then((data_res) => {
                            let data_res_new = Object.assign({}, data_res[0]);
                            if(brand){
                                data_res_new.brand_logo = brand.logo;
                                data_res_new.brand_width = brand.width;
                                data_res_new.brand_transparency = brand.transparency;
                                data_res_new.brand_pos_x = brand.position_x;
                                data_res_new.brand_pos_y = brand.position_y;
                                data_res_new.brand_url = brand.url;
                            }
                            props.ifloorplanStateAdd(data_res_new);
                            props.nextStep("upload plans", "_");
                        });
                })
        }

        const setTemplate = (data) => {
            let newState = JSON.parse(JSON.stringify(data));
            delete newState.author;
            delete newState.id;
            delete newState.videos;
            newState.template_name = "";
            newState.start_live = new Date().getTime()/1000;
            newState.end_live = new Date().getTime()/1000+3600;
            newState.level = [{plan_level: {img: null},images:[],tabLabel: "First Floor", index: 0}];
            createNewPlan(newState)
                .then((data_res) => {
                    setTemplatesWrap(false);
                    props.ifloorplanStateAdd(data_res[0]);
                    props.nextStep("upload plans", "template");
                });
        }

        const startTemplate = () => {
            const data_request = {
                user_id: document.getElementById("user_id").value,
            }
            setTemplatesWrap(true);
            getTemplates(data_request)
                .then(data=>{
                    const temp = [];
                    for(let i = 0; i < data.length; i++){
                        if(data[i].template_name != ""){
                            temp.push(
                                <div className="template" onClick={()=>{setTemplate(data[i])}} key={i}>
                                    <h3>{data[i].template_name}</h3>
                                </div>
                            )
                        }
                    }
                    setTemplates(temp);
                });
        }

        return (
            <div className={step === "" ? "wrap" : step}>
                {step !== "" ? <TopPanel/> : ''}
                {step === "" ? <div className="subHeader"><div className="title">New</div></div> : '' }
                {step === "" ? <div className="mainApp"><button onClick={startNewPlan}>Start new iFloorPlan</button><p>Or</p><button onClick={startTemplate}>Start new from Template</button></div> : '' }

                {step === "upload plans" ? <UploadPlans/> : ''}
                {step === "upload files" ? <UploadFiles/> : ''}
                {step === "upload images" ? <UploadImage/> : ''}
                {step === "scale layout" ? <ScaleLayout/> : ''}
                {step === "scale and repos" ? <ScaleLayout/> : ''}
                {step === "plan library" ? <PlanLibrary/> : ''}
                {step === "image library" ? <ImageLibrary/> : ''}
                {step === "single plan upload" ? <SingleUploadPlan/> : ''}
                {step === "cams position" ? <CamsPosition/> : ''}
                {step === "simple upload image" ? <SingleUploadImage/> : ''}
                {step === "new level create option" ? <NewLevelCreateOption/> : ''}
                {step === "text editor" ? <TextEditor/> : ''}
                {step === "add video" ? <Video/> : ''}
                {step === "add media" ? <Media/> : ''}
                {step === "panorama" ? <Panorama/> : ''}
                {step === "manage levels" ? <ManageLevels/> : ''}
                {step === "add lat and long" ? <LatLong/> : ''}
                {step === "360 library" ? <PanoramaLibrary/> : ''}
                {step === "single 360" ? <UploadSinglePanoramaLib/> : ''}
                {step === "360 library upload" ? <UploadPanoramaLib/> : ''}
                {step === "panorama single upload file" ? <UploadSinglePanoramaLib/> : ''}
                {step === "clients" ? <Clients/> : ''}
                {step === "layout" ? <Layout/> : ''}
                {step === "ken burns" ? <KenBurns/> : ''}
                {step === "single plan update image" ? <UpdateSinglePlan/> : ''}
                {step === "position videos" ? <PositionVideos/> : ''}

                <div className="tempWrap" style={{ display: templatesWrap ? "block" : "none"}}>
                    <h3 className="template_title">Choose a Template</h3>
                    {templates}
                </div>
            </div>
        )
}

const putState = (state) => {
    return {
        step: state.step,
        dropFiles: state.dropFiles,
        dropFilesTitle: state.dropFilesTitle,
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        LibPlanAdd: (array) => dispatch(LibPlanAdd(array)),
        LibImageAdd: (array) => dispatch(LibImageAdd(array)),
        panelumLib: (items) => dispatch(panelumLib(items)),
        setType: (key) => dispatch(setType(key)),
        clearIfloorState: () => dispatch(clearIfloorState())
    }
}


export default connect(putState, putActions) (NewOrTemp);