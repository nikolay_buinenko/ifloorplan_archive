import React, { useState, useEffect } from 'react';
import { getTemplates, updateNewPlan, deletePlan } from './../requests';
import Plan from './Plan';

const Published = () => {
    const [render, setRender] = useState([]);
    const [dataMain, setDataMain] = useState([]);
    const [nowPage, setNowPage] = useState(1);
    const [activeEdit, setActiveEdit] = useState("");
    const [countPages, setCountPages] = useState(1);
    const [countPlans, setCountPlans] = useState(0);
    const [dataSearch, setDataSearch] = useState([]);
    const [searchText, setSearcheText] = useState("");
    const [displayDataPagination, setDisplayDataPagination] = useState({start:0, finish:0});
    const data_request = {
            user_id: document.getElementById("user_id").value,
        }

    const Update = () => {
        getTemplates(data_request)
            .then(data=>{
                //init when page in upload
                const new_data = [];
                for(let i = 0; i < data.length; i++){
                    if(data[i].level[0] && data[i].template_name == "" && data[i].level[0].images.length != 0){
                        new_data.push(data[i]);
                    }
                }
                setRender([]);
                setCountPlans(new_data.length);
                setCountPages(Math.ceil(new_data.length / 2));
                setDataMain(new_data);
                setDataSearch(new_data);
                initPlans(new_data);
            });
    }

    useEffect(()=>{
        setRender([]);
        const temp = [];
        const upLevel = nowPage * 2;
        const lowLevel = upLevel - 2;
        for(let i = 0; i < dataSearch.length; i++){
            if(i >= lowLevel && i <= upLevel - 1) {
                const expired_time_sec = dataSearch[i].end_live - parseInt(new Date().getTime() / 1000);
                const expire_time_days = parseInt(expired_time_sec / 60 / 60 / 24);
                temp.push(<Plan key={i}
                                data={dataSearch[i]}
                                setActive={setActiveEdit}
                                active={activeEdit === dataSearch[i].id ? true : false}
                                time={expire_time_days}
                                update={Update}/>)
            }
        }
        setRender(temp);
    }, [activeEdit])

    const initPlans = (dataInput = [], search = false) => {
        const upLevel = nowPage * 2;
        const lowLevel = upLevel - 2;
        const temp = [];
        console.log(dataInput)
        for(let i = 0; i < dataInput.length; i++){
            if(i >= lowLevel && i <= upLevel - 1) {
                const expired_time_sec = dataInput[i].end_live - parseInt(new Date().getTime() / 1000);
                const expire_time_days = parseInt(expired_time_sec / 60 / 60 / 24);
                temp.push(<Plan key={i}
                                setActive={setActiveEdit}
                                active={activeEdit == dataInput[i].id ? true : false}
                                data={dataInput[i]}
                                time={expire_time_days}
                                update={Update}/>)
            }
        }
        setDisplayDataPagination({start: lowLevel + 1, finish: upLevel})
        setRender(temp);
    }

    const showAll = () => {
        const temp = [];
        console.log(dataSearch)
        for(let i = 0; i < dataSearch.length; i++){
                const expired_time_sec = dataSearch[i].end_live - parseInt(new Date().getTime() / 1000);
                const expire_time_days = parseInt(expired_time_sec / 60 / 60 / 24);
                temp.push(<Plan key={i}
                                setActive={setActiveEdit}
                                data={dataSearch[i]}
                                active={activeEdit == dataSearch[i].id ? true : false}
                                time={expire_time_days}
                                update={Update}/>)
        }
        setRender(temp);
    }

    useEffect(()=>{
        const upLevel = nowPage * 2;
        const lowLevel = upLevel - 2;
        const temp = [];
        console.log(dataSearch)
        for(let i = 0; i < dataSearch.length; i++){
            if(i >= lowLevel && i <= upLevel - 1) {
                const expired_time_sec = dataSearch[i].end_live - parseInt(new Date().getTime() / 1000);
                const expire_time_days = parseInt(expired_time_sec / 60 / 60 / 24);
                temp.push(<Plan key={i}
                                setActive={setActiveEdit}
                                data={dataSearch[i]}
                                active={activeEdit == dataSearch[i].id ? true : false}
                                time={expire_time_days}
                                update={Update}/>)
            }
        }
        setDisplayDataPagination({start: lowLevel + 1, finish: upLevel})
        setRender(temp);
    }, [nowPage]);

    useEffect(()=>{
        getTemplates(data_request)
            .then(data=>{
                //init when page in upload
                const new_data = [];
                for(let i = 0; i < data.length; i++){
                    if(data[i].level[0] && data[i].template_name == "" && data[i].level[0].images.length != 0){
                        new_data.push(data[i]);
                    }
                }
                setCountPlans(new_data.length);
                setCountPages(Math.ceil(new_data.length / 2));
                setDataMain(new_data);
                setDataSearch(new_data);
                initPlans(new_data);
            });
    }, [])

    useEffect(()=>{
        const result = dataMain.filter(item =>
            item.address.toLowerCase().includes(searchText.toLowerCase())
        )
        setDataSearch(result);
        if(searchText === ""){
            initPlans(dataMain, true);
        } else {
            initPlans(result, true);
        }
    }, [searchText]);

    return(
        <div className="publ_wrap">
            <div className="search">
                <i className="fas fa-search"></i>
                <input type="text"
                       placeholder='Search for...'
                       onChange={(event)=>{setSearcheText(event.target.value)}}/>
            </div>
            <div className="render">
                {render}
            </div>
            <div className="pagination">

                <button onClick={() => setNowPage(1)}>&lt;&lt;</button>
                <button onClick={() => setNowPage(nowPage == 1 ? nowPage : nowPage - 1)}>&lt; </button>
                <p>Page {nowPage} of {countPages} (Shoving items {displayDataPagination.start} - {displayDataPagination.finish} of {countPlans})</p>
                <button onClick={() => setNowPage(nowPage == countPages ? nowPage : nowPage + 1)}>&gt;</button>
                <button onClick={() => setNowPage(countPages)}>&gt;&gt;</button>
                <button onClick={showAll}>Show all</button>
            </div>
        </div>
    )
}
export default Published;