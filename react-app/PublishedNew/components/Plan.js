import React, {useState, useEffect} from 'react';
import {getTemplates, updateNewPlan, deletePlan} from './../requests';
import {CSRFTOKEN} from '../../New/redux/constant/constants';
import {createNewPlan, getBrand, getImages} from "../../New/serverRequests/requests";
import NewOrTemp from '../containers/NewOrTemp';
import Share from './Share';
import {connect} from 'react-redux';
import {
    nextStep,
    ifloorplanStateAdd,
    setType,
    LibImageAdd,
    LibPlanAdd,
    panelumLib,
    clearIfloorState
} from '../../New/redux/actions/actions';

const Plan = (props) => {
    const [editAction, setEditAction] = useState(props.active);
    const [changedUrl, setChengedUrl] = useState("https://ifloorplan360.com/ifloorplan/" + props.data.id);
    const [embedAction, setEmbedAction] = useState(false);
    const [shareActon, setShareAction] = useState(false);
    let url = "https://ifloorplan360.com/ifloorplan/" + props.data.id;

    const deletePlanObj = (id_plan) => {
        const data = {
            id: id_plan
        }
        deletePlan(data)
            .then(data => {
                props.update();
            })
    }

    const changeOption = (event) => {
        console.log(event.target.value);
        // setChengedUrl(url+'/'+event.target.value);
    };

    return (
        <div className="plan_wrap_item" id={props.data.id}>
            <div className="plan_item">
                <div className="logo"><img style={{width: "150px"}} src={props.data.level[0].images[0].img} alt=""/>
                </div>
                <div className="tools">
                    <div className="address"><h2>{props.data.address}</h2><p>Published</p></div>
                    <div className="url_option">
                        <input type="text" value={changedUrl} onChange={(event) => {
                            event.preventDefault()
                        }}/>
                        <select value={changedUrl} onChange={event => changeOption(event)}>
                            <option value="">Show all brending</option>
                            <option value="?hb=1">Hide all brending</option>
                            <option value="?hbo=1">Hide my brending</option>
                            <option value="?hco=1">Hide client brending</option>
                        </select>
                    </div>
                    <div className="btn_group">
                        <a href={changedUrl} target="_blank">View</a>
                        <a href={"https://ifloorplan360.com/user/edit/" + props.data.id} target="_blank">Edit</a>
                        <a href="" onClick={(event) => {
                            event.preventDefault();
                            setShareAction(!shareActon);
                        }}>Share</a>
                        <a href="" onClick={(event) => {
                            event.preventDefault();
                            setEmbedAction(!embedAction);
                        }}>Embed</a>
                        <a href="">Notes</a>
                        <a href="" onClick={(event) => {
                            event.preventDefault();
                            deletePlanObj(props.data.id);
                        }}>Delete</a>
                    </div>
                    {shareActon ? <div className="shareWrap"><Share/></div> : ""}
                    {embedAction ? <div className="embed">
                        <pre>&lt;iframe src="{changedUrl}" style="border: 0"
                            width="800" height="500" frameBorder="0"
                            scrolling="no">&lt;/iframe></pre>
                    </div> : ""}
                    <div className="info">
                        <p>Expired in {props.time} days | Viewed {props.data.viewed} times</p>
                    </div>
                </div>
                <div className="brand_title">Best Real Estate</div>
            </div>
            {/*{props.active ? <div className="editWrap"><NewOrTemp/></div> : ""}*/}
        </div>
    )
}
const putState = (state) => {
    return {
        step: state.step
    }
}

const putActions = (dispatch) => {
    return {
        nextStep: (step, prevStep) => dispatch(nextStep(step, prevStep)),
        ifloorplanStateAdd: (plan) => dispatch(ifloorplanStateAdd(plan)),
        LibPlanAdd: (array) => dispatch(LibPlanAdd(array)),
        LibImageAdd: (array) => dispatch(LibImageAdd(array)),
        panelumLib: (items) => dispatch(panelumLib(items)),
        setType: (key) => dispatch(setType(key)),
        clearIfloorState: () => dispatch(clearIfloorState())
    }
}


export default connect(putState, putActions)(Plan);