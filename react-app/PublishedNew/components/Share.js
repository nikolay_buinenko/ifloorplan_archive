import React from 'react';
import {
  EmailShareButton,
  FacebookShareButton,
  HatenaShareButton,
  InstapaperShareButton,
  LineShareButton,
  LinkedinShareButton,
  LivejournalShareButton,
  MailruShareButton,
  OKShareButton,
  PinterestShareButton,
  PocketShareButton,
  RedditShareButton,
  TelegramShareButton,
  TumblrShareButton,
  TwitterShareButton,
  ViberShareButton,
  VKShareButton,
  WhatsappShareButton,
  WorkplaceShareButton
} from "react-share";

import {
  EmailIcon,
  FacebookIcon,
  FacebookMessengerIcon,
  HatenaIcon,
  InstapaperIcon,
  LineIcon,
  LinkedinIcon,
  LivejournalIcon,
  MailruIcon,
  OKIcon,
  PinterestIcon,
  PocketIcon,
  RedditIcon,
  TelegramIcon,
  TumblrIcon,
  TwitterIcon,
  ViberIcon,
  VKIcon,
  WeiboIcon,
  WhatsappIcon,
  WorkplaceIcon
} from "react-share";

const Share = (props) => {
    return (
        <div className="share">
            <EmailShareButton
                url="ifloorplan360.com"
                subject="ifloorplan"
                body="My new Floor Plan"
            ><EmailIcon size={32} round={true}></EmailIcon></EmailShareButton>
            <FacebookShareButton
                quote="My new Floor Plan"
                url="ifloorplan360.com"
                hashtag="#ifloorplan"
            ><FacebookIcon size={32} round={true}></FacebookIcon></FacebookShareButton>
            <HatenaShareButton
                url="ifloorplan360.com"
                title="My new Floor Plan"
            ><HatenaIcon size={32} round={true}></HatenaIcon></HatenaShareButton>
            <InstapaperShareButton
                url="ifloorplan360.com"
                title="ifloorplan"
                description="My new Floor Plan"
            ><InstapaperIcon size={32} round={true}></InstapaperIcon></InstapaperShareButton>
            <LineShareButton
                url="ifloorplan360.com"
                title="My new Floor Plan"
            ><LineIcon size={32} round={true}></LineIcon></LineShareButton>
            <LinkedinShareButton
                url="ifloorplan360.com"
                title="ifloorplan"
                summary="My new Floor Plan"
                source="ifloorplan360.com"
            ><LinkedinIcon size={32} round={true}></LinkedinIcon></LinkedinShareButton>
            <LivejournalShareButton
                url="ifloorplan360.com"
                title="ifloorplan"
                description="My new Floor Plan"
            ><LivejournalIcon size={32} round={true}></LivejournalIcon></LivejournalShareButton>
            <MailruShareButton
                url="ifloorplan360.com"
                title="ifloorplan"
                description="My new Floor Plan"
                imageUrl="https://www.ifloorplan360.com/static/user/img/ifloorplanLogo.png"
            ><MailruIcon size={32} round={true}></MailruIcon></MailruShareButton>
            <OKShareButton
                url="ifloorplan360.com"
                title="ifloorplan"
                description="My new Floor Plan"
                imageUrl="https://www.ifloorplan360.com/static/user/img/ifloorplanLogo.png"
            ><OKIcon size={32} round={true}></OKIcon></OKShareButton>
            <PinterestShareButton
                url="ifloorplan360.com"
                description="My new Floor Plan"
            ><PinterestIcon size={32} round={true}></PinterestIcon></PinterestShareButton>
            <PocketShareButton
                url="ifloorplan360.com"
                title="ifloorplan"
            ><PocketIcon size={32} round={true}></PocketIcon></PocketShareButton>
            <RedditShareButton
                url="ifloorplan360.com"
                title="ifloorplan"
            ><RedditIcon size={32} round={true}></RedditIcon></RedditShareButton>
            <TelegramShareButton
                url="ifloorplan360.com"
                title="ifloorplan"
            ><TelegramIcon size={32} round={true}></TelegramIcon></TelegramShareButton>
            <TumblrShareButton
                url="ifloorplan360.com"
            ><TumblrIcon size={32} round={true}></TumblrIcon></TumblrShareButton>
            <TwitterShareButton
                url="ifloorplan360.com"
                title="ifloorplan"
                via="My new Floor Plan"
                hashtags={["#ifloorplan"]}
            ><TwitterIcon size={32} round={true}></TwitterIcon></TwitterShareButton>
            <ViberShareButton
                url="ifloorplan360.com"
                title="ifloorplan"
            ><ViberIcon size={32} round={true}></ViberIcon></ViberShareButton>
            <VKShareButton
                url="ifloorplan360.com"
                title="ifloorplan"
            ><VKIcon size={32} round={true}></VKIcon></VKShareButton>
            <WhatsappShareButton
                title="ifloorplan"
                url="ifloorplan360.com"
            ><WhatsappIcon size={32} round={true}></WhatsappIcon></WhatsappShareButton>
            <WorkplaceShareButton
                quote="My new Floor Plan"
                url="ifloorplan360.com"
                hashtag="#ifloorplan"
            ><WorkplaceIcon size={32} round={true}></WorkplaceIcon></WorkplaceShareButton>
        </div>
    )
}
export default Share;