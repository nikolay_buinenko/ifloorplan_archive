import React, {useState, useEffect} from 'react';
import {getTemplates, updateNewPlan, deletePlan} from './../requests';
import Popup from "reactjs-popup";

const Templates = () => {
    const [templates, setTemplates] = useState([]);
    const [templateData, setTemplateData] = useState({});
    const [deletePopup, setDeletePopup] = useState(false);
    const [renamePopup, setRenamePopup] = useState(false);
    const [newName, setNewName] = useState("");


    const Update = () => {
        const data_request = {
            user_id: document.getElementById("user_id").value,
        }
        getTemplates(data_request)
            .then(data=>{
                const temp = [];
                for(let i = 0; i < data.length; i++){
                    if(data[i].template_name != ""){
                        temp.push(
                            <div className="template" key={i}>
                                <h3>{data[i].template_name}</h3>
                                <div className="btn_group">
                                    <button onClick={()=>{
                                        setTemplateData(data[i]);
                                        setDeletePopup(false);
                                        setRenamePopup(true);
                                    }}>Rename</button>
                                    <p>|</p>
                                    <button onClick={()=>{
                                        setTemplateData(data[i]);
                                        setRenamePopup(false);
                                        setDeletePopup(true);
                                    }}>Delete</button>
                                </div>
                            </div>
                        )
                    }
                }
                setTemplates(temp);
            });
    }


    const Rename = () => {
        setRenamePopup(false);
        let newState = JSON.parse(JSON.stringify(templateData));
        newState.template_name= newName;
        updateNewPlan(newState)
            .then((data)=>{
                setTemplates([]);
                Update();
            })

    }

    const Delete = () => {
        setDeletePopup(false);
        const data = {
            id: templateData.id
        }
        deletePlan(data)
            .then(data=>{
                setTemplates([]);
                Update();
                console.log(data)
            })
    }

    useEffect(()=>{
        Update();
    }, [])

    return(
        <div className="wrap">
            <div className="description"><h2>Select a template to rename or delete</h2></div>
            <div className="templates_list">
                {templates}
            </div>

            <Popup
                open={renamePopup}
                closeOnDocumentClick
                onClose={()=> {
                    setRenamePopup(false);
                }}
            >
                <div className="modal">
                    <h2>Rename this Template</h2>
                    <div className="inpt">
                        <p>Write a new name for this Template</p>
                        <input type="text" onChange={(event => setNewName(event.target.value ))}/>
                        <p>Qualifying text</p>
                        <div className="btn_popup">
                            <button onClick={()=>setRenamePopup(false)}>Cancel</button>
                            <button onClick={Rename}>Save</button>
                        </div>
                    </div>
                </div>
            </Popup>

            <Popup
                open={deletePopup}
                closeOnDocumentClick
                onClose={()=> {
                    setDeletePopup(false);
                }}
            >
                <div className="modal">
                    <h2>Are you sure you want to delete the Template {templateData.template_name}?</h2>
                    <h2>This cannot be undone!</h2>
                    <div className="btn_popup">
                        <button onClick={()=>setDeletePopup(false)}>Cancel</button>
                        <button onClick={Delete}>Delete</button>
                    </div>
                </div>
            </Popup>
        </div>
    )
}
export default Templates;