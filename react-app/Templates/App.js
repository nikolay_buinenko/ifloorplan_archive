import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Templates from './components/Templates';

class App extends Component {
    render() {
        return (
            <div className="TemplatesWrapper">
                <div className="title_block"><h2>Templates</h2></div>
                <Templates />
            </div>
        );
    }
}
ReactDOM.render(<App />, document.getElementById('app'));