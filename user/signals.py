from django.shortcuts import get_object_or_404
from paypal.standard.ipn.signals import valid_ipn_received
from django.dispatch import receiver
from public.models import Profile, Transaction
from loguru import logger
from ifloorplan.settings import EMAIL_HOST_USER
from django.core.mail import send_mail

logger.add("debug.log", format="{time} {level} {message}", level="DEBUG")


@receiver(valid_ipn_received)
def payment_notification(sender, **kwargs):
    ipn = sender
    transaction = Transaction.objects.filter(transaction_hash_code=ipn.custom).first()
    # check for Buy Now IPN
    if ipn.txn_type == 'web_accept':
        if ipn.payment_status == 'Completed':
            # payment was successful
            if transaction:
                profile = Profile.objects.get(user=transaction.user.id)
                credits = transaction.product_purchase.count_credits
                if not transaction.paid:
                    transaction.paid = True
                    transaction.payment_method = "PayPal"
                    transaction.save()
                    profile.credits = profile.credits + credits
                    profile.save()

    # check for subscription signup IPN
    elif ipn.txn_type == "subscr_signup":

        # get user id and activate the account
        pass

    # check for subscription payment IPN
    elif ipn.txn_type == "subscr_payment":
        if transaction:
            profile = Profile.objects.get(user=transaction.user.id)
            credits = transaction.product_subscription.credit_count
            profile.subscription_send_message = True
            if ipn.payment_status == 'Completed':
                if not transaction.paid:
                    transaction.paid = True
                    transaction.payment_method = "PayPal"
                    transaction.save()
                    profile.credits = profile.credits + credits
                profile.frozen = False
                profile.subscription = True
                profile.save()

    # check for failed subscription payment IPN
    elif ipn.txn_type == "subscr_failed":
        pass

    # check for subscription cancellation IPN
    elif ipn.txn_type == "subscr_cancel":
        if transaction:
            profile = Profile.objects.get(user=transaction.user.id)
            transaction.delete()
            profile.frozen = False
            profile.subscription = False
            if profile.credits - profile.subscription_credits < 0:
                profile.credits = 0
            else:
                profile.credits = profile.credits - profile.subscription_credits
            profile.subscription_credits = 0
            profile.subscription_object = None
            profile.date_over_subscription = 0
            profile.date_start_subscription = 0
            profile.date_alert_subscription_over = 0
            transaction.delete()
            profile.save()

    elif ipn.txn_type == "subscr_eot":
        if transaction:
            profile = Profile.objects.get(user=transaction.user.id)
            credits = transaction.product_subscription.credit_count
            if profile.subscription_credits != 0:
                profile.credits = profile.credits - credits
            if profile.subscription_send_message:
                subject = "Subscription is overed"
                message = "Your subscription is over in letter 3 days"
                recepient = profile.user.email
                try:
                    send_mail(subject, message, EMAIL_HOST_USER, [recepient], fail_silently = False)
                except Exception as e:
                    pass
                profile.subscription_send_message = False
            profile.frozen = True
            profile.save()