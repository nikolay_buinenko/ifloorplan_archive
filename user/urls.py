from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.UserView.index),
    path('new/', views.UserView.index, name='new'),
    path('edit/<int:pk>', views.UserView.edit, name='edit'),
    path('purchase/', views.UserView.purchase, name='purchase'),
    path('markers/', views.UserView.markers, name='markers'),
    path('published/', views.UserView.published, name='published'),
    path('clients/', views.UserView.clients, name='clients'),
    path('branding/', views.UserView.branding, name='branding'),
    path('templates/', views.UserView.templates, name='templates'),
    path('pass_change/', views.UserView.passChg, name='passChg'),
    path('userguide/', views.UserView.userguide, name='userguide'),
    path('email_change/', views.UserView.emailChg, name='emailChg'),
    path('domain/', views.UserView.domainApi, name='domainApi'),
    path('snapshot/', views.UserView.snapshot, name='snapshot'),
    path('country_term/', views.UserView.countryTerm, name='countryTerm'),
    path('process-payment/', views.UserView.process_payment, name='process_payment'),
    path('payment-done/', views.payment_done, name='payment_done'),
    path('paypal-payment-done/', views.paypal_payment_done, name='paypal-payment_done'),
    path('payment-cancelled/', views.payment_canceled, name='payment_cancelled'),
    path('apple-register-hash/', views.UserView.apple_register_hash, name='apple_register_hash'),
]