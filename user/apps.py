from django.apps import AppConfig
import traceback2 as traceback
from loguru import logger

logger.add("debug.log", format="{time} {level} {message}", level="DEBUG")


class UserConfig(AppConfig):
    name = 'user'

    def ready(self):
        # try:
        import user.signals
        # except Exception as e:
        #     logger.debug(traceback.format_exc())
