from django.shortcuts import render, redirect, reverse
from django.contrib import messages
from public.models import Profile, PurchaseCredits, SubscriptionPrice, Transaction
from api.models import IFloorPlan
from django.conf import settings
from decimal import Decimal
from paypal.standard.forms import PayPalPaymentsForm
from django.contrib.auth.models import User
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from ifloorplan.settings import EMAIL_HOST_USER
from django.core.mail import send_mail
import time
from django.views.decorators.csrf import csrf_exempt
import shutil
from django.http import JsonResponse
from loguru import logger
import stripe

logger.add("debug.log", format="{time} {level} {message}", level="DEBUG")


#================stripe keys==========================
stripe.api_key = "sk_test_CrJwgXtaK4yovet1O8jqysjQ000X8CWyGr"

stripe.ApplePayDomain.create(
  domain_name='ifloorplan360.com',
)
#=====================================================

def countCredits(user):
    profile = Profile.objects.filter(user=user).first()
    return profile.credits

def countPlans(user):
    plans = IFloorPlan.objects.filter(author=user, published=True).count()
    return plans

def provizor():
    #delete not finished plans or not actually plans
    plans = IFloorPlan.objects.all()
    for plan in plans:
        if plan.end_live <= time.time():
            path = '/home/django/ifloorplan/media/ifloorplans/ifloorplan_'+str(plan.id)
            try:
                shutil.rmtree(path)
            except:
                pass
            plan.delete()

def subsription_init():
    #check subsription membership valid
    profiles = Profile.objects.filter(subscription=True)
    for profile in profiles:
        if profile.date_over_subscription < time.time() and profile.date_over_subscription != 0:
            profile.frozen = False
            profile.subscription = False
            if profile.credits - profile.subscription_credits < 0:
                profile.credits = 0
            else:
                profile.credits = profile.credits - profile.subscription_credits
            profile.subscription_credits = 0
            profile.subscription_object = None
            profile.date_over_subscription = 0
            profile.date_start_subscription = 0
            profile.date_alert_subscription_over = 0
        if profile.date_alert_subscription_over < time.time() and profile.date_alert_subscription_over != 0:
            if profile.subscription_send_message:
                subject = "Subscription is overed"
                message = "Your subscription is over in letter 3 days"
                recepient = profile.user.email
                try:
                    send_mail(subject, message, EMAIL_HOST_USER, [recepient], fail_silently = False)
                except Exception as e:
                    pass
                profile.subscription_send_message = False
        profile.save()
subsription_init()

def hash_code_gen():
    import random
    lower = "qwertyuiopasdfghjklzxcvbnm"
    upper = "QWERTYUIOPASDFGHJKLZXCVBNM"
    numbers = "0987654321"
    symbols = "[]{}()_-"
    all = lower + upper + numbers + symbols
    return "".join(random.sample(all, 10))

@csrf_exempt
def payment_done(request):
    transaction = Transaction.objects.filter(transaction_hash_code=request.GET['hash']).first()
    profile = Profile.objects.get(user=request.user.id)
    if transaction.product_purchase:
        credits = transaction.product_purchase.count_credits
    else:
        credits = transaction.product_subscription.credit_count
    if transaction:
        if not transaction.paid:
            transaction.paid = True
            profile.subscription_send_message = True
            if request.GET['method'] == "google":
                transaction.payment_method = "Google Pay"
            elif request.GET['method'] == "apple":
                transaction.payment_method = "Apple Pay"
            transaction.save()
            if transaction.product_purchase:
                profile.credits = profile.credits + credits
            else:
                if profile.subscription_credits != 0:
                    profile.credits = profile.credits - profile.subscription_credits
                profile.type_subscription = transaction.pear
                profile.subscription_credits = credits
                profile.credits = profile.credits + credits
                profile.subscription_object = transaction.product_subscription
                profile.frozen = False
                profile.subscription = True
                profile.date_start_subscription = time.time()
                seconds = 2592000 if transaction.pear == 'mount' else 31536000
                seconds_to_notif = 2332800 if transaction.pear == 'mount' else 31276800
                profile.date_over_subscription = time.time() + seconds
                profile.date_alert_subscription_over = time.time() + seconds_to_notif
            profile.save()
        return render(request, 'user/payment_done.html', {"alert": "Payment done"})
    else:
        return render(request, 'user/payment_done.html', {"alert": "Transaction not exist"})


@csrf_exempt
def payment_canceled(request):
    return render(request, 'user/payment_cancelled.html')

@csrf_exempt
def paypal_payment_done(request):
    return render(request, 'user/payment_done.html', {"alert": "Payment done"})

class UserView:

    def apple_register_hash(request):
        logger.debug("flknv")
        transaction = Transaction.objects.filter(transaction_hash_code=request.POST.get('old_hash', '')).first()
        transaction.transaction_hash_code = request.POST.get('token', '')
        transaction.save()
        return JsonResponse({'message': "ok"})

    def edit(request, pk):
        return render(request, 'user/edit.html', {"id": pk})

    def process_payment(request):
        host = request.get_host()
        hash_code = hash_code_gen() #generate unically hash transaction code
        if request.GET['type'] == "simple":
            instance = PurchaseCredits.objects.get(id=request.GET['product_id'])
            amount = instance.cost
            invoice = instance.count_credits
            product_type = "simple"
            Transaction(user=request.user, transaction_hash_code=hash_code, payment_method="*",
                        product_purchase=instance).save()
            paypal_dict = {
                'business': settings.PAYPAL_RECEIVER_EMAIL,
                'amount': amount,
                'item_name': instance.title,
                'invoice': hash_code,
                'currency_code': 'USD',
                'custom': hash_code,
                'notify_url': 'http://{}{}'.format(host,
                                                   reverse('paypal-ipn')),
                'return_url': 'http://{}{}'.format(host,
                                                   reverse('paypal-payment_done')),
                'cancel_return': 'http://{}{}'.format(host,
                                                      reverse('payment_cancelled')),
            }
        if request.GET['type'] == "sub":
            instance = SubscriptionPrice.objects.get(id=request.GET['product_id'])
            amount = instance.cost_by_mount if request.GET['pear'] == 'mount' else instance.cost_by_year
            invoice = instance.credit_count
            product_type = "subscription"
            Transaction(user=request.user, transaction_hash_code=hash_code, payment_method="*",
                        product_subscription=instance, pear=request.GET['pear']).save()
            paypal_dict = {
                "cmd": "_xclick-subscriptions",
                "business": settings.PAYPAL_RECEIVER_EMAIL,
                "a3": amount,  # monthly price
                "p3": 1,  # duration of each unit (depends on unit)
                "t3": "M" if request.GET['pear'] == 'mount' else "Y",  # duration unit ("M for Month")
                "src": "1",  # make payments recur
                'currency_code': 'USD',
                'custom': hash_code,
                "sra": "1",  # reattempt payment on payment error
                "no_note": "1",  # remove extra notes (optional)
                "item_name": instance.title,
                "notify_url": 'http://{}{}'.format(host,
                                                   reverse('paypal-ipn')),
                "return": 'http://{}{}'.format(host,
                                                   reverse('paypal-payment_done')),
                "cancel_return": 'http://{}{}'.format(host,
                                                      reverse('payment_cancelled')),
            }



        form = PayPalPaymentsForm(initial=paypal_dict)
        return render(request, 'user/process_payment.html', {'form': form,
                                                             'cost': amount,
                                                             'count': invoice,
                                                             'product_id': instance.id,
                                                             'product_type': product_type,
                                                             "hash": hash_code})

    def index(request):

        if request.user.is_authenticated:
            credits = countCredits(request.user)
            plans = countPlans(request.user)
            different = credits - plans
            if different > 0:
                new = True
            else:
                new = False
            provizor()
            return render(request, 'user/new.html', {"user_id": request.user.id,
                                                     "credits": 0 if different < 0 else different,
                                                     "plans": plans,
                                                     "new": new,
                                                     "username": request.user.username})
        else:
            return redirect('Login')

    def purchase(request):
        if request.user.is_authenticated:
            provizor()
            credits = countCredits(request.user)
            plans = countPlans(request.user)
            purchase = PurchaseCredits.objects.all()
            subscriptions = SubscriptionPrice.objects.all()
            return render(request, 'user/purchase.html', {"user_id":request.user.id,
                                                          "credits": credits - plans,
                                                          "plans": plans,
                                                          "username": request.user.username,
                                                          "data_purchase": purchase,
                                                          "data_subscriptions": subscriptions
                                                          })
        else:
            return redirect('Login')

    def published(request):
        if request.user.is_authenticated:
            provizor()
            credits = countCredits(request.user)
            plans = countPlans(request.user)
            return render(request, 'user/published.html', {"user_id":request.user.id,
                                                           "credits": credits - plans,
                                                           "plans": plans,
                                                           "username": request.user.username
                                                           })
        else:
            return redirect('Login')

    def markers(request):
        if request.user.is_authenticated:
            provizor()
            credits = countCredits(request.user)
            plans = countPlans(request.user)
            return render(request, 'user/markers.html', {"user_id":request.user.id,
                                                           "credits": credits - plans,
                                                           "plans": plans,
                                                           "username": request.user.username
                                                           })
        else:
            return redirect('Login')

    def clients(request):
        if request.user.is_authenticated:
            provizor()
            credits = countCredits(request.user)
            plans = countPlans(request.user)
            return render(request, 'user/clients.html', {"user_id":request.user.id,
                                                         "credits": credits - plans,
                                                         "plans": plans,
                                                         "username": request.user.username
                                                         })
        else:
            return redirect('Login')

    def branding(request):
        if request.user.is_authenticated:
            provizor()
            credits = countCredits(request.user)
            plans = countPlans(request.user)
            return render(request, 'user/branding.html', {"user_id":request.user.id,
                                                          "credits": credits - plans,
                                                          "plans": plans,
                                                          "username": request.user.username
                                                          })
        else:
            return redirect('Login')

    def templates(request):
        if request.user.is_authenticated:
            provizor()
            credits = countCredits(request.user)
            plans = countPlans(request.user)
            return render(request, 'user/template.html', {"user_id":request.user.id,
                                                          "credits": credits,
                                                          "plans": plans,
                                                          "username": request.user.username
                                                          })
        else:
            return redirect('Login')

    def passChg(request):
        if request.user.is_authenticated:
            provizor()
            credits = countCredits(request.user)
            plans = countPlans(request.user)

            if request.method == 'POST':
                form = PasswordChangeForm(request.user, request.POST)
                if form.is_valid():
                    user = form.save()
                    update_session_auth_hash(request, user)
                    messages.success(request, 'Your password was successfully updated!')
                    return redirect('options')
                else:
                    messages.error(request, "Please correct the error below.")
            else:
                form = PasswordChangeForm(request.user)

            return render(request, 'user/passChg.html', {"user_id":request.user.id, 'form': form,
                                                         "credits": credits - plans,
                                                         "plans": plans,
                                                         "username": request.user.username
                                                         })
        else:
            return redirect('Login')

    def userguide(request):
        if request.user.is_authenticated:
            provizor()
            credits = countCredits(request.user)
            plans = countPlans(request.user)
            return render(request, 'user/guide.html', {"user_id":request.user.id,
                                                       "credits": credits - plans,
                                                       "plans": plans,
                                                       "username": request.user.username
                                                       })
        else:
            return redirect('Login')

    def emailChg(request):
        if request.user.is_authenticated:
            provizor()
            credits = countCredits(request.user)
            plans = countPlans(request.user)
            user_now = User.objects.get(id=request.user.id)
            if 'email' in request.POST and 'email2' in request.POST:
                if request.POST.get('email') != '':
                    if request.POST.get("email") == request.POST.get("email2"):
                        user_now.email = request.POST.get("email")
                        user_now.save()
                        messages.success(request, 'Your email was successfully updated!')
                    else:
                        messages.error(request, "Emails do not match!")
                else:
                    messages.error(request, "Fill in all the fields!")
            else:
                messages.error(request, "Fill in all the fields!")
            return render(request, 'user/email_change.html', {"user_id":request.user.id,
                                                       "credits": credits - plans,
                                                       "plans": plans,
                                                        'email': user_now.email,
                                                       "username": request.user.username
                                                       })
        else:
            return redirect('Login')

    def domainApi(request):
        if request.user.is_authenticated:
            provizor()
            credits = countCredits(request.user)
            plans = countPlans(request.user)
            return render(request, 'user/domain.html', {"user_id":request.user.id,
                                                       "credits": credits - plans,
                                                       "plans": plans,
                                                       "username": request.user.username
                                                       })
        else:
            return redirect('Login')

    def snapshot(request):
        if request.user.is_authenticated:
            provizor()
            credits = countCredits(request.user)
            plans = countPlans(request.user)
            return render(request, 'user/snapshot.html', {"user_id":request.user.id,
                                                       "credits": credits - plans,
                                                       "plans": plans,
                                                       "username": request.user.username
                                                       })
        else:
            return redirect('Login')

    def countryTerm(request):
        if request.user.is_authenticated:
            provizor()
            credits = countCredits(request.user)
            plans = countPlans(request.user)

            if request.method == 'POST':
                profile = Profile.objects.get(user=request.user)
                profile.country = request.POST.get('country')
                profile.save()
                messages.success(request, 'Your country was successfully updated!')
            return render(request, 'user/country.html', {"user_id":request.user.id,
                                                       "credits": credits - plans,
                                                       "plans": plans,
                                                       "username": request.user.username
                                                       })
        else:
            return redirect('Login')