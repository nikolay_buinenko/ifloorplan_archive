import React, { Component } from 'react';
import axios from "axios";
import constData from "./../../../state";


function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

var csrftoken = readCookie('csrftoken');

class StartButton extends Component{

    continue = e => {
        e.preventDefault();

        axios.post("http://45.56.80.77/api/create/?token=1234567", this.props.values, {headers: {"X-CSRFToken":csrftoken }})
            .then(res => {
                const dataResponse = res.data;
                constData.new = dataResponse;
            })
            .catch(error=>console.log(error));
        this.props.nextStep();
    }

    render() {
        return (
            <div className='startNew'>
                <button type="submit" onClick={this.continue}>Start new iFloorPlan</button>
                <br/>
                <span>Or</span>
            </div>
        )
    }
}


//

//
// const StartButton = props => {
//
//     const handleSubmit = (event) => {
//     // event.preventDefault();

//         props.addNew();
//
// }
//
//   return (
//     <div className='startNew'>
//         <button type="submit" onClick={handleSubmit}>Start new iFloorPlan</button>
//         <br/>
//         <span>Or</span><br/>
//         <button  onClick={props.startTemplate}>Start new from Template</button>
//     </div>
//   )
// }


export default StartButton;
