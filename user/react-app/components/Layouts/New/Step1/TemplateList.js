import React, { Component } from 'react';



const TemplateList = () => {

  return (
    <div className='TemplateList'>
            <h3>Choose a Template</h3><br/>
            <ul>
                <li>Best Real Estate - Mark Gray</li>
                <li>Best Real Estate - Susan Gray</li>
                <li>Best Real Estate - Adam Scott</li>
                <li>Best Real Estate - Mark Gray</li>
                <li>Best Real Estate - Mark Gray</li>
                <li>Best Real Estate - Mark Gray</li>
            </ul>
    </div>
  )
}


export default TemplateList;
