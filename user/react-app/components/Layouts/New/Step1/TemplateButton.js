import React, { Component } from 'react';
import axios from "axios";
import TemplateList from "./TemplateList";



class TemplateButton extends Component{

    constructor(props) {
    super(props);
    this.state = {
      selectTemplate: false
    };
  }


    render() {
        return (
            <div className='startNew'>
                <button onClick={() => this.setState({ selectTemplate: true })}>Start new from Template</button>
                {
                    this.state.selectTemplate ? <TemplateList/> : null

                }
            </div>
        )
    }
}

export default TemplateButton;