import React, { Component, useState } from 'react';
import Dropzone from "react-dropzone";
import axios from "axios";
import constData from "./../../../state";

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

var csrftoken = readCookie('csrftoken');

class PositionScaleImg extends Component {

    state = {
        plan: null,
        image: null,
        plan_width: 50,
        image_width: 50,
    }

    getItems = () => {
        const config = {
              headers: {
                  "X-CSRFToken": csrftoken,
                  'content-type': 'multipart/form-data'
              }
          }
          axios.get("http://45.56.80.77/api/imagedownload/"+constData.new[0]['id'], {}, config)
              .then(res => {
                  this.setState({ image: res.data[0]['image'] });
              })
              .catch(error => console.log(error));
        axios.get("http://45.56.80.77/api/imageplandownload/"+constData.new[0]['id'], {}, config)
              .then(res => {
                  this.setState({ plan: res.data[0]['plan_image'] });
              })
              .catch(error => console.log(error));
    }

    handleChangeImg = (e) => {
        this.setState({ image_width: e.target.value });
    }

    handleChangePlan = (e) => {
        this.setState({ plan_width: e.target.value });
    }

  render() {
        this.getItems();
      return (
          <div>
              <div>
                  <div>
                      <span>Scale images</span>
                      <input type="range" max="100" min="0" step="1" onChange={this.handleChangeImg}/>
                  </div>
                  <div>
                      <button onClick={this.props.nextStep}>Continue</button>
                  </div>
                  <div>
                      <span>Scale floor plans</span>
                      <input type="range" max="100" min="0" step="1" onChange={this.handleChangePlan}/>
                  </div>
              </div>
              <div>
                  <div><img style={{ width: this.state.image_width+"%"}} src={this.state.image} alt=""/></div>
                  <div><img style={{ width: this.state.plan_width+"%"}} src={this.state.plan} alt=""/></div>

                  {/*<div><img style={{ width: this.state.image_width+"%"}} src="/media/ifloorplans_source/1.png" alt="" /></div>*/}
                  {/*<div><img style={{ width: this.state.plan_width+"%"}} src="/media/ifloorplans_source/371.jpg" alt=""/></div>*/}

              </div>
              <div><input type="text" placeholder="Type adress of this property here"/></div>
          </div>
      )
  }
}


export default PositionScaleImg;