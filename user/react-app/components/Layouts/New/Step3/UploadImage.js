import React, { Component, useState } from 'react';
import Dropzone from "react-dropzone";
import axios from "axios";
import constData from "./../../../state";




function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

var csrftoken = readCookie('csrftoken');

class UploadImage extends Component {

    state = {
        parcenttage: 0,
        image: null
    };


      handleDrop = (acceptedFiles) => {
          this.setState({
              image: acceptedFiles
          })


          let form_data = new FormData();

          form_data.append('ifloorplan_id', constData.new[0]['id']);
          for (var x = 0; x < this.state.image.length; x += 1){
              form_data.append('image_'+x, this.state.image[x], this.state.image[x].name);
          }
          const config = {
              onUploadProgress: (progressEvent) => {
                  const {loaded, total} = progressEvent;
                  let percent = Math.floor((loaded * 100) / total );
                  if (percent < 100){
                      this.setState({ parcenttage: percent});
                  }
              },
              headers: {
                  "X-CSRFToken": csrftoken,
                  'content-type': 'multipart/form-data'
              }
          }

          console.log(form_data);
          axios.post("http://45.56.80.77/api/imageupload/", form_data, config)
              .then(res => {
                  this.setState({ parcenttage: 100 }, ()=>{
                      setTimeout(() => {
                          this.setState({ parcenttage: 0 });
                          this.props.nextStep();

                      }, 1000);
                  });
              })
              .catch(error => console.log(error));
        // this.props.nextStep();  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      }

  render() {
          const {parcenttage} = this.state;
          return (
    <div className='UploadPlan'>
        <Dropzone onDrop={this.handleDrop}>
        {({ getRootProps, getInputProps }) => (
          <div {...getRootProps({ className: "dropzone" })}>
              <h3>Drag & Drop your Images here</h3><br/>
            <h3>Or</h3><br/>
            <input {...getInputProps()} />
            <button>Choose files</button><br/>
            <p>Creating a multi level iFloorPlan?</p>
            <p>You can upload all your images for this property. Access them from the Images library.</p>
          </div>

        )}
      </Dropzone>
        <div style={{border: '1px solid black', width: '500px', heigth: '30px',}}><div style={{background: 'green', height: '30px', width: parcenttage+'%',}}></div></div>

    </div>
  )
}
}

export default UploadImage;