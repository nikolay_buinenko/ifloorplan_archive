import React, { Component } from 'react';
import HeaderNavigation from './HeaderNavigation';
import PositionScaleImg from './New/Step4/PositionScaleImg';

class Test extends Component {
    render() {
        return (
            <div className="step4">
                <HeaderNavigation/>
                <div>
                    <PositionScaleImg
                    />
                </div>
            </div>
        );
    }
}

export default Test;