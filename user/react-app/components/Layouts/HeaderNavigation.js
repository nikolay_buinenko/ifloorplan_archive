import React, { Component, useState } from 'react';


const HeaderNavigation = () => {


  return (
    <div className='Header_Navigation'>
        <div>
            <a href="#"><i className="fas fa-sort-up"></i></a>
            <a href="#">Tools<i className="fas fa-sort-down"></i></a>
            <a href="#">Previous<i className="fas fa-caret-left"></i></a>
            <a href="#">Next<i className="fas fa-caret-right"></i></a>
            <a href="#">Add Levels...</a>
            <a href="#">Creating new.level - First Floor</a>
        </div>
        <div>
            <a href="#">Save now...</a>
            <a href="#">Save as..</a>
            <a href="#">Preview</a>
            <a href="#">Publish</a>
        </div>
    </div>
  )
}


export default HeaderNavigation;