import React, { Component } from 'react';
import StartButton from './New/Step1/StartButton';
import UploadPlan from './New/Step2/UploadPlan';
import UploadImage from './New/Step3/UploadImage';
import PositionScaleImg from './New/Step4/PositionScaleImg';
import CamPosition from './New/Step5/CamPosition';
import TemplateButton from './New/Step1/TemplateButton';
import HeaderNavigation from './HeaderNavigation';
import constData from './../state';

class New extends Component {

  state = {
    step: 1,
    new : {
        "level": [
            {
                "plan_level": {
                    "img": null,
                    "img_height": null,
                    "img_width": null,
                    "position_x": null,
                    "position_y": null
                },
                "images": [
                    {
                        "cam": [
                            {
                                "ken_burns": {
                                    "effect": null,
                                    "start_pos_x": null,
                                    "start_pos_y": null,
                                    "finish_pos_x": null,
                                    "finish_pos_y": null,
                                    "start_width": null,
                                    "start_height": null,
                                    "finish_width": null,
                                    "finish_height": null
                                },
                                "rotation": 0.0,
                                "pos_x": 0.0,
                                "pos_y": 0.0,
                                "width": 0.0,
                                "height": 0.0
                            }
                        ],
                        "img": null
                    }
                ],
                "tabLabel": "test"
            }
        ],
        "textColor": "000000",
        "bgColor": "ffffff",
        "camColor": "0babe6"
    },
  }

  //Proceed next step
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1
    });
  }

  //Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1
    });
  }

  //Hendle data change
  handleChange = data => {
    this.setState({
      new : data
    });
  }


  render() {
    const { step } = this.state;
    const { values } = this.state.new;
    switch (step) {

      case 1:
        return (
            <div className="step1">
              <StartButton
                  nextStep={this.nextStep}
                  handleChange={this.handleChange}
                  values={this.state.new}
              />
              <TemplateButton/>
            </div>
        )

      case 2:
        return (
            <div className="step2">
                <HeaderNavigation/>
                <div>
                    <UploadPlan
                        nextStep={this.nextStep}
                        handleChange={this.handleChange}
                        values={this.state.new}
                    />
                </div>
            </div>

        )

        case 3:
        return (
            <div className="step3">
                <HeaderNavigation/>
                <div>
                    <UploadImage
                        nextStep={this.nextStep}
                        handleChange={this.handleChange}
                        values={this.state.new}
                    />
                </div>
            </div>

        )

        case 4:
        return (
            <div className="step4">
                <HeaderNavigation/>
                <div>
                    <PositionScaleImg
                        nextStep={this.nextStep}
                        handleChange={this.handleChange}
                        values={this.state.new}
                    />
                </div>
            </div>

        )

        case 5:
        return (
            <div className="step5">
                <HeaderNavigation/>
                <div>
                    <CamPosition
                        nextStep={this.nextStep}
                        handleChange={this.handleChange}
                        values={this.state.new}
                    />
                </div>
            </div>
        )


    }
  }
}


export default New;