import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Route, BrowserRouter} from "react-router-dom";
import New from "./Layouts/New";
import Test from "./Layouts/Test";


class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <div>
                    <Route path='/user/new' component={New}/>
                    <Route path='/user/clients/' component={Test}/>
                </div>
            </BrowserRouter>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('app'));