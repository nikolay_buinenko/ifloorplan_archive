let constData = {
    new : {
        "level": [
            {
                "plan_level": {
                    "img": null,
                    "img_height": null,
                    "img_width": null,
                    "position_x": null,
                    "position_y": null
                },
                "images": [
                    {
                        "cam": [
                            {
                                "ken_burns": {
                                    "effect": null,
                                    "start_pos_x": null,
                                    "start_pos_y": null,
                                    "finish_pos_x": null,
                                    "finish_pos_y": null,
                                    "start_width": null,
                                    "start_height": null,
                                    "finish_width": null,
                                    "finish_height": null
                                },
                                "rotation": 0.0,
                                "pos_x": 0.0,
                                "pos_y": 0.0,
                                "width": 0.0,
                                "height": 0.0
                            }
                        ],
                        "img": null
                    }
                ],
                "tabLabel": "test"
            }
        ],
        "textColor": "000000",
        "bgColor": "ffffff",
        "camColor": "0babe6"
    },
    created : "Nothing",
}

export default constData;